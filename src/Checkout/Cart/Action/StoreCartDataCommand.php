<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Action;

use BeastMakers\Checkout\Cart\Infra\CartData;
use BeastMakers\Checkout\Cart\Infra\Repository\WriteRepository;
use BeastMakers\Checkout\Shared\ErrorCode;
use BeastMakers\Checkout\Shared\Result\StoreCartDataResult;
use BeastMakers\Checkout\Shared\SessionKey;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\SessionStorage\SessionManager;

class StoreCartDataCommand
{
  private WriteRepository $writeRepository;

  private SessionManager $sessionManager;

  /**
   * @param WriteRepository $writeRepository
   * @param SessionManager $sessionManager
   */
  public function __construct(
    WriteRepository $writeRepository,
    SessionManager $sessionManager
  ) {
    $this->writeRepository = $writeRepository;
    $this->sessionManager = $sessionManager;
  }

  /**
   * @param CartData $cartData
   *
   * @return StoreCartDataResult
   */
  public function storeCartData(CartData $cartData): StoreCartDataResult
  {
    $modelResult = new StoreCartDataResult();
    $checkoutId = $this->sessionManager->read(SessionKey::CHECKOUT_ID, '');

    if (!$checkoutId) {
      $modelResult->addError(new Error(ErrorCode::MISSING_CHECKOUT_ID, []));

      return $modelResult;
    }

    $this->writeRepository->pushCartData($cartData, $checkoutId);

    $modelResult->setIsSuccess(true);

    return $modelResult;
  }
}
