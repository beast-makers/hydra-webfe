import CheckoutClient from '../CheckoutClient';
import {setCartItems, setVoucher} from '../Facade';

export default class Cart {

  /**
   * @return {Promise<Object>}
   */
  fetchCartPageData () {
    return new Promise((resolve) => {
      CheckoutClient.fetchCartPageData().then((cartPage) => {
        setCartItems(cartPage.cart_items);
        setVoucher(cartPage.voucher);

        resolve(cartPage);
      });
    });
  }

  /**
   * @param {Object} requestData
   *
   * @return {Promise}
   */
  storeCartData (requestData) {
    return new Promise((resolve, reject) => {
      CheckoutClient.storeCartData(requestData)
        .then(() => {
          resolve(true);
        })
        .catch((errorMessage) => {
          reject(errorMessage);
        });
    });
  }
}
