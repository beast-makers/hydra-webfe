import Cart from './Cart/Cart';
import CartUi from './Cart/CartUi';
import CartStep from './Steps/CartStep';
import Delivery from './Delivery/Delivery';
import DeliveryForm from './Delivery/DeliveryForm';
import DeliveryFormFields from './Delivery/DeliveryFormFields';
import DeliveryUi from './Delivery/DeliveryUi';
import DeliveryStep from './Steps/DeliveryStep';
import SepaUi from './Payment/SepaUi';
import StepEngine from '../StepEngine/StepEngine';
import PaymentUi from './Payment/PaymentUi';
import PayPalUi from './Payment/PayPalUi';
import Payment from "./Payment/Payment";
import PaymentStep from './Steps/PaymentStep';
import SepaForm from "./Payment/SepaForm";
import SepaFormFields from "./Payment/SepaFormFields";

/**
 * @param {Element} containerElement
 *
 * @return {StepEngine}
 */
export function newStepEngine (containerElement) {
  return new StepEngine(containerElement);
}

/**
 * @param {StepEngine} stepEngine
 *
 * @return {CartStep}
 */
export function newCartStep (stepEngine) {
  const cart = new Cart();
  return new CartStep(stepEngine, cart, new CartUi(cart));
}

/**
 * @param {StepEngine} stepEngine
 *
 * @return {DeliveryStep}
 */
export function newDeliveryStep (stepEngine) {
  const delivery = new Delivery();
  const deliveryForm = new DeliveryForm(DeliveryFormFields);

  return new DeliveryStep(
    stepEngine,
    deliveryForm,
    new DeliveryUi(delivery, deliveryForm)
  );
}

/**
 * @param {StepEngine} stepEngine
 *
 * @return {PaymentStep}
 */
export function newPaymentStep (stepEngine) {
  const paymentUi = new PaymentUi(
    new Payment(),
    [
      new PayPalUi(),
      new SepaUi(new SepaForm(SepaFormFields)),
    ]
  );

  return new PaymentStep(
    stepEngine,
    paymentUi
  );
}
