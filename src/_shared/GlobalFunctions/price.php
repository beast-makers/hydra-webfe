<?php
declare(strict_types=1);

const PRICE_DIVIDER = 10000;

$nfmt = new \NumberFormatter('de_DE', NumberFormatter::DECIMAL_ALWAYS_SHOWN);
$nfmt->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');

/**
 * @param string $intPrice
 *
 * @return float
 */
function price(string $intPrice): float
{
  $price = intval($intPrice);
  if ($price === 0 && $intPrice !== '0') {
    throw new UnexpectedValueException('Price should be a valid integer value');
  }

  return intval($intPrice) / PRICE_DIVIDER;
}

/**
 * @param string $intPrice
 * @param string $currencySymbol
 *
 * @return string
 */
function price_with_currency(string $intPrice, string $currencySymbol): string
{
  global $nfmt;

  $calculatedPrice = price($intPrice);
  $formattedPrice = $nfmt->format($calculatedPrice, NumberFormatter::TYPE_DOUBLE);

  return "{$formattedPrice} {$currencySymbol}";
}


/**
 * @param string $intPrice
 * @param string $default
 *
 * @return float|int|string
 */
function price_coalesce(string $intPrice, string $default = '')
{
  $calculatedPrice = price($intPrice);

  if ($calculatedPrice === 0.0) {
    return $default;
  }

  return $calculatedPrice;
}

/**
 * @param string $intPrice
 * @param string $currencySymbol
 * @param string $default
 *
 * @return string
 */
function price_coalesce_with_currency(string $intPrice, string $currencySymbol, string $default = ''): string
{
  $calculatedPrice = price($intPrice);

  if ($calculatedPrice === 0.0) {
    return $default;
  }

  return "{$calculatedPrice} {$currencySymbol}";
}
