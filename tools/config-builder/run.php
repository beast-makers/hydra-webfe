#!/usr/bin/php
<?php
define('PROJECT_DIR', '/var/webfe/');
define('TOOLS_DIR', PROJECT_DIR . 'tools/');
define('CONFIG_DIR', PROJECT_DIR . 'config/');

renderConfig('default');
renderConfig('type', 'APP_TYPE='. getenv('APP_TYPE'));
renderConfig('env', 'APP_ENV='. getenv('APP_ENV'));

$mergedConfig = mergeConfigs();
renderFinalConfigFiles($mergedConfig);

/**
 * @param string $type
 * @param string $envVars
 *
 * @return void
 */
function renderConfig(string $type, string $envVars = ''): void
{
  $exitCode = 0;
  system("{$envVars} " . TOOLS_DIR . "config-builder/render-{$type}-config-template.sh", $exitCode);
  if ($exitCode !== 0) {
    exit("ERROR: Rendering config [{$type}, {$envVars}] template failed with code [{$exitCode}]\n");
  }
}

/**
 * @return array
 */
function mergeConfigs(): array
{
  $defaultConfig = json_decode(file_get_contents(CONFIG_DIR . '_generated/config-default.json'), true);
  $modeConfig = json_decode(file_get_contents(CONFIG_DIR . '_generated/config-type.json'), true);
  $envConfig = json_decode(file_get_contents(CONFIG_DIR . "_generated/config-env.json"), true);

  return array_replace_recursive($defaultConfig, $modeConfig, $envConfig);
}

/**
 * @param array $config
 *
 * @return void
 */
function renderFinalConfigFiles(array $config): void
{
  $mergedConfigOutput = var_export($config, true);
  $mergedConfigFinalContent = <<<CONFIG
<?php
return {$mergedConfigOutput};
CONFIG;

  file_put_contents(CONFIG_DIR . '_generated/config.php', $mergedConfigFinalContent);
  file_put_contents(CONFIG_DIR . '_generated/config.json', json_encode($config));
}
