#!/usr/bin/env bash
set -euo pipefail

printf "Building [default] config\n"

INPUT_TEMPLATE_FILE="/var/webfe/config/env/config-default.tmpl.json"
OUTPUT_CONFIG_FILE="/var/webfe/config/_generated/config-default.json"

envsubst < ${INPUT_TEMPLATE_FILE} > ${OUTPUT_CONFIG_FILE}
