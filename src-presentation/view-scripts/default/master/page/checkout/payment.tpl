{append var=paymentMethods value=['title' => 'PayPal', 'targetClass' => 't-tabs-content-payment-paypal', 'tpl' => 'page/checkout/payment_acc/paypal-panel.tpl']}
{append var=paymentMethods value=['title' => 'Sepa', 'targetClass' => 't-tabs-content-payment-sepa', 'tpl' => 'page/checkout/payment_acc/sepa-panel.tpl', 'data' => ['form' => $sepaForm]]}
{append var=paymentMethods value=['title' => 'Credit Card', 'targetClass' => 't-tabs-content-payment-credit-card', 'tpl' => 'page/checkout/payment_acc/credit-card-panel.tpl']}
{include file="component/tabbed-accordion/render.tpl" paymentMethods=$paymentMethods scope="parent"}

<div class="checkout-payment-page">
    <div class="page-title">Payment</div>
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-7">
            <div class="box">
                <div class="qa-checkout-payment-selection c-tabbed-accordion-theme-payment">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            {$tabbedAccordionNavigation}
                        </div>
                        <div class="col-12 col-md-7">
                            {$tabbedAccordionContent}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
