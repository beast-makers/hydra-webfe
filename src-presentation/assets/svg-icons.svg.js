function requireAll(r) {
  r.keys().forEach(r);
}

requireAll(require.context('.', true, /svg-checkout\/.*\.svg$/));
requireAll(require.context('.', true, /svg-icons\/.*\.svg$/));
