<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\Kernel\BaseFacade;
use BeastMakers\Shared\Kernel\DependencyContainer;

trait FacadeLoader
{
  /**
   * @param string $className
   *
   * @return BaseFacade
   */
  protected function shareFacade(string $className): BaseFacade
  {
    $dc = DependencyContainer::getInstance();
    $dcKey = $className;

    if (!$dc->contains($dcKey)) {
      $facade = new $className();
      $dc->set($dcKey, static function () use ($facade) {
        return $facade;
      });
    }

    return $dc->get($dcKey);
  }
}
