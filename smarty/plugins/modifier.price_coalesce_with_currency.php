<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     modifier.price_coalesce_with_currency.php
 * Type:     modifier
 * Name:     price_coalesce_with_currency
 * Purpose:  format price
 * -------------------------------------------------------------
 */
function smarty_modifier_price_coalesce_with_currency(string $intPrice, string $currencySymbol, string $default = '')
{
  return price_coalesce_with_currency($intPrice, $currencySymbol, $default);
}
