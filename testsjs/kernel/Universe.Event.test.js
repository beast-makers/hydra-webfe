import Universe from '../../src-presentation/js/kernel/Universe';

test('call subscriber', function () {
  const eventName = 'subscriber-event';

  const mockCallback = jest.fn(function(key, notificationData) {
    expect(key).toBe(eventName);
    expect(notificationData.foo).toBe('bar');
  });

  Universe.Event.subscribe(eventName, mockCallback);
  Universe.Event.notify(eventName, {foo: 'bar'});

  expect(mockCallback.mock.calls.length).toBe(1);
});

test('call subscriber with priorities', function () {
  const eventName = 'subscriber-event-with-priority';
  let priorityResult = '';

  const mockCallbackA = jest.fn(function(key, notificationData) {
    expect(key).toBe(eventName);
    expect(notificationData.foo).toBe('bar');

    priorityResult += 'A';
  });

  const mockCallbackB = jest.fn(function(key, notificationData) {
    expect(key).toBe(eventName);
    expect(notificationData.foo).toBe('bar');

    priorityResult += 'B';
  });

  const mockCallbackC = jest.fn(function(key, notificationData) {
    expect(key).toBe(eventName);
    expect(notificationData.foo).toBe('bar');

    priorityResult += 'C';
  });

  Universe.Event.subscribe(eventName, mockCallbackA, 7);
  Universe.Event.subscribe(eventName, mockCallbackB, 3);
  Universe.Event.subscribe(eventName, mockCallbackC, 9);

  Universe.Event.notify(eventName, {foo: 'bar'});

  expect(mockCallbackA.mock.calls.length).toBe(1);
  expect(mockCallbackB.mock.calls.length).toBe(1);
  expect(mockCallbackC.mock.calls.length).toBe(1);
  expect(priorityResult).toBe('BAC');
});
