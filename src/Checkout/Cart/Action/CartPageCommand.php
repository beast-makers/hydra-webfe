<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Action;

use BeastMakers\Checkout\Shared\Domain\CartDetails;
use BeastMakers\Checkout\Shared\Domain\CartDetails as cd;
use BeastMakers\Checkout\Shared\Domain\CartItemLine;
use BeastMakers\Checkout\Shared\Domain\CartItemLine as cil;

class CartPageCommand
{
  /**
   * @param string $cartId
   *
   * @return cd
   */
  public function fetchCartDetails(string $cartId): CartDetails
  {
    return CartDetails::createFromArray([
      cd::ATTR_CART_ITEM_LINES => $this->fakeCartItemDetails(),
      cd::ATTR_SHIPPING_PRICE => 0,
      cd::ATTR_PRICE_TO_PAY => 25480000,
      cd::ATTR_CURRENCY_SYMBOL => '€',
    ]);
  }

  /**
   * @return array
   */
  private function fakeCartItemDetails(): array
  {
    $description = <<<DESC
12 x Behälter mit Abdruckmasse (6 weiße, 6 blaue)<br/>
4 x Abdrucklöffel<br/>
1 x Wangenhalter<br/>
1 x Handschuhe<br/>
1 x Anleitung<br/>
1 x vorfrankierte Rücksendebox<br/>
1 x Versandbeutel
DESC;

    $cartItemLine1 = CartItemLine::createFromArray([
      cil::ATTR_HEADLINE => 'Plusdental',
      cil::ATTR_SUBHEADLINE => 'Ihr Zahnabdruckset',
      cil::ATTR_DESCRIPTION => $description,
      cil::ATTR_ITEM_IMAGE_URL => 'https://images.ctfassets.net/pzpukssxv20m/38CSLH1mflvUGSBFa6S0og/569b64809f4557f50439f778d37804ae/kit-checkout-set.jpg',
      cil::ATTR_PRICE => 490000,
      cil::ATTR_CURRENCY_SYMBOL => '€',
    ]);

    $description = <<<DESC
12 x Upper Jaw Aligners<br />
6 x Lower Jaw Aligners<br />
15 Weeks treatment duration
DESC;

    $cartItemLine2 = CartItemLine::createFromArray([
      cil::ATTR_HEADLINE => 'Plusdental',
      cil::ATTR_SUBHEADLINE => 'Ihr Treatment',
      cil::ATTR_DESCRIPTION => $description,
      cil::ATTR_ITEM_IMAGE_URL => 'https://images.ctfassets.net/pzpukssxv20m/38CSLH1mflvUGSBFa6S0og/569b64809f4557f50439f778d37804ae/kit-checkout-set.jpg',
      cil::ATTR_PRICE => 24990000,
      cil::ATTR_CURRENCY_SYMBOL => '€',
    ]);

    return [
      $cartItemLine1,
      $cartItemLine2,
    ];
  }
}
