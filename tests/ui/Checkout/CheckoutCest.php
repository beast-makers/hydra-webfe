<?php
declare(strict_types=1);

namespace BeastMakersTest\Checkout;

use BeastMakers\Checkout\Delivery\Infra\DeliveryForm;
use BeastMakersTest\Page\Ui\Checkout\CheckoutAll;
use BeastMakersTest\Page\Ui\Checkout\DeliveryPage;
use BeastMakersTest\UiTester;

class CheckoutCest
{
  /**
   * @param UiTester $i
   * @param CheckoutAll $checkoutAll
   * @return void
   * @throws \Exception
   */
  public function successfulCheckout(UiTester $i, CheckoutAll $checkoutAll): void
  {
    $this->visitCartPage($i, $checkoutAll);
    $this->visitDeliveryPage($i, $checkoutAll);
    $this->seeEmptyDeliveryPageFieldErrors($i, $checkoutAll);
    $this->fillDeliveryPageForm($i, $checkoutAll->deliveryPage);
    $this->visitPaymentPage($i, $checkoutAll);
  }

  /**
   * @param UiTester $i
   * @param CheckoutAll $checkoutAll
   *
   * @return void
   * @throws \Exception
   */
  private function visitCartPage(UiTester $i, CheckoutAll $checkoutAll): void
  {
    $i->amOnPage('/checkout');
    $i->shortWaitForElementVisible($checkoutAll::LOADING_ICON_SELECTOR);
    $i->shortWaitForElementVisible(DeliveryPage::CART_ITEM_LINE_SELECTOR);
    $i->shortWaitForElementNotVisible($checkoutAll::LOADING_ICON_SELECTOR);
  }

  /**
   * @param UiTester $i
   * @param CheckoutAll $checkoutAll
   *
   * @return void
   * @throws \Exception
   */
  private function visitDeliveryPage(UiTester $i, CheckoutAll $checkoutAll): void
  {
    $i->click(DeliveryPage::ACCEPT_CART_BUTTON);
    $i->shortWaitForElementVisible($checkoutAll::LOADING_ICON_SELECTOR);
    $i->shortWaitForElementVisible(DeliveryPage::FORM_SELECTOR);
    $i->shortWaitForElementNotVisible($checkoutAll::LOADING_ICON_SELECTOR);
  }

  /**
   * @param UiTester $i
   * @param CheckoutAll $checkoutAll
   *
   * @return void
   * @throws \Exception
   */
  private function seeEmptyDeliveryPageFieldErrors(UiTester $i, CheckoutAll $checkoutAll): void
  {
    $i->click(DeliveryPage::ACCEPT_DELIVERY_BUTTON);
    $i->shortWaitForElementVisible($checkoutAll::LOADING_ICON_SELECTOR);
    $i->shortWaitForElementNotVisible($checkoutAll::LOADING_ICON_SELECTOR);
    $i->shortWaitForElementVisible(DeliveryPage::INPUT_FIELD_MESSAGE_SELECTOR);
    $i->expectTo('see all error messages for required fields');
    $i->seeNumberOfNotEmptyElements(DeliveryPage::INPUT_FIELD_MESSAGE_SELECTOR, 8);
  }

  /**
   * @param UiTester $i
   * @param CheckoutAll $checkoutAll
   *
   * @return void
   * @throws \Exception
   */
  private function visitPaymentPage(UiTester $i, CheckoutAll $checkoutAll): void
  {
    $i->click(DeliveryPage::ACCEPT_DELIVERY_BUTTON);
    $i->shortWaitForElementVisible($checkoutAll::LOADING_ICON_SELECTOR);
    $i->shortWaitForElementVisible(DeliveryPage::PAYMENT_SELECTION_SELECTOR);
    $i->shortWaitForElementNotVisible($checkoutAll::LOADING_ICON_SELECTOR);
  }

  /**
   * @param UiTester $i
   * @param DeliveryPage $deliveryPage
   *
   * @return void
   * @noinspection DuplicatedCode
   */
  private function fillDeliveryPageForm(UiTester $i, DeliveryPage $deliveryPage): void
  {
    $faker = $i->faker();

    $deliveryPage->fillField(DeliveryForm::FIELD_FIRST_NAME, $faker->firstName);
    $deliveryPage->fillField(DeliveryForm::FIELD_LAST_NAME, $faker->lastName);
    $deliveryPage->fillField(DeliveryForm::FIELD_EMAIL, $faker->email);
    $deliveryPage->fillField(DeliveryForm::FIELD_BIRTH_DATE, $faker->date('d-m-Y'));

    $deliveryPage->selectFirstOption(DeliveryForm::FIELD_COUNTRY_CODE);
    $deliveryPage->fillField(DeliveryForm::FIELD_STREET, $faker->streetAddress);
    $deliveryPage->fillField(DeliveryForm::FIELD_CITY, $faker->city);
    $deliveryPage->fillField(DeliveryForm::FIELD_ZIP, $faker->postcode);
  }
}
