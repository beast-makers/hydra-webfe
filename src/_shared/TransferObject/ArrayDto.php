<?php
declare(strict_types=1);

namespace BeastMakers\Shared\TransferObject;

use BeastMakers\Shared\TransferObject\Strategy\MergeStrategy;
use BeastMakers\Shared\TransferObject\Strategy\ToArrayStrategy;
use Closure;

abstract class ArrayDto
{
  protected array $properties = [];

  private static array $toArrayHandlers = [];

  private static array $mergeHandlers = [];

  protected const TYPE_SCALAR = 1;
  protected const TYPE_ARRAY = 2;
  protected const TYPE_ARRAY_UNIQUE = 3;
  protected const TYPE_ASSOCIATIVE_ARRAY = 4;
  protected const TYPE_OBJECT = 5;

  protected const DEF_SCALAR = [self::TYPE_SCALAR, ''];
  protected const DEF_ARRAY = [self::TYPE_ARRAY, self::TYPE_SCALAR];
  protected const DEF_ARRAY_UNIQUE = [self::TYPE_ARRAY_UNIQUE, self::TYPE_SCALAR];

  /**
   * @return array
   */
  abstract public function definition(): array;

  /**
   * @param string $property
   * @param mixed $value
   *
   * @return void
   */
  protected function set(string $property, $value): void
  {
    $this->properties[$property] = $value;
  }

  /**
   * @param string $property
   * @param mixed $fallback
   *
   * @return mixed
   */
  protected function get(string $property, $fallback)
  {
    return $this->properties[$property] ?? $fallback;
  }

  /**
   * @psalm-suppress UnsafeInstantiation
   * @param array $properties
   *
   * @return static
   */
  public static function createFromArray(array $properties): self
  {
    $self = new static();
    $self->applyArray($properties);

    return $self;
  }

  /**
   * @param string $json
   * @return static
   */
  public static function createFromJson(string $json): self
  {
    $asArray = json_decode($json, true);

    return static::createFromArray($asArray);
  }

  /**
   * @param array $properties
   *
   * @return ArrayDto
   */
  public function applyArray(array $properties): self
  {
    $definition = $this->definition();
    foreach ($properties as $propertyName => $propertyValue) {
      if (isset($definition[$propertyName])) {
        [$type, $childType] = $definition[$propertyName];
        $targetValue = $this->get($propertyName, null);
        $handler = self::pickMergeHandler($targetValue, $type, $childType);
        $value = $handler($targetValue, $childType, $propertyValue);
        $this->set($propertyName, $value);
      }
    }

    return $this;
  }

  /**
   * @return array
   */
  public function asArray(): array
  {
    return $this->properties;
  }

  /**
   * @return array
   */
  public function toArray(): array
  {
    $definition = $this->definition();

    $result = [];
    foreach ($this->properties as $propertyName => $propertyValue) {
      [$type, $childType] = $definition[$propertyName];
      $toArrayHandler = self::pickToArrayHandler($type, $propertyValue);
      $result[$propertyName] = $toArrayHandler($propertyValue);
    }

    return $result;
  }

  /**
   * @return string
   */
  public function toJson(): string
  {
    return json_encode($this->toArray());
  }

  /**
   * @psalm-suppress UnsafeInstantiation
   * @param ArrayDto $dto
   *
   * @return static
   */
  public function merge(ArrayDto $dto): self
  {
    $clone = new static();
    $clone->applyArray($this->toArray());
    $clone->applyArray($dto->toArray());

    return $clone;
  }

  /**
   * @param array $source
   * @param array $propertiesMap
   *
   * @return void
   */
  public function mapArrayToDto(array $source, array $propertiesMap): void
  {
    foreach ($propertiesMap as $sourceKey => $targetKey) {
      if (array_key_exists($sourceKey, $source)) {
        $this->set($targetKey, $source[$sourceKey]);
      }
    }
  }

  /**
   * @psalm-suppress UnsafeInstantiation
   * @param array $source
   * @param array $propertiesMap
   *
   * @return static
   */
  public static function mapArrayToNewDto(array $source, array $propertiesMap): self
  {
    $dto = new static();
    $dto->mapArrayToDto($source, $propertiesMap);

    return $dto;
  }

  /**
   * @param array $propertiesMap
   *
   * @return array
   */
  public function mapToArray(array $propertiesMap): array
  {
    $result = [];

    foreach ($propertiesMap as $dtoKey => $targetKey) {
      if (array_key_exists($dtoKey, $this->properties)) {
        $result[$targetKey] = $this->properties[$dtoKey];
      }
    }

    return $result;
  }

  /**
   * @param mixed $ownValue
   * @param int $type
   * @param mixed $childType
   *
   * @return Closure
   */
  private static function pickMergeHandler($ownValue, int $type, $childType): Closure
  {
    $handlerKey = self::createMergeHandlerKey($ownValue, $type, $childType);

    return self::getMergeHandlerByKey($handlerKey);
  }

  /**
   * @param $ownValue
   * @param int $type
   * @param $childType
   *
   * @return string
   */
  private static function createMergeHandlerKey($ownValue, int $type, $childType): string
  {
    if ($ownValue === null) {
      return self::TYPE_SCALAR . '0';
    }

    if (empty($childType) || is_int($childType)) {
      return $type . '0';
    }

    $parentClasses = class_parents($childType);
    $isDto = isset($parentClasses[ArrayDto::class]) ? 1 : 0;

    return $type . $isDto;
  }

  /**
   * @param string $key
   *
   * @return Closure
   */
  private static function getMergeHandlerByKey(string $key): Closure
  {
    if (!self::$mergeHandlers) {
      self::$mergeHandlers = [
        self::TYPE_SCALAR . '0' =>
          static fn ($targetValue, $childType, $value) => MergeStrategy::scalar($value),
        self::TYPE_ARRAY_UNIQUE . '0' =>
          static fn ($targetValue, $childType, $value) => MergeStrategy::arrayUnique($targetValue, $value),
        self::TYPE_ARRAY . '0' =>
          static fn ($targetValue, $childType, $value) => MergeStrategy::array($targetValue, $value),
        self::TYPE_ASSOCIATIVE_ARRAY . '1' =>
          static fn ($targetValue, $childType, $value) => MergeStrategy::assocArrayWithDto($targetValue, $childType, $value),
        self::TYPE_ASSOCIATIVE_ARRAY . '0' =>
          static fn ($targetValue, $childType, $value) => MergeStrategy::array($targetValue, $value),
        self::TYPE_OBJECT . '0' =>
          static fn ($targetValue, $childType, $value) => MergeStrategy::scalar($value),
        self::TYPE_OBJECT . '1' =>
          static fn ($targetValue, $childType, $value) => MergeStrategy::dto($targetValue, $childType, $value),
      ];
    }

    return self::$mergeHandlers[$key];
  }

  /**
   * @param int $type
   * @param $value
   *
   * @return Closure
   */
  private static function pickToArrayHandler(int $type, $value): Closure
  {
    $handlerKey = self::createToArrayHandlerKey($type, $value);

    return self::getToArrayHandlerByKey($handlerKey);
  }

  /**
   * @param int $type
   * @param $value
   *
   * @return string
   */
  private static function createToArrayHandlerKey(int $type, $value): string
  {
    $v = is_array($value) ? current($value) : $value;
    $isDto = $v instanceof ArrayDto ? 1 : 0;

    return $type . $isDto;
  }

  /**
   * @param string $key
   *
   * @return Closure
   */
  private static function getToArrayHandlerByKey(string $key): Closure
  {
    if (!self::$toArrayHandlers) {
      self::$toArrayHandlers = [
        self::TYPE_SCALAR . '0' =>
          static fn ($value) => ToArrayStrategy::scalar($value),
        self::TYPE_ARRAY_UNIQUE . '0' =>
          static fn ($value) => ToArrayStrategy::scalar($value),
        self::TYPE_ARRAY . '0' =>
          static fn ($value) => ToArrayStrategy::scalar($value),
        self::TYPE_ASSOCIATIVE_ARRAY . '1' =>
          static fn ($value) => ToArrayStrategy::assocArrayWithDto($value),
        self::TYPE_OBJECT . '0' =>
          static fn ($value) => ToArrayStrategy::scalar($value),
        self::TYPE_OBJECT . '1' =>
          static fn ($value) => ToArrayStrategy::dtoToArray($value),
      ];
    }

    return self::$toArrayHandlers[$key];
  }
}
