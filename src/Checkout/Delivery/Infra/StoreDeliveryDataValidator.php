<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra;

use BeastMakers\Checkout\Delivery\Infra\Request\DeliveryDataRequest;
use BeastMakers\Checkout\Shared\Domain\Address;
use BeastMakers\Checkout\Shared\Domain\Customer;
use BeastMakers\Shared\BackendClient\BackendClient;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use GuzzleHttp\Exception\GuzzleException;

class StoreDeliveryDataValidator
{
  private BackendClient $backendClient;

  /**
   * @param BackendClient $backendClientFacade
   */
  public function __construct(BackendClient $backendClientFacade)
  {
    $this->backendClient = $backendClientFacade;
  }

  /**
   * @param DeliveryData $deliveryData
   * @return array
   *
   * @throws GuzzleException
   * @throws MissingConfigOptionException
   */
  public function validate(DeliveryData $deliveryData): array
  {
    return array_merge(
      $this->validateAddress($deliveryData->getAddress()),
      $this->validateCustomer($deliveryData->getCustomer()),
    );
  }

  /**
   * @param Customer $customer
   *
   * @return array
   * @throws GuzzleException
   * @throws MissingConfigOptionException
   * @throws \Exception
   */
  private function validateCustomer(Customer $customer): array
  {
    $response = $this->backendClient->postJson(
      'customer/validate',
      $this->mapCustomer2BackendRequest($customer)
    );

    if ($response->getStatusCode() === 204) {
      return [];
    }

    $responseContent = $response->getBody()->getContents();
    if ($response->getStatusCode() === 400) {
      $errors = json_decode($responseContent, true)[0];
      $errorCode = $errors['errorCode'];

      $validationMessages = $this->extractCustomerValidationErrors($errors, $errorCode);

      return $this->replaceCustomerValidationPlaceholders($validationMessages);
    }

    $responseContent = $response->getBody()->getContents();
    throw new \Exception("Unhandled customer validation response:\n" . print_r($responseContent, true));
  }

  /**
   * @param Address $address
   *
   * @return array
   * @throws GuzzleException
   * @throws MissingConfigOptionException
   * @throws \Exception
   */
  private function validateAddress(Address $address): array
  {
    $response = $this->backendClient->postJson(
      'address/validate',
      $this->mapAddress2BackendRequest($address)
    );

    if ($response->getStatusCode() === 204) {
      return [];
    }

    $responseContent = $response->getBody()->getContents();
    if ($response->getStatusCode() === 400) {
      $errors = json_decode($responseContent, true)[0];
      $errorCode = $errors['errorCode'];

      $validationMessages = $this->extractAddressValidationErrors($errors, $errorCode);

      return $this->replaceAddressValidationPlaceholders($validationMessages);
    }

    $responseContent = $response->getBody()->getContents();
    throw new \Exception("Unhandled customer validation response:\n" . print_r($responseContent, true));
  }

  /**
   * @param Customer $customer
   *
   * @return array
   */
  private function mapCustomer2BackendRequest(Customer $customer): array
  {
    $mapDefinition = $this->getCustomer2BackendRequestMap();

    return $customer->mapToArray($mapDefinition);
  }

  /**
   * @param Address $address
   *
   * @return array
   */
  private function mapAddress2BackendRequest(Address $address): array
  {
    $mapDefinition = $this->getAddress2BackendRequestMap();

    return $address->mapToArray($mapDefinition);
  }

  /**
   * @param array $errors
   * @param string $errorCode
   *
   * @return array
   * @throws \Exception
   */
  private function extractCustomerValidationErrors(array $errors, string $errorCode): array
  {
    if ($errorCode === 'CUSTOMER_VALIDATION') {
      $mapDefinition = array_flip($this->getCustomer2BackendRequestMap());

      return $this->mapValidationMessages2DtoAttributes($errors, $mapDefinition);
    }

    throw new \Exception("Unhandled customer validation error code [{$errorCode}]:\n" . print_r($errors, true));
  }

  /**
   * @param array $errors
   * @param string $errorCode
   *
   * @return array
   * @throws \Exception
   */
  private function extractAddressValidationErrors(array $errors, string $errorCode): array
  {
    if ($errorCode === 'ADDRESS_VALIDATION') {
      $mapDefinition = array_flip($this->getAddress2BackendRequestMap());

      return $this->mapValidationMessages2DtoAttributes($errors, $mapDefinition);
    }

    throw new \Exception("Unhandled address validation error code [{$errorCode}]:\n" . print_r($errors, true));
  }

  /**
   * @return array
   */
  private function getCustomer2BackendRequestMap(): array
  {
    return [
      Customer::ATTR_FIRST_NAME => 'first_name',
      Customer::ATTR_LAST_NAME => 'last_name',
      Customer::ATTR_EMAIL => 'email',
      Customer::ATTR_BIRTH_DATE => 'birth_date',
    ];
  }

  /**
   * @return array
   */
  private function getAddress2BackendRequestMap(): array
  {
    return [
      Address::ATTR_FIRST_NAME => 'first_name',
      Address::ATTR_LAST_NAME => 'last_name',
      Address::ATTR_COUNTRY_CODE => 'country_code',
      Address::ATTR_STREET => 'street',
      Address::ATTR_ADDRESS_EXTENSION => 'address_extension',
      Address::ATTR_CITY => 'city',
      Address::ATTR_ZIP => 'zip',
    ];
  }

  /**
   * @param array $errors
   * @param array $mapDefinition
   *
   * @return array
   */
  private function mapValidationMessages2DtoAttributes(array $errors, array $mapDefinition): array
  {
    $validationMessages = $errors['message'];

    $result = [];
    foreach ($validationMessages as $attribute => $messages) {
      foreach ($messages as $message) {
        if (isset($mapDefinition[$attribute])) {
          $result[$mapDefinition[$attribute]][] = $message;
        }
      }
    }

    return $result;
  }

  /**
   * @param array $validationMessages
   *
   * @return array
   */
  private function replaceCustomerValidationPlaceholders(array $validationMessages): array
  {
    $map = [
      DeliveryDataRequest::ATTR_FIRST_NAME => CustomerLabel::FIRST_NAME,
      DeliveryDataRequest::ATTR_LAST_NAME => CustomerLabel::LAST_NAME,
      DeliveryDataRequest::ATTR_EMAIL => CustomerLabel::EMAIL,
      DeliveryDataRequest::ATTR_BIRTH_DATE => CustomerLabel::BIRTH_DATE,
    ];

    foreach ($validationMessages as $attribute => &$messages) {
      if (isset($map[$attribute])) {
        foreach ($messages as &$message) {
          $message = str_replace(':field_label', $map[$attribute], $message);
        }
      }
    }

    return $validationMessages;
  }

  /**
   * @param array $validationMessages
   *
   * @return array
   */
  public function replaceAddressValidationPlaceholders(array $validationMessages): array
  {
    $map = [
      DeliveryDataRequest::ATTR_FIRST_NAME => CustomerLabel::FIRST_NAME,
      DeliveryDataRequest::ATTR_LAST_NAME => CustomerLabel::LAST_NAME,
      DeliveryDataRequest::ATTR_COUNTRY_CODE => AddressLabel::COUNTRY,
      DeliveryDataRequest::ATTR_STREET => AddressLabel::STREET,
      DeliveryDataRequest::ATTR_ADDRESS_EXTENSION => AddressLabel::ADDRESS_EXTENSION,
      DeliveryDataRequest::ATTR_CITY => AddressLabel::CITY,
      DeliveryDataRequest::ATTR_ZIP => AddressLabel::ZIP,
    ];

    foreach ($validationMessages as $attribute => &$messages) {
      if (isset($map[$attribute])) {
        foreach ($messages as &$message) {
          $message = str_replace(':field_label', $map[$attribute], $message);
        }
      }
    }

    return $validationMessages;
  }
}
