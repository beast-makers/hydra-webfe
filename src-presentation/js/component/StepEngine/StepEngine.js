import {sayApplicationError} from '../../kernel/Facade';
import {sayStepDisplayed} from './Facade';

class StepEngine {
  /**
   * @param {Element} container
   */
  constructor (container) {
    this.container = container;
    this.currentStepIndex = 0;
    this.steps = [];
  }

  /**
   * @param {Step[]} steps
   *
   * @return {void}
   */
  run (steps) {
    Promise.all(this.initSteps(steps)).then(() => {
      this.renderSteps();
    });
  }

  /**
   * @param {Step[]} steps
   *
   * @return {Promise[]}
   */
  initSteps (steps) {
    const stepPromises = [];

    const stepsCount = steps.length;
    for (let stepIndex = 0; stepIndex < stepsCount; stepIndex++) {
      const step = steps[stepIndex];
      const stepContainerElement = document.createElement('div');

      stepPromises.push(step.init(stepContainerElement));

      this.steps[stepIndex] = {
        'step': step,
        'containerElement': stepContainerElement
      };
    }

    return stepPromises;
  }

  /**
   * @return {void}
   */
  async renderSteps () {
    const stepsCount = this.steps.length;

    for (let stepIndex = 0; stepIndex < stepsCount; stepIndex++) {
      const stepItem = this.steps[stepIndex];
      const step = stepItem.step;

      this.container.appendChild(stepItem.containerElement);
      try {
        await step.mount(stepItem.containerElement);
      } catch (error) {
        sayApplicationError('Something went wrong. We are working on fixing the issue. Please try again later');
        console.log('Could not mount step: ', stepItem, error);
        return;
      }

      const isFinalized = await step.isFinalized();
      if (!isFinalized) {
        this.currentStepIndex = stepIndex;
        await this.displayStep(stepIndex);
        break;
      }
    }
  }

  /**
   * @param {int} index
   * @param {int} hideIndex
   *
   * @return {void}
   */
  displayStep (index, hideIndex= -1) {
    if (this.steps[hideIndex]) {
      const stepItemToHide = this.steps[hideIndex];
      stepItemToHide.step.hide(stepItemToHide.containerElement);
    }

    const stepItemToShow = this.steps[index];
    stepItemToShow.step.show(stepItemToShow.containerElement);
    sayStepDisplayed();
  }

  /**
   * @return {void}
   */
  async next () {
    if (this.currentStepIndex < this.steps.length - 1) {
      const stepItem = this.steps[this.currentStepIndex+1];

      if (stepItem.containerElement.innerHTML === '') {
        this.container.appendChild(stepItem.containerElement);

        try {
          await stepItem.step.mount(stepItem.containerElement);
        } catch (error) {
          sayApplicationError('Something went wrong. We are working on fixing the issue. Please try again later');
          console.log('Could not mount step: ', stepItem, error);
          return;
        }
      }

      this.currentStepIndex++;
      this.displayStep(this.currentStepIndex, this.currentStepIndex - 1);
    }
  }

  /**
   * @return {void}
   */
  async prev () {
    if (this.currentStepIndex > 0) {
      this.currentStepIndex--;
      this.displayStep(this.currentStepIndex, this.currentStepIndex + 1);
    }
  }
}

export default StepEngine;
