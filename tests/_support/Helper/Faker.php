<?php
declare(strict_types=1);

namespace BeastMakersTest\Helper;

use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;

class Faker extends \Codeception\Module
{
  /**
   * @var FakerGenerator|null
   */
  private $fakerGenerator = null;

  /**
   * @return \Faker\Generator
   */
  public function faker(): FakerGenerator
  {
    if (!$this->fakerGenerator) {
      $this->fakerGenerator = FakerFactory::create();
    }

    return $this->fakerGenerator;
  }
}
