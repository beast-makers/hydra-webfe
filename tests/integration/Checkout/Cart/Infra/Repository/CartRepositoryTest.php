<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\Checkout\Cart\Infra\Repository;

use BeastMakers\Checkout\Cart\Infra\CartData;
use BeastMakers\Checkout\Cart\Infra\Repository\ReadRepository;
use BeastMakers\Checkout\Cart\Infra\Repository\WriteRepository;
use BeastMakersTest\Integration\Checkout\Cart\FactoryPublicDouble;
use BeastMakersTest\IntegrationTester;
use Codeception\Test\Unit;

class CartRepositoryTest extends Unit
{
  protected IntegrationTester $tester;

  private static ReadRepository $readRepository;

  private static WriteRepository $writeRepository;

  /**
   * @return void
   */
  public static function setUpBeforeClass(): void
  {
    $moduleFactory = new FactoryPublicDouble();
    self::$readRepository = $moduleFactory->shareCartReadRepository();
    self::$writeRepository = $moduleFactory->shareCartWriteRepository();
  }

  /**
   * @return void
   */
  public function testWriteReadCartData(): void
  {
    $expectedCartData = $this->buildCartDataSample();
    $checkoutId = $this->tester->faker()->uuid;

    self::$writeRepository->pushCartData($expectedCartData, $checkoutId);
    $actualCartData = self::$readRepository->fetchCartData($checkoutId);

    $this->assertSame(
      $expectedCartData->getProducts(),
      $actualCartData->getProducts()
    );
    $this->assertSame(
      $expectedCartData->getVoucher(),
      $actualCartData->getVoucher()
    );
  }

  /**
   * @return CartData
   */
  private function buildCartDataSample(): CartData
  {
    return new CartData();
  }
}
