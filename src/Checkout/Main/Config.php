<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Main;

use BeastMakers\Shared\Kernel\BaseConfig;

class Config extends BaseConfig
{
  /**
   * @return string
   */
  public function generateCheckoutId(): string
  {
    return uniqid('', true);
  }
}
