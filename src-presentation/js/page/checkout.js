import 'element-closest-polyfill';
import {bootstrap} from '../kernel/Bootstrap';
import {initCheckout} from '../component/Checkout/Facade';
import {initModal} from '../component/Modal/Facade';
import {initTabbedAccordion} from '../component/TabbedAccordion/Facade';

bootstrap();
initModal();
initCheckout();
initTabbedAccordion();
