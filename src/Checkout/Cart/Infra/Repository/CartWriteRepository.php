<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Infra\Repository;

use BeastMakers\Checkout\Cart\Infra\CartData;
use BeastMakers\Shared\RedisConnector\RedisClient;
use BeastMakers\Shared\RedisConnector\RedisException;

class CartWriteRepository implements WriteRepository
{
  private const EXPIRY_3_MONTHS = 3600 * 24 * 30 * 3;

  private RedisClient $redisClient;

  public function __construct(RedisClient $redisClient)
  {
    $this->redisClient = $redisClient;
  }

  /**
   * @inheritDoc
   * @throws RedisException
   */
  public function pushCartData(CartData $cartData, string $checkoutId): void
  {
    $products = $cartData->getProducts();
    $this->redisClient->redis()->setex(
      Key::CHECKOUT_PRODUCTS_PREFIX . $checkoutId,
      self::EXPIRY_3_MONTHS,
      json_encode($products)
    );

    $voucherCode = $cartData->getVoucher();
    $this->redisClient->redis()->setex(
      Key::CHECKOUT_VOUCHER_PREFIX . $checkoutId,
      self::EXPIRY_3_MONTHS,
      json_encode($voucherCode)
    );
  }
}
