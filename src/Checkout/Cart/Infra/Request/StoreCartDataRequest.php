<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Infra\Request;

use BeastMakers\Checkout\Cart\Infra\CartData;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

class StoreCartDataRequest
{
  public const PARAM_PRODUCTS = 'products';
  public const PARAM_VOUCHER_CODE = 'voucher_code';

  private ArrayValidator $validator;

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return CartData
   * @throws ArrayValidationException
   */
  public function createCartDataFromRequest(ServerRequestInterface $request): CartData
  {
    /** @var array $params */
    $params = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);
    $request->getBody()->rewind();

    return $this->createCartDataFromArray($params);
  }

  /**
   * @param array $input
   *
   * @return CartData
   * @throws ArrayValidationException
   */
  private function createCartDataFromArray(array $input): CartData
  {
    $this->validator->validate($input, static::defineValidationRules());
    $map = self::defineRequestParamsToCartDataMap();

    $input[self::PARAM_VOUCHER_CODE] = [$input[self::PARAM_VOUCHER_CODE]];

    return CartData::mapArrayToNewDto($input, $map);
  }

  /**
   * @return array
   */
  private static function defineValidationRules(): array
  {
    return [
      self::PARAM_PRODUCTS => 'required',
      self::PARAM_VOUCHER_CODE => 'required',
    ];
  }

  /**
   * @return array
   */
  private function defineRequestParamsToCartDataMap(): array
  {
    return [
      self::PARAM_PRODUCTS => CartData::ATTR_PRODUCTS,
      self::PARAM_VOUCHER_CODE => CartData::ATTR_VOUCHER,
    ];
  }
}
