<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra\Request;

use BeastMakers\Checkout\Shared\Domain\Address;
use BeastMakers\Checkout\Shared\Domain\Customer;
use BeastMakers\Shared\Validation\ArrayValidator;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;

class DeliveryDataRequest
{
  public const PARAM_FORM_DATA = 'form_data';

  public const ATTR_FIRST_NAME = 'first_name';
  public const ATTR_LAST_NAME = 'last_name';
  public const ATTR_COUNTRY_CODE = 'country_code';
  public const ATTR_STREET = 'street';
  public const ATTR_ADDRESS_EXTENSION = 'address_extension';
  public const ATTR_CITY = 'city';
  public const ATTR_ZIP = 'zip';
  public const ATTR_EMAIL = 'email';
  public const ATTR_BIRTH_DATE = 'birth_date';

  /**
   * @var string[]
   */
  private array $deliveryData = [];

  private ArrayValidator $validator;

  /**
   * @param ArrayValidator $validator
   */
  public function __construct(ArrayValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return $this
   */
  public function readRequest(ServerRequestInterface $request): self
  {
    $body = $request->getBody();
    $deliveryDataBody = $body->getContents();
    $body->rewind();

    $deliveryData = json_decode($deliveryDataBody, true);
    $this->deliveryData = $deliveryData[self::PARAM_FORM_DATA];

    return $this;
  }

  /**
   * @return Address
   * @throws ArrayValidationException
   */
  public function createAddress(): Address
  {
    return $this->createAddressFromArray($this->deliveryData);
  }

  /**
   * @return Customer
   * @throws ArrayValidationException
   */
  public function createCustomer(): Customer
  {
    return $this->createCustomerFromArray($this->deliveryData);
  }

  /**
   * @param array $deliveryData
   *
   * @return Address
   * @throws ArrayValidationException
   */
  private function createAddressFromArray(array $deliveryData): Address
  {
    $this->validator->validate($deliveryData, self::defineAddressValidationRules());

    return Address::mapArrayToNewDto($deliveryData, self::defineRequestParamsToAddressMap());
  }

  /**
   * @param array $deliveryData
   *
   * @return Customer
   * @throws ArrayValidationException
   */
  private function createCustomerFromArray(array $deliveryData): Customer
  {
    $this->validator->validate($deliveryData, self::defineCustomerValidationRules());

    return Customer::mapArrayToNewDto($deliveryData, self::defineRequestParamsToCustomerMap());
  }

  /**
   * @return string[]
   */
  private static function defineAddressValidationRules(): array
  {
    return [
      self::ATTR_FIRST_NAME => 'required',
      self::ATTR_LAST_NAME => 'required',
      self::ATTR_COUNTRY_CODE => 'required',
      self::ATTR_STREET => 'required',
      self::ATTR_ADDRESS_EXTENSION => 'required',
      self::ATTR_CITY => 'required',
      self::ATTR_ZIP => 'required',
    ];
  }

  /**
   * @return string[]
   */
  private static function defineCustomerValidationRules(): array
  {
    return [
      self::ATTR_FIRST_NAME => 'required',
      self::ATTR_LAST_NAME => 'required',
      self::ATTR_EMAIL => 'required',
      self::ATTR_BIRTH_DATE => 'required',
    ];
  }

  /**
   * @return array
   */
  public static function defineRequestParamsToAddressMap(): array
  {
    return [
      self::ATTR_FIRST_NAME => Address::ATTR_FIRST_NAME,
      self::ATTR_LAST_NAME => Address::ATTR_LAST_NAME,
      self::ATTR_COUNTRY_CODE => Address::ATTR_COUNTRY_CODE,
      self::ATTR_STREET => Address::ATTR_STREET,
      self::ATTR_ADDRESS_EXTENSION => Address::ATTR_ADDRESS_EXTENSION,
      self::ATTR_CITY => Address::ATTR_CITY,
      self::ATTR_ZIP => Address::ATTR_ZIP,
    ];
  }

  /**
   * @return array
   */
  public static function defineRequestParamsToCustomerMap(): array
  {
    return [
      self::ATTR_FIRST_NAME => Customer::ATTR_FIRST_NAME,
      self::ATTR_LAST_NAME => Customer::ATTR_LAST_NAME,
      self::ATTR_EMAIL => Customer::ATTR_EMAIL,
      self::ATTR_BIRTH_DATE => Customer::ATTR_BIRTH_DATE,
    ];
  }
}
