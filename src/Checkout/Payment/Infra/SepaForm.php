<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Payment\Infra;

use BeastMakers\Shared\Form\Form;
use BeastMakers\Shared\Form\FormField;
use BeastMakers\Shared\Form\FormFieldSingleValue;

class SepaForm extends Form
{
  public const FIELD_PAYMENT_TYPE = 'payment_type';
  public const FIELD_ACCOUNT_OWNER = 'account_owner';
  public const FIELD_ACCOUNT_NUMBER = 'account_number';

  /**
   * @return void
   */
  public function initialize(): void
  {
    $this->addField(static::FIELD_ACCOUNT_OWNER, $this->createAccountOwnerField());
    $this->addField(static::FIELD_ACCOUNT_NUMBER, $this->createAccountNumberField());
    $this->addField(static::FIELD_PAYMENT_TYPE, $this->createPaymentTypeField());
  }

  /**
   * @return FormFieldSingleValue
   */
  private function createAccountOwnerField(): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => 'account_owner',
      FormField::ATTR_LABEL => SepaLabel::ACCOUNT_OWNER,
      FormField::ATTR_PLACEHOLDER => 'John Smith',
    ]);

    return $formField;
  }

  /**
   * @return FormFieldSingleValue
   */
  private function createAccountNumberField(): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => 'account_number',
      FormField::ATTR_LABEL => SepaLabel::ACCOUNT_NUMBER,
      FormField::ATTR_PLACEHOLDER => 'DE 7551 2108 0012 4512 6199',
    ]);

    return $formField;
  }

  /**
   * @return FormFieldSingleValue
   */
  private function createPaymentTypeField(): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => 'payment_type',
      FormField::ATTR_VALUE => PaymentType::SEPA,
    ]);

    return $formField;
  }
}
