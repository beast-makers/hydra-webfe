import {newCartStep, newDeliveryStep, newPaymentStep} from './Factory';
import {sayApplicationError} from '../../kernel/Facade';

const stepEngineMap = {
  kit: function(stepEngine) { runKitStepEngine(stepEngine); }
}

/**
 * @param {String} checkoutVariant
 * @param {StepEngine} stepEngine
 *
 * @return {void}
 */
export function runStepEngine (checkoutVariant, stepEngine) {
  if (!validateCheckoutVariant(checkoutVariant)) {
    sayApplicationError('Something went wrong. Please try again later');
    console.log(`Unsupported checkout variant: [${checkoutVariant}]`);
    return;
  }

  (stepEngineMap[checkoutVariant])(stepEngine);
}

/**
 * @param {String} checkoutVariant
 *
 * @return {boolean}
 */
function validateCheckoutVariant (checkoutVariant) {
  return stepEngineMap.hasOwnProperty(checkoutVariant);
}

/**
 * @param {StepEngine} stepEngine
 *
 * @return {StepEngine}
 */
function runKitStepEngine (stepEngine) {
  stepEngine.run([
    newCartStep(stepEngine),
    newDeliveryStep(stepEngine),
    newPaymentStep(stepEngine),
  ]);
}
