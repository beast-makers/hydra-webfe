{extends file='layout/default.tpl'}

{block name=main_body}
    {fetch file='ui/assets/svg-checkout.svg' assign='svg_checkout'}
    {$svg_checkout|replace:'__svg-sprite__':'svg-checkout'}
    <div class="checkout-header">{block name=header}{/block}</div>
    {$smarty.block.child}
    <div class="checkout-footer">{block name=footer}{/block}</div>
{/block}
