#!/usr/bin/env bash
set -euxo pipefail

exec node_modules/stylelint/bin/stylelint.js 'src-presentation/style/**/*.scss' --config ./.stylelintrc.json $@
