/**
 * @callback liveClickCallback
 * @param {Event} event
 *
 * @return {void}
 */
/**
 * @param {string} selector
 * @param {liveClickCallback} callback
 * @param {Element} [elementScope=document]
 *
 * @return {void}
 */
export function onLiveClick (selector, callback, elementScope = document) {
  elementScope.addEventListener('click', function (event) {
    const element = event.target.closest(selector);

    if (element) {
      callback(event);
    }
  });
}

/**
 * @param {Function} func
 * @param {int} wait
 *
 * @return {function(...[*]=)}
 */
export function debounce (func, wait) {
  let timeout;

  return function () {
    function laterCall () {
      timeout = null;
      func();
    }

    clearTimeout(timeout);
    timeout = setTimeout(laterCall, wait);
  };
}

/**
 * @param {Function} func
 * @param {int} [waitMs=100]
 * @param {int} [limit=10]
 *
 * @return {void}
 */
export function deferrCall (func, waitMs = 100, limit = 10) {
  const interval = window.setInterval(() => {
    const isDone = func();
    if (limit-- < 1 || isDone) {
      window.clearInterval(interval);
    }
  }, waitMs);
}

/**
 *
 * @param {FormData} formData
 *
 * @return {Object}
 */
export function formData2Object (formData) {
  const obj = {};
  for (const key of formData.keys()) {
    obj[key] = formData.get(key);
  }

  return obj;
}
