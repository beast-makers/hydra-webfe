/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src-presentation/js/page/default.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/element-closest-polyfill/index.js":
/*!********************************************************!*\
  !*** ./node_modules/element-closest-polyfill/index.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function () {
  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
  }

  if (!Element.prototype.closest) {
    Element.prototype.closest = function (s) {
      var el = this;

      do {
        if (el.matches(s)) return el;
        el = el.parentElement || el.parentNode;
      } while (el !== null && el.nodeType === 1);

      return null;
    };
  }
})();

/***/ }),

/***/ "./src-presentation/js/component/Modal/EventKey.js":
/*!*********************************************************!*\
  !*** ./src-presentation/js/component/Modal/EventKey.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  OPEN_LOADING_SCREEN: 'modal-open_loading_screen',
  CLOSE_LOADING_SCREEN: 'modal-close_loading_screen',
  OPEN_ERROR_NOTIFICATION_MODAL: 'modal-open_error_notification'
});

/***/ }),

/***/ "./src-presentation/js/component/Modal/Facade.js":
/*!*******************************************************!*\
  !*** ./src-presentation/js/component/Modal/Facade.js ***!
  \*******************************************************/
/*! exports provided: initModal, openLoadingScreen, closeLoadingScreen, openErrorNotificationModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initModal", function() { return initModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "openLoadingScreen", function() { return openLoadingScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "closeLoadingScreen", function() { return closeLoadingScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "openErrorNotificationModal", function() { return openErrorNotificationModal; });
/* harmony import */ var _Starter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Starter */ "./src-presentation/js/component/Modal/Starter.js");
/* harmony import */ var _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../kernel/Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/component/Modal/EventKey.js");



function initModal() {
  Object(_Starter__WEBPACK_IMPORTED_MODULE_0__["startModal"])();
}
function openLoadingScreen() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_2__["default"].OPEN_LOADING_SCREEN);
}
function closeLoadingScreen() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_2__["default"].CLOSE_LOADING_SCREEN);
}
function openErrorNotificationModal(message) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_2__["default"].OPEN_ERROR_NOTIFICATION_MODAL, {
    message
  });
}

/***/ }),

/***/ "./src-presentation/js/component/Modal/NotificationModal.js":
/*!******************************************************************!*\
  !*** ./src-presentation/js/component/Modal/NotificationModal.js ***!
  \******************************************************************/
/*! exports provided: bindEvents, showNotificationModal, hideNotificationModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bindEvents", function() { return bindEvents; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showNotificationModal", function() { return showNotificationModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hideNotificationModal", function() { return hideNotificationModal; });
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../own */ "./src-presentation/js/component/own.js");
/* harmony import */ var _Screen_Overlay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Screen/Overlay */ "./src-presentation/js/component/Modal/Screen/Overlay.js");



function buildHtml(headerHtml, contentHtml) {
  return `
  <div class="c-own-modal js-own-modal">
    <div class="content-frame">
      <button class="btn-close js-own-modal-close"></button>
      <div class="content-wrapper">
        <div class="c-own-modal-header">
          ${headerHtml}
        </div>
        <div class="c-own-modal-content">
          ${contentHtml}
        </div>
        <footer class="c-own-modal-footer">
          <button class="button is-small js-own-modal-close">Close</button>
        </footer>
      </div>
    </div>
  </div>
</div>
    `;
}
/**
 * @return {void}
 */


function bindEvents() {
  Object(_own__WEBPACK_IMPORTED_MODULE_0__["onLiveClick"])('.js-own-modal-close', function () {
    hideNotificationModal();
    Object(_Screen_Overlay__WEBPACK_IMPORTED_MODULE_1__["hideOverlay"])();
  });
}
/**
 * @param {string} headerHtml
 * @param {string} contentHtml
 *
 * @return {void}
 */

function showNotificationModal(headerHtml, contentHtml) {
  const element = document.createElement('div');
  element.innerHTML = buildHtml(headerHtml, contentHtml);
  const modalElement = element.firstElementChild;
  Object(_Screen_Overlay__WEBPACK_IMPORTED_MODULE_1__["showOverlay"])();
  Object(_Screen_Overlay__WEBPACK_IMPORTED_MODULE_1__["replaceOverlayContentElement"])(modalElement);
  modalElement.classList.add('c-own-modal__is-visible');
}
/**
 * @return {void}
 */

function hideNotificationModal() {
  const modalElement = findModalElement();

  if (modalElement) {
    modalElement.classList.remove('c-own-modal__is-visible');
  }
}
/**
 * @return {Element}
 */

function findModalElement() {
  return document.querySelector('.js-own-modal');
}

/***/ }),

/***/ "./src-presentation/js/component/Modal/Screen/Loading.js":
/*!***************************************************************!*\
  !*** ./src-presentation/js/component/Modal/Screen/Loading.js ***!
  \***************************************************************/
/*! exports provided: showLoadingScreen, hideLoadingScreen */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showLoadingScreen", function() { return showLoadingScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hideLoadingScreen", function() { return hideLoadingScreen; });
/* harmony import */ var _Overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Overlay */ "./src-presentation/js/component/Modal/Screen/Overlay.js");

const LOADING_IS_HIDDEN = 'hidden';
const LOADING_IS_VISIBLE = 'visible';
const LOADING_NOT_FOUND = 'not_found';
/**
 * @return {string}
 */

function buildHtml() {
  return `
<div class="c-own-loading js-own-loading">
    <span class="c-own-loading--icon qa-own-loading-icon"></span>
</div>`;
}
/**
 * @return {void}
 */


function showLoadingScreen() {
  removeHiddenLoading();
  const element = document.createElement('div');
  element.innerHTML = buildHtml();
  const loadingElement = element.firstElementChild;
  Object(_Overlay__WEBPACK_IMPORTED_MODULE_0__["showOverlay"])();
  Object(_Overlay__WEBPACK_IMPORTED_MODULE_0__["replaceOverlayContentElement"])(loadingElement);
  loadingElement.classList.add('c-own-loading--icon__is-visible');
}
/**
 * @return {void}
 */

function hideLoadingScreen() {
  hideLoading();
  Object(_Overlay__WEBPACK_IMPORTED_MODULE_0__["hideOverlay"])();
}
/**
 * @return {void}
 */

function removeHiddenLoading() {
  const loadingElement = findHiddenLoading();

  if (loadingElement) {
    loadingElement.remove();
  }
}
/**
 * @return {void}
 */


function hideLoading() {
  const loadingElement = findLoadingElement();

  if (loadingElement) {
    loadingElement.classList.remove('c-own-loading--icon__is-visible');
  }
}
/**
 * @param {Element|null} loadingElement
 *
 * @return {boolean}
 */


function isLoadingHidden(loadingElement) {
  return discoverLoadingVisibility(loadingElement) === LOADING_IS_HIDDEN;
}
/**
 * @param {Element|null} loadingElement
 *
 * @return {string}
 */


function discoverLoadingVisibility(loadingElement) {
  if (!loadingElement) {
    return LOADING_NOT_FOUND;
  }

  if (loadingElement.classList.contains('c-own-loading--icon__is-visible')) {
    return LOADING_IS_VISIBLE;
  }

  return LOADING_IS_HIDDEN;
}
/**
 * @return {Element|null}
 */


function findHiddenLoading() {
  const loadingElement = findLoadingElement();

  if (isLoadingHidden(loadingElement)) {
    return loadingElement;
  }

  return null;
}
/**
 * @return {Element}
 */


function findLoadingElement() {
  return document.querySelector('.js-own-loading');
}

/***/ }),

/***/ "./src-presentation/js/component/Modal/Screen/Overlay.js":
/*!***************************************************************!*\
  !*** ./src-presentation/js/component/Modal/Screen/Overlay.js ***!
  \***************************************************************/
/*! exports provided: showOverlay, replaceOverlayContentElement, hideOverlay */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showOverlay", function() { return showOverlay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "replaceOverlayContentElement", function() { return replaceOverlayContentElement; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hideOverlay", function() { return hideOverlay; });
const OVERLAY_IS_HIDDEN = 'hidden';
const OVERLAY_IS_VISIBLE = 'visible';
const OVERLAY_NOT_FOUND = 'not_found';
/**
 * @return {Element}
 */

function showOverlay() {
  const overlayElement = findVisibleOverlay();

  if (overlayElement) {
    return overlayElement;
  }

  return createAndShowOverlay();
}
/**
 * @param {Element} contentElement
 */

function replaceOverlayContentElement(contentElement) {
  const overlayElement = findOverlayElement();
  overlayElement.innerHTML = '';
  overlayElement.insertAdjacentElement('afterbegin', contentElement);
}
/**
 * @return {void}
 */

function hideOverlay() {
  const overlayElement = findOverlayElement();

  if (overlayElement) {
    overlayElement.classList.remove('c-own-overlay__is-visible');
  }
}
/**
 * @return {Element}
 */

function createAndShowOverlay() {
  removeHiddenOverlay();
  const element = document.createElement('div');
  element.innerHTML = '<div class="c-own-overlay js-own-overlay"></div>';
  const overlayElement = element.firstElementChild;
  document.body.insertAdjacentElement('afterbegin', overlayElement);
  overlayElement.classList.add('c-own-overlay__is-visible');
  return overlayElement;
}
/**
 * @return {void}
 */


function removeHiddenOverlay() {
  const overlayElement = findHiddenOverlay();

  if (overlayElement) {
    overlayElement.remove();
  }
}
/**
 * @param {Element|null} overlayElement
 *
 * @return {boolean}
 */


function isOverlayHidden(overlayElement) {
  return discoverOverlayVisibility(overlayElement) === OVERLAY_IS_HIDDEN;
}
/**
 * @param {Element|null} overlayElement
 *
 * @return {boolean}
 */


function isOverlayVisible(overlayElement) {
  return discoverOverlayVisibility(overlayElement) === OVERLAY_IS_VISIBLE;
}
/**
 * @param {Element|null} overlayElement
 *
 * @return {string}
 */


function discoverOverlayVisibility(overlayElement) {
  if (!overlayElement) {
    return OVERLAY_NOT_FOUND;
  }

  if (overlayElement.classList.contains('c-own-overlay__is-visible')) {
    return OVERLAY_IS_VISIBLE;
  }

  return OVERLAY_IS_HIDDEN;
}
/**
 * @return {Element|null}
 */


function findHiddenOverlay() {
  const overlayElement = findOverlayElement();

  if (isOverlayHidden(overlayElement)) {
    return overlayElement;
  }

  return null;
}
/**
 * @return {Element|null}
 */


function findVisibleOverlay() {
  const overlayElement = findOverlayElement();

  if (isOverlayVisible(overlayElement)) {
    return overlayElement;
  }

  return null;
}
/**
 * @return {Element}
 */


function findOverlayElement() {
  return document.querySelector('.js-own-overlay');
}

/***/ }),

/***/ "./src-presentation/js/component/Modal/Starter.js":
/*!********************************************************!*\
  !*** ./src-presentation/js/component/Modal/Starter.js ***!
  \********************************************************/
/*! exports provided: startModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startModal", function() { return startModal; });
/* harmony import */ var _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../kernel/Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/component/Modal/EventKey.js");
/* harmony import */ var _Screen_Loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Screen/Loading */ "./src-presentation/js/component/Modal/Screen/Loading.js");
/* harmony import */ var _NotificationModal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./NotificationModal */ "./src-presentation/js/component/Modal/NotificationModal.js");




function startModal() {
  Object(_NotificationModal__WEBPACK_IMPORTED_MODULE_3__["bindEvents"])();
  registerModalListeners();
}

function registerModalListeners() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].OPEN_LOADING_SCREEN, function () {
    Object(_Screen_Loading__WEBPACK_IMPORTED_MODULE_2__["showLoadingScreen"])();
  });
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].CLOSE_LOADING_SCREEN, function () {
    Object(_Screen_Loading__WEBPACK_IMPORTED_MODULE_2__["hideLoadingScreen"])();
  });
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].OPEN_ERROR_NOTIFICATION_MODAL, function (key, data) {
    Object(_NotificationModal__WEBPACK_IMPORTED_MODULE_3__["showNotificationModal"])('Error Message', data.message);
  });
}

/***/ }),

/***/ "./src-presentation/js/component/own.js":
/*!**********************************************!*\
  !*** ./src-presentation/js/component/own.js ***!
  \**********************************************/
/*! exports provided: onLiveClick, debounce, deferrCall, formData2Object */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onLiveClick", function() { return onLiveClick; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "debounce", function() { return debounce; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deferrCall", function() { return deferrCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formData2Object", function() { return formData2Object; });
/**
 * @callback liveClickCallback
 * @param {Event} event
 *
 * @return {void}
 */

/**
 * @param {string} selector
 * @param {liveClickCallback} callback
 * @param {Element} [elementScope=document]
 *
 * @return {void}
 */
function onLiveClick(selector, callback, elementScope = document) {
  elementScope.addEventListener('click', function (event) {
    const element = event.target.closest(selector);

    if (element) {
      callback(event);
    }
  });
}
/**
 * @param {Function} func
 * @param {int} wait
 *
 * @return {function(...[*]=)}
 */

function debounce(func, wait) {
  let timeout;
  return function () {
    function laterCall() {
      timeout = null;
      func();
    }

    clearTimeout(timeout);
    timeout = setTimeout(laterCall, wait);
  };
}
/**
 * @param {Function} func
 * @param {int} [waitMs=100]
 * @param {int} [limit=10]
 *
 * @return {void}
 */

function deferrCall(func, waitMs = 100, limit = 10) {
  const interval = window.setInterval(() => {
    const isDone = func();

    if (limit-- < 1 || isDone) {
      window.clearInterval(interval);
    }
  }, waitMs);
}
/**
 *
 * @param {FormData} formData
 *
 * @return {Object}
 */

function formData2Object(formData) {
  const obj = {};

  for (const key of formData.keys()) {
    obj[key] = formData.get(key);
  }

  return obj;
}

/***/ }),

/***/ "./src-presentation/js/kernel/Bootstrap.js":
/*!*************************************************!*\
  !*** ./src-presentation/js/kernel/Bootstrap.js ***!
  \*************************************************/
/*! exports provided: bootstrap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bootstrap", function() { return bootstrap; });
/* harmony import */ var _component_Modal_Facade__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../component/Modal/Facade */ "./src-presentation/js/component/Modal/Facade.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Facade */ "./src-presentation/js/kernel/Facade.js");


function bootstrap() {
  Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["onApplicationError"])(function (key, data) {
    console.log('Runtime error: ' + data.message);
    Object(_component_Modal_Facade__WEBPACK_IMPORTED_MODULE_0__["openErrorNotificationModal"])(data.message);
  });
}

/***/ }),

/***/ "./src-presentation/js/kernel/EventKey.js":
/*!************************************************!*\
  !*** ./src-presentation/js/kernel/EventKey.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  RUNTIME_APPLICATION_ERROR: 'runtime-application-error'
});

/***/ }),

/***/ "./src-presentation/js/kernel/Facade.js":
/*!**********************************************!*\
  !*** ./src-presentation/js/kernel/Facade.js ***!
  \**********************************************/
/*! exports provided: sayApplicationError, onApplicationError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sayApplicationError", function() { return sayApplicationError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onApplicationError", function() { return onApplicationError; });
/* harmony import */ var _Universe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/kernel/EventKey.js");


/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */

/**
 * @param {String} message
 */

function sayApplicationError(message) {
  _Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].RUNTIME_APPLICATION_ERROR, {
    'message': message
  });
}
/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */

function onApplicationError(callback) {
  _Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].RUNTIME_APPLICATION_ERROR, callback);
}

/***/ }),

/***/ "./src-presentation/js/kernel/Universe.js":
/*!************************************************!*\
  !*** ./src-presentation/js/kernel/Universe.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const Universe = {};
/**
 * @param {Object[]} target
 * @param {Object} properties
 * @param {string} properties.key
 * @param {int} properties.priority
 * @param {Function} properties.callback
 */

function subscribeTo(target, properties) {
  const {
    key,
    priority,
    callback
  } = properties;

  if (!target[key]) {
    target[key] = [];
  }

  if (!target[key][priority]) {
    target[key][priority] = [];
  }

  target[key][priority].push(callback);
}

const state = {};
const stateListeners = [];
Universe.State = {
  /**
   * @param {string} key
   * @param {*} value
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */
  set(key, value, notificationData = {}) {
    state[key] = value;
    this.notify(key, value, notificationData);
  },

  /**
   * @param {string} key
   * @param {*} fallback
   *
   * @return {*}
   */
  get(key, fallback = undefined) {
    if (!state.hasOwnProperty(key)) {
      return fallback;
    }

    return state[key];
  },

  /**
   * @callback stateSubscribeCallback
   * @param {string} key
   * @param {*} value
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */

  /**
   * @param {string} key
   * @param {stateSubscribeCallback} callback
   * @param {int} [priority=500]
   *
   * @return {void}
   */
  subscribe(key, callback, priority = 500) {
    subscribeTo(stateListeners, {
      key,
      priority,
      callback
    });
  },

  /**
   * @param {string} key
   * @param {*} value
   * @param {*} notificationData
   *
   * @return {void}
   */
  notify(key, value, notificationData = {}) {
    if (stateListeners[key]) {
      const priorities = Object.keys(stateListeners[key]);

      for (const priorityKey in priorities) {
        const priority = priorities[priorityKey];

        for (const index in stateListeners[key][priority]) {
          const callback = stateListeners[key][priority][index];
          callback(key, value, notificationData);
        }
      }
    }
  }

};
const eventListeners = [];
Universe.Event = {
  /**
   * @callback eventSubscribeCallback
   * @param {string} key
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */

  /**
   * @param {string} key
   * @param {eventSubscribeCallback} callback
   * @param {int} [priority=500]
   *
   * @return {void}
   */
  subscribe(key, callback, priority = 500) {
    subscribeTo(eventListeners, {
      key,
      priority,
      callback
    });
  },

  /**
   * @param {string} key
   * @param {*} [notificationData={}]
   *
   * @return {Array}
   */
  notify(key, notificationData = {}) {
    let responses = [];

    if (eventListeners[key]) {
      const priorities = Object.keys(eventListeners[key]);

      for (const priorityKey in priorities) {
        const priority = priorities[priorityKey];

        for (const index in eventListeners[key][priority]) {
          const callback = eventListeners[key][priority][index];
          responses.push(callback(key, notificationData));
        }
      }
    }

    return responses;
  }

};
/* harmony default export */ __webpack_exports__["default"] = (Universe);

/***/ }),

/***/ "./src-presentation/js/page/default.js":
/*!*********************************************!*\
  !*** ./src-presentation/js/page/default.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var element_closest_polyfill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! element-closest-polyfill */ "./node_modules/element-closest-polyfill/index.js");
/* harmony import */ var element_closest_polyfill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(element_closest_polyfill__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _kernel_Bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../kernel/Bootstrap */ "./src-presentation/js/kernel/Bootstrap.js");
/* harmony import */ var _component_Modal_Facade__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../component/Modal/Facade */ "./src-presentation/js/component/Modal/Facade.js");



Object(_kernel_Bootstrap__WEBPACK_IMPORTED_MODULE_1__["bootstrap"])();
Object(_component_Modal_Facade__WEBPACK_IMPORTED_MODULE_2__["initModal"])();

/***/ })

/******/ });
//# sourceMappingURL=default_script.js.map