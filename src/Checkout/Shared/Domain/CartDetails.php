<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class CartDetails extends ArrayDto
{
  public const ATTR_CART_ITEM_LINES = 'item_lines';
  public const ATTR_SHIPPING_PRICE = 'shipping_price';
  public const ATTR_PRICE_TO_PAY = 'price_to_pay';
  public const ATTR_CURRENCY_SYMBOL = 'currency_symbol';

  /**
   * @return CartItemLine[]
   */
  public function getCartItemLines(): array
  {
    return $this->get(self::ATTR_CART_ITEM_LINES, []);
  }

  /**
   * @return int
   */
  public function getShippingPrice(): int
  {
    return $this->get(self::ATTR_SHIPPING_PRICE, []);
  }

  /**
   * @return string
   */
  public function getShippingPriceFormatted(): string
  {
    if (!$this->getShippingPrice()) {
      return '';
    }

    return "{$this->getShippingPrice()} {$this->getCurrencySymbol()}";
  }

  /**
   * @return int
   */
  public function getPriceToPay(): int
  {
    return $this->get(self::ATTR_PRICE_TO_PAY, -1);
  }

  /**
   * @return string
   */
  public function getPriceToPayFormatted(): string
  {
    return "{$this->getPriceToPay()} {$this->getCurrencySymbol()}";
  }

  /**
   * @return string
   */
  public function getCurrencySymbol(): string
  {
    return $this->get(self::ATTR_CURRENCY_SYMBOL, '');
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_CART_ITEM_LINES => [self::TYPE_ARRAY, CartItemLine::class],
      self::ATTR_SHIPPING_PRICE => self::DEF_SCALAR,
      self::ATTR_PRICE_TO_PAY => self::DEF_SCALAR,
      self::ATTR_CURRENCY_SYMBOL => self::DEF_SCALAR,
    ];
  }
}
