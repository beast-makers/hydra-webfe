import {debounce} from '../own';
import validate from 'validate.js';
import {FieldsResult} from './FieldsResult';
import MessageCollection from '../Result/MessageCollection';
import {ValidationResult} from '../Result/ValidationResult';

const IS_ERROR_CLASS = 'is-error';

function discoverContainerElement (input) {
  return input.element.closest(input.containerSelector);
}

function discoverMessageElement (input) {
  const containerElement = discoverContainerElement(input);
  return containerElement.querySelector(input.messageElementSelector);
}

function defineTextInputAccessors (input) {
  Object.defineProperty(input, 'value', {
    get () {
      return input.element.value;
    },

    set (value) {
      input.element.value = value;
    }
  });
}

function defineMessageTextAccessors (input) {
  Object.defineProperty(input, 'messageText', {
    get () {
      return discoverMessageElement(input).textContent;
    },

    set (value) {
      discoverMessageElement(input).textContent = value;
    }
  });
}

function defineMessageHtmlAccessors (input) {
  Object.defineProperty(input, 'messageHtml', {
    get () {
      return discoverMessageElement(input).innerHTML;
    },

    set (value) {
      discoverMessageElement(input).innerHTML = value;
    }
  });
}

function defineContainerElementGetter (input) {
  Object.defineProperty(input, 'container', {
    get () {
      return discoverContainerElement(input);
    }
  });
}

export class Form {
  constructor (formFields) {
    this.formFields = formFields;
  }

  /**
   * @return {void}
   */
  bind () {
    for (const key in this.formFields) {
      const field = this.formFields[key];
      field.element = document.querySelector(field.inputSelector);

      defineTextInputAccessors(field);
      defineMessageTextAccessors(field);
      defineMessageHtmlAccessors(field);
      defineContainerElementGetter(field);
    }
  }

  /**
   * @return {void}
   */
  bindValidation () {
    for (const key in this.formFields) {
      const field = this.formFields[key];
      field.element.oninput = debounce(() => {
        const errorMessages = this.validateField(field);
        this.updateFieldError(field, errorMessages);
      }, 150);
    }
  }

  /**
   * @param {Object} field
   * @param {String} errorMessage
   * @return {void}
   */
  setError (field, errorMessage) {
    field.messageText = errorMessage;
    field.container.classList.add(IS_ERROR_CLASS);
  }

  /**
   * @param {Object} field
   * @return {void}
   */
  clearError (field) {
    field.container.classList.remove(IS_ERROR_CLASS);
    field.messageText = '';
  }

  /**
   * @param {Object} field
   *
   * @return {Array}
   */
  validateField (field) {
    const errorMessages = validate.single(field.value, field.constraints);

    return errorMessages || [];
  }

  /**
   * @return {Object}
   */
  validateAllFields () {
    const result = {
      isValid: true,
      messages: []
    };

    for (const key in this.formFields) {
      const field = this.formFields[key];
      const errorMessages = this.validateField(field);
      if (errorMessages.length) {
        result.isValid = false;
        result.messages[key] = errorMessages;
      }
    }

    return result;
  }

  /**
   * @return {ValidationResult}
   */
  validateForm () {
    const result = new ValidationResult();

    for (const key in this.formFields) {
      const field = this.formFields[key];
      const errorMessages = this.validateField(field);
      if (errorMessages.length) {
        result.setIsSuccess(false);
        result.addMessageCollection(new MessageCollection(key, errorMessages));
      }
      this.updateFieldError(field, errorMessages);
    }

    return result;
  }

  /**
   * @param {Object} field
   * @param {Array} errorMessages
   * @return {void}
   */
  updateFieldError (field, errorMessages) {
    if (errorMessages && errorMessages.length > 0) {
      this.setError(field, errorMessages[0]);
    } else {
      this.clearError(field);
    }
  }

  /**
   * @return {FieldsResult}
   */
  findEmptyFields () {
    const emptyFieldsResult = new FieldsResult();

    for (const key in this.formFields) {
      const field = this.formFields[key];
      if (field.value !== '') {
        emptyFieldsResult.addField(key, field);
      }
    }

    return emptyFieldsResult;
  }

  /**
   * @return {Element}
   */
  findFormElement () {
    // noinspection LoopStatementThatDoesntLoopJS
    for (const key in this.formFields) {
      const field = this.formFields[key];
      return field.element.closest('form');
    }

    throw new Error('Form element not found');
  }
}

export const defaultOptions = {
  containerSelector: '.js-form-field',
  messageElementSelector: '.js-input-message',
}
