export default class PaymentUi {

  /**
   * @param {Payment} payment
   * @param {Array} paymentMethodsHandlers
   */
  constructor (payment, paymentMethodsHandlers) {
    this.payment = payment;
    this.paymentMethodsHandlers = paymentMethodsHandlers;
  }

  /**
   * @return {void}
   */
  init () {
    for (const index in this.paymentMethodsHandlers) {
      this.paymentMethodsHandlers[index].init();
    }
  }

  /**
   * @param {Element} containerElement
   *
   * @return {void}
   */
  async mount (containerElement) {
    if (document.querySelector('.js-accept-payment-marker')) {
      return;
    }

    await this.payment.fetchPaymentPageData()
      .then((paymentPage) => {
        const tempContainer = document.createElement('div');
        tempContainer.innerHTML = paymentPage.payment_page_html;

        containerElement.classList.add('is-hidden');
        containerElement.innerHTML = tempContainer.innerHTML;

        this.mountPaymentMethods();
      })
      .catch(() => {
        throw new Error('Could not mount PaymentUi');
      });
  }

  mountPaymentMethods () {
    for (const handler in this.paymentMethodsHandlers) {
      this.paymentMethodsHandlers[handler].mount();
    }
  }
}
