<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\RedisConnectionCatalog;
use BeastMakers\Shared\RedisConnector\ClientPool;
use BeastMakers\Shared\RedisConnector\RedisClient;

trait RedisWebfeClient
{
  /**
   * @return RedisClient
   */
  protected function shareRedisWebfeClient(): RedisClient
  {
    /** @var ClientPool $connectionPool */
    $connectionPool = DependencyContainer::getInstance()->get(DependencyContainerItem::REDIS_CONNECTION_POOL);

    return $connectionPool->get(RedisConnectionCatalog::WEBFE);
  }
}
