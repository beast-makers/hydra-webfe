<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\Kernel;

use BeastMakers\Shared\Kernel\DependencyContainer;

class DependencyContainerTest extends \Codeception\Test\Unit
{
  public function testSetGetMethods(): void
  {
    $dc = DependencyContainer::getInstance();
    $expected = 'message';

    $dc->set('CUSTOM_KEY', function () use ($expected) {
      return $expected;
    });

    $this->assertSame($expected, $dc->get('CUSTOM_KEY'));
  }
}
