<?php
declare(strict_types=1);

namespace BeastMakersTest\Checkout;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Checkout\Delivery\Infra\DeliveryForm;
use BeastMakers\Checkout\Shared\Domain\Address;
use BeastMakers\Checkout\Shared\Domain\Customer;
use BeastMakersTest\Page\Ui\Checkout\DeliveryPage;
use BeastMakersTest\UiTester;

class CheckoutRestoringStateCest
{
  private DeliveryData $deliveryData;

  /**
   * @param UiTester $i
   * @param DeliveryPage $deliveryPage
   *
   * @return void
   * @throws \Exception
   */
  public function verifyRestoredDeliveryData(UiTester $i, DeliveryPage $deliveryPage): void
  {
    $this->deliveryData = $this->buildSampleDeliveryData($i);

    $this->visitDeliveryPage($i);
    $this->fillDeliveryPageFormWithErrors($deliveryPage);
    $i->click(DeliveryPage::ACCEPT_DELIVERY_BUTTON);
    $i->expectTo('see error fields and store the data');

    $this->refreshCheckoutPage($i, DeliveryPage::FORM_SELECTOR);
    $this->seeDeliverPageFormErrorsAfterPageRefresh($deliveryPage);

    $this->fillFailingDeliveryPageFormFields($deliveryPage);
    $this->continueToPaymentPage($i);

    $this->refreshCheckoutPage($i, DeliveryPage::PAYMENT_SELECTION_SELECTOR);
  }

  /**
   * @param UiTester $i
   *
   * @return DeliveryData
   */
  private function buildSampleDeliveryData(UiTester $i): DeliveryData
  {
    $faker = $i->faker();
    $firstName = $faker->firstName;
    $lastName = $faker->lastName;

    $addressDto = Address::createFromArray([
      Address::ATTR_FIRST_NAME => $firstName,
      Address::ATTR_LAST_NAME => $lastName,
      Address::ATTR_COUNTRY_CODE => $faker->countryCode,
      Address::ATTR_STREET => $faker->streetAddress,
      Address::ATTR_CITY => $faker->city,
      Address::ATTR_ZIP => $faker->postcode,
    ]);

    $customerDto = Customer::createFromArray([
      Customer::ATTR_FIRST_NAME => $firstName,
      Customer::ATTR_LAST_NAME => $lastName,
      Customer::ATTR_EMAIL => $faker->email,
      Customer::ATTR_BIRTH_DATE => $faker->date('d-m-Y'),
    ]);

    $deliveryData = new DeliveryData();
    $deliveryData->setAddress($addressDto);
    $deliveryData->setCustomer($customerDto);

    return $deliveryData;
  }

  /**
   * @param UiTester $i
   *
   * @return void
   * @throws \Exception
   */
  private function visitDeliveryPage(UiTester $i): void
  {
    $i->amOnPage('/checkout');
    $i->shortWaitForElement(DeliveryPage::CART_ITEM_LINE_SELECTOR);
    $i->click(DeliveryPage::ACCEPT_CART_BUTTON);
    $i->shortWaitForElement(DeliveryPage::FORM_SELECTOR);
  }

  /**
   * @param DeliveryPage $deliveryPage
   *
   * @return void
   */
  private function fillDeliveryPageFormWithErrors(DeliveryPage $deliveryPage): void
  {
    $customerDto = $this->deliveryData->getCustomer();
    $deliveryPage->fillField(DeliveryForm::FIELD_FIRST_NAME, $customerDto->getFirstName());
    $deliveryPage->fillField(DeliveryForm::FIELD_LAST_NAME, $customerDto->getLastName());
    $deliveryPage->fillField(DeliveryForm::FIELD_EMAIL, $customerDto->getEmail());
    $deliveryPage->fillField(DeliveryForm::FIELD_BIRTH_DATE, ''); //error missing birth date

    $addressDto = $this->deliveryData->getAddress();
    $selectedOption = $deliveryPage->selectFirstOption(DeliveryForm::FIELD_COUNTRY_CODE);
    $addressDto->setCountryCode(strtoupper($selectedOption));
    $deliveryPage->fillField(DeliveryForm::FIELD_STREET, $addressDto->getStreet());
    $deliveryPage->fillField(DeliveryForm::FIELD_CITY, $addressDto->getCity());
    $deliveryPage->fillField(DeliveryForm::FIELD_ZIP, $addressDto->getZip());
  }

  /**
   * @param DeliveryPage $deliveryPage
   *
   * @return void
   * @depends showDeliveryPageFieldErrors
   */
  private function seeDeliverPageFormErrorsAfterPageRefresh(DeliveryPage $deliveryPage): void
  {
    $customerDto = $this->deliveryData->getCustomer();
    $deliveryPage->seeInField(DeliveryForm::FIELD_FIRST_NAME, $customerDto->getFirstName());
    $deliveryPage->seeInField(DeliveryForm::FIELD_LAST_NAME, $customerDto->getLastName());
    $deliveryPage->seeInField(DeliveryForm::FIELD_EMAIL, $customerDto->getEmail());
    $deliveryPage->seeInField(DeliveryForm::FIELD_BIRTH_DATE, '');

    $addressDto = $this->deliveryData->getAddress();
    $deliveryPage->seeInSelect(DeliveryForm::FIELD_COUNTRY_CODE, strtolower($addressDto->getCountryCode()));
    $deliveryPage->seeInField(DeliveryForm::FIELD_STREET, $addressDto->getStreet());
    $deliveryPage->seeInField(DeliveryForm::FIELD_CITY, $addressDto->getCity());
    $deliveryPage->seeInField(DeliveryForm::FIELD_ZIP, $addressDto->getZip());
  }

  /**
   * @param DeliveryPage $deliveryPage
   *
   * @return void
   * @depends showDeliveryPageFieldErrors
   */
  private function fillFailingDeliveryPageFormFields(DeliveryPage $deliveryPage): void
  {
    $customerDto = $this->deliveryData->getCustomer();
    $deliveryPage->fillField(DeliveryForm::FIELD_BIRTH_DATE, $customerDto->getBirthDate());
  }

  /**
   * @param UiTester $i
   * @param string $seeExpectedSelector
   *
   * @return void
   * @throws \Exception
   */
  private function refreshCheckoutPage(UiTester $i, string $seeExpectedSelector): void
  {
    $i->amOnPage('/checkout');
    $i->expectTo("see {$seeExpectedSelector}");
    $i->longWaitForElement($seeExpectedSelector); //long is needed as loading and validating checkout is time consuming
  }

  /**
   * @param UiTester $i
   *
   * @return void
   * @throws \Exception
   */
  private function continueToPaymentPage(UiTester $i): void
  {
    $i->click(DeliveryPage::ACCEPT_DELIVERY_BUTTON);
    $i->expectTo('store the data and load the payment page');
    $i->shortWaitForElementVisible(DeliveryPage::PAYMENT_SELECTION_SELECTOR);
  }
}
