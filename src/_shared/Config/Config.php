<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Config;

class Config
{
  protected array $config = [];

  /**
   * @param array $configuration
   */
  public function __construct(array $configuration)
  {
    $this->config = $configuration;
  }

  /**
   * @param string $path
   * @return Config
   *
   * @throws MissingConfigOptionException
   * @psalm-suppress InvalidReturnType
   */
  public function getConfig(string $path): self
  {
    /** @psalm-suppress InvalidReturnStatement */
    return $this->get($path);
  }

  /**
   * @param string $path
   * @return string
   *
   * @throws MissingConfigOptionException
   * @psalm-suppress InvalidReturnType
   */
  public function getString(string $path): string
  {
    /** @psalm-suppress InvalidReturnStatement */
    return $this->get($path);
  }

  /**
   * @param string $path
   * @return bool
   *
   * @throws MissingConfigOptionException
   * @psalm-suppress InvalidReturnType
   */
  public function getBool(string $path): bool
  {
    /** @psalm-suppress InvalidReturnStatement */
    return $this->get($path);
  }

  /**
   * @param string $path
   * @return int
   *
   * @throws MissingConfigOptionException
   * @psalm-suppress InvalidReturnType
   */
  public function getInt(string $path): int
  {
    /** @psalm-suppress InvalidReturnStatement */
    return intval($this->get($path));
  }

  /**
   * @param string $path
   * @return array
   *
   * @throws MissingConfigOptionException
   */
  public function getArray(string $path): array
  {
    /** @psalm-suppress PossiblyInvalidPropertyFetch */
    return $this->get($path)->config;
  }

  /**
   * @psalm-suppress UnsafeInstantiation
   * @param string $path
   *
   * @return array|bool|int|string|Config
   * @throws MissingConfigOptionException
   */
  private function get(string $path)
  {
    $tokens = explode('.', $path);
    $config = $this->config;

    foreach ($tokens as $token) {
      if (!array_key_exists($token, $config)) {
        throw new MissingConfigOptionException($path);
      }

      $config = $config[$token];
    }

    if (is_array($config)) {
      return new static($config);
    }

    return $config;
  }

  /**
   * @param string $path
   * @param $value
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  public function set(string $path, $value): void
  {
    $tokens = explode('.', $path);
    $config = &$this->config;

    foreach ($tokens as $token) {
      if (!array_key_exists($token, $config)) {
        throw new MissingConfigOptionException($path);
      }

      $config = &$config[$token];
    }

    $config = $value;
  }
}
