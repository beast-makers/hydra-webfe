<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

class BaseFactory
{
  /**
   * @var object[]
   */
  protected $instanceCache = [];

  /**
   * @var BaseDependencyProvider
   */
  protected $dependencyProvider;

  /**
   * @var BaseConfig
   */
  protected $config;

  /**
   * @param BaseDependencyProvider $dependencyProvider
   * @param BaseConfig $config
   */
  public function __construct(BaseDependencyProvider $dependencyProvider, BaseConfig $config)
  {
    $this->dependencyProvider = $dependencyProvider;
    $this->config = $config;
  }

  /**
   * @return BaseConfig
   */
  public function getConfig(): BaseConfig
  {
    return $this->config;
  }

  /**
   * @param string $key
   * @param object $instance
   */
  protected function setCachedInstance(string $key, object $instance): void
  {
    $this->instanceCache[$key] = $instance;
  }

  /**
   * @param string $key
   *
   * @return object|null
   */
  protected function getCachedInstance(string $key): ?object
  {
    return $this->instanceCache[$key] ?? null;
  }

  /**
   * @param string $key
   * @param \Closure $constructorFunction
   *
   * @return object
   */
  protected function shareInstance(string $key, \Closure $constructorFunction): object
  {
    $instance = $this->getCachedInstance($key);
    if ($instance === null) {
      $instance = $constructorFunction($this);
      $this->setCachedInstance($key, $instance);
    }

    return $instance;
  }
}
