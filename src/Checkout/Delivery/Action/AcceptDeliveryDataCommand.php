<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Action;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Checkout\Shared\Result\AcceptDeliveryDataResult;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use GuzzleHttp\Exception\GuzzleException;

class AcceptDeliveryDataCommand
{
  private StoreDeliveryDataCommand $storeDeliveryDataCommand;

  private ValidateDeliveryDataCommand $validateDeliveryDataCommand;

  /**
   * @param ValidateDeliveryDataCommand $validateDeliveryDataModel
   * @param StoreDeliveryDataCommand $storeDeliveryDataModel
   */
  public function __construct(
    ValidateDeliveryDataCommand $validateDeliveryDataModel,
    StoreDeliveryDataCommand $storeDeliveryDataModel
  ) {
    $this->storeDeliveryDataCommand = $storeDeliveryDataModel;
    $this->validateDeliveryDataCommand = $validateDeliveryDataModel;
  }

  /**
   * @param DeliveryData $deliveryData
   *
   * @return AcceptDeliveryDataResult
   * @throws GuzzleException
   * @throws MissingConfigOptionException
   */
  public function acceptDeliveryData(DeliveryData $deliveryData): AcceptDeliveryDataResult
  {
    $modelResult = new AcceptDeliveryDataResult();

    $validateDataResult = $this->validateDeliveryDataCommand->validateDeliveryData($deliveryData);
    if (!$validateDataResult->getIsSuccess()) {
      $modelResult->getErrorCollection()->merge($validateDataResult->getErrorCollection());

      return $modelResult;
    }

    $storeDataResult = $this->storeDeliveryDataCommand->storeDeliveryData($deliveryData);
    if (!$storeDataResult->getIsSuccess()) {
      $modelResult->getErrorCollection()->merge($validateDataResult->getErrorCollection());

      return $modelResult;
    }

    return $modelResult;
  }
}
