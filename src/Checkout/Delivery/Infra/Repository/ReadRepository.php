<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra\Repository;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;

interface ReadRepository
{
  /**
   * @param string $checkoutId
   *
   * @return DeliveryData
   */
  public function fetchDeliveryData(string $checkoutId): DeliveryData;
}
