<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class Customer extends ArrayDto
{
  public const ATTR_FIRST_NAME = 'first_name';
  public const ATTR_LAST_NAME = 'last_name';
  public const ATTR_EMAIL = 'email';
  public const ATTR_BIRTH_DATE = 'birth_date';

  /**
   * @return string
   */
  public function getFirstName(): string
  {
    return $this->get(self::ATTR_FIRST_NAME, '');
  }

  /**
   * @return string
   */
  public function getLastName(): string
  {
    return $this->get(self::ATTR_LAST_NAME, '');
  }

  /**
   * @return string
   */
  public function getEmail(): string
  {
    return $this->get(self::ATTR_EMAIL, '');
  }

  /**
   * @return string
   */
  public function getBirthDate(): string
  {
    return $this->get(self::ATTR_BIRTH_DATE, '');
  }

  /**
   * @inheritDoc
   */
  public function definition(): array
  {
    return [
      self::ATTR_FIRST_NAME => self::DEF_SCALAR,
      self::ATTR_LAST_NAME => self::DEF_SCALAR,
      self::ATTR_EMAIL => self::DEF_SCALAR,
      self::ATTR_BIRTH_DATE => self::DEF_SCALAR,
    ];
  }
}
