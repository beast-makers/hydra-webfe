import {closeLoadingScreen, openLoadingScreen} from '../Modal/Facade';
import {newStepEngine} from './Factory';
import {onStepDisplayed} from '../StepEngine/Facade';
import {getCheckoutConfig, setCheckoutConfig} from './Facade';
import {runStepEngine} from './StepEngineCommand';

export function startCheckout () {
  onStepDisplayed(function () {
    closeLoadingScreen();
  })

  openLoadingScreen();
  loadConfig();
  startStepEngine();
}

/**
 * @return {void}
 */
function loadConfig () {
  const config = JSON.parse(document.getElementById('js-checkout-config').innerHTML);
  setCheckoutConfig(config);
}

/**
 * @return {void}
 */
function startStepEngine () {
  const stepEngine = newStepEngine(document.querySelector('.js-checkout-container'));
  const {checkout_variant: checkoutVariant} = getCheckoutConfig();

  runStepEngine(checkoutVariant, stepEngine);
}
