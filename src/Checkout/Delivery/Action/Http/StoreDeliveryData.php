<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Action\Http;

use BeastMakers\Checkout\Delivery\Factory;
use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Checkout\Shared\ErrorCode;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class StoreDeliveryData extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws ArrayValidationException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $ddRequest = $this->factory->newDeliveryDataRequest()->readRequest($request);
    $deliveryData = (new DeliveryData())
      ->setAddress($ddRequest->createAddress())
      ->setCustomer($ddRequest->createCustomer())
    ;

    $model = $this->factory->newStoreDeliveryDataCommand();
    $storeDeliveryDataResult = $model->storeDeliveryData($deliveryData);

    if ($storeDeliveryDataResult->getErrorCollection()->hasErrors()) {
      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($storeDeliveryDataResult, $this->getHttpCodeErrorMap());
    }

    return $this->actionResponseBuilder->createJsonHttpResponse(204, []);
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::MISSING_CHECKOUT_ID => 500,
    ];
  }
}
