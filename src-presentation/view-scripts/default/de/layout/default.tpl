<!DOCTYPE html>
<html class="default_de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" href="/ui/style/default_style.css?cb=12345" />
    {block name="styles"}{/block}
    <title>{$title}</title>
</head>
<body class="{$bodyClass|default:''}">
{fetch file='ui/assets/svg-icons.svg' assign='common_svg'}
{$common_svg|replace:'__svg-sprite__':'svg-sprite'}
{block name=main_body}{/block}

{block name="scripts"}{/block}
</body>
</html>
