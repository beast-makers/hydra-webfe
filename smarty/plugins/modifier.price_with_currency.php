<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     modifier.price_with_currency.php
 * Type:     modifier
 * Name:     price_with_currency
 * Purpose:  format price
 * -------------------------------------------------------------
 */
function smarty_modifier_price_with_currency(string $intPrice, string $currencySymbol)
{
  return price_with_currency($intPrice, $currencySymbol);
}
