const Universe = {};

/**
 * @param {Object[]} target
 * @param {Object} properties
 * @param {string} properties.key
 * @param {int} properties.priority
 * @param {Function} properties.callback
 */
function subscribeTo (target, properties) {
  const {key, priority, callback} = properties;

  if (!target[key]) {
    target[key] = [];
  }

  if (!target[key][priority]) {
    target[key][priority] = [];
  }

  target[key][priority].push(callback);
}

const state = {};
const stateListeners = [];
Universe.State = {
  /**
   * @param {string} key
   * @param {*} value
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */
  set (key, value, notificationData = {}) {
    state[key] = value;
    this.notify(key, value, notificationData);
  },

  /**
   * @param {string} key
   * @param {*} fallback
   *
   * @return {*}
   */
  get (key, fallback=undefined) {
    if (!state.hasOwnProperty(key)) {
      return fallback;
    }

    return state[key];
  },

  /**
   * @callback stateSubscribeCallback
   * @param {string} key
   * @param {*} value
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */
  /**
   * @param {string} key
   * @param {stateSubscribeCallback} callback
   * @param {int} [priority=500]
   *
   * @return {void}
   */
  subscribe (key, callback, priority = 500) {
    subscribeTo(stateListeners, {key, priority, callback});
  },

  /**
   * @param {string} key
   * @param {*} value
   * @param {*} notificationData
   *
   * @return {void}
   */
  notify (key, value, notificationData = {}) {
    if (stateListeners[key]) {
      const priorities = Object.keys(stateListeners[key]);

      for (const priorityKey in priorities) {
        const priority = priorities[priorityKey];

        for (const index in stateListeners[key][priority]) {
          const callback = stateListeners[key][priority][index];

          callback(key, value, notificationData);
        }
      }
    }
  }
};

const eventListeners = [];
Universe.Event = {
  /**
   * @callback eventSubscribeCallback
   * @param {string} key
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */
  /**
   * @param {string} key
   * @param {eventSubscribeCallback} callback
   * @param {int} [priority=500]
   *
   * @return {void}
   */
  subscribe (key, callback, priority = 500) {
    subscribeTo(eventListeners, {key, priority, callback});
  },

  /**
   * @param {string} key
   * @param {*} [notificationData={}]
   *
   * @return {Array}
   */
  notify (key, notificationData = {}) {
    let responses = [];

    if (eventListeners[key]) {
      const priorities = Object.keys(eventListeners[key]);

      for (const priorityKey in priorities) {
        const priority = priorities[priorityKey];

        for (const index in eventListeners[key][priority]) {
          const callback = eventListeners[key][priority][index];

          responses.push(callback(key, notificationData));
        }
      }
    }

    return responses;
  }
};

export default Universe;
