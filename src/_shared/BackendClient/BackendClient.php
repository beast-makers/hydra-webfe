<?php
declare(strict_types=1);

namespace BeastMakers\Shared\BackendClient;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class BackendClient
{
  private const CONNECTION_ATTEMPT_LIMIT = 3;

  private const WAIT_TIME = 200;

  private ClientInterface $httpClient;

  private Config $authConfig;

  private string $authToken = '';

  private array $requestOptions = [
    'http_errors' => false,
  ];

  /**
   * @param ClientInterface $httpClient
   * @param Config $authConfig
   */
  public function __construct(ClientInterface $httpClient, Config $authConfig)
  {
    $this->httpClient = $httpClient;
    $this->authConfig = $authConfig;
  }

  /**
   * @param string $uri
   * @param array $data
   *
   * @return ResponseInterface
   * @throws GuzzleException
   * @throws MissingConfigOptionException
   */
  public function postJson(string $uri, array $data): ResponseInterface
  {
    return $this->requestLoop('post', $uri, [
      'json' => $data,
    ]);
  }

  /**
   * @param string $method
   * @param string $uri
   * @param array $options
   *
   * @return ResponseInterface
   * @throws MissingConfigOptionException
   * @throws GuzzleException
   * @psalm-suppress InvalidReturnType
   */
  private function requestLoop(string $method, string $uri, array $options = []): ResponseInterface
  {
    $attemptLimit = self::CONNECTION_ATTEMPT_LIMIT;

    while ($attemptLimit > 0) {
      try {
        --$attemptLimit;

        return $this->request($method, $uri, $options);
      } catch (ConnectException $e) {
        if ($attemptLimit < 1) {
          throw $e;
        }

        usleep(self::WAIT_TIME);
      }
    }
  }

  /**
   * @param string $method
   * @param string $uri
   * @param array $options
   *
   * @return ResponseInterface
   * @throws MissingConfigOptionException
   * @throws GuzzleException
   */
  private function request(string $method, string $uri, array $options = []): ResponseInterface
  {
    $this->authenticate();

    return $this->httpClient->request($method, $uri, array_merge($this->requestOptions, $options));
  }

  /**
   * @return void
   * @throws MissingConfigOptionException
   */
  private function authenticate(): void
  {
    if (!empty($this->authToken)) {
      return;
    }

    /** @var ResponseInterface $response */
    /** @psalm-suppress UndefinedInterfaceMethod */
    $response = $this->httpClient->post('/api-auth/login', [
      'json' => [
        'username' => $this->authConfig->getString('username'),
        'password' => $this->authConfig->getString('password'),
      ],
    ]);

    if ($response->getStatusCode() === 200) {
      $this->handleAuthenticationOK($response);
      return;
    }
  }

  /**
   * @param ResponseInterface $response
   *
   * @return void
   */
  private function handleAuthenticationOK(ResponseInterface $response): void
  {
    $contents = $response->getBody()->getContents();
    $data = json_decode($contents, true);

    if (isset($data['token'])) {
      $token = $data['token'];
      $this->authToken = $token;
      $this->requestOptions = $this->addAuthenticationHeader($this->requestOptions, $token);
    }
  }

  /**
   * @param array $requestOptions
   * @param string $token
   *
   * @return array
   */
  private function addAuthenticationHeader(array $requestOptions, string $token): array
  {
    return array_merge_recursive($requestOptions, [
      'headers' => [
        'Authorization' => "Bearer {$token}",
      ],
    ]);
  }
}
