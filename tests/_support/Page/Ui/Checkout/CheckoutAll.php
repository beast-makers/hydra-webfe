<?php
declare(strict_types=1);

namespace BeastMakersTest\Page\Ui\Checkout;

use BeastMakersTest\UiTester;

class CheckoutAll
{
  public const LOADING_ICON_SELECTOR = '.qa-loading-icon';

  /**
   * @var UiTester;
   */
  protected $uiTester;

  /**
   * @var DeliveryPage
   */
  public $deliveryPage;

  /**
   * CheckoutCommon constructor.
   * @param UiTester $i
   * @param DeliveryPage $deliveryPage
   */
  public function __construct(
    UiTester $i,
    DeliveryPage $deliveryPage
  ) {
    $this->uiTester = $i;
    $this->deliveryPage = $deliveryPage;
  }
}
