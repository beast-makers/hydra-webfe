<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Result;

interface CommonErrorCodes
{
  public const INVALID_INPUT_DATA = 'INVALID_INPUT_DATA';
  public const INVALID_JSON_FORMAT = 'INVALID_JSON_FORMAT';
}
