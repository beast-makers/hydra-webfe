{extends file='layout/checkout.tpl'}

{block name=styles}
    <link rel="stylesheet" href="/ui/style/checkout_style.css" />
{/block}

{block name="scripts"}
    <script src="/ui/js/checkout_script.js"></script>
{/block}

{block name=header}
    Checkout Kit
{/block}

{block name=main_body}
    <div class="container-fluid js-checkout-container"></div>
    <script id="js-checkout-config" type="application/json">
        {$checkout_config|@json_encode}
    </script>
{/block}

{block name=footer}
    Checkout Footer
{/block}

