<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra\Repository;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;

interface WriteRepository
{
  /**
   * @param DeliveryData $deliveryData
   * @param string $checkoutId
   *
   * @return void
   */
  public function pushDeliveryData(DeliveryData $deliveryData, string $checkoutId): void;
}
