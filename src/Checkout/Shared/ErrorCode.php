<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Shared;

use BeastMakers\Shared\Result\CommonErrorCodes;

interface ErrorCode extends CommonErrorCodes
{
  public const MISSING_CHECKOUT_ID = 'MISSING_CHECKOUT_ID';
  public const DELIVERY_DATA_VALIDATION = 'DELIVER_DATA_VALIDATION';
}
