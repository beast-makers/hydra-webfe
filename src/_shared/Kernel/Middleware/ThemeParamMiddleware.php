<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel\Middleware;

use BeastMakers\Shared\Config\ConfigSetter;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ThemeParamMiddleware
{
  private const PARAM_THEME = 'theme';

  /**
   * @param ServerRequestInterface $request
   * @param RequestHandlerInterface $handler
   *
   * @return ResponseInterface
   * @throws MissingConfigOptionException
   */
  public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
  {
    $params = $request->getQueryParams();
    $theme = $params[self::PARAM_THEME] ?? '';

    if (!empty($theme)) {
      ConfigSetter::set('smarty_current_theme', $theme);
    }

    return $handler->handle($request);
  }
}
