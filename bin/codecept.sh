#!/usr/bin/env bash
set -euxo pipefail

exec vendor/bin/codecept run -c tests/codeception.yml

### command
# APP_STORE=DE HYDRA_WEBFE_SERVER_NAME_DE=http://http://customer-webportal-dev.de.local bin/codecept.sh
