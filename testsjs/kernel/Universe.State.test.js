import Universe from '../../src-presentation/js/kernel/Universe';

test('set/get assigned value', function () {
  Universe.State.set('test1', 69);

  expect(Universe.State.get('test1')).toBe(69);
});

test('get missing value', function () {
  expect(Universe.State.get('missing')).toBe(undefined);
});

test('call subscriber', function () {
  const stateKey = 'subscriber-key';
  const expectedValue = 'myvalue';

  const mockCallback = jest.fn(function(key, value, notificationData) {
    expect(key).toBe(stateKey);
    expect(value).toBe(expectedValue);
    expect(notificationData.foo).toBe('bar');
  });

  Universe.State.subscribe(stateKey, mockCallback);
  Universe.State.set(stateKey, expectedValue, {foo: 'bar'});

  expect(mockCallback.mock.calls.length).toBe(1);
});

test('call subscriber with priorities', function () {
  const stateKey = 'subscriber-key-with-priority';
  const expectedValue = 'prioritized-value';
  let priorityResult = '';

  const mockCallbackA = jest.fn(function(key, value, notificationData) {
    expect(key).toBe(stateKey);
    expect(value).toBe(expectedValue);
    expect(notificationData.foo).toBe('bar');

    priorityResult += 'A';
  });

  const mockCallbackB = jest.fn(function(key, value, notificationData) {
    expect(key).toBe(stateKey);
    expect(value).toBe(expectedValue);
    expect(notificationData.foo).toBe('bar');

    priorityResult += 'B';
  });

  const mockCallbackC = jest.fn(function(key, value, notificationData) {
    expect(key).toBe(stateKey);
    expect(value).toBe(expectedValue);
    expect(notificationData.foo).toBe('bar');

    priorityResult += 'C';
  });

  Universe.State.subscribe(stateKey, mockCallbackA, 10);
  Universe.State.subscribe(stateKey, mockCallbackB, 5);
  Universe.State.subscribe(stateKey, mockCallbackC, 7);

  Universe.State.set(stateKey, expectedValue, {foo: 'bar'});

  expect(mockCallbackA.mock.calls.length).toBe(1);
  expect(mockCallbackB.mock.calls.length).toBe(1);
  expect(mockCallbackC.mock.calls.length).toBe(1);
  expect(priorityResult).toBe('BCA');
});
