<div class="form-field js-form-field">
    {if !empty($label)}
        <label class="label" for="{$id}">{$label}</label>
    {/if}
    <div class="form-field-control">
        <input class="input" id="{$id}" name="{$id}" value="{$value|default:''}" placeholder="{$placeholder|default:''}"/>
        <div class="input-message js-input-message qa-input-message"></div>
    </div>
</div>
