const OVERLAY_IS_HIDDEN = 'hidden';
const OVERLAY_IS_VISIBLE = 'visible';
const OVERLAY_NOT_FOUND = 'not_found';

/**
 * @return {Element}
 */
export function showOverlay () {
  const overlayElement = findVisibleOverlay();
  if (overlayElement) {
    return overlayElement;
  }

  return createAndShowOverlay();
}

/**
 * @param {Element} contentElement
 */
export function replaceOverlayContentElement (contentElement) {
  const overlayElement = findOverlayElement();
  overlayElement.innerHTML = '';
  overlayElement.insertAdjacentElement('afterbegin', contentElement);
}

/**
 * @return {void}
 */
export function hideOverlay () {
  const overlayElement = findOverlayElement();

  if (overlayElement) {
    overlayElement.classList.remove('c-own-overlay__is-visible');
  }
}

/**
 * @return {Element}
 */
function createAndShowOverlay () {
  removeHiddenOverlay();
  const element = document.createElement('div');
  element.innerHTML = '<div class="c-own-overlay js-own-overlay"></div>';
  const overlayElement = element.firstElementChild;
  document.body.insertAdjacentElement('afterbegin', overlayElement);
  overlayElement.classList.add('c-own-overlay__is-visible');

  return overlayElement;
}

/**
 * @return {void}
 */
function removeHiddenOverlay () {
  const overlayElement = findHiddenOverlay();

  if (overlayElement) {
    overlayElement.remove();
  }
}

/**
 * @param {Element|null} overlayElement
 *
 * @return {boolean}
 */
function isOverlayHidden (overlayElement) {
  return discoverOverlayVisibility(overlayElement) === OVERLAY_IS_HIDDEN;
}

/**
 * @param {Element|null} overlayElement
 *
 * @return {boolean}
 */
function isOverlayVisible (overlayElement) {
  return discoverOverlayVisibility(overlayElement) === OVERLAY_IS_VISIBLE;
}

/**
 * @param {Element|null} overlayElement
 *
 * @return {string}
 */
function discoverOverlayVisibility (overlayElement) {
  if (!overlayElement) {
    return OVERLAY_NOT_FOUND;
  }

  if (overlayElement.classList.contains('c-own-overlay__is-visible')) {
    return OVERLAY_IS_VISIBLE;
  }

  return OVERLAY_IS_HIDDEN;
}

/**
 * @return {Element|null}
 */
function findHiddenOverlay () {
  const overlayElement = findOverlayElement();

  if (isOverlayHidden(overlayElement)) {
    return overlayElement;
  }

  return null;
}

/**
 * @return {Element|null}
 */
function findVisibleOverlay () {
  const overlayElement = findOverlayElement();

  if (isOverlayVisible(overlayElement)) {
    return overlayElement;
  }

  return null;
}

/**
 * @return {Element}
 */
function findOverlayElement () {
  return document.querySelector('.js-own-overlay');
}

