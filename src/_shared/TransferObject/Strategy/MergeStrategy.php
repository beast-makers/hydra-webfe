<?php
declare(strict_types=1);

namespace BeastMakers\Shared\TransferObject\Strategy;

use BeastMakers\Shared\TransferObject\ArrayDto;

class MergeStrategy
{
  /**
   * @param mixed $value
   *
   * @return mixed
   */
  public static function scalar($value)
  {
    return $value;
  }

  /**
   * @param array $target
   * @param array $source
   *
   * @return array
   */
  public static function array(array $target, array $source): array
  {
    return array_merge($target, $source);
  }

  /**
   * @param array $target
   * @param array $source
   *
   * @return array
   */
  public static function arrayUnique(array $target, array $source): array
  {
    return array_unique(self::array($target, $source));
  }

  /**
   * @param array $target
   * @param $childType
   * @param array $source
   *
   * @return array
   */
  public static function assocArrayWithDto(array $target, $childType, array $source): array
  {
    $result = $target;
    foreach ($target as $key => $dtoProperties) {
      /** @var ArrayDto $resultDto */
      $resultDto = new $childType();
      $resultDto->applyArray($dtoProperties);
      $result[$key] = $resultDto;
    }

    foreach ($source as $key => $dtoProperties) {
      $resultDto = array_key_exists($key, $result) ? $result[$key] : new $childType();

      /** @var ArrayDto $resultDto */
      $resultDto->applyArray($dtoProperties);
      $result[$key] = $resultDto;
    }

    return $result;
  }

  /**
   * @param $target
   * @param $childType
   * @param array $source
   *
   * @return ArrayDto
   */
  public static function dto(array $target, $childType, array $source): ArrayDto
  {
    /** @var ArrayDto $result */
    $result = new $childType();
    $result->applyArray($target);
    $result->applyArray($source);

    return $result;
  }
}
