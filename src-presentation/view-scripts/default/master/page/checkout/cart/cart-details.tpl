{foreach $cart_details->getCartItemLines() as $cart_item_line}
    {include "component/cart/cart-item-line.tpl"
        product_image = $cart_item_line->getItemImageUrl()
        headline = $cart_item_line->getHeadline()
        subheadline = $cart_item_line->getSubheadline()
        description = $cart_item_line->getDescription()
        price = $cart_item_line->getPrice()|price_with_currency:$cart_item_line->getCurrencySymbol()
    }
{/foreach}
<div class="cart-summary">
    <div class="row justify-content-center">
        <div class="col-xs-12 col-sm-6">
            <div class="cart-summary-line">
                <div class="term">Versand</div>
                <div class="value">{$cart_details->getShippingPrice()|price_coalesce_with_currency:$cart_details->getCurrencySymbol():'kostenlos'}</div>
            </div>
            <div class="cart-summary-line">
                <div class="term">Ingesamt</div>
                <div class="value">{$cart_details->getPriceToPay()|price_with_currency:$cart_details->getCurrencySymbol()}</div>
            </div>
        </div>
    </div>
</div>

<div class="cart-continue-line">
    <button class="button js-accept-cart qa-accept-cart">Continue</button>
</div>
