<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Infra;

use BeastMakers\Shared\TransferObject\ArrayDto;

class CartData extends ArrayDto
{
  public const ATTR_PRODUCTS = 'products';
  public const ATTR_VOUCHER = 'voucher';

  /**
   * @return array
   */
  public function getProducts(): array
  {
    return $this->get(self::ATTR_PRODUCTS, []);
  }

  /**
   * @param array $products
   *
   * @return $this
   */
  public function setProducts(array $products): self
  {
    $this->set(self::ATTR_PRODUCTS, $products);

    return $this;
  }

  /**
   * @return array
   */
  public function getVoucher(): array
  {
    return $this->get(self::ATTR_VOUCHER, []);
  }

  /**
   * @param array $voucher
   *
   * @return $this
   */
  public function setVoucher(array $voucher): self
  {
    $this->set(self::ATTR_VOUCHER, $voucher);

    return $this;
  }

  /**
   * @return array
   */
  public function definition(): array
  {
    return [
      self::ATTR_PRODUCTS => self::DEF_ARRAY,
      self::ATTR_VOUCHER => self::DEF_SCALAR,
    ];
  }
}
