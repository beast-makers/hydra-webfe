import Universe from '../../kernel/Universe';
import EventKey from './EventKey';
import {showLoadingScreen, hideLoadingScreen} from './Screen/Loading';
import {bindEvents, showNotificationModal} from './NotificationModal';

export function startModal ()
{
  bindEvents();
  registerModalListeners();
}

function registerModalListeners () {
  Universe.Event.subscribe(EventKey.OPEN_LOADING_SCREEN, function () {
    showLoadingScreen();
  });

  Universe.Event.subscribe(EventKey.CLOSE_LOADING_SCREEN, function () {
    hideLoadingScreen();
  });

  Universe.Event.subscribe(EventKey.OPEN_ERROR_NOTIFICATION_MODAL, function (key, data ) {
    showNotificationModal('Error Message', data.message);
  });
}
