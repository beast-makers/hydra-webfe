import Universe from './Universe';
import AppEventKey from './EventKey';

/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */

/**
 * @param {String} message
 */
export function sayApplicationError(message) {
  Universe.Event.notify(AppEventKey.RUNTIME_APPLICATION_ERROR, {'message': message});
}

/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */
export function onApplicationError(callback) {
  Universe.Event.subscribe(AppEventKey.RUNTIME_APPLICATION_ERROR, callback);
}

