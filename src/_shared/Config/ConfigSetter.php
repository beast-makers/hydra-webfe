<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Config;

use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;

class ConfigSetter
{
  /**
   * @param string $path
   * @param $value
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  public static function set(string $path, $value): void
  {
    $diContainer = DependencyContainer::getInstance();
    /** @var Config $config */
    $config = $diContainer->get(DependencyContainerItem::APPLICATION_CONFIG);
    $config->set($path, $value);
  }
}
