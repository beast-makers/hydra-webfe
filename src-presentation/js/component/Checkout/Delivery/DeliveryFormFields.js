import {defaultOptions} from '../../Form/Form';

const internationalNameRegExp = new RegExp(/[^0-9_!¡÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]*/);

export default {
  first_name: {
    inputSelector: '#first_name.input',
    constraints: {
      presence: {allowEmpty: false},
      format: {
        pattern: internationalNameRegExp
      }
    },
    ...defaultOptions
  },
  last_name: {
    inputSelector: '#last_name.input',
    constraints: {
      presence: {allowEmpty: false},
      format: {
        pattern: internationalNameRegExp
      }
    },
    ...defaultOptions
  },

  email: {
    inputSelector: '#email.input',
    constraints: {
      presence: {allowEmpty: false},
      email: true
    },
    ...defaultOptions
  },

  birth_date: {
    inputSelector: '#birth_date.input',
    constraints: {
      presence: {allowEmpty: false}
    },
    ...defaultOptions
  },

  country_code: {
    inputSelector: '#country_code.select',
    constraints: {
      presence: {allowEmpty: false}
    },
    ...defaultOptions
  },

  street: {
    inputSelector: '#street.input',
    constraints: {
      presence: {allowEmpty: false}
    },
    ...defaultOptions
  },

  city: {
    inputSelector: '#city.input',
    constraints: {
      presence: {allowEmpty: false},
      format: {
        pattern: internationalNameRegExp
      }
    },
    ...defaultOptions
  },

  zip: {
    inputSelector: '#zip.input',
    containerSelector: '.js-form-field',
    messageElementSelector: '.js-input-message',
    constraints: {
      presence: {allowEmpty: false},
      format: {
        pattern: new RegExp(/^[0-9\-]*$/)
      }
    }
  }
};
