<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\SessionStorage\PHPSession;
use BeastMakers\Shared\SessionStorage\SessionManager;

trait PHPSessionManagerLoader
{
  /**
   * @return SessionManager
   */
  protected function sharePHPSessionManager(): SessionManager
  {
    $dc = DependencyContainer::getInstance();
    $dcKey = SessionManager::class;

    if (!$dc->contains($dcKey)) {
      $phpSessionManager = new PHPSession();
      $dc->set($dcKey, static function () use ($phpSessionManager) {
        return $phpSessionManager;
      });
    }

    return $dc->get($dcKey);
  }
}
