<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Shared;

interface SessionKey
{
  public const CHECKOUT_ID = 'checkout_id';
}
