<div class="checkout-payment-credit_card">
    <form method="get" action="">
        {include file="component/form/input-text.tpl"
            id="account_owner"
            label="Account Owner"
            placeholder="Konrad Gawlinski"
        }
        {include file="component/form/input-text.tpl"
            id="credit_card_number"
            label="Credit Card Number"
            placeholder="4111 1111 1111 1111"
        }
        <div class="text-center">
            <button type="button" class="button js-accept-credit-card">Continue</button>
        </div>
    </form>
</div>
