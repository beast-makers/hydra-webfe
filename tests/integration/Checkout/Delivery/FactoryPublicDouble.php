<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\Checkout\Delivery;

use BeastMakers\Checkout\Delivery\Config;
use BeastMakers\Checkout\Delivery\DependencyProvider;
use BeastMakers\Checkout\Delivery\Factory;
use BeastMakers\Checkout\Delivery\Infra\Repository\ReadRepository;
use BeastMakers\Checkout\Delivery\Infra\Repository\WriteRepository;

class FactoryPublicDouble extends Factory
{
  public function __construct()
  {
    parent::__construct(new DependencyProvider(), new Config());
  }

  /**
   * @return ReadRepository
   */
  public function shareDeliveryReadRepository(): ReadRepository
  {
    return parent::shareDeliveryReadRepository();
  }

  /**
   * @return WriteRepository
   */
  public function shareDeliveryWriteRepository(): WriteRepository
  {
    return parent::shareDeliveryWriteRepository();
  }
}
