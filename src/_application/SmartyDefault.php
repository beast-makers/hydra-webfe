<?php
declare(strict_types=1);

namespace BeastMakers\Application;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;

class SmartyDefault extends \Smarty
{
  /** @psalm-suppress UndefinedConstant */
  private const PROJECT_DIR = PROJECT_DIR;
  /** @psalm-suppress UndefinedConstant */
  private const MAIN_DIR = PROJECT_DIR . 'smarty/';

  /** @var array */
  protected $template_dir = [];

  /**
   * @param Config $config
   *
   * @throws MissingConfigOptionException
   */
  public function __construct(Config $config)
  {
    parent::__construct();

    $this->setupSmarty();
    $this->setupTemplateDirs($config);
    $this->setupPluginsDirs($config);
  }

  private function setupSmarty(): void
  {
    $this->error_reporting = error_reporting() & ~E_NOTICE;

    $this->setCacheDir(static::MAIN_DIR . 'cache');
    $this->setCompileDir(static::MAIN_DIR . 'templates_c');
    $this->setConfigDir(static::MAIN_DIR . 'config');
  }

  /**
   * @param Config $config
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  private function setupTemplateDirs(Config $config): void
  {
    /**
     * @var string $templatesDir
     * @var string $defaultTheme
     * @var string $currentTheme
     * @var string $currentStoreDirName
     */
    extract($this->getConfigVars($config));

    if ($defaultTheme !== $currentTheme) {
      $this->addTemplateDir("{$templatesDir}{$currentTheme}/{$currentStoreDirName}");
      $this->addTemplateDir("{$templatesDir}{$currentTheme}/master");
    }

    $this->addTemplateDir("{$templatesDir}{$defaultTheme}/{$currentStoreDirName}");
    $this->addTemplateDir("{$templatesDir}{$defaultTheme}/master");
  }

  /**
   * @param Config $config
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  private function setupPluginsDirs(Config $config): void
  {
    /**
     * @var string $templatesDir
     * @var string $defaultTheme
     * @var string $currentTheme
     * @var string $currentStoreDirName
     */
    extract($this->getConfigVars($config));

    if ($defaultTheme !== $currentTheme) {
      $this->addPluginsDir("{$templatesDir}{$currentTheme}/{$currentStoreDirName}/plugins");
      $this->addPluginsDir("{$templatesDir}{$currentTheme}/master/plugins");
    }

    $this->addPluginsDir("{$templatesDir}{$defaultTheme}/{$currentStoreDirName}/plugins");
    $this->addPluginsDir("{$templatesDir}{$defaultTheme}/master/plugins");
    $this->addPluginsDir(static::MAIN_DIR . 'plugins');
  }

  /**
   * @param Config $config
   * @return string[]
   * @throws MissingConfigOptionException
   */
  private function getConfigVars(Config $config): array
  {
    return [
      'templatesDir' => static::PROJECT_DIR . $config->getString('smarty_templates_dir'),
      'defaultTheme' => $config->getString('smarty_default_theme'),
      'currentTheme' => $config->getString('smarty_current_theme'),
      'currentStoreDirName' => strtolower($config->getString('current_store')),
    ];
  }
}
