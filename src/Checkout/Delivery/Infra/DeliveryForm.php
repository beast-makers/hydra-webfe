<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra;

use BeastMakers\Checkout\Shared\Domain\Address;
use BeastMakers\Checkout\Shared\Domain\Customer;
use BeastMakers\Shared\Form\Form;
use BeastMakers\Shared\Form\FormField;
use BeastMakers\Shared\Form\FormFieldSingleMultiValue;
use BeastMakers\Shared\Form\FormFieldSingleValue;

class DeliveryForm extends Form
{
  public const FIELD_FIRST_NAME = 'first_name';
  public const FIELD_LAST_NAME = 'last_name';
  public const FIELD_EMAIL = 'email';
  public const FIELD_BIRTH_DATE = 'birth_date';

  public const FIELD_COUNTRY_CODE = 'country_code';
  public const FIELD_STREET = 'street';
  public const FIELD_ADDRESS_EXTENSION = 'address_extension';
  public const FIELD_CITY = 'city';
  public const FIELD_ZIP = 'zip';

  /**
   * @param DeliveryData $deliveryData
   *
   * @return void
   */
  public function initialize(DeliveryData $deliveryData): void
  {
    $addressDto = $deliveryData->getAddress();
    $customerDto = $deliveryData->getCustomer();

    $this->addField(static::FIELD_FIRST_NAME, $this->createFirstNameField($customerDto));
    $this->addField(static::FIELD_LAST_NAME, $this->createLastNameField($customerDto));
    $this->addField(static::FIELD_EMAIL, $this->createEmailField($customerDto));
    $this->addField(static::FIELD_BIRTH_DATE, $this->createBirthDateField($customerDto));

    $this->addField(static::FIELD_COUNTRY_CODE, $this->createCountryCodeField($addressDto));
    $this->addField(static::FIELD_STREET, $this->createStreetField($addressDto));
    $this->addField(static::FIELD_ADDRESS_EXTENSION, $this->createAddressExtensionField($addressDto));
    $this->addField(static::FIELD_CITY, $this->createCityField($addressDto));
    $this->addField(static::FIELD_ZIP, $this->createZipField($addressDto));
  }

  /**
   * @param Customer $customerDto
   *
   * @return FormFieldSingleValue
   */
  private function createFirstNameField(Customer $customerDto): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_FIRST_NAME,
      FormField::ATTR_LABEL => CustomerLabel::FIRST_NAME,
      FormField::ATTR_PLACEHOLDER => 'John',
      FormField::ATTR_VALUE => $customerDto->getFirstName(),
    ]);

    return $formField;
  }

  /**
   * @param Customer $customerDto
   *
   * @return FormFieldSingleValue
   */
  private function createLastNameField(Customer $customerDto): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_LAST_NAME,
      FormField::ATTR_LABEL => CustomerLabel::LAST_NAME,
      FormField::ATTR_PLACEHOLDER => 'Smith',
      FormField::ATTR_VALUE => $customerDto->getLastName(),
    ]);

    return $formField;
  }

  /**
   * @param Customer $customerDto
   *
   * @return FormFieldSingleValue
   */
  private function createEmailField(Customer $customerDto): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_EMAIL,
      FormField::ATTR_LABEL => CustomerLabel::EMAIL,
      FormField::ATTR_PLACEHOLDER => 'john.smith@domain.com',
      FormField::ATTR_VALUE => $customerDto->getEmail(),
    ]);

    return $formField;
  }

  /**
   * @param Customer $customerDto
   *
   * @return FormFieldSingleValue
   */
  private function createBirthDateField(Customer $customerDto): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_BIRTH_DATE,
      FormField::ATTR_LABEL => CustomerLabel::BIRTH_DATE,
      FormField::ATTR_PLACEHOLDER => '13-10-1984',
      FormField::ATTR_VALUE => $customerDto->getBirthDate(),
    ]);

    return $formField;
  }

  /**
   * @param Address $addressDto
   *
   * @return FormFieldSingleMultiValue
   */
  private function createCountryCodeField(Address $addressDto): FormFieldSingleMultiValue
  {
    $formField = new FormFieldSingleMultiValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_COUNTRY_CODE,
      FormField::ATTR_LABEL => AddressLabel::COUNTRY,
      FormField::ATTR_PLACEHOLDER => 'Country',
      FormFieldSingleMultiValue::ATTR_ALL_VALUES => [
        'de' => 'Germany',
        'pl' => 'Poland',
        'gb' => 'United Kingdom',
      ],
      FormField::ATTR_VALUE => $addressDto->getCountryCode(),
    ]);

    return $formField;
  }

  /**
   * @param Address $addressDto
   *
   * @return FormFieldSingleValue
   */
  private function createStreetField(Address $addressDto): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_STREET,
      FormField::ATTR_LABEL => AddressLabel::STREET,
      FormField::ATTR_PLACEHOLDER => 'Chocolate 12',
      FormField::ATTR_VALUE => $addressDto->getStreet(),
    ]);

    return $formField;
  }

  /**
   * @param Address $addressDto
   *
   * @return FormFieldSingleValue
   */
  private function createAddressExtensionField(Address $addressDto): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_ADDRESS_EXTENSION,
      FormField::ATTR_LABEL => AddressLabel::ADDRESS_EXTENSION,
      FormField::ATTR_PLACEHOLDER => 'apartment 12',
      FormField::ATTR_VALUE => $addressDto->getAddressExtension(),
    ]);

    return $formField;
  }

  /**
   * @param Address $addressDto
   *
   * @return FormFieldSingleValue
   */
  private function createCityField(Address $addressDto): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_CITY,
      FormField::ATTR_LABEL => AddressLabel::CITY,
      FormField::ATTR_PLACEHOLDER => 'London',
      FormField::ATTR_VALUE => $addressDto->getCity(),
    ]);

    return $formField;
  }

  /**
   * @param Address $addressDto
   *
   * @return FormFieldSingleValue
   */
  private function createZipField(Address $addressDto): FormFieldSingleValue
  {
    $formField = new FormFieldSingleValue();
    $formField->fromArray([
      FormField::ATTR_ID => static::FIELD_ZIP,
      FormField::ATTR_LABEL => AddressLabel::ZIP,
      FormField::ATTR_PLACEHOLDER => '54312',
      FormField::ATTR_VALUE => $addressDto->getZip(),
    ]);

    return $formField;
  }
}
