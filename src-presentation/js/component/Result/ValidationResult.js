import Result from './Result';

export class ValidationResult extends Result {
  constructor () {
    super(true);
  }

  /**
   * @return {boolean}
   */
  get isValid () {
    return this.isSuccess;
  }
}
