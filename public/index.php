<?php
session_start();

//This is thrown by default if no other response code is set
header('HTTP/1.1 500 Internal Server Error');

$app = require __DIR__ . '/../src/_application/bootstrap.php';
$app->run();
