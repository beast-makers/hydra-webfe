<?php
declare(strict_types=1);

namespace BeastMakers\Shared\RedisConnector;

class ClientPool
{
  /**
   * @var RedisClient[]
   */
  protected array $pool = [];

  /**
   * @param RedisClient $redisClient
   * @param string $key
   *
   * @return void
   */
  public function register(RedisClient $redisClient, string $key): void
  {
    $this->pool[$key] = $redisClient;
  }

  /**
   * @param string $key
   *
   * @return RedisClient
   */
  public function get(string $key): RedisClient
  {
    return $this->pool[$key];
  }
}
