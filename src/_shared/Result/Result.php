<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Result;

class Result
{
  private bool $isSuccess = false;

  private ErrorCollection $errorCollection;

  public function __construct()
  {
    $this->errorCollection = new ErrorCollection();
  }

  /**
   * @return bool
   */
  public function getIsSuccess(): bool
  {
    return $this->isSuccess;
  }

  /**
   * @param bool $flag
   *
   * @return void
   */
  public function setIsSuccess(bool $flag): void
  {
    $this->isSuccess = $flag;
  }

  /**
   * @param Error $error
   *
   * @return void
   */
  public function addError(Error $error): void
  {
    $this->errorCollection->addError($error);
  }

  /**
   * @return ErrorCollection
   */
  public function getErrorCollection(): ErrorCollection
  {
    return $this->errorCollection;
  }

  /**
   * @param Result $result
   *
   * @return void
   */
  public function mergeWith(self $result): void
  {
    $this->errorCollection->merge($result->getErrorCollection());
  }

  /**
   * @param ErrorCollection $collection
   *
   * @return void
   */
  public function mergeWithCollection(ErrorCollection $collection): void
  {
    $this->errorCollection->merge($collection);
  }
}
