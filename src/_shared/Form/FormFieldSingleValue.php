<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Form;

class FormFieldSingleValue extends FormField
{
  /**
   * @return string
   */
  public function getValue(): string
  {
    return $this->attributes[static::ATTR_VALUE];
  }
}
