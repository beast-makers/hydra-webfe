import MessageSet from './MessageSet';

export default class Result {
  constructor (isSuccess = false) {
    this.isSuccess = isSuccess;
    this.messageSet = new MessageSet();
  }

  /**
   * @return {boolean}
   */
  getIsSuccess () {
    return this.isSuccess;
  }

  /**
   * @param {boolean} flag
   *
   * @return {void}
   */
  setIsSuccess (flag) {
    this.isSuccess = flag;
  }

  /**
   * @param {MessageCollection} messageCollection
   *
   * @return {void}
   */
  addMessageCollection (messageCollection) {
    this.messageSet.addMessageCollection(messageCollection);
  }

  /**
   * @return {MessageSet}
   */
  getMessageSet () {
    return this.messageSet;
  }
}
