<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel\Action;

use BeastMakers\Shared\Kernel\BaseConfig;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\Kernel\FactoryRegistry;

class BaseAction
{
  protected BaseFactory $factory;

  protected BaseConfig $config;

  public function __construct()
  {
    $factory = $this->resolveModuleFactory();

    $this->setFactory($factory);
    $this->setConfig($factory->getConfig());
  }

  /**
   * @param BaseFactory $factory
   *
   * @return void
   */
  public function setFactory(BaseFactory $factory): void
  {
    $this->factory = $factory;
  }

  /**
   * @param BaseConfig $config
   *
   * @return void
   */
  public function setConfig(BaseConfig $config): void
  {
    $this->config = $config;
  }

  /**
   * @return BaseFactory
   */
  protected function resolveModuleFactory(): BaseFactory
  {
    return FactoryRegistry::getFactoryInstance(static::class);
  }
}
