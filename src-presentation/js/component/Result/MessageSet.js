import MessageCollection from './MessageCollection';

export default class MessageSet {
  constructor () {
    this.isEmpty = true;
    this.messages = {};
  }

  /**
   * @return {boolean}
   */
  hasMessages () {
    return !this.isEmpty;
  }

  /**
   * @param {MessageCollection} messageCollection
   *
   * @return {void}
   */
  addMessageCollection (messageCollection) {
    this.isEmpty = false;
    this.messages[messageCollection.code] = messageCollection;
  }

  /**
   * @return {MessageCollection[]}
   */
  getAll () {
    return this.messages;
  }

  /**
   * @return {MessageCollection}
   */
  getOnlyChild () {
    if (this.hasMessages()) {
      // noinspection LoopStatementThatDoesntLoopJS
      for (const code in this.messages) {
        return this.messages[code];
      }
    }

    return new MessageCollection();
  }

  /**
   * @param {string} code
   *
   * @return {MessageCollection}
   */
  getByCode (code) {
    return this.messages[code];
  }

  /**
   * @param {MessageSet} messageSet
   *
   * @return {this}
   */
  merge (messageSet) {
    this.messages = this.messages.concat(messageSet.getAll());

    return this;
  }
}
