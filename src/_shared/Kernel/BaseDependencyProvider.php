<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

class BaseDependencyProvider
{
  /**
   * @var DependencyContainer
   */
  protected $di;

  public function __construct()
  {
    $this->di = DependencyContainer::getInstance();
    $this->selfRegister();
  }

  /**
   * @return void
   */
  private function selfRegister(): void
  {
    $this->di->set(static::class, function (): self {
      return $this;
    });
  }
}
