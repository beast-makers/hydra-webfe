<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra;

use BeastMakers\Checkout\Shared\Domain\Address;
use BeastMakers\Checkout\Shared\Domain\Customer;

class DeliveryData
{
  private Address $address;

  private Customer $customer;

  /**
   * @return Address
   */
  public function getAddress(): Address
  {
    return $this->address;
  }

  /**
   * @param Address $address
   *
   * @return $this
   */
  public function setAddress(Address $address): self
  {
    $this->address = $address;

    return $this;
  }

  /**
   * @return Customer
   */
  public function getCustomer(): Customer
  {
    return $this->customer;
  }

  /**
   * @param Customer $customer
   *
   * @return $this
   */
  public function setCustomer(Customer $customer): self
  {
    $this->customer = $customer;
    return $this;
  }
}
