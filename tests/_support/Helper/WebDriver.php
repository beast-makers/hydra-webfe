<?php
declare(strict_types=1);

namespace BeastMakersTest\Helper;

use Codeception\Exception\ModuleException;
use Codeception\Module\WebDriver as CodecptionWebDriver;
use Facebook\WebDriver\WebDriverElement;

class WebDriver extends CodecptionWebDriver
{
  private const SHORT_WAIT_FOR_ELEMENT_TIME = 3;

  private const LONG_WAIT_FOR_ELEMENT_TIME = 10;

  /**
   * @param $element
   *
   * @return void
   * @throws \Exception
   */
  public function shortWaitForElement($element): void
  {
    $this->waitForElement($element, static::SHORT_WAIT_FOR_ELEMENT_TIME);
  }

  /**
   * @param $element
   *
   * @return void
   * @throws \Exception
   */
  public function shortWaitForElementVisible($element): void
  {
    $this->waitForElementVisible($element, static::SHORT_WAIT_FOR_ELEMENT_TIME);
  }

  /**
   * @param $element
   *
   * @return void
   * @throws \Exception
   */
  public function shortWaitForElementNotVisible($element): void
  {
    $this->waitForElementNotVisible($element, static::SHORT_WAIT_FOR_ELEMENT_TIME);
  }

  /**
   * @param $element
   *
   * @return void
   * @throws \Exception
   */
  public function longWaitForElement($element): void
  {
    $this->waitForElement($element, static::LONG_WAIT_FOR_ELEMENT_TIME);
  }

  /**
   * @param $selector
   * @param $expected
   *
   * @return void
   * @throws ModuleException
   */
  public function seeNumberOfNotEmptyElements($selector, $expected): void
  {
    $counted = count($this->matchVisibleNotEmpty($selector));
    if (is_array($expected)) {
      [$floor, $ceil] = $expected;
      $this->assertTrue(
        $floor <= $counted && $ceil >= $counted,
        'Number of elements counted differs from expected range'
      );
    } else {
      $this->assertSame(
        $expected,
        $counted,
        'Number of elements counted differs from expected number'
      );
    }
  }

  /**
   * @param $selector
   *
   * @return array
   * @throws ModuleException
   */
  protected function matchVisibleNotEmpty($selector): array
  {
    $els = $this->match($this->getBaseElement(), $selector);

    return array_filter(
      $els,
      function (WebDriverElement $el) {
        return $el->isDisplayed() && !empty($el->getText());
      }
    );
  }

  /**
   * @param string $xpathSelector
   *
   * @return string
   */
  public function selectFirstOption(string $xpathSelector): string
  {
    $option = $this->grabTextFrom("{$xpathSelector}//option[1]");
    $value = $this->grabValueFrom("{$xpathSelector}//option[1]");
    $this->selectOption($xpathSelector, $option);

    return $value;
  }
}
