<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Form;

abstract class FormField
{
  public const ATTR_ID = 'id';
  public const ATTR_LABEL = 'label';
  public const ATTR_VALUE = 'value';
  public const ATTR_PLACEHOLDER = 'placeholder';

  protected array $attributes = [];

  /**
   * @param array $properties
   */
  public function fromArray(array $properties): void
  {
    foreach ($this->attributeList() as $attributeName) {
      if (array_key_exists($attributeName, $properties)) {
        $this->attributes[$attributeName] = $properties[$attributeName];
      }
    }
  }

  /**
   * @return array
   */
  public function asArray(): array
  {
    return $this->attributes;
  }

  /**
   * @return string
   */
  public function getId(): string
  {
    return $this->attributes[static::ATTR_ID] ?? '';
  }

  /**
   * @return string
   */
  public function getLabel(): string
  {
    return $this->attributes[static::ATTR_LABEL] ?? '';
  }

  /**
   * @return string
   */
  public function getPlaceholder(): string
  {
    return $this->attributes[static::ATTR_PLACEHOLDER] ?? '';
  }

  /**
   * @return array
   */
  protected function attributeList(): array
  {
    return [
      self::ATTR_ID,
      self::ATTR_LABEL,
      self::ATTR_VALUE,
      self::ATTR_PLACEHOLDER,
    ];
  }
}
