'use strict'

const Glob = require("glob");
const Path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const CopyPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');

// const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

function getEntries(globPattern, extension, outputExtension) {
  let entries = {};
  Glob.sync(globPattern).map(function (filePath) {
    const entryName = Path.basename(filePath, extension) + outputExtension;
    entries[entryName] = filePath;
  })

  return entries;
}

module.exports = {
  mode: 'development',
  entry: Object.assign(
    getEntries('./src-presentation/js/page/*.js', '.js', '_script'),
    getEntries('./src-presentation/style/default/page/*.scss', '.scss', '_style'),
    { svg_icons: './src-presentation/assets/svg-icons.svg.js' }
  ),
  output: {
    filename: 'js/[name].js',
    sourceMapFilename: '[file].map',
    path: Path.resolve(__dirname, '../public/ui')
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [autoprefixer()]
            }
          },
          {
            loader: "sass-loader",
            options: {
              implementation: require("sass"),
              sassOptions: {
                includePaths: ['./src-presentation/style']
              }
            }
          }
        ]
      },
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      {
        test: /\.svg$/,
        loader: 'svg-sprite-loader',
        options: {
          extract: true,
          spriteFilename: svgPath => {
            const outputFilename = Path.basename(Path.dirname(svgPath));

            return `assets/${outputFilename}.svg`;
          }
        }
      }
    ]
  },
  plugins: [
    new FixStyleOnlyEntriesPlugin(),
    new MiniCssExtractPlugin({
      filename: 'style/[name].css'
    }),
    new SpriteLoaderPlugin({
      plainSprite: true,
      spriteAttrs: {
        id: '__svg-sprite__',
        style: "display: none"
      }
    }),
    new CleanWebpackPlugin({
      protectWebpackAssets: false,
      cleanStaleWebpackAssets: false,
      cleanAfterEveryBuildPatterns: ['**/js/svg_icons*'],
    }),
    new CopyPlugin([
      { from: 'src-presentation/assets/fonts/ready/', to: 'assets/font/' },
    ]),
    // new OptimizeCssAssetsPlugin()
  ]
}
