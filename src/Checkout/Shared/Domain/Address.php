<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class Address extends ArrayDto
{
  public const ATTR_FIRST_NAME = 'first_name';
  public const ATTR_LAST_NAME = 'last_name';
  public const ATTR_COUNTRY_CODE = 'country_code';
  public const ATTR_STREET = 'street';
  public const ATTR_ADDRESS_EXTENSION = 'address_extension';
  public const ATTR_CITY = 'city';
  public const ATTR_ZIP = 'zip';

  /**
   * @return string
   */
  public function getFirstName(): string
  {
    return $this->get(self::ATTR_FIRST_NAME, '');
  }

  /**
   * @return string
   */
  public function getLastName(): string
  {
    return $this->get(self::ATTR_LAST_NAME, '');
  }


  /**
   * @param string $countryCode
   *
   * @return $this
   */
  public function setCountryCode(string $countryCode): self
  {
    $this->set(self::ATTR_COUNTRY_CODE, strtoupper($countryCode));

    return $this;
  }

  /**
   * @return string
   */
  public function getCountryCode(): string
  {
    return $this->get(self::ATTR_COUNTRY_CODE, '');
  }

  /**
   * @return string
   */
  public function getStreet(): string
  {
    return $this->get(self::ATTR_STREET, '');
  }

  /**
   * @return string
   */
  public function getAddressExtension(): string
  {
    return $this->get(self::ATTR_ADDRESS_EXTENSION, '');
  }

  /**
   * @return string
   */
  public function getCity(): string
  {
    return $this->get(self::ATTR_CITY, '');
  }

  /**
   * @return string
   */
  public function getZip(): string
  {
    return $this->get(self::ATTR_ZIP, '');
  }

  /**
   * @inheritDoc
   */
  public function definition(): array
  {
    return [
      self::ATTR_FIRST_NAME => self::DEF_SCALAR,
      self::ATTR_LAST_NAME => self::DEF_SCALAR,
      self::ATTR_COUNTRY_CODE => self::DEF_SCALAR,
      self::ATTR_STREET => self::DEF_SCALAR,
      self::ATTR_ADDRESS_EXTENSION => self::DEF_SCALAR,
      self::ATTR_CITY => self::DEF_SCALAR,
      self::ATTR_ZIP => self::DEF_SCALAR,
    ];
  }
}
