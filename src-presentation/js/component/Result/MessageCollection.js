export default class MessageCollection {
  /**
   * @param {string} code
   * @param {Array} messages
   *
   * @return {void}
   */
  constructor (code = '', messages = []) {
    this.code = code;
    this.messages = messages;
  }
}
