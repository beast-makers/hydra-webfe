<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Infra\Repository;

use BeastMakers\Checkout\Cart\Infra\CartData;

interface ReadRepository
{
  /**
   * @param string $checkoutId
   *
   * @return CartData
   */
  public function fetchCartData(string $checkoutId): CartData;
}
