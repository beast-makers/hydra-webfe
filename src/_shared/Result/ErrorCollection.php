<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Result;

class ErrorCollection
{
  /**
   * @var Error[]
   */
  protected array $errors = [];

  /**
   * @return bool
   */
  public function hasErrors(): bool
  {
    return !empty($this->errors);
  }

  /**
   * @param Error $error
   *
   * @return void
   */
  public function addError(Error $error): void
  {
    $this->errors[] = $error;
  }

  /**
   * @return Error[]
   */
  public function getErrors(): array
  {
    return $this->errors;
  }

  /**
   * @return Error
   */
  public function getFirstError(): Error
  {
    return $this->errors[0];
  }

  /**
   * @param ErrorCollection $errorCollection
   *
   * @return $this
   */
  public function merge(self $errorCollection): self
  {
    $this->errors = array_merge($this->errors, $errorCollection->getErrors());

    return $this;
  }
}
