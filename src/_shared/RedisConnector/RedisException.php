<?php
declare(strict_types=1);

namespace BeastMakers\Shared\RedisConnector;

class RedisException extends \Exception
{
}
