<?php
declare(strict_types=1);

namespace BeastMakers\Shared\BackendClient;

use BeastMakers\Shared\Config\Config as LibConfig;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\Kernel\BaseConfig;

class Config extends BaseConfig
{
  /**
   * @return string
   * @throws MissingConfigOptionException
   */
  public function getBackendClientHost(): string
  {
    return $this->getValue('backend_client.host');
  }

  /**
   * @return string
   * @throws MissingConfigOptionException
   */
  public function getBackendClientBaseUri(): string
  {
    return "http://{$this->getBackendClientHost()}";
  }

  /**
   * @return LibConfig
   * @throws MissingConfigOptionException
   */
  public function getBackendClientAuthConfig(): LibConfig
  {
    return $this->getConfig('backend_client.auth');
  }
}
