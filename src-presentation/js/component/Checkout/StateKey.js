export default {
  CHECKOUT_CONFIG: 'checkout-config',
  CART_ITEMS: 'checkout-cart_items',
  CART_VOUCHER: 'checkout-voucher',

  CART_PAGE_CONFIG: 'checkout-cart_page_config',
  DELIVERY_PAGE_CONFIG: 'checkout-delivery_page_config',
  PAYMENT_PAGE_CONFIG: 'checkout-payment_page_config'
};

