<?php
declare(strict_types=1);

if (!defined('PROJECT_DIR')) {
  define('PROJECT_DIR', __DIR__ . '/../../');
}

define('VIEW_TEMPLATES', PROJECT_DIR . 'src-presentation/view-scripts');

/** @psalm-suppress UnresolvableInclude */
require PROJECT_DIR . 'vendor/autoload.php';
require PROJECT_DIR . '/src/_shared/GlobalFunctions/price.php';

return (new BeastMakers\Application\Application())->bootstrap();
