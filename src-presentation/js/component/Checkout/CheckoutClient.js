import BackendClient from '../../kernel/BackendClient';
import {formData2Object} from '../own';
import {
  getCartPageConfig,
  getCheckoutConfig,
  getDeliveryPageConfig,
  setCartPageConfig,
  setDeliveryPageConfig,
  setPaymentPageConfig
} from './Facade';

export default {
  /**
   * @return {Promise<Object>}
   */
  fetchCartPageData () {
    return new Promise((resolve, reject) => {
      BackendClient.get(getCheckoutConfig().endpoints.cart_page).then((cartPage) => {
        setCartPageConfig(cartPage.body.cart_page_config);

        resolve({
          'cart_page_html': cartPage.body.cart_page_html,
          'cart_details_html': cartPage.body.cart_details_html,
          'cart_items': cartPage.body.cart_items,
          'voucher': cartPage.body.voucher
        });
      })
        .catch((error) => {
          reject(error);
        });
    });
  },

  /**
   *
   * @param {Object} requestData
   *
   * @return {Promise<unknown>}
   */
  storeCartData (requestData) {
    return new Promise((resolve, reject) => {
      BackendClient.post(getCartPageConfig().endpoints.store_cart_data, requestData)
        .then(() => {
          resolve(true);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  /**
   * @return {Promise<Object>}
   */
  fetchDeliveryPageData () {
    return new Promise((resolve, reject) => {
      BackendClient.get(getCheckoutConfig().endpoints.delivery_page)
        .then((deliveryPage) => {
          setDeliveryPageConfig(deliveryPage.body.delivery_page_config);

          resolve({
            'delivery_page_html': deliveryPage.body.delivery_page_html
          });
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  /**
   *
   * @param {FormData} deliveryData
   *
   * @return {Promise<unknown>}
   */
  storeDeliveryData (deliveryData) {
    return new Promise((resolve, reject) => {
      const requestData = {
        'form_data': formData2Object(deliveryData)
      };

      BackendClient.post(getDeliveryPageConfig().endpoints.store_delivery_data, requestData)
        .then(() => {
          resolve(true);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  /**
   * @return {Promise<Object>}
   */
  fetchPaymentPageData () {
    return new Promise((resolve, reject) => {
      BackendClient.get(getCheckoutConfig().endpoints.payment_page)
        .then((paymentPage) => {
          setPaymentPageConfig(paymentPage.body.payment_page_config);

          resolve({
            'payment_page_html': paymentPage.body.payment_page_html
          });
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};
