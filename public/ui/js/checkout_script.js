/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src-presentation/js/page/checkout.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/component-emitter/index.js":
/*!*************************************************!*\
  !*** ./node_modules/component-emitter/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Expose `Emitter`.
 */
if (true) {
  module.exports = Emitter;
}
/**
 * Initialize a new `Emitter`.
 *
 * @api public
 */


function Emitter(obj) {
  if (obj) return mixin(obj);
}

;
/**
 * Mixin the emitter properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */

function mixin(obj) {
  for (var key in Emitter.prototype) {
    obj[key] = Emitter.prototype[key];
  }

  return obj;
}
/**
 * Listen on the given `event` with `fn`.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */


Emitter.prototype.on = Emitter.prototype.addEventListener = function (event, fn) {
  this._callbacks = this._callbacks || {};
  (this._callbacks['$' + event] = this._callbacks['$' + event] || []).push(fn);
  return this;
};
/**
 * Adds an `event` listener that will be invoked a single
 * time then automatically removed.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */


Emitter.prototype.once = function (event, fn) {
  function on() {
    this.off(event, on);
    fn.apply(this, arguments);
  }

  on.fn = fn;
  this.on(event, on);
  return this;
};
/**
 * Remove the given callback for `event` or all
 * registered callbacks.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */


Emitter.prototype.off = Emitter.prototype.removeListener = Emitter.prototype.removeAllListeners = Emitter.prototype.removeEventListener = function (event, fn) {
  this._callbacks = this._callbacks || {}; // all

  if (0 == arguments.length) {
    this._callbacks = {};
    return this;
  } // specific event


  var callbacks = this._callbacks['$' + event];
  if (!callbacks) return this; // remove all handlers

  if (1 == arguments.length) {
    delete this._callbacks['$' + event];
    return this;
  } // remove specific handler


  var cb;

  for (var i = 0; i < callbacks.length; i++) {
    cb = callbacks[i];

    if (cb === fn || cb.fn === fn) {
      callbacks.splice(i, 1);
      break;
    }
  } // Remove event specific arrays for event types that no
  // one is subscribed for to avoid memory leak.


  if (callbacks.length === 0) {
    delete this._callbacks['$' + event];
  }

  return this;
};
/**
 * Emit `event` with the given args.
 *
 * @param {String} event
 * @param {Mixed} ...
 * @return {Emitter}
 */


Emitter.prototype.emit = function (event) {
  this._callbacks = this._callbacks || {};
  var args = new Array(arguments.length - 1),
      callbacks = this._callbacks['$' + event];

  for (var i = 1; i < arguments.length; i++) {
    args[i - 1] = arguments[i];
  }

  if (callbacks) {
    callbacks = callbacks.slice(0);

    for (var i = 0, len = callbacks.length; i < len; ++i) {
      callbacks[i].apply(this, args);
    }
  }

  return this;
};
/**
 * Return array of callbacks for `event`.
 *
 * @param {String} event
 * @return {Array}
 * @api public
 */


Emitter.prototype.listeners = function (event) {
  this._callbacks = this._callbacks || {};
  return this._callbacks['$' + event] || [];
};
/**
 * Check if this emitter has `event` handlers.
 *
 * @param {String} event
 * @return {Boolean}
 * @api public
 */


Emitter.prototype.hasListeners = function (event) {
  return !!this.listeners(event).length;
};

/***/ }),

/***/ "./node_modules/element-closest-polyfill/index.js":
/*!********************************************************!*\
  !*** ./node_modules/element-closest-polyfill/index.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function () {
  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
  }

  if (!Element.prototype.closest) {
    Element.prototype.closest = function (s) {
      var el = this;

      do {
        if (el.matches(s)) return el;
        el = el.parentElement || el.parentNode;
      } while (el !== null && el.nodeType === 1);

      return null;
    };
  }
})();

/***/ }),

/***/ "./node_modules/fast-safe-stringify/index.js":
/*!***************************************************!*\
  !*** ./node_modules/fast-safe-stringify/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = stringify;
stringify.default = stringify;
stringify.stable = deterministicStringify;
stringify.stableStringify = deterministicStringify;
var arr = [];
var replacerStack = []; // Regular stringify

function stringify(obj, replacer, spacer) {
  decirc(obj, '', [], undefined);
  var res;

  if (replacerStack.length === 0) {
    res = JSON.stringify(obj, replacer, spacer);
  } else {
    res = JSON.stringify(obj, replaceGetterValues(replacer), spacer);
  }

  while (arr.length !== 0) {
    var part = arr.pop();

    if (part.length === 4) {
      Object.defineProperty(part[0], part[1], part[3]);
    } else {
      part[0][part[1]] = part[2];
    }
  }

  return res;
}

function decirc(val, k, stack, parent) {
  var i;

  if (typeof val === 'object' && val !== null) {
    for (i = 0; i < stack.length; i++) {
      if (stack[i] === val) {
        var propertyDescriptor = Object.getOwnPropertyDescriptor(parent, k);

        if (propertyDescriptor.get !== undefined) {
          if (propertyDescriptor.configurable) {
            Object.defineProperty(parent, k, {
              value: '[Circular]'
            });
            arr.push([parent, k, val, propertyDescriptor]);
          } else {
            replacerStack.push([val, k]);
          }
        } else {
          parent[k] = '[Circular]';
          arr.push([parent, k, val]);
        }

        return;
      }
    }

    stack.push(val); // Optimize for Arrays. Big arrays could kill the performance otherwise!

    if (Array.isArray(val)) {
      for (i = 0; i < val.length; i++) {
        decirc(val[i], i, stack, val);
      }
    } else {
      var keys = Object.keys(val);

      for (i = 0; i < keys.length; i++) {
        var key = keys[i];
        decirc(val[key], key, stack, val);
      }
    }

    stack.pop();
  }
} // Stable-stringify


function compareFunction(a, b) {
  if (a < b) {
    return -1;
  }

  if (a > b) {
    return 1;
  }

  return 0;
}

function deterministicStringify(obj, replacer, spacer) {
  var tmp = deterministicDecirc(obj, '', [], undefined) || obj;
  var res;

  if (replacerStack.length === 0) {
    res = JSON.stringify(tmp, replacer, spacer);
  } else {
    res = JSON.stringify(tmp, replaceGetterValues(replacer), spacer);
  }

  while (arr.length !== 0) {
    var part = arr.pop();

    if (part.length === 4) {
      Object.defineProperty(part[0], part[1], part[3]);
    } else {
      part[0][part[1]] = part[2];
    }
  }

  return res;
}

function deterministicDecirc(val, k, stack, parent) {
  var i;

  if (typeof val === 'object' && val !== null) {
    for (i = 0; i < stack.length; i++) {
      if (stack[i] === val) {
        var propertyDescriptor = Object.getOwnPropertyDescriptor(parent, k);

        if (propertyDescriptor.get !== undefined) {
          if (propertyDescriptor.configurable) {
            Object.defineProperty(parent, k, {
              value: '[Circular]'
            });
            arr.push([parent, k, val, propertyDescriptor]);
          } else {
            replacerStack.push([val, k]);
          }
        } else {
          parent[k] = '[Circular]';
          arr.push([parent, k, val]);
        }

        return;
      }
    }

    if (typeof val.toJSON === 'function') {
      return;
    }

    stack.push(val); // Optimize for Arrays. Big arrays could kill the performance otherwise!

    if (Array.isArray(val)) {
      for (i = 0; i < val.length; i++) {
        deterministicDecirc(val[i], i, stack, val);
      }
    } else {
      // Create a temporary object in the required way
      var tmp = {};
      var keys = Object.keys(val).sort(compareFunction);

      for (i = 0; i < keys.length; i++) {
        var key = keys[i];
        deterministicDecirc(val[key], key, stack, val);
        tmp[key] = val[key];
      }

      if (parent !== undefined) {
        arr.push([parent, k, val]);
        parent[k] = tmp;
      } else {
        return tmp;
      }
    }

    stack.pop();
  }
} // wraps replacer function to handle values we couldn't replace
// and mark them as [Circular]


function replaceGetterValues(replacer) {
  replacer = replacer !== undefined ? replacer : function (k, v) {
    return v;
  };
  return function (key, val) {
    if (replacerStack.length > 0) {
      for (var i = 0; i < replacerStack.length; i++) {
        var part = replacerStack[i];

        if (part[1] === key && part[0] === val) {
          val = '[Circular]';
          replacerStack.splice(i, 1);
          break;
        }
      }
    }

    return replacer.call(this, key, val);
  };
}

/***/ }),

/***/ "./node_modules/superagent/lib/agent-base.js":
/*!***************************************************!*\
  !*** ./node_modules/superagent/lib/agent-base.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function Agent() {
  this._defaults = [];
}

['use', 'on', 'once', 'set', 'query', 'type', 'accept', 'auth', 'withCredentials', 'sortQuery', 'retry', 'ok', 'redirects', 'timeout', 'buffer', 'serialize', 'parse', 'ca', 'key', 'pfx', 'cert', 'disableTLSCerts'].forEach(function (fn) {
  // Default setting for all requests from this agent
  Agent.prototype[fn] = function () {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    this._defaults.push({
      fn: fn,
      args: args
    });

    return this;
  };
});

Agent.prototype._setDefaults = function (req) {
  this._defaults.forEach(function (def) {
    req[def.fn].apply(req, _toConsumableArray(def.args));
  });
};

module.exports = Agent;

/***/ }),

/***/ "./node_modules/superagent/lib/client.js":
/*!***********************************************!*\
  !*** ./node_modules/superagent/lib/client.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}
/**
 * Root reference for iframes.
 */


var root;

if (typeof window !== 'undefined') {
  // Browser window
  root = window;
} else if (typeof self === 'undefined') {
  // Other environments
  console.warn('Using browser-only version of superagent in non-browser environment');
  root = void 0;
} else {
  // Web Worker
  root = self;
}

var Emitter = __webpack_require__(/*! component-emitter */ "./node_modules/component-emitter/index.js");

var safeStringify = __webpack_require__(/*! fast-safe-stringify */ "./node_modules/fast-safe-stringify/index.js");

var RequestBase = __webpack_require__(/*! ./request-base */ "./node_modules/superagent/lib/request-base.js");

var isObject = __webpack_require__(/*! ./is-object */ "./node_modules/superagent/lib/is-object.js");

var ResponseBase = __webpack_require__(/*! ./response-base */ "./node_modules/superagent/lib/response-base.js");

var Agent = __webpack_require__(/*! ./agent-base */ "./node_modules/superagent/lib/agent-base.js");
/**
 * Noop.
 */


function noop() {}
/**
 * Expose `request`.
 */


module.exports = function (method, url) {
  // callback
  if (typeof url === 'function') {
    return new exports.Request('GET', method).end(url);
  } // url first


  if (arguments.length === 1) {
    return new exports.Request('GET', method);
  }

  return new exports.Request(method, url);
};

exports = module.exports;
var request = exports;
exports.Request = Request;
/**
 * Determine XHR.
 */

request.getXHR = function () {
  if (root.XMLHttpRequest && (!root.location || root.location.protocol !== 'file:' || !root.ActiveXObject)) {
    return new XMLHttpRequest();
  }

  try {
    return new ActiveXObject('Microsoft.XMLHTTP');
  } catch (_unused) {}

  try {
    return new ActiveXObject('Msxml2.XMLHTTP.6.0');
  } catch (_unused2) {}

  try {
    return new ActiveXObject('Msxml2.XMLHTTP.3.0');
  } catch (_unused3) {}

  try {
    return new ActiveXObject('Msxml2.XMLHTTP');
  } catch (_unused4) {}

  throw new Error('Browser-only version of superagent could not find XHR');
};
/**
 * Removes leading and trailing whitespace, added to support IE.
 *
 * @param {String} s
 * @return {String}
 * @api private
 */


var trim = ''.trim ? function (s) {
  return s.trim();
} : function (s) {
  return s.replace(/(^\s*|\s*$)/g, '');
};
/**
 * Serialize the given `obj`.
 *
 * @param {Object} obj
 * @return {String}
 * @api private
 */

function serialize(obj) {
  if (!isObject(obj)) return obj;
  var pairs = [];

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) pushEncodedKeyValuePair(pairs, key, obj[key]);
  }

  return pairs.join('&');
}
/**
 * Helps 'serialize' with serializing arrays.
 * Mutates the pairs array.
 *
 * @param {Array} pairs
 * @param {String} key
 * @param {Mixed} val
 */


function pushEncodedKeyValuePair(pairs, key, val) {
  if (val === undefined) return;

  if (val === null) {
    pairs.push(encodeURI(key));
    return;
  }

  if (Array.isArray(val)) {
    val.forEach(function (v) {
      pushEncodedKeyValuePair(pairs, key, v);
    });
  } else if (isObject(val)) {
    for (var subkey in val) {
      if (Object.prototype.hasOwnProperty.call(val, subkey)) pushEncodedKeyValuePair(pairs, "".concat(key, "[").concat(subkey, "]"), val[subkey]);
    }
  } else {
    pairs.push(encodeURI(key) + '=' + encodeURIComponent(val));
  }
}
/**
 * Expose serialization method.
 */


request.serializeObject = serialize;
/**
 * Parse the given x-www-form-urlencoded `str`.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */

function parseString(str) {
  var obj = {};
  var pairs = str.split('&');
  var pair;
  var pos;

  for (var i = 0, len = pairs.length; i < len; ++i) {
    pair = pairs[i];
    pos = pair.indexOf('=');

    if (pos === -1) {
      obj[decodeURIComponent(pair)] = '';
    } else {
      obj[decodeURIComponent(pair.slice(0, pos))] = decodeURIComponent(pair.slice(pos + 1));
    }
  }

  return obj;
}
/**
 * Expose parser.
 */


request.parseString = parseString;
/**
 * Default MIME type map.
 *
 *     superagent.types.xml = 'application/xml';
 *
 */

request.types = {
  html: 'text/html',
  json: 'application/json',
  xml: 'text/xml',
  urlencoded: 'application/x-www-form-urlencoded',
  form: 'application/x-www-form-urlencoded',
  'form-data': 'application/x-www-form-urlencoded'
};
/**
 * Default serialization map.
 *
 *     superagent.serialize['application/xml'] = function(obj){
 *       return 'generated xml here';
 *     };
 *
 */

request.serialize = {
  'application/x-www-form-urlencoded': serialize,
  'application/json': safeStringify
};
/**
 * Default parsers.
 *
 *     superagent.parse['application/xml'] = function(str){
 *       return { object parsed from str };
 *     };
 *
 */

request.parse = {
  'application/x-www-form-urlencoded': parseString,
  'application/json': JSON.parse
};
/**
 * Parse the given header `str` into
 * an object containing the mapped fields.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */

function parseHeader(str) {
  var lines = str.split(/\r?\n/);
  var fields = {};
  var index;
  var line;
  var field;
  var val;

  for (var i = 0, len = lines.length; i < len; ++i) {
    line = lines[i];
    index = line.indexOf(':');

    if (index === -1) {
      // could be empty line, just skip it
      continue;
    }

    field = line.slice(0, index).toLowerCase();
    val = trim(line.slice(index + 1));
    fields[field] = val;
  }

  return fields;
}
/**
 * Check if `mime` is json or has +json structured syntax suffix.
 *
 * @param {String} mime
 * @return {Boolean}
 * @api private
 */


function isJSON(mime) {
  // should match /json or +json
  // but not /json-seq
  return /[/+]json($|[^-\w])/.test(mime);
}
/**
 * Initialize a new `Response` with the given `xhr`.
 *
 *  - set flags (.ok, .error, etc)
 *  - parse header
 *
 * Examples:
 *
 *  Aliasing `superagent` as `request` is nice:
 *
 *      request = superagent;
 *
 *  We can use the promise-like API, or pass callbacks:
 *
 *      request.get('/').end(function(res){});
 *      request.get('/', function(res){});
 *
 *  Sending data can be chained:
 *
 *      request
 *        .post('/user')
 *        .send({ name: 'tj' })
 *        .end(function(res){});
 *
 *  Or passed to `.send()`:
 *
 *      request
 *        .post('/user')
 *        .send({ name: 'tj' }, function(res){});
 *
 *  Or passed to `.post()`:
 *
 *      request
 *        .post('/user', { name: 'tj' })
 *        .end(function(res){});
 *
 * Or further reduced to a single call for simple cases:
 *
 *      request
 *        .post('/user', { name: 'tj' }, function(res){});
 *
 * @param {XMLHTTPRequest} xhr
 * @param {Object} options
 * @api private
 */


function Response(req) {
  this.req = req;
  this.xhr = this.req.xhr; // responseText is accessible only if responseType is '' or 'text' and on older browsers

  this.text = this.req.method !== 'HEAD' && (this.xhr.responseType === '' || this.xhr.responseType === 'text') || typeof this.xhr.responseType === 'undefined' ? this.xhr.responseText : null;
  this.statusText = this.req.xhr.statusText;
  var status = this.xhr.status; // handle IE9 bug: http://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request

  if (status === 1223) {
    status = 204;
  }

  this._setStatusProperties(status);

  this.headers = parseHeader(this.xhr.getAllResponseHeaders());
  this.header = this.headers; // getAllResponseHeaders sometimes falsely returns "" for CORS requests, but
  // getResponseHeader still works. so we get content-type even if getting
  // other headers fails.

  this.header['content-type'] = this.xhr.getResponseHeader('content-type');

  this._setHeaderProperties(this.header);

  if (this.text === null && req._responseType) {
    this.body = this.xhr.response;
  } else {
    this.body = this.req.method === 'HEAD' ? null : this._parseBody(this.text ? this.text : this.xhr.response);
  }
} // eslint-disable-next-line new-cap


ResponseBase(Response.prototype);
/**
 * Parse the given body `str`.
 *
 * Used for auto-parsing of bodies. Parsers
 * are defined on the `superagent.parse` object.
 *
 * @param {String} str
 * @return {Mixed}
 * @api private
 */

Response.prototype._parseBody = function (str) {
  var parse = request.parse[this.type];

  if (this.req._parser) {
    return this.req._parser(this, str);
  }

  if (!parse && isJSON(this.type)) {
    parse = request.parse['application/json'];
  }

  return parse && str && (str.length > 0 || str instanceof Object) ? parse(str) : null;
};
/**
 * Return an `Error` representative of this response.
 *
 * @return {Error}
 * @api public
 */


Response.prototype.toError = function () {
  var req = this.req;
  var method = req.method;
  var url = req.url;
  var msg = "cannot ".concat(method, " ").concat(url, " (").concat(this.status, ")");
  var err = new Error(msg);
  err.status = this.status;
  err.method = method;
  err.url = url;
  return err;
};
/**
 * Expose `Response`.
 */


request.Response = Response;
/**
 * Initialize a new `Request` with the given `method` and `url`.
 *
 * @param {String} method
 * @param {String} url
 * @api public
 */

function Request(method, url) {
  var self = this;
  this._query = this._query || [];
  this.method = method;
  this.url = url;
  this.header = {}; // preserves header name case

  this._header = {}; // coerces header names to lowercase

  this.on('end', function () {
    var err = null;
    var res = null;

    try {
      res = new Response(self);
    } catch (err_) {
      err = new Error('Parser is unable to parse the response');
      err.parse = true;
      err.original = err_; // issue #675: return the raw response if the response parsing fails

      if (self.xhr) {
        // ie9 doesn't have 'response' property
        err.rawResponse = typeof self.xhr.responseType === 'undefined' ? self.xhr.responseText : self.xhr.response; // issue #876: return the http status code if the response parsing fails

        err.status = self.xhr.status ? self.xhr.status : null;
        err.statusCode = err.status; // backwards-compat only
      } else {
        err.rawResponse = null;
        err.status = null;
      }

      return self.callback(err);
    }

    self.emit('response', res);
    var new_err;

    try {
      if (!self._isResponseOK(res)) {
        new_err = new Error(res.statusText || res.text || 'Unsuccessful HTTP response');
      }
    } catch (err_) {
      new_err = err_; // ok() callback can throw
    } // #1000 don't catch errors from the callback to avoid double calling it


    if (new_err) {
      new_err.original = err;
      new_err.response = res;
      new_err.status = res.status;
      self.callback(new_err, res);
    } else {
      self.callback(null, res);
    }
  });
}
/**
 * Mixin `Emitter` and `RequestBase`.
 */
// eslint-disable-next-line new-cap


Emitter(Request.prototype); // eslint-disable-next-line new-cap

RequestBase(Request.prototype);
/**
 * Set Content-Type to `type`, mapping values from `request.types`.
 *
 * Examples:
 *
 *      superagent.types.xml = 'application/xml';
 *
 *      request.post('/')
 *        .type('xml')
 *        .send(xmlstring)
 *        .end(callback);
 *
 *      request.post('/')
 *        .type('application/xml')
 *        .send(xmlstring)
 *        .end(callback);
 *
 * @param {String} type
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.type = function (type) {
  this.set('Content-Type', request.types[type] || type);
  return this;
};
/**
 * Set Accept to `type`, mapping values from `request.types`.
 *
 * Examples:
 *
 *      superagent.types.json = 'application/json';
 *
 *      request.get('/agent')
 *        .accept('json')
 *        .end(callback);
 *
 *      request.get('/agent')
 *        .accept('application/json')
 *        .end(callback);
 *
 * @param {String} accept
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.accept = function (type) {
  this.set('Accept', request.types[type] || type);
  return this;
};
/**
 * Set Authorization field value with `user` and `pass`.
 *
 * @param {String} user
 * @param {String} [pass] optional in case of using 'bearer' as type
 * @param {Object} options with 'type' property 'auto', 'basic' or 'bearer' (default 'basic')
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.auth = function (user, pass, options) {
  if (arguments.length === 1) pass = '';

  if (_typeof(pass) === 'object' && pass !== null) {
    // pass is optional and can be replaced with options
    options = pass;
    pass = '';
  }

  if (!options) {
    options = {
      type: typeof btoa === 'function' ? 'basic' : 'auto'
    };
  }

  var encoder = function encoder(string) {
    if (typeof btoa === 'function') {
      return btoa(string);
    }

    throw new Error('Cannot use basic auth, btoa is not a function');
  };

  return this._auth(user, pass, options, encoder);
};
/**
 * Add query-string `val`.
 *
 * Examples:
 *
 *   request.get('/shoes')
 *     .query('size=10')
 *     .query({ color: 'blue' })
 *
 * @param {Object|String} val
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.query = function (val) {
  if (typeof val !== 'string') val = serialize(val);
  if (val) this._query.push(val);
  return this;
};
/**
 * Queue the given `file` as an attachment to the specified `field`,
 * with optional `options` (or filename).
 *
 * ``` js
 * request.post('/upload')
 *   .attach('content', new Blob(['<a id="a"><b id="b">hey!</b></a>'], { type: "text/html"}))
 *   .end(callback);
 * ```
 *
 * @param {String} field
 * @param {Blob|File} file
 * @param {String|Object} options
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.attach = function (field, file, options) {
  if (file) {
    if (this._data) {
      throw new Error("superagent can't mix .send() and .attach()");
    }

    this._getFormData().append(field, file, options || file.name);
  }

  return this;
};

Request.prototype._getFormData = function () {
  if (!this._formData) {
    this._formData = new root.FormData();
  }

  return this._formData;
};
/**
 * Invoke the callback with `err` and `res`
 * and handle arity check.
 *
 * @param {Error} err
 * @param {Response} res
 * @api private
 */


Request.prototype.callback = function (err, res) {
  if (this._shouldRetry(err, res)) {
    return this._retry();
  }

  var fn = this._callback;
  this.clearTimeout();

  if (err) {
    if (this._maxRetries) err.retries = this._retries - 1;
    this.emit('error', err);
  }

  fn(err, res);
};
/**
 * Invoke callback with x-domain error.
 *
 * @api private
 */


Request.prototype.crossDomainError = function () {
  var err = new Error('Request has been terminated\nPossible causes: the network is offline, Origin is not allowed by Access-Control-Allow-Origin, the page is being unloaded, etc.');
  err.crossDomain = true;
  err.status = this.status;
  err.method = this.method;
  err.url = this.url;
  this.callback(err);
}; // This only warns, because the request is still likely to work


Request.prototype.agent = function () {
  console.warn('This is not supported in browser version of superagent');
  return this;
};

Request.prototype.ca = Request.prototype.agent;
Request.prototype.buffer = Request.prototype.ca; // This throws, because it can't send/receive data as expected

Request.prototype.write = function () {
  throw new Error('Streaming is not supported in browser version of superagent');
};

Request.prototype.pipe = Request.prototype.write;
/**
 * Check if `obj` is a host object,
 * we don't want to serialize these :)
 *
 * @param {Object} obj host object
 * @return {Boolean} is a host object
 * @api private
 */

Request.prototype._isHost = function (obj) {
  // Native objects stringify to [object File], [object Blob], [object FormData], etc.
  return obj && _typeof(obj) === 'object' && !Array.isArray(obj) && Object.prototype.toString.call(obj) !== '[object Object]';
};
/**
 * Initiate request, invoking callback `fn(res)`
 * with an instanceof `Response`.
 *
 * @param {Function} fn
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.end = function (fn) {
  if (this._endCalled) {
    console.warn('Warning: .end() was called twice. This is not supported in superagent');
  }

  this._endCalled = true; // store callback

  this._callback = fn || noop; // querystring

  this._finalizeQueryString();

  this._end();
};

Request.prototype._setUploadTimeout = function () {
  var self = this; // upload timeout it's wokrs only if deadline timeout is off

  if (this._uploadTimeout && !this._uploadTimeoutTimer) {
    this._uploadTimeoutTimer = setTimeout(function () {
      self._timeoutError('Upload timeout of ', self._uploadTimeout, 'ETIMEDOUT');
    }, this._uploadTimeout);
  }
}; // eslint-disable-next-line complexity


Request.prototype._end = function () {
  if (this._aborted) return this.callback(new Error('The request has been aborted even before .end() was called'));
  var self = this;
  this.xhr = request.getXHR();
  var xhr = this.xhr;
  var data = this._formData || this._data;

  this._setTimeouts(); // state change


  xhr.onreadystatechange = function () {
    var readyState = xhr.readyState;

    if (readyState >= 2 && self._responseTimeoutTimer) {
      clearTimeout(self._responseTimeoutTimer);
    }

    if (readyState !== 4) {
      return;
    } // In IE9, reads to any property (e.g. status) off of an aborted XHR will
    // result in the error "Could not complete the operation due to error c00c023f"


    var status;

    try {
      status = xhr.status;
    } catch (_unused5) {
      status = 0;
    }

    if (!status) {
      if (self.timedout || self._aborted) return;
      return self.crossDomainError();
    }

    self.emit('end');
  }; // progress


  var handleProgress = function handleProgress(direction, e) {
    if (e.total > 0) {
      e.percent = e.loaded / e.total * 100;

      if (e.percent === 100) {
        clearTimeout(self._uploadTimeoutTimer);
      }
    }

    e.direction = direction;
    self.emit('progress', e);
  };

  if (this.hasListeners('progress')) {
    try {
      xhr.addEventListener('progress', handleProgress.bind(null, 'download'));

      if (xhr.upload) {
        xhr.upload.addEventListener('progress', handleProgress.bind(null, 'upload'));
      }
    } catch (_unused6) {// Accessing xhr.upload fails in IE from a web worker, so just pretend it doesn't exist.
      // Reported here:
      // https://connect.microsoft.com/IE/feedback/details/837245/xmlhttprequest-upload-throws-invalid-argument-when-used-from-web-worker-context
    }
  }

  if (xhr.upload) {
    this._setUploadTimeout();
  } // initiate request


  try {
    if (this.username && this.password) {
      xhr.open(this.method, this.url, true, this.username, this.password);
    } else {
      xhr.open(this.method, this.url, true);
    }
  } catch (err) {
    // see #1149
    return this.callback(err);
  } // CORS


  if (this._withCredentials) xhr.withCredentials = true; // body

  if (!this._formData && this.method !== 'GET' && this.method !== 'HEAD' && typeof data !== 'string' && !this._isHost(data)) {
    // serialize stuff
    var contentType = this._header['content-type'];

    var _serialize = this._serializer || request.serialize[contentType ? contentType.split(';')[0] : ''];

    if (!_serialize && isJSON(contentType)) {
      _serialize = request.serialize['application/json'];
    }

    if (_serialize) data = _serialize(data);
  } // set header fields


  for (var field in this.header) {
    if (this.header[field] === null) continue;
    if (Object.prototype.hasOwnProperty.call(this.header, field)) xhr.setRequestHeader(field, this.header[field]);
  }

  if (this._responseType) {
    xhr.responseType = this._responseType;
  } // send stuff


  this.emit('request', this); // IE11 xhr.send(undefined) sends 'undefined' string as POST payload (instead of nothing)
  // We need null here if data is undefined

  xhr.send(typeof data === 'undefined' ? null : data);
};

request.agent = function () {
  return new Agent();
};

['GET', 'POST', 'OPTIONS', 'PATCH', 'PUT', 'DELETE'].forEach(function (method) {
  Agent.prototype[method.toLowerCase()] = function (url, fn) {
    var req = new request.Request(method, url);

    this._setDefaults(req);

    if (fn) {
      req.end(fn);
    }

    return req;
  };
});
Agent.prototype.del = Agent.prototype.delete;
/**
 * GET `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.get = function (url, data, fn) {
  var req = request('GET', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.query(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * HEAD `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


request.head = function (url, data, fn) {
  var req = request('HEAD', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.query(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * OPTIONS query to `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


request.options = function (url, data, fn) {
  var req = request('OPTIONS', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * DELETE `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} [data]
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


function del(url, data, fn) {
  var req = request('DELETE', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
}

request.del = del;
request.delete = del;
/**
 * PATCH `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} [data]
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.patch = function (url, data, fn) {
  var req = request('PATCH', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * POST `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} [data]
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


request.post = function (url, data, fn) {
  var req = request('POST', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * PUT `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


request.put = function (url, data, fn) {
  var req = request('PUT', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

/***/ }),

/***/ "./node_modules/superagent/lib/is-object.js":
/*!**************************************************!*\
  !*** ./node_modules/superagent/lib/is-object.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}
/**
 * Check if `obj` is an object.
 *
 * @param {Object} obj
 * @return {Boolean}
 * @api private
 */


function isObject(obj) {
  return obj !== null && _typeof(obj) === 'object';
}

module.exports = isObject;

/***/ }),

/***/ "./node_modules/superagent/lib/request-base.js":
/*!*****************************************************!*\
  !*** ./node_modules/superagent/lib/request-base.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}
/**
 * Module of mixed-in functions shared between node and client code
 */


var isObject = __webpack_require__(/*! ./is-object */ "./node_modules/superagent/lib/is-object.js");
/**
 * Expose `RequestBase`.
 */


module.exports = RequestBase;
/**
 * Initialize a new `RequestBase`.
 *
 * @api public
 */

function RequestBase(obj) {
  if (obj) return mixin(obj);
}
/**
 * Mixin the prototype properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */


function mixin(obj) {
  for (var key in RequestBase.prototype) {
    if (Object.prototype.hasOwnProperty.call(RequestBase.prototype, key)) obj[key] = RequestBase.prototype[key];
  }

  return obj;
}
/**
 * Clear previous timeout.
 *
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.clearTimeout = function () {
  clearTimeout(this._timer);
  clearTimeout(this._responseTimeoutTimer);
  clearTimeout(this._uploadTimeoutTimer);
  delete this._timer;
  delete this._responseTimeoutTimer;
  delete this._uploadTimeoutTimer;
  return this;
};
/**
 * Override default response body parser
 *
 * This function will be called to convert incoming data into request.body
 *
 * @param {Function}
 * @api public
 */


RequestBase.prototype.parse = function (fn) {
  this._parser = fn;
  return this;
};
/**
 * Set format of binary response body.
 * In browser valid formats are 'blob' and 'arraybuffer',
 * which return Blob and ArrayBuffer, respectively.
 *
 * In Node all values result in Buffer.
 *
 * Examples:
 *
 *      req.get('/')
 *        .responseType('blob')
 *        .end(callback);
 *
 * @param {String} val
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.responseType = function (val) {
  this._responseType = val;
  return this;
};
/**
 * Override default request body serializer
 *
 * This function will be called to convert data set via .send or .attach into payload to send
 *
 * @param {Function}
 * @api public
 */


RequestBase.prototype.serialize = function (fn) {
  this._serializer = fn;
  return this;
};
/**
 * Set timeouts.
 *
 * - response timeout is time between sending request and receiving the first byte of the response. Includes DNS and connection time.
 * - deadline is the time from start of the request to receiving response body in full. If the deadline is too short large files may not load at all on slow connections.
 * - upload is the time  since last bit of data was sent or received. This timeout works only if deadline timeout is off
 *
 * Value of 0 or false means no timeout.
 *
 * @param {Number|Object} ms or {response, deadline}
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.timeout = function (options) {
  if (!options || _typeof(options) !== 'object') {
    this._timeout = options;
    this._responseTimeout = 0;
    this._uploadTimeout = 0;
    return this;
  }

  for (var option in options) {
    if (Object.prototype.hasOwnProperty.call(options, option)) {
      switch (option) {
        case 'deadline':
          this._timeout = options.deadline;
          break;

        case 'response':
          this._responseTimeout = options.response;
          break;

        case 'upload':
          this._uploadTimeout = options.upload;
          break;

        default:
          console.warn('Unknown timeout option', option);
      }
    }
  }

  return this;
};
/**
 * Set number of retry attempts on error.
 *
 * Failed requests will be retried 'count' times if timeout or err.code >= 500.
 *
 * @param {Number} count
 * @param {Function} [fn]
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.retry = function (count, fn) {
  // Default to 1 if no count passed or true
  if (arguments.length === 0 || count === true) count = 1;
  if (count <= 0) count = 0;
  this._maxRetries = count;
  this._retries = 0;
  this._retryCallback = fn;
  return this;
};

var ERROR_CODES = ['ECONNRESET', 'ETIMEDOUT', 'EADDRINFO', 'ESOCKETTIMEDOUT'];
/**
 * Determine if a request should be retried.
 * (Borrowed from segmentio/superagent-retry)
 *
 * @param {Error} err an error
 * @param {Response} [res] response
 * @returns {Boolean} if segment should be retried
 */

RequestBase.prototype._shouldRetry = function (err, res) {
  if (!this._maxRetries || this._retries++ >= this._maxRetries) {
    return false;
  }

  if (this._retryCallback) {
    try {
      var override = this._retryCallback(err, res);

      if (override === true) return true;
      if (override === false) return false; // undefined falls back to defaults
    } catch (err_) {
      console.error(err_);
    }
  }

  if (res && res.status && res.status >= 500 && res.status !== 501) return true;

  if (err) {
    if (err.code && ERROR_CODES.includes(err.code)) return true; // Superagent timeout

    if (err.timeout && err.code === 'ECONNABORTED') return true;
    if (err.crossDomain) return true;
  }

  return false;
};
/**
 * Retry request
 *
 * @return {Request} for chaining
 * @api private
 */


RequestBase.prototype._retry = function () {
  this.clearTimeout(); // node

  if (this.req) {
    this.req = null;
    this.req = this.request();
  }

  this._aborted = false;
  this.timedout = false;
  this.timedoutError = null;
  return this._end();
};
/**
 * Promise support
 *
 * @param {Function} resolve
 * @param {Function} [reject]
 * @return {Request}
 */


RequestBase.prototype.then = function (resolve, reject) {
  var _this = this;

  if (!this._fullfilledPromise) {
    var self = this;

    if (this._endCalled) {
      console.warn('Warning: superagent request was sent twice, because both .end() and .then() were called. Never call .end() if you use promises');
    }

    this._fullfilledPromise = new Promise(function (resolve, reject) {
      self.on('abort', function () {
        if (_this.timedout && _this.timedoutError) {
          reject(_this.timedoutError);
          return;
        }

        var err = new Error('Aborted');
        err.code = 'ABORTED';
        err.status = _this.status;
        err.method = _this.method;
        err.url = _this.url;
        reject(err);
      });
      self.end(function (err, res) {
        if (err) reject(err);else resolve(res);
      });
    });
  }

  return this._fullfilledPromise.then(resolve, reject);
};

RequestBase.prototype.catch = function (cb) {
  return this.then(undefined, cb);
};
/**
 * Allow for extension
 */


RequestBase.prototype.use = function (fn) {
  fn(this);
  return this;
};

RequestBase.prototype.ok = function (cb) {
  if (typeof cb !== 'function') throw new Error('Callback required');
  this._okCallback = cb;
  return this;
};

RequestBase.prototype._isResponseOK = function (res) {
  if (!res) {
    return false;
  }

  if (this._okCallback) {
    return this._okCallback(res);
  }

  return res.status >= 200 && res.status < 300;
};
/**
 * Get request header `field`.
 * Case-insensitive.
 *
 * @param {String} field
 * @return {String}
 * @api public
 */


RequestBase.prototype.get = function (field) {
  return this._header[field.toLowerCase()];
};
/**
 * Get case-insensitive header `field` value.
 * This is a deprecated internal API. Use `.get(field)` instead.
 *
 * (getHeader is no longer used internally by the superagent code base)
 *
 * @param {String} field
 * @return {String}
 * @api private
 * @deprecated
 */


RequestBase.prototype.getHeader = RequestBase.prototype.get;
/**
 * Set header `field` to `val`, or multiple fields with one object.
 * Case-insensitive.
 *
 * Examples:
 *
 *      req.get('/')
 *        .set('Accept', 'application/json')
 *        .set('X-API-Key', 'foobar')
 *        .end(callback);
 *
 *      req.get('/')
 *        .set({ Accept: 'application/json', 'X-API-Key': 'foobar' })
 *        .end(callback);
 *
 * @param {String|Object} field
 * @param {String} val
 * @return {Request} for chaining
 * @api public
 */

RequestBase.prototype.set = function (field, val) {
  if (isObject(field)) {
    for (var key in field) {
      if (Object.prototype.hasOwnProperty.call(field, key)) this.set(key, field[key]);
    }

    return this;
  }

  this._header[field.toLowerCase()] = val;
  this.header[field] = val;
  return this;
};
/**
 * Remove header `field`.
 * Case-insensitive.
 *
 * Example:
 *
 *      req.get('/')
 *        .unset('User-Agent')
 *        .end(callback);
 *
 * @param {String} field field name
 */


RequestBase.prototype.unset = function (field) {
  delete this._header[field.toLowerCase()];
  delete this.header[field];
  return this;
};
/**
 * Write the field `name` and `val`, or multiple fields with one object
 * for "multipart/form-data" request bodies.
 *
 * ``` js
 * request.post('/upload')
 *   .field('foo', 'bar')
 *   .end(callback);
 *
 * request.post('/upload')
 *   .field({ foo: 'bar', baz: 'qux' })
 *   .end(callback);
 * ```
 *
 * @param {String|Object} name name of field
 * @param {String|Blob|File|Buffer|fs.ReadStream} val value of field
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.field = function (name, val) {
  // name should be either a string or an object.
  if (name === null || undefined === name) {
    throw new Error('.field(name, val) name can not be empty');
  }

  if (this._data) {
    throw new Error(".field() can't be used if .send() is used. Please use only .send() or only .field() & .attach()");
  }

  if (isObject(name)) {
    for (var key in name) {
      if (Object.prototype.hasOwnProperty.call(name, key)) this.field(key, name[key]);
    }

    return this;
  }

  if (Array.isArray(val)) {
    for (var i in val) {
      if (Object.prototype.hasOwnProperty.call(val, i)) this.field(name, val[i]);
    }

    return this;
  } // val should be defined now


  if (val === null || undefined === val) {
    throw new Error('.field(name, val) val can not be empty');
  }

  if (typeof val === 'boolean') {
    val = String(val);
  }

  this._getFormData().append(name, val);

  return this;
};
/**
 * Abort the request, and clear potential timeout.
 *
 * @return {Request} request
 * @api public
 */


RequestBase.prototype.abort = function () {
  if (this._aborted) {
    return this;
  }

  this._aborted = true;
  if (this.xhr) this.xhr.abort(); // browser

  if (this.req) this.req.abort(); // node

  this.clearTimeout();
  this.emit('abort');
  return this;
};

RequestBase.prototype._auth = function (user, pass, options, base64Encoder) {
  switch (options.type) {
    case 'basic':
      this.set('Authorization', "Basic ".concat(base64Encoder("".concat(user, ":").concat(pass))));
      break;

    case 'auto':
      this.username = user;
      this.password = pass;
      break;

    case 'bearer':
      // usage would be .auth(accessToken, { type: 'bearer' })
      this.set('Authorization', "Bearer ".concat(user));
      break;

    default:
      break;
  }

  return this;
};
/**
 * Enable transmission of cookies with x-domain requests.
 *
 * Note that for this to work the origin must not be
 * using "Access-Control-Allow-Origin" with a wildcard,
 * and also must set "Access-Control-Allow-Credentials"
 * to "true".
 *
 * @api public
 */


RequestBase.prototype.withCredentials = function (on) {
  // This is browser-only functionality. Node side is no-op.
  if (on === undefined) on = true;
  this._withCredentials = on;
  return this;
};
/**
 * Set the max redirects to `n`. Does nothing in browser XHR implementation.
 *
 * @param {Number} n
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.redirects = function (n) {
  this._maxRedirects = n;
  return this;
};
/**
 * Maximum size of buffered response body, in bytes. Counts uncompressed size.
 * Default 200MB.
 *
 * @param {Number} n number of bytes
 * @return {Request} for chaining
 */


RequestBase.prototype.maxResponseSize = function (n) {
  if (typeof n !== 'number') {
    throw new TypeError('Invalid argument');
  }

  this._maxResponseSize = n;
  return this;
};
/**
 * Convert to a plain javascript object (not JSON string) of scalar properties.
 * Note as this method is designed to return a useful non-this value,
 * it cannot be chained.
 *
 * @return {Object} describing method, url, and data of this request
 * @api public
 */


RequestBase.prototype.toJSON = function () {
  return {
    method: this.method,
    url: this.url,
    data: this._data,
    headers: this._header
  };
};
/**
 * Send `data` as the request body, defaulting the `.type()` to "json" when
 * an object is given.
 *
 * Examples:
 *
 *       // manual json
 *       request.post('/user')
 *         .type('json')
 *         .send('{"name":"tj"}')
 *         .end(callback)
 *
 *       // auto json
 *       request.post('/user')
 *         .send({ name: 'tj' })
 *         .end(callback)
 *
 *       // manual x-www-form-urlencoded
 *       request.post('/user')
 *         .type('form')
 *         .send('name=tj')
 *         .end(callback)
 *
 *       // auto x-www-form-urlencoded
 *       request.post('/user')
 *         .type('form')
 *         .send({ name: 'tj' })
 *         .end(callback)
 *
 *       // defaults to x-www-form-urlencoded
 *      request.post('/user')
 *        .send('name=tobi')
 *        .send('species=ferret')
 *        .end(callback)
 *
 * @param {String|Object} data
 * @return {Request} for chaining
 * @api public
 */
// eslint-disable-next-line complexity


RequestBase.prototype.send = function (data) {
  var isObj = isObject(data);
  var type = this._header['content-type'];

  if (this._formData) {
    throw new Error(".send() can't be used if .attach() or .field() is used. Please use only .send() or only .field() & .attach()");
  }

  if (isObj && !this._data) {
    if (Array.isArray(data)) {
      this._data = [];
    } else if (!this._isHost(data)) {
      this._data = {};
    }
  } else if (data && this._data && this._isHost(this._data)) {
    throw new Error("Can't merge these send calls");
  } // merge


  if (isObj && isObject(this._data)) {
    for (var key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) this._data[key] = data[key];
    }
  } else if (typeof data === 'string') {
    // default to x-www-form-urlencoded
    if (!type) this.type('form');
    type = this._header['content-type'];

    if (type === 'application/x-www-form-urlencoded') {
      this._data = this._data ? "".concat(this._data, "&").concat(data) : data;
    } else {
      this._data = (this._data || '') + data;
    }
  } else {
    this._data = data;
  }

  if (!isObj || this._isHost(data)) {
    return this;
  } // default to json


  if (!type) this.type('json');
  return this;
};
/**
 * Sort `querystring` by the sort function
 *
 *
 * Examples:
 *
 *       // default order
 *       request.get('/user')
 *         .query('name=Nick')
 *         .query('search=Manny')
 *         .sortQuery()
 *         .end(callback)
 *
 *       // customized sort function
 *       request.get('/user')
 *         .query('name=Nick')
 *         .query('search=Manny')
 *         .sortQuery(function(a, b){
 *           return a.length - b.length;
 *         })
 *         .end(callback)
 *
 *
 * @param {Function} sort
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.sortQuery = function (sort) {
  // _sort default to true but otherwise can be a function or boolean
  this._sort = typeof sort === 'undefined' ? true : sort;
  return this;
};
/**
 * Compose querystring to append to req.url
 *
 * @api private
 */


RequestBase.prototype._finalizeQueryString = function () {
  var query = this._query.join('&');

  if (query) {
    this.url += (this.url.includes('?') ? '&' : '?') + query;
  }

  this._query.length = 0; // Makes the call idempotent

  if (this._sort) {
    var index = this.url.indexOf('?');

    if (index >= 0) {
      var queryArr = this.url.slice(index + 1).split('&');

      if (typeof this._sort === 'function') {
        queryArr.sort(this._sort);
      } else {
        queryArr.sort();
      }

      this.url = this.url.slice(0, index) + '?' + queryArr.join('&');
    }
  }
}; // For backwards compat only


RequestBase.prototype._appendQueryString = function () {
  console.warn('Unsupported');
};
/**
 * Invoke callback with timeout error.
 *
 * @api private
 */


RequestBase.prototype._timeoutError = function (reason, timeout, errno) {
  if (this._aborted) {
    return;
  }

  var err = new Error("".concat(reason + timeout, "ms exceeded"));
  err.timeout = timeout;
  err.code = 'ECONNABORTED';
  err.errno = errno;
  this.timedout = true;
  this.timedoutError = err;
  this.abort();
  this.callback(err);
};

RequestBase.prototype._setTimeouts = function () {
  var self = this; // deadline

  if (this._timeout && !this._timer) {
    this._timer = setTimeout(function () {
      self._timeoutError('Timeout of ', self._timeout, 'ETIME');
    }, this._timeout);
  } // response timeout


  if (this._responseTimeout && !this._responseTimeoutTimer) {
    this._responseTimeoutTimer = setTimeout(function () {
      self._timeoutError('Response timeout of ', self._responseTimeout, 'ETIMEDOUT');
    }, this._responseTimeout);
  }
};

/***/ }),

/***/ "./node_modules/superagent/lib/response-base.js":
/*!******************************************************!*\
  !*** ./node_modules/superagent/lib/response-base.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Module dependencies.
 */

var utils = __webpack_require__(/*! ./utils */ "./node_modules/superagent/lib/utils.js");
/**
 * Expose `ResponseBase`.
 */


module.exports = ResponseBase;
/**
 * Initialize a new `ResponseBase`.
 *
 * @api public
 */

function ResponseBase(obj) {
  if (obj) return mixin(obj);
}
/**
 * Mixin the prototype properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */


function mixin(obj) {
  for (var key in ResponseBase.prototype) {
    if (Object.prototype.hasOwnProperty.call(ResponseBase.prototype, key)) obj[key] = ResponseBase.prototype[key];
  }

  return obj;
}
/**
 * Get case-insensitive `field` value.
 *
 * @param {String} field
 * @return {String}
 * @api public
 */


ResponseBase.prototype.get = function (field) {
  return this.header[field.toLowerCase()];
};
/**
 * Set header related properties:
 *
 *   - `.type` the content type without params
 *
 * A response of "Content-Type: text/plain; charset=utf-8"
 * will provide you with a `.type` of "text/plain".
 *
 * @param {Object} header
 * @api private
 */


ResponseBase.prototype._setHeaderProperties = function (header) {
  // TODO: moar!
  // TODO: make this a util
  // content-type
  var ct = header['content-type'] || '';
  this.type = utils.type(ct); // params

  var params = utils.params(ct);

  for (var key in params) {
    if (Object.prototype.hasOwnProperty.call(params, key)) this[key] = params[key];
  }

  this.links = {}; // links

  try {
    if (header.link) {
      this.links = utils.parseLinks(header.link);
    }
  } catch (_unused) {// ignore
  }
};
/**
 * Set flags such as `.ok` based on `status`.
 *
 * For example a 2xx response will give you a `.ok` of __true__
 * whereas 5xx will be __false__ and `.error` will be __true__. The
 * `.clientError` and `.serverError` are also available to be more
 * specific, and `.statusType` is the class of error ranging from 1..5
 * sometimes useful for mapping respond colors etc.
 *
 * "sugar" properties are also defined for common cases. Currently providing:
 *
 *   - .noContent
 *   - .badRequest
 *   - .unauthorized
 *   - .notAcceptable
 *   - .notFound
 *
 * @param {Number} status
 * @api private
 */


ResponseBase.prototype._setStatusProperties = function (status) {
  var type = status / 100 | 0; // status / class

  this.statusCode = status;
  this.status = this.statusCode;
  this.statusType = type; // basics

  this.info = type === 1;
  this.ok = type === 2;
  this.redirect = type === 3;
  this.clientError = type === 4;
  this.serverError = type === 5;
  this.error = type === 4 || type === 5 ? this.toError() : false; // sugar

  this.created = status === 201;
  this.accepted = status === 202;
  this.noContent = status === 204;
  this.badRequest = status === 400;
  this.unauthorized = status === 401;
  this.notAcceptable = status === 406;
  this.forbidden = status === 403;
  this.notFound = status === 404;
  this.unprocessableEntity = status === 422;
};

/***/ }),

/***/ "./node_modules/superagent/lib/utils.js":
/*!**********************************************!*\
  !*** ./node_modules/superagent/lib/utils.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Return the mime type for the given `str`.
 *
 * @param {String} str
 * @return {String}
 * @api private
 */

exports.type = function (str) {
  return str.split(/ *; */).shift();
};
/**
 * Return header field parameters.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */


exports.params = function (str) {
  return str.split(/ *; */).reduce(function (obj, str) {
    var parts = str.split(/ *= */);
    var key = parts.shift();
    var val = parts.shift();
    if (key && val) obj[key] = val;
    return obj;
  }, {});
};
/**
 * Parse Link header fields.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */


exports.parseLinks = function (str) {
  return str.split(/ *, */).reduce(function (obj, str) {
    var parts = str.split(/ *; */);
    var url = parts[0].slice(1, -1);
    var rel = parts[1].split(/ *= */)[1].slice(1, -1);
    obj[rel] = url;
    return obj;
  }, {});
};
/**
 * Strip content related fields from `header`.
 *
 * @param {Object} header
 * @return {Object} header
 * @api private
 */


exports.cleanHeader = function (header, changesOrigin) {
  delete header['content-type'];
  delete header['content-length'];
  delete header['transfer-encoding'];
  delete header.host; // secuirty

  if (changesOrigin) {
    delete header.authorization;
    delete header.cookie;
  }

  return header;
};

/***/ }),

/***/ "./node_modules/validate.js/validate.js":
/*!**********************************************!*\
  !*** ./node_modules/validate.js/validate.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {/*!
 * validate.js 0.13.1
 *
 * (c) 2013-2019 Nicklas Ansman, 2013 Wrapp
 * Validate.js may be freely distributed under the MIT license.
 * For all details and documentation:
 * http://validatejs.org/
 */
(function (exports, module, define) {
  "use strict"; // The main function that calls the validators specified by the constraints.
  // The options are the following:
  //   - format (string) - An option that controls how the returned value is formatted
  //     * flat - Returns a flat array of just the error messages
  //     * grouped - Returns the messages grouped by attribute (default)
  //     * detailed - Returns an array of the raw validation data
  //   - fullMessages (boolean) - If `true` (default) the attribute name is prepended to the error.
  //
  // Please note that the options are also passed to each validator.

  var validate = function (attributes, constraints, options) {
    options = v.extend({}, v.options, options);
    var results = v.runValidations(attributes, constraints, options),
        attr,
        validator;

    if (results.some(function (r) {
      return v.isPromise(r.error);
    })) {
      throw new Error("Use validate.async if you want support for promises");
    }

    return validate.processValidationResults(results, options);
  };

  var v = validate; // Copies over attributes from one or more sources to a single destination.
  // Very much similar to underscore's extend.
  // The first argument is the target object and the remaining arguments will be
  // used as sources.

  v.extend = function (obj) {
    [].slice.call(arguments, 1).forEach(function (source) {
      for (var attr in source) {
        obj[attr] = source[attr];
      }
    });
    return obj;
  };

  v.extend(validate, {
    // This is the version of the library as a semver.
    // The toString function will allow it to be coerced into a string
    version: {
      major: 0,
      minor: 13,
      patch: 1,
      metadata: null,
      toString: function () {
        var version = v.format("%{major}.%{minor}.%{patch}", v.version);

        if (!v.isEmpty(v.version.metadata)) {
          version += "+" + v.version.metadata;
        }

        return version;
      }
    },
    // Below is the dependencies that are used in validate.js
    // The constructor of the Promise implementation.
    // If you are using Q.js, RSVP or any other A+ compatible implementation
    // override this attribute to be the constructor of that promise.
    // Since jQuery promises aren't A+ compatible they won't work.
    Promise: typeof Promise !== "undefined" ? Promise :
    /* istanbul ignore next */
    null,
    EMPTY_STRING_REGEXP: /^\s*$/,
    // Runs the validators specified by the constraints object.
    // Will return an array of the format:
    //     [{attribute: "<attribute name>", error: "<validation result>"}, ...]
    runValidations: function (attributes, constraints, options) {
      var results = [],
          attr,
          validatorName,
          value,
          validators,
          validator,
          validatorOptions,
          error;

      if (v.isDomElement(attributes) || v.isJqueryElement(attributes)) {
        attributes = v.collectFormValues(attributes);
      } // Loops through each constraints, finds the correct validator and run it.


      for (attr in constraints) {
        value = v.getDeepObjectValue(attributes, attr); // This allows the constraints for an attribute to be a function.
        // The function will be called with the value, attribute name, the complete dict of
        // attributes as well as the options and constraints passed in.
        // This is useful when you want to have different
        // validations depending on the attribute value.

        validators = v.result(constraints[attr], value, attributes, attr, options, constraints);

        for (validatorName in validators) {
          validator = v.validators[validatorName];

          if (!validator) {
            error = v.format("Unknown validator %{name}", {
              name: validatorName
            });
            throw new Error(error);
          }

          validatorOptions = validators[validatorName]; // This allows the options to be a function. The function will be
          // called with the value, attribute name, the complete dict of
          // attributes as well as the options and constraints passed in.
          // This is useful when you want to have different
          // validations depending on the attribute value.

          validatorOptions = v.result(validatorOptions, value, attributes, attr, options, constraints);

          if (!validatorOptions) {
            continue;
          }

          results.push({
            attribute: attr,
            value: value,
            validator: validatorName,
            globalOptions: options,
            attributes: attributes,
            options: validatorOptions,
            error: validator.call(validator, value, validatorOptions, attr, attributes, options)
          });
        }
      }

      return results;
    },
    // Takes the output from runValidations and converts it to the correct
    // output format.
    processValidationResults: function (errors, options) {
      errors = v.pruneEmptyErrors(errors, options);
      errors = v.expandMultipleErrors(errors, options);
      errors = v.convertErrorMessages(errors, options);
      var format = options.format || "grouped";

      if (typeof v.formatters[format] === 'function') {
        errors = v.formatters[format](errors);
      } else {
        throw new Error(v.format("Unknown format %{format}", options));
      }

      return v.isEmpty(errors) ? undefined : errors;
    },
    // Runs the validations with support for promises.
    // This function will return a promise that is settled when all the
    // validation promises have been completed.
    // It can be called even if no validations returned a promise.
    async: function (attributes, constraints, options) {
      options = v.extend({}, v.async.options, options);

      var WrapErrors = options.wrapErrors || function (errors) {
        return errors;
      }; // Removes unknown attributes


      if (options.cleanAttributes !== false) {
        attributes = v.cleanAttributes(attributes, constraints);
      }

      var results = v.runValidations(attributes, constraints, options);
      return new v.Promise(function (resolve, reject) {
        v.waitForResults(results).then(function () {
          var errors = v.processValidationResults(results, options);

          if (errors) {
            reject(new WrapErrors(errors, options, attributes, constraints));
          } else {
            resolve(attributes);
          }
        }, function (err) {
          reject(err);
        });
      });
    },
    single: function (value, constraints, options) {
      options = v.extend({}, v.single.options, options, {
        format: "flat",
        fullMessages: false
      });
      return v({
        single: value
      }, {
        single: constraints
      }, options);
    },
    // Returns a promise that is resolved when all promises in the results array
    // are settled. The promise returned from this function is always resolved,
    // never rejected.
    // This function modifies the input argument, it replaces the promises
    // with the value returned from the promise.
    waitForResults: function (results) {
      // Create a sequence of all the results starting with a resolved promise.
      return results.reduce(function (memo, result) {
        // If this result isn't a promise skip it in the sequence.
        if (!v.isPromise(result.error)) {
          return memo;
        }

        return memo.then(function () {
          return result.error.then(function (error) {
            result.error = error || null;
          });
        });
      }, new v.Promise(function (r) {
        r();
      })); // A resolved promise
    },
    // If the given argument is a call: function the and: function return the value
    // otherwise just return the value. Additional arguments will be passed as
    // arguments to the function.
    // Example:
    // ```
    // result('foo') // 'foo'
    // result(Math.max, 1, 2) // 2
    // ```
    result: function (value) {
      var args = [].slice.call(arguments, 1);

      if (typeof value === 'function') {
        value = value.apply(null, args);
      }

      return value;
    },
    // Checks if the value is a number. This function does not consider NaN a
    // number like many other `isNumber` functions do.
    isNumber: function (value) {
      return typeof value === 'number' && !isNaN(value);
    },
    // Returns false if the object is not a function
    isFunction: function (value) {
      return typeof value === 'function';
    },
    // A simple check to verify that the value is an integer. Uses `isNumber`
    // and a simple modulo check.
    isInteger: function (value) {
      return v.isNumber(value) && value % 1 === 0;
    },
    // Checks if the value is a boolean
    isBoolean: function (value) {
      return typeof value === 'boolean';
    },
    // Uses the `Object` function to check if the given argument is an object.
    isObject: function (obj) {
      return obj === Object(obj);
    },
    // Simply checks if the object is an instance of a date
    isDate: function (obj) {
      return obj instanceof Date;
    },
    // Returns false if the object is `null` of `undefined`
    isDefined: function (obj) {
      return obj !== null && obj !== undefined;
    },
    // Checks if the given argument is a promise. Anything with a `then`
    // function is considered a promise.
    isPromise: function (p) {
      return !!p && v.isFunction(p.then);
    },
    isJqueryElement: function (o) {
      return o && v.isString(o.jquery);
    },
    isDomElement: function (o) {
      if (!o) {
        return false;
      }

      if (!o.querySelectorAll || !o.querySelector) {
        return false;
      }

      if (v.isObject(document) && o === document) {
        return true;
      } // http://stackoverflow.com/a/384380/699304

      /* istanbul ignore else */


      if (typeof HTMLElement === "object") {
        return o instanceof HTMLElement;
      } else {
        return o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName === "string";
      }
    },
    isEmpty: function (value) {
      var attr; // Null and undefined are empty

      if (!v.isDefined(value)) {
        return true;
      } // functions are non empty


      if (v.isFunction(value)) {
        return false;
      } // Whitespace only strings are empty


      if (v.isString(value)) {
        return v.EMPTY_STRING_REGEXP.test(value);
      } // For arrays we use the length property


      if (v.isArray(value)) {
        return value.length === 0;
      } // Dates have no attributes but aren't empty


      if (v.isDate(value)) {
        return false;
      } // If we find at least one property we consider it non empty


      if (v.isObject(value)) {
        for (attr in value) {
          return false;
        }

        return true;
      }

      return false;
    },
    // Formats the specified strings with the given values like so:
    // ```
    // format("Foo: %{foo}", {foo: "bar"}) // "Foo bar"
    // ```
    // If you want to write %{...} without having it replaced simply
    // prefix it with % like this `Foo: %%{foo}` and it will be returned
    // as `"Foo: %{foo}"`
    format: v.extend(function (str, vals) {
      if (!v.isString(str)) {
        return str;
      }

      return str.replace(v.format.FORMAT_REGEXP, function (m0, m1, m2) {
        if (m1 === '%') {
          return "%{" + m2 + "}";
        } else {
          return String(vals[m2]);
        }
      });
    }, {
      // Finds %{key} style patterns in the given string
      FORMAT_REGEXP: /(%?)%\{([^\}]+)\}/g
    }),
    // "Prettifies" the given string.
    // Prettifying means replacing [.\_-] with spaces as well as splitting
    // camel case words.
    prettify: function (str) {
      if (v.isNumber(str)) {
        // If there are more than 2 decimals round it to two
        if (str * 100 % 1 === 0) {
          return "" + str;
        } else {
          return parseFloat(Math.round(str * 100) / 100).toFixed(2);
        }
      }

      if (v.isArray(str)) {
        return str.map(function (s) {
          return v.prettify(s);
        }).join(", ");
      }

      if (v.isObject(str)) {
        if (!v.isDefined(str.toString)) {
          return JSON.stringify(str);
        }

        return str.toString();
      } // Ensure the string is actually a string


      str = "" + str;
      return str // Splits keys separated by periods
      .replace(/([^\s])\.([^\s])/g, '$1 $2') // Removes backslashes
      .replace(/\\+/g, '') // Replaces - and - with space
      .replace(/[_-]/g, ' ') // Splits camel cased words
      .replace(/([a-z])([A-Z])/g, function (m0, m1, m2) {
        return "" + m1 + " " + m2.toLowerCase();
      }).toLowerCase();
    },
    stringifyValue: function (value, options) {
      var prettify = options && options.prettify || v.prettify;
      return prettify(value);
    },
    isString: function (value) {
      return typeof value === 'string';
    },
    isArray: function (value) {
      return {}.toString.call(value) === '[object Array]';
    },
    // Checks if the object is a hash, which is equivalent to an object that
    // is neither an array nor a function.
    isHash: function (value) {
      return v.isObject(value) && !v.isArray(value) && !v.isFunction(value);
    },
    contains: function (obj, value) {
      if (!v.isDefined(obj)) {
        return false;
      }

      if (v.isArray(obj)) {
        return obj.indexOf(value) !== -1;
      }

      return value in obj;
    },
    unique: function (array) {
      if (!v.isArray(array)) {
        return array;
      }

      return array.filter(function (el, index, array) {
        return array.indexOf(el) == index;
      });
    },
    forEachKeyInKeypath: function (object, keypath, callback) {
      if (!v.isString(keypath)) {
        return undefined;
      }

      var key = "",
          i,
          escape = false;

      for (i = 0; i < keypath.length; ++i) {
        switch (keypath[i]) {
          case '.':
            if (escape) {
              escape = false;
              key += '.';
            } else {
              object = callback(object, key, false);
              key = "";
            }

            break;

          case '\\':
            if (escape) {
              escape = false;
              key += '\\';
            } else {
              escape = true;
            }

            break;

          default:
            escape = false;
            key += keypath[i];
            break;
        }
      }

      return callback(object, key, true);
    },
    getDeepObjectValue: function (obj, keypath) {
      if (!v.isObject(obj)) {
        return undefined;
      }

      return v.forEachKeyInKeypath(obj, keypath, function (obj, key) {
        if (v.isObject(obj)) {
          return obj[key];
        }
      });
    },
    // This returns an object with all the values of the form.
    // It uses the input name as key and the value as value
    // So for example this:
    // <input type="text" name="email" value="foo@bar.com" />
    // would return:
    // {email: "foo@bar.com"}
    collectFormValues: function (form, options) {
      var values = {},
          i,
          j,
          input,
          inputs,
          option,
          value;

      if (v.isJqueryElement(form)) {
        form = form[0];
      }

      if (!form) {
        return values;
      }

      options = options || {};
      inputs = form.querySelectorAll("input[name], textarea[name]");

      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);

        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        var name = input.name.replace(/\./g, "\\\\.");
        value = v.sanitizeFormValue(input.value, options);

        if (input.type === "number") {
          value = value ? +value : null;
        } else if (input.type === "checkbox") {
          if (input.attributes.value) {
            if (!input.checked) {
              value = values[name] || null;
            }
          } else {
            value = input.checked;
          }
        } else if (input.type === "radio") {
          if (!input.checked) {
            value = values[name] || null;
          }
        }

        values[name] = value;
      }

      inputs = form.querySelectorAll("select[name]");

      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);

        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        if (input.multiple) {
          value = [];

          for (j in input.options) {
            option = input.options[j];

            if (option && option.selected) {
              value.push(v.sanitizeFormValue(option.value, options));
            }
          }
        } else {
          var _val = typeof input.options[input.selectedIndex] !== 'undefined' ? input.options[input.selectedIndex].value :
          /* istanbul ignore next */
          '';

          value = v.sanitizeFormValue(_val, options);
        }

        values[input.name] = value;
      }

      return values;
    },
    sanitizeFormValue: function (value, options) {
      if (options.trim && v.isString(value)) {
        value = value.trim();
      }

      if (options.nullify !== false && value === "") {
        return null;
      }

      return value;
    },
    capitalize: function (str) {
      if (!v.isString(str)) {
        return str;
      }

      return str[0].toUpperCase() + str.slice(1);
    },
    // Remove all errors who's error attribute is empty (null or undefined)
    pruneEmptyErrors: function (errors) {
      return errors.filter(function (error) {
        return !v.isEmpty(error.error);
      });
    },
    // In
    // [{error: ["err1", "err2"], ...}]
    // Out
    // [{error: "err1", ...}, {error: "err2", ...}]
    //
    // All attributes in an error with multiple messages are duplicated
    // when expanding the errors.
    expandMultipleErrors: function (errors) {
      var ret = [];
      errors.forEach(function (error) {
        // Removes errors without a message
        if (v.isArray(error.error)) {
          error.error.forEach(function (msg) {
            ret.push(v.extend({}, error, {
              error: msg
            }));
          });
        } else {
          ret.push(error);
        }
      });
      return ret;
    },
    // Converts the error mesages by prepending the attribute name unless the
    // message is prefixed by ^
    convertErrorMessages: function (errors, options) {
      options = options || {};
      var ret = [],
          prettify = options.prettify || v.prettify;
      errors.forEach(function (errorInfo) {
        var error = v.result(errorInfo.error, errorInfo.value, errorInfo.attribute, errorInfo.options, errorInfo.attributes, errorInfo.globalOptions);

        if (!v.isString(error)) {
          ret.push(errorInfo);
          return;
        }

        if (error[0] === '^') {
          error = error.slice(1);
        } else if (options.fullMessages !== false) {
          error = v.capitalize(prettify(errorInfo.attribute)) + " " + error;
        }

        error = error.replace(/\\\^/g, "^");
        error = v.format(error, {
          value: v.stringifyValue(errorInfo.value, options)
        });
        ret.push(v.extend({}, errorInfo, {
          error: error
        }));
      });
      return ret;
    },
    // In:
    // [{attribute: "<attributeName>", ...}]
    // Out:
    // {"<attributeName>": [{attribute: "<attributeName>", ...}]}
    groupErrorsByAttribute: function (errors) {
      var ret = {};
      errors.forEach(function (error) {
        var list = ret[error.attribute];

        if (list) {
          list.push(error);
        } else {
          ret[error.attribute] = [error];
        }
      });
      return ret;
    },
    // In:
    // [{error: "<message 1>", ...}, {error: "<message 2>", ...}]
    // Out:
    // ["<message 1>", "<message 2>"]
    flattenErrorsToArray: function (errors) {
      return errors.map(function (error) {
        return error.error;
      }).filter(function (value, index, self) {
        return self.indexOf(value) === index;
      });
    },
    cleanAttributes: function (attributes, whitelist) {
      function whitelistCreator(obj, key, last) {
        if (v.isObject(obj[key])) {
          return obj[key];
        }

        return obj[key] = last ? true : {};
      }

      function buildObjectWhitelist(whitelist) {
        var ow = {},
            lastObject,
            attr;

        for (attr in whitelist) {
          if (!whitelist[attr]) {
            continue;
          }

          v.forEachKeyInKeypath(ow, attr, whitelistCreator);
        }

        return ow;
      }

      function cleanRecursive(attributes, whitelist) {
        if (!v.isObject(attributes)) {
          return attributes;
        }

        var ret = v.extend({}, attributes),
            w,
            attribute;

        for (attribute in attributes) {
          w = whitelist[attribute];

          if (v.isObject(w)) {
            ret[attribute] = cleanRecursive(ret[attribute], w);
          } else if (!w) {
            delete ret[attribute];
          }
        }

        return ret;
      }

      if (!v.isObject(whitelist) || !v.isObject(attributes)) {
        return {};
      }

      whitelist = buildObjectWhitelist(whitelist);
      return cleanRecursive(attributes, whitelist);
    },
    exposeModule: function (validate, root, exports, module, define) {
      if (exports) {
        if (module && module.exports) {
          exports = module.exports = validate;
        }

        exports.validate = validate;
      } else {
        root.validate = validate;

        if (validate.isFunction(define) && define.amd) {
          define([], function () {
            return validate;
          });
        }
      }
    },
    warn: function (msg) {
      if (typeof console !== "undefined" && console.warn) {
        console.warn("[validate.js] " + msg);
      }
    },
    error: function (msg) {
      if (typeof console !== "undefined" && console.error) {
        console.error("[validate.js] " + msg);
      }
    }
  });
  validate.validators = {
    // Presence validates that the value isn't empty
    presence: function (value, options) {
      options = v.extend({}, this.options, options);

      if (options.allowEmpty !== false ? !v.isDefined(value) : v.isEmpty(value)) {
        return options.message || this.message || "can't be blank";
      }
    },
    length: function (value, options, attribute) {
      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var is = options.is,
          maximum = options.maximum,
          minimum = options.minimum,
          tokenizer = options.tokenizer || function (val) {
        return val;
      },
          err,
          errors = [];

      value = tokenizer(value);
      var length = value.length;

      if (!v.isNumber(length)) {
        return options.message || this.notValid || "has an incorrect length";
      } // Is checks


      if (v.isNumber(is) && length !== is) {
        err = options.wrongLength || this.wrongLength || "is the wrong length (should be %{count} characters)";
        errors.push(v.format(err, {
          count: is
        }));
      }

      if (v.isNumber(minimum) && length < minimum) {
        err = options.tooShort || this.tooShort || "is too short (minimum is %{count} characters)";
        errors.push(v.format(err, {
          count: minimum
        }));
      }

      if (v.isNumber(maximum) && length > maximum) {
        err = options.tooLong || this.tooLong || "is too long (maximum is %{count} characters)";
        errors.push(v.format(err, {
          count: maximum
        }));
      }

      if (errors.length > 0) {
        return options.message || errors;
      }
    },
    numericality: function (value, options, attribute, attributes, globalOptions) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);
      var errors = [],
          name,
          count,
          checks = {
        greaterThan: function (v, c) {
          return v > c;
        },
        greaterThanOrEqualTo: function (v, c) {
          return v >= c;
        },
        equalTo: function (v, c) {
          return v === c;
        },
        lessThan: function (v, c) {
          return v < c;
        },
        lessThanOrEqualTo: function (v, c) {
          return v <= c;
        },
        divisibleBy: function (v, c) {
          return v % c === 0;
        }
      },
          prettify = options.prettify || globalOptions && globalOptions.prettify || v.prettify; // Strict will check that it is a valid looking number

      if (v.isString(value) && options.strict) {
        var pattern = "^-?(0|[1-9]\\d*)";

        if (!options.onlyInteger) {
          pattern += "(\\.\\d+)?";
        }

        pattern += "$";

        if (!new RegExp(pattern).test(value)) {
          return options.message || options.notValid || this.notValid || this.message || "must be a valid number";
        }
      } // Coerce the value to a number unless we're being strict.


      if (options.noStrings !== true && v.isString(value) && !v.isEmpty(value)) {
        value = +value;
      } // If it's not a number we shouldn't continue since it will compare it.


      if (!v.isNumber(value)) {
        return options.message || options.notValid || this.notValid || this.message || "is not a number";
      } // Same logic as above, sort of. Don't bother with comparisons if this
      // doesn't pass.


      if (options.onlyInteger && !v.isInteger(value)) {
        return options.message || options.notInteger || this.notInteger || this.message || "must be an integer";
      }

      for (name in checks) {
        count = options[name];

        if (v.isNumber(count) && !checks[name](value, count)) {
          // This picks the default message if specified
          // For example the greaterThan check uses the message from
          // this.notGreaterThan so we capitalize the name and prepend "not"
          var key = "not" + v.capitalize(name);
          var msg = options[key] || this[key] || this.message || "must be %{type} %{count}";
          errors.push(v.format(msg, {
            count: count,
            type: prettify(name)
          }));
        }
      }

      if (options.odd && value % 2 !== 1) {
        errors.push(options.notOdd || this.notOdd || this.message || "must be odd");
      }

      if (options.even && value % 2 !== 0) {
        errors.push(options.notEven || this.notEven || this.message || "must be even");
      }

      if (errors.length) {
        return options.message || errors;
      }
    },
    datetime: v.extend(function (value, options) {
      if (!v.isFunction(this.parse) || !v.isFunction(this.format)) {
        throw new Error("Both the parse and format functions needs to be set to use the datetime/date validator");
      } // Empty values are fine


      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);
      var err,
          errors = [],
          earliest = options.earliest ? this.parse(options.earliest, options) : NaN,
          latest = options.latest ? this.parse(options.latest, options) : NaN;
      value = this.parse(value, options); // 86400000 is the number of milliseconds in a day, this is used to remove
      // the time from the date

      if (isNaN(value) || options.dateOnly && value % 86400000 !== 0) {
        err = options.notValid || options.message || this.notValid || "must be a valid date";
        return v.format(err, {
          value: arguments[0]
        });
      }

      if (!isNaN(earliest) && value < earliest) {
        err = options.tooEarly || options.message || this.tooEarly || "must be no earlier than %{date}";
        err = v.format(err, {
          value: this.format(value, options),
          date: this.format(earliest, options)
        });
        errors.push(err);
      }

      if (!isNaN(latest) && value > latest) {
        err = options.tooLate || options.message || this.tooLate || "must be no later than %{date}";
        err = v.format(err, {
          date: this.format(latest, options),
          value: this.format(value, options)
        });
        errors.push(err);
      }

      if (errors.length) {
        return v.unique(errors);
      }
    }, {
      parse: null,
      format: null
    }),
    date: function (value, options) {
      options = v.extend({}, options, {
        dateOnly: true
      });
      return v.validators.datetime.call(v.validators.datetime, value, options);
    },
    format: function (value, options) {
      if (v.isString(options) || options instanceof RegExp) {
        options = {
          pattern: options
        };
      }

      options = v.extend({}, this.options, options);
      var message = options.message || this.message || "is invalid",
          pattern = options.pattern,
          match; // Empty values are allowed

      if (!v.isDefined(value)) {
        return;
      }

      if (!v.isString(value)) {
        return message;
      }

      if (v.isString(pattern)) {
        pattern = new RegExp(options.pattern, options.flags);
      }

      match = pattern.exec(value);

      if (!match || match[0].length != value.length) {
        return message;
      }
    },
    inclusion: function (value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      if (v.isArray(options)) {
        options = {
          within: options
        };
      }

      options = v.extend({}, this.options, options);

      if (v.contains(options.within, value)) {
        return;
      }

      var message = options.message || this.message || "^%{value} is not included in the list";
      return v.format(message, {
        value: value
      });
    },
    exclusion: function (value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      if (v.isArray(options)) {
        options = {
          within: options
        };
      }

      options = v.extend({}, this.options, options);

      if (!v.contains(options.within, value)) {
        return;
      }

      var message = options.message || this.message || "^%{value} is restricted";

      if (v.isString(options.within[value])) {
        value = options.within[value];
      }

      return v.format(message, {
        value: value
      });
    },
    email: v.extend(function (value, options) {
      options = v.extend({}, this.options, options);
      var message = options.message || this.message || "is not a valid email"; // Empty values are fine

      if (!v.isDefined(value)) {
        return;
      }

      if (!v.isString(value)) {
        return message;
      }

      if (!this.PATTERN.exec(value)) {
        return message;
      }
    }, {
      PATTERN: /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/i
    }),
    equality: function (value, options, attribute, attributes, globalOptions) {
      if (!v.isDefined(value)) {
        return;
      }

      if (v.isString(options)) {
        options = {
          attribute: options
        };
      }

      options = v.extend({}, this.options, options);
      var message = options.message || this.message || "is not equal to %{attribute}";

      if (v.isEmpty(options.attribute) || !v.isString(options.attribute)) {
        throw new Error("The attribute must be a non empty string");
      }

      var otherValue = v.getDeepObjectValue(attributes, options.attribute),
          comparator = options.comparator || function (v1, v2) {
        return v1 === v2;
      },
          prettify = options.prettify || globalOptions && globalOptions.prettify || v.prettify;

      if (!comparator(value, otherValue, options, attribute, attributes)) {
        return v.format(message, {
          attribute: prettify(options.attribute)
        });
      }
    },
    // A URL validator that is used to validate URLs with the ability to
    // restrict schemes and some domains.
    url: function (value, options) {
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);
      var message = options.message || this.message || "is not a valid url",
          schemes = options.schemes || this.schemes || ['http', 'https'],
          allowLocal = options.allowLocal || this.allowLocal || false,
          allowDataUrl = options.allowDataUrl || this.allowDataUrl || false;

      if (!v.isString(value)) {
        return message;
      } // https://gist.github.com/dperini/729294


      var regex = "^" + // protocol identifier
      "(?:(?:" + schemes.join("|") + ")://)" + // user:pass authentication
      "(?:\\S+(?::\\S*)?@)?" + "(?:";
      var tld = "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))";

      if (allowLocal) {
        tld += "?";
      } else {
        regex += // IP address exclusion
        // private & local networks
        "(?!(?:10|127)(?:\\.\\d{1,3}){3})" + "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" + "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})";
      }

      regex += // IP address dotted notation octets
      // excludes loopback network 0.0.0.0
      // excludes reserved space >= 224.0.0.0
      // excludes network & broacast addresses
      // (first & last IP address of each class)
      "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" + "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" + "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" + "|" + // host name
      "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" + // domain name
      "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" + tld + ")" + // port number
      "(?::\\d{2,5})?" + // resource path
      "(?:[/?#]\\S*)?" + "$";

      if (allowDataUrl) {
        // RFC 2397
        var mediaType = "\\w+\\/[-+.\\w]+(?:;[\\w=]+)*";
        var urlchar = "[A-Za-z0-9-_.!~\\*'();\\/?:@&=+$,%]*";
        var dataurl = "data:(?:" + mediaType + ")?(?:;base64)?," + urlchar;
        regex = "(?:" + regex + ")|(?:^" + dataurl + "$)";
      }

      var PATTERN = new RegExp(regex, 'i');

      if (!PATTERN.exec(value)) {
        return message;
      }
    },
    type: v.extend(function (value, originalOptions, attribute, attributes, globalOptions) {
      if (v.isString(originalOptions)) {
        originalOptions = {
          type: originalOptions
        };
      }

      if (!v.isDefined(value)) {
        return;
      }

      var options = v.extend({}, this.options, originalOptions);
      var type = options.type;

      if (!v.isDefined(type)) {
        throw new Error("No type was specified");
      }

      var check;

      if (v.isFunction(type)) {
        check = type;
      } else {
        check = this.types[type];
      }

      if (!v.isFunction(check)) {
        throw new Error("validate.validators.type.types." + type + " must be a function.");
      }

      if (!check(value, options, attribute, attributes, globalOptions)) {
        var message = originalOptions.message || this.messages[type] || this.message || options.message || (v.isFunction(type) ? "must be of the correct type" : "must be of type %{type}");

        if (v.isFunction(message)) {
          message = message(value, originalOptions, attribute, attributes, globalOptions);
        }

        return v.format(message, {
          attribute: v.prettify(attribute),
          type: type
        });
      }
    }, {
      types: {
        object: function (value) {
          return v.isObject(value) && !v.isArray(value);
        },
        array: v.isArray,
        integer: v.isInteger,
        number: v.isNumber,
        string: v.isString,
        date: v.isDate,
        boolean: v.isBoolean
      },
      messages: {}
    })
  };
  validate.formatters = {
    detailed: function (errors) {
      return errors;
    },
    flat: v.flattenErrorsToArray,
    grouped: function (errors) {
      var attr;
      errors = v.groupErrorsByAttribute(errors);

      for (attr in errors) {
        errors[attr] = v.flattenErrorsToArray(errors[attr]);
      }

      return errors;
    },
    constraint: function (errors) {
      var attr;
      errors = v.groupErrorsByAttribute(errors);

      for (attr in errors) {
        errors[attr] = errors[attr].map(function (result) {
          return result.validator;
        }).sort();
      }

      return errors;
    }
  };
  validate.exposeModule(validate, this, exports, module, __webpack_require__(/*! !webpack amd define */ "./node_modules/webpack/buildin/amd-define.js"));
}).call(this,  true ?
/* istanbul ignore next */
exports : undefined,  true ?
/* istanbul ignore next */
module : undefined, __webpack_require__(/*! !webpack amd define */ "./node_modules/webpack/buildin/amd-define.js"));
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/module.js */ "./node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "./node_modules/webpack/buildin/amd-define.js":
/*!***************************************!*\
  !*** (webpack)/buildin/amd-define.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),

/***/ "./node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (module) {
  if (!module.webpackPolyfill) {
    module.deprecate = function () {};

    module.paths = []; // module.parent = undefined by default

    if (!module.children) module.children = [];
    Object.defineProperty(module, "loaded", {
      enumerable: true,
      get: function () {
        return module.l;
      }
    });
    Object.defineProperty(module, "id", {
      enumerable: true,
      get: function () {
        return module.i;
      }
    });
    module.webpackPolyfill = 1;
  }

  return module;
};

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Cart/Cart.js":
/*!*************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Cart/Cart.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Cart; });
/* harmony import */ var _CheckoutClient__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../CheckoutClient */ "./src-presentation/js/component/Checkout/CheckoutClient.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Facade */ "./src-presentation/js/component/Checkout/Facade.js");


class Cart {
  /**
   * @return {Promise<Object>}
   */
  fetchCartPageData() {
    return new Promise(resolve => {
      _CheckoutClient__WEBPACK_IMPORTED_MODULE_0__["default"].fetchCartPageData().then(cartPage => {
        Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["setCartItems"])(cartPage.cart_items);
        Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["setVoucher"])(cartPage.voucher);
        resolve(cartPage);
      });
    });
  }
  /**
   * @param {Object} requestData
   *
   * @return {Promise}
   */


  storeCartData(requestData) {
    return new Promise((resolve, reject) => {
      _CheckoutClient__WEBPACK_IMPORTED_MODULE_0__["default"].storeCartData(requestData).then(() => {
        resolve(true);
      }).catch(errorMessage => {
        reject(errorMessage);
      });
    });
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Cart/CartUi.js":
/*!***************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Cart/CartUi.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CartUi; });
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../own */ "./src-presentation/js/component/own.js");
/* harmony import */ var _Modal_Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Modal/Facade */ "./src-presentation/js/component/Modal/Facade.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Facade */ "./src-presentation/js/component/Checkout/Facade.js");
/* harmony import */ var _kernel_Facade__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../kernel/Facade */ "./src-presentation/js/kernel/Facade.js");




class CartUi {
  /**
   * @param {Cart} cart
   */
  constructor(cart) {
    this.cart = cart;
  }
  /**
   * @return {void}
   */


  init() {
    this.bindEvents();
  }
  /**
   * @return {void}
   */


  bindEvents() {
    Object(_own__WEBPACK_IMPORTED_MODULE_0__["onLiveClick"])('.js-accept-cart', this.onAcceptCart.bind(this));
  }
  /**
   * @param {Element} containerElement
   *
   * @return {void}
   */


  async mount(containerElement) {
    if (document.querySelector('.js-accept-cart')) {
      return;
    }

    await this.cart.fetchCartPageData().then(cartPage => {
      const tempContainer = document.createElement('div');
      tempContainer.innerHTML = cartPage.cart_page_html;
      tempContainer.querySelector('.js-cart-details').innerHTML = cartPage.cart_details_html;
      containerElement.classList.add('is-hidden');
      containerElement.innerHTML = tempContainer.innerHTML;
    }).catch(() => {
      throw new Error('Could not mount CartUi');
    });
  }
  /**
   * @return {void}
   */


  onAcceptCart() {
    Object(_Modal_Facade__WEBPACK_IMPORTED_MODULE_1__["openLoadingScreen"])();
    const requestData = {
      products: {
        'id_1234': {}
      },
      voucher_code: 'babayaga'
    };
    this.cart.storeCartData(requestData).then(() => {
      Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["sayCartAccepted"])();
    }).catch(() => {
      Object(_kernel_Facade__WEBPACK_IMPORTED_MODULE_3__["sayApplicationError"])('Something went wrong. We are working on fixing the issue. Please try again later');
    });
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/CheckoutClient.js":
/*!******************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/CheckoutClient.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _kernel_BackendClient__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../kernel/BackendClient */ "./src-presentation/js/kernel/BackendClient.js");
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../own */ "./src-presentation/js/component/own.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Facade */ "./src-presentation/js/component/Checkout/Facade.js");



/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * @return {Promise<Object>}
   */
  fetchCartPageData() {
    return new Promise((resolve, reject) => {
      _kernel_BackendClient__WEBPACK_IMPORTED_MODULE_0__["default"].get(Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["getCheckoutConfig"])().endpoints.cart_page).then(cartPage => {
        Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["setCartPageConfig"])(cartPage.body.cart_page_config);
        resolve({
          'cart_page_html': cartPage.body.cart_page_html,
          'cart_details_html': cartPage.body.cart_details_html,
          'cart_items': cartPage.body.cart_items,
          'voucher': cartPage.body.voucher
        });
      }).catch(error => {
        reject(error);
      });
    });
  },

  /**
   *
   * @param {Object} requestData
   *
   * @return {Promise<unknown>}
   */
  storeCartData(requestData) {
    return new Promise((resolve, reject) => {
      _kernel_BackendClient__WEBPACK_IMPORTED_MODULE_0__["default"].post(Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["getCartPageConfig"])().endpoints.store_cart_data, requestData).then(() => {
        resolve(true);
      }).catch(error => {
        reject(error);
      });
    });
  },

  /**
   * @return {Promise<Object>}
   */
  fetchDeliveryPageData() {
    return new Promise((resolve, reject) => {
      _kernel_BackendClient__WEBPACK_IMPORTED_MODULE_0__["default"].get(Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["getCheckoutConfig"])().endpoints.delivery_page).then(deliveryPage => {
        Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["setDeliveryPageConfig"])(deliveryPage.body.delivery_page_config);
        resolve({
          'delivery_page_html': deliveryPage.body.delivery_page_html
        });
      }).catch(error => {
        reject(error);
      });
    });
  },

  /**
   *
   * @param {FormData} deliveryData
   *
   * @return {Promise<unknown>}
   */
  storeDeliveryData(deliveryData) {
    return new Promise((resolve, reject) => {
      const requestData = {
        'form_data': Object(_own__WEBPACK_IMPORTED_MODULE_1__["formData2Object"])(deliveryData)
      };
      _kernel_BackendClient__WEBPACK_IMPORTED_MODULE_0__["default"].post(Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["getDeliveryPageConfig"])().endpoints.store_delivery_data, requestData).then(() => {
        resolve(true);
      }).catch(error => {
        reject(error);
      });
    });
  },

  /**
   * @return {Promise<Object>}
   */
  fetchPaymentPageData() {
    return new Promise((resolve, reject) => {
      _kernel_BackendClient__WEBPACK_IMPORTED_MODULE_0__["default"].get(Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["getCheckoutConfig"])().endpoints.payment_page).then(paymentPage => {
        Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["setPaymentPageConfig"])(paymentPage.body.payment_page_config);
        resolve({
          'payment_page_html': paymentPage.body.payment_page_html
        });
      }).catch(error => {
        reject(error);
      });
    });
  }

});

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Delivery/Delivery.js":
/*!*********************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Delivery/Delivery.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Delivery; });
/* harmony import */ var _CheckoutClient__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../CheckoutClient */ "./src-presentation/js/component/Checkout/CheckoutClient.js");

class Delivery {
  /**
   * @return {Promise<Object>}
   */
  fetchDeliveryPageData() {
    return new Promise((resolve, reject) => {
      _CheckoutClient__WEBPACK_IMPORTED_MODULE_0__["default"].fetchDeliveryPageData().then(deliveryPage => {
        resolve(deliveryPage);
      }).catch(error => {
        reject(error);
      });
    });
  }
  /**
   * @param {FormData} formData
   *
   * @return {Promise}
   */


  storeDeliveryData(formData) {
    return new Promise(resolve => {
      _CheckoutClient__WEBPACK_IMPORTED_MODULE_0__["default"].storeDeliveryData(formData).then(() => {
        resolve(true);
      });
    });
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Delivery/DeliveryForm.js":
/*!*************************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Delivery/DeliveryForm.js ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DeliveryForm; });
/* harmony import */ var _Form_Form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Form/Form */ "./src-presentation/js/component/Form/Form.js");

class DeliveryForm {
  /**
   * @param {Object} formFields
   */
  constructor(formFields) {
    this.formFields = formFields;
    this.formBinder = new _Form_Form__WEBPACK_IMPORTED_MODULE_0__["Form"](this.formFields);
  }
  /**
   * @return {void}
   */


  mount() {
    this.formBinder.bind();
    this.formBinder.bindValidation();
  }
  /**
   * @return {boolean}
   */


  areAllFieldsEmpty() {
    const result = this.formBinder.findEmptyFields();
    return result.isEmpty;
  }
  /**
   * @return {void}
   */


  refreshForm() {
    this.formBinder.validateForm();
  }
  /**
   * @return {ValidationResult}
   */


  validateForm() {
    return this.formBinder.validateForm();
  }
  /**
   * @return {boolean}
   */


  isFormValid() {
    const result = this.formBinder.validateAllFields();
    return result.isValid;
  }
  /**
   * @return {Element}
   */


  findFormElement() {
    return this.formBinder.findFormElement();
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Delivery/DeliveryFormFields.js":
/*!*******************************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Delivery/DeliveryFormFields.js ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Form_Form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Form/Form */ "./src-presentation/js/component/Form/Form.js");

const internationalNameRegExp = new RegExp(/[^0-9_!¡÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]*/);
/* harmony default export */ __webpack_exports__["default"] = ({
  first_name: {
    inputSelector: '#first_name.input',
    constraints: {
      presence: {
        allowEmpty: false
      },
      format: {
        pattern: internationalNameRegExp
      }
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  },
  last_name: {
    inputSelector: '#last_name.input',
    constraints: {
      presence: {
        allowEmpty: false
      },
      format: {
        pattern: internationalNameRegExp
      }
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  },
  email: {
    inputSelector: '#email.input',
    constraints: {
      presence: {
        allowEmpty: false
      },
      email: true
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  },
  birth_date: {
    inputSelector: '#birth_date.input',
    constraints: {
      presence: {
        allowEmpty: false
      }
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  },
  country_code: {
    inputSelector: '#country_code.select',
    constraints: {
      presence: {
        allowEmpty: false
      }
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  },
  street: {
    inputSelector: '#street.input',
    constraints: {
      presence: {
        allowEmpty: false
      }
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  },
  city: {
    inputSelector: '#city.input',
    constraints: {
      presence: {
        allowEmpty: false
      },
      format: {
        pattern: internationalNameRegExp
      }
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  },
  zip: {
    inputSelector: '#zip.input',
    containerSelector: '.js-form-field',
    messageElementSelector: '.js-input-message',
    constraints: {
      presence: {
        allowEmpty: false
      },
      format: {
        pattern: new RegExp(/^[0-9\-]*$/)
      }
    }
  }
});

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Delivery/DeliveryUi.js":
/*!***********************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Delivery/DeliveryUi.js ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DeliveryUi; });
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../own */ "./src-presentation/js/component/own.js");
/* harmony import */ var _Modal_Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Modal/Facade */ "./src-presentation/js/component/Modal/Facade.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Facade */ "./src-presentation/js/component/Checkout/Facade.js");
/* harmony import */ var _kernel_Facade__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../kernel/Facade */ "./src-presentation/js/kernel/Facade.js");




class DeliveryUi {
  /**
   * @param {Delivery} delivery
   * @param {DeliveryForm} deliveryForm
   */
  constructor(delivery, deliveryForm) {
    this.delivery = delivery;
    this.deliveryForm = deliveryForm;
  }
  /**
   * @return {void}
   */


  async init() {
    this.bindEvents();
  }
  /**
   * @return {void}
   */


  bindEvents() {
    Object(_own__WEBPACK_IMPORTED_MODULE_0__["onLiveClick"])('.js-accept-delivery', this.onAcceptDelivery.bind(this));
  }
  /**
   * @param {Element} containerElement
   *
   * @return {void}
   */


  async mount(containerElement) {
    if (document.querySelector('.js-accept-delivery')) {
      return;
    }

    await this.delivery.fetchDeliveryPageData().then(deliveryPage => {
      const tempContainer = document.createElement('div');
      tempContainer.innerHTML = deliveryPage.delivery_page_html;
      containerElement.classList.add('is-hidden');
      containerElement.innerHTML = tempContainer.innerHTML;
    }).catch(() => {
      throw new Error('Could not mount DeliveryUi');
    });
  }
  /**
   * @return {void}
   */


  onAcceptDelivery() {
    const validationResult = this.deliveryForm.validateForm();
    const formData = new FormData(this.deliveryForm.findFormElement());

    if (validationResult.isValid) {
      Object(_Modal_Facade__WEBPACK_IMPORTED_MODULE_1__["openLoadingScreen"])();
    }

    this.delivery.storeDeliveryData(formData).then(() => {}).catch(() => {
      Object(_kernel_Facade__WEBPACK_IMPORTED_MODULE_3__["sayApplicationError"])('Something went wrong. We are working on fixing the issue. Please try again later');
    });

    if (validationResult.isValid) {
      Object(_Facade__WEBPACK_IMPORTED_MODULE_2__["sayDeliveryAccepted"])();
    }
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/EventKey.js":
/*!************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/EventKey.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  CART_ACCEPTED: 'checkout-cart_accepted',
  PAYMENT_ACCEPTED: 'checkout-payment_accepted',
  DELIVERY_ACCEPTED: 'checkout-delivery_accepted'
});

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Facade.js":
/*!**********************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Facade.js ***!
  \**********************************************************/
/*! exports provided: initCheckout, setCheckoutConfig, getCheckoutConfig, setCartPageConfig, getCartPageConfig, setDeliveryPageConfig, getDeliveryPageConfig, setPaymentPageConfig, sayCartAccepted, onCartAccepted, sayDeliveryAccepted, onDeliveryAccepted, sayPaymentAccepted, onPaymentAccepted, setCartItems, getCartItems, setVoucher */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initCheckout", function() { return initCheckout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setCheckoutConfig", function() { return setCheckoutConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCheckoutConfig", function() { return getCheckoutConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setCartPageConfig", function() { return setCartPageConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCartPageConfig", function() { return getCartPageConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setDeliveryPageConfig", function() { return setDeliveryPageConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDeliveryPageConfig", function() { return getDeliveryPageConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setPaymentPageConfig", function() { return setPaymentPageConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sayCartAccepted", function() { return sayCartAccepted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onCartAccepted", function() { return onCartAccepted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sayDeliveryAccepted", function() { return sayDeliveryAccepted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onDeliveryAccepted", function() { return onDeliveryAccepted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sayPaymentAccepted", function() { return sayPaymentAccepted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onPaymentAccepted", function() { return onPaymentAccepted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setCartItems", function() { return setCartItems; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCartItems", function() { return getCartItems; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setVoucher", function() { return setVoucher; });
/* harmony import */ var _Starter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Starter */ "./src-presentation/js/component/Checkout/Starter.js");
/* harmony import */ var _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../kernel/Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _StateKey__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./StateKey */ "./src-presentation/js/component/Checkout/StateKey.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/component/Checkout/EventKey.js");




/**
 * @return {void}
 */

function initCheckout() {
  Object(_Starter__WEBPACK_IMPORTED_MODULE_0__["startCheckout"])();
}
/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */

/**
 * @param {Object} config
 *
 * @return {void}
 */

function setCheckoutConfig(config) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.set(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].CHECKOUT_CONFIG, config);
}
/**
 * @return {*}
 */

function getCheckoutConfig() {
  return _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.get(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].CHECKOUT_CONFIG);
}
/**
 * @param {Object} config
 *
 * @return {void}
 */

function setCartPageConfig(config) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.set(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].CART_PAGE_CONFIG, config);
}
/**
 * @return {*}
 */

function getCartPageConfig() {
  return _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.get(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].CART_PAGE_CONFIG);
}
/**
 * @param {Object} config
 *
 * @return {void}
 */

function setDeliveryPageConfig(config) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.set(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].DELIVERY_PAGE_CONFIG, config);
}
/**
 * @return {*}
 */

function getDeliveryPageConfig() {
  return _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.get(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].DELIVERY_PAGE_CONFIG);
}
/**
 * @param {Object} config
 *
 * @return {void}
 */

function setPaymentPageConfig(config) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.set(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].PAYMENT_PAGE_CONFIG, config);
}
/**
 * @return {void}
 */

function sayCartAccepted() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_3__["default"].CART_ACCEPTED);
}
/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */

function onCartAccepted(callback) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_3__["default"].CART_ACCEPTED, callback);
}
/**
 * @return {void}
 */

function sayDeliveryAccepted() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_3__["default"].DELIVERY_ACCEPTED);
}
/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */

function onDeliveryAccepted(callback) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_3__["default"].DELIVERY_ACCEPTED, callback);
}
/**
 * @return {void}
 */

function sayPaymentAccepted() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_3__["default"].PAYMENT_ACCEPTED);
}
/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */

function onPaymentAccepted(callback) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_3__["default"].PAYMENT_ACCEPTED, callback);
}
/**
 * @param {Object} cartItems
 *
 * @return {void}
 */

function setCartItems(cartItems) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.set(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].CART_ITEMS, cartItems);
}
/**
 * @return {*}
 */

function getCartItems() {
  return _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.get(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].CART_ITEMS);
}
/**
 * @param {String} voucher
 *
 * @return {void}
 */

function setVoucher(voucher) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].State.set(_StateKey__WEBPACK_IMPORTED_MODULE_2__["default"].CART_VOUCHER, voucher);
}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Factory.js":
/*!***********************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Factory.js ***!
  \***********************************************************/
/*! exports provided: newStepEngine, newCartStep, newDeliveryStep, newPaymentStep */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newStepEngine", function() { return newStepEngine; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newCartStep", function() { return newCartStep; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newDeliveryStep", function() { return newDeliveryStep; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newPaymentStep", function() { return newPaymentStep; });
/* harmony import */ var _Cart_Cart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Cart/Cart */ "./src-presentation/js/component/Checkout/Cart/Cart.js");
/* harmony import */ var _Cart_CartUi__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Cart/CartUi */ "./src-presentation/js/component/Checkout/Cart/CartUi.js");
/* harmony import */ var _Steps_CartStep__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Steps/CartStep */ "./src-presentation/js/component/Checkout/Steps/CartStep.js");
/* harmony import */ var _Delivery_Delivery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Delivery/Delivery */ "./src-presentation/js/component/Checkout/Delivery/Delivery.js");
/* harmony import */ var _Delivery_DeliveryForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Delivery/DeliveryForm */ "./src-presentation/js/component/Checkout/Delivery/DeliveryForm.js");
/* harmony import */ var _Delivery_DeliveryFormFields__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Delivery/DeliveryFormFields */ "./src-presentation/js/component/Checkout/Delivery/DeliveryFormFields.js");
/* harmony import */ var _Delivery_DeliveryUi__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Delivery/DeliveryUi */ "./src-presentation/js/component/Checkout/Delivery/DeliveryUi.js");
/* harmony import */ var _Steps_DeliveryStep__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Steps/DeliveryStep */ "./src-presentation/js/component/Checkout/Steps/DeliveryStep.js");
/* harmony import */ var _Payment_SepaUi__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Payment/SepaUi */ "./src-presentation/js/component/Checkout/Payment/SepaUi.js");
/* harmony import */ var _StepEngine_StepEngine__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../StepEngine/StepEngine */ "./src-presentation/js/component/StepEngine/StepEngine.js");
/* harmony import */ var _Payment_PaymentUi__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./Payment/PaymentUi */ "./src-presentation/js/component/Checkout/Payment/PaymentUi.js");
/* harmony import */ var _Payment_PayPalUi__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Payment/PayPalUi */ "./src-presentation/js/component/Checkout/Payment/PayPalUi.js");
/* harmony import */ var _Payment_Payment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./Payment/Payment */ "./src-presentation/js/component/Checkout/Payment/Payment.js");
/* harmony import */ var _Steps_PaymentStep__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./Steps/PaymentStep */ "./src-presentation/js/component/Checkout/Steps/PaymentStep.js");
/* harmony import */ var _Payment_SepaForm__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./Payment/SepaForm */ "./src-presentation/js/component/Checkout/Payment/SepaForm.js");
/* harmony import */ var _Payment_SepaFormFields__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./Payment/SepaFormFields */ "./src-presentation/js/component/Checkout/Payment/SepaFormFields.js");
















/**
 * @param {Element} containerElement
 *
 * @return {StepEngine}
 */

function newStepEngine(containerElement) {
  return new _StepEngine_StepEngine__WEBPACK_IMPORTED_MODULE_9__["default"](containerElement);
}
/**
 * @param {StepEngine} stepEngine
 *
 * @return {CartStep}
 */

function newCartStep(stepEngine) {
  const cart = new _Cart_Cart__WEBPACK_IMPORTED_MODULE_0__["default"]();
  return new _Steps_CartStep__WEBPACK_IMPORTED_MODULE_2__["default"](stepEngine, cart, new _Cart_CartUi__WEBPACK_IMPORTED_MODULE_1__["default"](cart));
}
/**
 * @param {StepEngine} stepEngine
 *
 * @return {DeliveryStep}
 */

function newDeliveryStep(stepEngine) {
  const delivery = new _Delivery_Delivery__WEBPACK_IMPORTED_MODULE_3__["default"]();
  const deliveryForm = new _Delivery_DeliveryForm__WEBPACK_IMPORTED_MODULE_4__["default"](_Delivery_DeliveryFormFields__WEBPACK_IMPORTED_MODULE_5__["default"]);
  return new _Steps_DeliveryStep__WEBPACK_IMPORTED_MODULE_7__["default"](stepEngine, deliveryForm, new _Delivery_DeliveryUi__WEBPACK_IMPORTED_MODULE_6__["default"](delivery, deliveryForm));
}
/**
 * @param {StepEngine} stepEngine
 *
 * @return {PaymentStep}
 */

function newPaymentStep(stepEngine) {
  const paymentUi = new _Payment_PaymentUi__WEBPACK_IMPORTED_MODULE_10__["default"](new _Payment_Payment__WEBPACK_IMPORTED_MODULE_12__["default"](), [new _Payment_PayPalUi__WEBPACK_IMPORTED_MODULE_11__["default"](), new _Payment_SepaUi__WEBPACK_IMPORTED_MODULE_8__["default"](new _Payment_SepaForm__WEBPACK_IMPORTED_MODULE_14__["default"](_Payment_SepaFormFields__WEBPACK_IMPORTED_MODULE_15__["default"]))]);
  return new _Steps_PaymentStep__WEBPACK_IMPORTED_MODULE_13__["default"](stepEngine, paymentUi);
}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Payment/PayPalUi.js":
/*!********************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Payment/PayPalUi.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PayPalUi; });
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../own */ "./src-presentation/js/component/own.js");

class PayPalUi {
  /**
   * @return {void}
   */
  init() {}

  mount() {
    const paypalPanel = document.querySelector('.js-paypal-panel');

    if (!paypalPanel.getAttribute('loaded-marker')) {
      paypalPanel.setAttribute('loaded-marker', true);
      this.loadPayPalScript();
      Object(_own__WEBPACK_IMPORTED_MODULE_0__["deferrCall"])(this.deferrPayPalButtonRender.bind(this));
    }
  }
  /**
   * @return {void}
   */


  loadPayPalScript() {
    const tag = document.createElement('script');
    tag.src = 'https://www.paypalobjects.com/api/checkout.js';
    document.querySelector('head').append(tag);
  }
  /**
   * @return {boolean}
   */


  deferrPayPalButtonRender() {
    if (window.paypal) {
      this.renderPayPalButton();
      return true;
    }

    return false;
  }
  /**
   * @return {void}
   */


  renderPayPalButton() {
    paypal.Button.render({
      env: 'sandbox',
      payment: function (data, actions) {
        console.log('payment');
      },
      onAuthorize: function (data, actions) {
        console.log('authorize');
      },
      style: {
        size: 'responsive'
      }
    }, '#paypal-button');
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Payment/Payment.js":
/*!*******************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Payment/Payment.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Payment; });
/* harmony import */ var _CheckoutClient__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../CheckoutClient */ "./src-presentation/js/component/Checkout/CheckoutClient.js");

class Payment {
  /**
   * @return {Promise<Object>}
   */
  fetchPaymentPageData() {
    return new Promise((resolve, reject) => {
      _CheckoutClient__WEBPACK_IMPORTED_MODULE_0__["default"].fetchPaymentPageData().then(paymentPage => {
        resolve(paymentPage);
      }).catch(error => {
        reject(error);
      });
    });
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Payment/PaymentUi.js":
/*!*********************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Payment/PaymentUi.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PaymentUi; });
class PaymentUi {
  /**
   * @param {Payment} payment
   * @param {Array} paymentMethodsHandlers
   */
  constructor(payment, paymentMethodsHandlers) {
    this.payment = payment;
    this.paymentMethodsHandlers = paymentMethodsHandlers;
  }
  /**
   * @return {void}
   */


  init() {
    for (const index in this.paymentMethodsHandlers) {
      this.paymentMethodsHandlers[index].init();
    }
  }
  /**
   * @param {Element} containerElement
   *
   * @return {void}
   */


  async mount(containerElement) {
    if (document.querySelector('.js-accept-payment-marker')) {
      return;
    }

    await this.payment.fetchPaymentPageData().then(paymentPage => {
      const tempContainer = document.createElement('div');
      tempContainer.innerHTML = paymentPage.payment_page_html;
      containerElement.classList.add('is-hidden');
      containerElement.innerHTML = tempContainer.innerHTML;
      this.mountPaymentMethods();
    }).catch(() => {
      throw new Error('Could not mount PaymentUi');
    });
  }

  mountPaymentMethods() {
    for (const handler in this.paymentMethodsHandlers) {
      this.paymentMethodsHandlers[handler].mount();
    }
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Payment/SepaForm.js":
/*!********************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Payment/SepaForm.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SepaForm; });
/* harmony import */ var _Form_Form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Form/Form */ "./src-presentation/js/component/Form/Form.js");

class SepaForm {
  /**
   * @param {Object} formFields
   */
  constructor(formFields) {
    this.formFields = formFields;
    this.formBinder = new _Form_Form__WEBPACK_IMPORTED_MODULE_0__["Form"](this.formFields);
  }
  /**
   * @return {void}
   */


  mount() {
    this.formBinder.bind();
    this.formBinder.bindValidation();
  }
  /**
   * @return {boolean}
   */


  areAllFieldsEmpty() {
    const result = this.formBinder.findEmptyFields();
    return result.isEmpty;
  }
  /**
   * @return {void}
   */


  refreshForm() {
    this.formBinder.validateForm();
  }
  /**
   * @return {ValidationResult}
   */


  validateForm() {
    return this.formBinder.validateForm();
  }
  /**
   * @return {boolean}
   */


  isFormValid() {
    const result = this.formBinder.validateAllFields();
    return result.isValid;
  }
  /**
   * @return {Element}
   */


  findFormElement() {
    return this.formBinder.findFormElement();
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Payment/SepaFormFields.js":
/*!**************************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Payment/SepaFormFields.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Form_Form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Form/Form */ "./src-presentation/js/component/Form/Form.js");

const internationalNameRegExp = new RegExp(/[^0-9_!¡÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]*/);
const accountNumberRegExp = new RegExp(/[^_!¡÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]*/);
/* harmony default export */ __webpack_exports__["default"] = ({
  account_owner: {
    inputSelector: '#account_owner.input',
    constraints: {
      presence: {
        allowEmpty: false
      },
      format: {
        pattern: internationalNameRegExp
      }
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  },
  account_number: {
    inputSelector: '#account_number.input',
    constraints: {
      presence: {
        allowEmpty: false
      },
      format: {
        pattern: accountNumberRegExp
      }
    },
    ..._Form_Form__WEBPACK_IMPORTED_MODULE_0__["defaultOptions"]
  }
});

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Payment/SepaUi.js":
/*!******************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Payment/SepaUi.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SepaUi; });
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../own */ "./src-presentation/js/component/own.js");

class SepaUi {
  /**
   * @param {SepaForm} sepaForm
   */
  constructor(sepaForm) {
    this.sepaForm = sepaForm;
  }
  /**
   * @return {void}
   */


  init() {
    this.bindEvents();
  }

  mount() {
    const sepaPanel = document.querySelector('.js-sepa-panel');

    if (!sepaPanel.getAttribute('loaded-marker')) {
      sepaPanel.setAttribute('loaded-marker', true);
      this.sepaForm.mount();

      if (!this.sepaForm.areAllFieldsEmpty()) {
        this.sepaForm.refreshForm();
      }
    }
  }
  /**
   * @return {void}
   */


  bindEvents() {
    Object(_own__WEBPACK_IMPORTED_MODULE_0__["onLiveClick"])('.js-accept-sepa', this.onAcceptPayment.bind(this));
  }
  /**
   * @return {void}
   */


  onAcceptPayment() {
    const validationResult = this.sepaForm.validateForm();

    if (validationResult.isValid) {
      console.log('payment is valid');
    }
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Starter.js":
/*!***********************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Starter.js ***!
  \***********************************************************/
/*! exports provided: startCheckout */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startCheckout", function() { return startCheckout; });
/* harmony import */ var _Modal_Facade__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Modal/Facade */ "./src-presentation/js/component/Modal/Facade.js");
/* harmony import */ var _Factory__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Factory */ "./src-presentation/js/component/Checkout/Factory.js");
/* harmony import */ var _StepEngine_Facade__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../StepEngine/Facade */ "./src-presentation/js/component/StepEngine/Facade.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Facade */ "./src-presentation/js/component/Checkout/Facade.js");
/* harmony import */ var _StepEngineCommand__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./StepEngineCommand */ "./src-presentation/js/component/Checkout/StepEngineCommand.js");





function startCheckout() {
  Object(_StepEngine_Facade__WEBPACK_IMPORTED_MODULE_2__["onStepDisplayed"])(function () {
    Object(_Modal_Facade__WEBPACK_IMPORTED_MODULE_0__["closeLoadingScreen"])();
  });
  Object(_Modal_Facade__WEBPACK_IMPORTED_MODULE_0__["openLoadingScreen"])();
  loadConfig();
  startStepEngine();
}
/**
 * @return {void}
 */

function loadConfig() {
  const config = JSON.parse(document.getElementById('js-checkout-config').innerHTML);
  Object(_Facade__WEBPACK_IMPORTED_MODULE_3__["setCheckoutConfig"])(config);
}
/**
 * @return {void}
 */


function startStepEngine() {
  const stepEngine = Object(_Factory__WEBPACK_IMPORTED_MODULE_1__["newStepEngine"])(document.querySelector('.js-checkout-container'));
  const {
    checkout_variant: checkoutVariant
  } = Object(_Facade__WEBPACK_IMPORTED_MODULE_3__["getCheckoutConfig"])();
  Object(_StepEngineCommand__WEBPACK_IMPORTED_MODULE_4__["runStepEngine"])(checkoutVariant, stepEngine);
}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/StateKey.js":
/*!************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/StateKey.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  CHECKOUT_CONFIG: 'checkout-config',
  CART_ITEMS: 'checkout-cart_items',
  CART_VOUCHER: 'checkout-voucher',
  CART_PAGE_CONFIG: 'checkout-cart_page_config',
  DELIVERY_PAGE_CONFIG: 'checkout-delivery_page_config',
  PAYMENT_PAGE_CONFIG: 'checkout-payment_page_config'
});

/***/ }),

/***/ "./src-presentation/js/component/Checkout/StepEngineCommand.js":
/*!*********************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/StepEngineCommand.js ***!
  \*********************************************************************/
/*! exports provided: runStepEngine */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "runStepEngine", function() { return runStepEngine; });
/* harmony import */ var _Factory__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Factory */ "./src-presentation/js/component/Checkout/Factory.js");
/* harmony import */ var _kernel_Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../kernel/Facade */ "./src-presentation/js/kernel/Facade.js");


const stepEngineMap = {
  kit: function (stepEngine) {
    runKitStepEngine(stepEngine);
  }
};
/**
 * @param {String} checkoutVariant
 * @param {StepEngine} stepEngine
 *
 * @return {void}
 */

function runStepEngine(checkoutVariant, stepEngine) {
  if (!validateCheckoutVariant(checkoutVariant)) {
    Object(_kernel_Facade__WEBPACK_IMPORTED_MODULE_1__["sayApplicationError"])('Something went wrong. Please try again later');
    console.log(`Unsupported checkout variant: [${checkoutVariant}]`);
    return;
  }

  stepEngineMap[checkoutVariant](stepEngine);
}
/**
 * @param {String} checkoutVariant
 *
 * @return {boolean}
 */

function validateCheckoutVariant(checkoutVariant) {
  return stepEngineMap.hasOwnProperty(checkoutVariant);
}
/**
 * @param {StepEngine} stepEngine
 *
 * @return {StepEngine}
 */


function runKitStepEngine(stepEngine) {
  stepEngine.run([Object(_Factory__WEBPACK_IMPORTED_MODULE_0__["newCartStep"])(stepEngine), Object(_Factory__WEBPACK_IMPORTED_MODULE_0__["newDeliveryStep"])(stepEngine), Object(_Factory__WEBPACK_IMPORTED_MODULE_0__["newPaymentStep"])(stepEngine)]);
}

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Steps/CartStep.js":
/*!******************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Steps/CartStep.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StepEngine_Step__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../StepEngine/Step */ "./src-presentation/js/component/StepEngine/Step.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Facade */ "./src-presentation/js/component/Checkout/Facade.js");



class CartStep extends _StepEngine_Step__WEBPACK_IMPORTED_MODULE_0__["default"] {
  /**
   * @param {StepEngine} stepEngine
   * @param {Cart} cart
   * @param {CartUi} cartUi
   */
  constructor(stepEngine, cart, cartUi) {
    super(stepEngine);
    this.cart = cart;
    this.cartUi = cartUi;
  }
  /**
   * @inheritDoc
   */


  async init(stepContainerElement) {
    this.cartUi.init();
    Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["onCartAccepted"])(this.onAcceptCart.bind(this));
    return super.init();
  }
  /**
   * @inheritDoc
   */


  async mount(stepContainerElement) {
    await this.cartUi.mount(stepContainerElement);
  }
  /**
   * @inheritDoc
   */


  async isFinalized() {
    const cartItems = Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["getCartItems"])();
    return cartItems.length > 0;
  }
  /**
   * @return {void}
   */


  onAcceptCart() {
    this.stepEngine.next();
  }

}

/* harmony default export */ __webpack_exports__["default"] = (CartStep);

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Steps/DeliveryStep.js":
/*!**********************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Steps/DeliveryStep.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StepEngine_Step__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../StepEngine/Step */ "./src-presentation/js/component/StepEngine/Step.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Facade */ "./src-presentation/js/component/Checkout/Facade.js");



class DeliveryStep extends _StepEngine_Step__WEBPACK_IMPORTED_MODULE_0__["default"] {
  /**
   * @param {StepEngine} stepEngine
   * @param {DeliveryForm} deliveryForm
   * @param {DeliveryUi} deliveryUi
   */
  constructor(stepEngine, deliveryForm, deliveryUi) {
    super(stepEngine);
    this.deliveryForm = deliveryForm;
    this.deliveryUi = deliveryUi;
  }
  /**
   * @inheritDoc
   */


  async init(stepContainerElement) {
    this.deliveryUi.init();
    Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["onDeliveryAccepted"])(this.onAcceptDelivery.bind(this));
    return super.init();
  }
  /**
   * @inheritDoc
   */


  async mount(stepContainerElement) {
    await this.deliveryUi.mount(stepContainerElement);
    this.deliveryForm.mount();

    if (!this.deliveryForm.areAllFieldsEmpty()) {
      this.deliveryForm.refreshForm();
    }
  }
  /**
   * @inheritDoc
   */


  async isFinalized() {
    return this.deliveryForm.isFormValid();
  }
  /**
   * @return {void}
   */


  onAcceptDelivery() {
    this.stepEngine.next();
  }

}

/* harmony default export */ __webpack_exports__["default"] = (DeliveryStep);

/***/ }),

/***/ "./src-presentation/js/component/Checkout/Steps/PaymentStep.js":
/*!*********************************************************************!*\
  !*** ./src-presentation/js/component/Checkout/Steps/PaymentStep.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StepEngine_Step__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../StepEngine/Step */ "./src-presentation/js/component/StepEngine/Step.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Facade */ "./src-presentation/js/component/Checkout/Facade.js");



class PaymentStep extends _StepEngine_Step__WEBPACK_IMPORTED_MODULE_0__["default"] {
  /**
   * @param {StepEngine} stepEngine
   * @param {PaymentUi} paymentUi
   */
  constructor(stepEngine, paymentUi) {
    super(stepEngine);
    this.paymentUi = paymentUi;
  }
  /**
   * @inheritDoc
   */


  async init(stepContainerElement) {
    this.paymentUi.init();
    Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["onPaymentAccepted"])(this.onAcceptPayment.bind(this));
    return super.init();
  }
  /**
   * @inheritDoc
   */


  async mount(stepContainerElement) {
    await this.paymentUi.mount(stepContainerElement);
  }
  /**
   * @inheritDoc
   */


  async isFinalized() {
    return false;
  }
  /**
   * @return {void}
   */


  onAcceptPayment() {
    console.log('payment accepted'); // this.stepEngine.next();
  }

}

/* harmony default export */ __webpack_exports__["default"] = (PaymentStep);

/***/ }),

/***/ "./src-presentation/js/component/Form/FieldsResult.js":
/*!************************************************************!*\
  !*** ./src-presentation/js/component/Form/FieldsResult.js ***!
  \************************************************************/
/*! exports provided: FieldsResult */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FieldsResult", function() { return FieldsResult; });
/* harmony import */ var _Result_Result__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Result/Result */ "./src-presentation/js/component/Result/Result.js");

class FieldsResult extends _Result_Result__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor() {
    super();
    this.isEmpty = true;
    this.fields = {};
  }
  /**
   * @param {string} key
   * @param {Object} field
   *
   * @return {void}
   */


  addField(key, field) {
    this.isEmpty = false;
    this.fields[key] = field;
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Form/Form.js":
/*!****************************************************!*\
  !*** ./src-presentation/js/component/Form/Form.js ***!
  \****************************************************/
/*! exports provided: Form, defaultOptions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Form", function() { return Form; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultOptions", function() { return defaultOptions; });
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../own */ "./src-presentation/js/component/own.js");
/* harmony import */ var validate_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! validate.js */ "./node_modules/validate.js/validate.js");
/* harmony import */ var validate_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(validate_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _FieldsResult__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./FieldsResult */ "./src-presentation/js/component/Form/FieldsResult.js");
/* harmony import */ var _Result_MessageCollection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Result/MessageCollection */ "./src-presentation/js/component/Result/MessageCollection.js");
/* harmony import */ var _Result_ValidationResult__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Result/ValidationResult */ "./src-presentation/js/component/Result/ValidationResult.js");





const IS_ERROR_CLASS = 'is-error';

function discoverContainerElement(input) {
  return input.element.closest(input.containerSelector);
}

function discoverMessageElement(input) {
  const containerElement = discoverContainerElement(input);
  return containerElement.querySelector(input.messageElementSelector);
}

function defineTextInputAccessors(input) {
  Object.defineProperty(input, 'value', {
    get() {
      return input.element.value;
    },

    set(value) {
      input.element.value = value;
    }

  });
}

function defineMessageTextAccessors(input) {
  Object.defineProperty(input, 'messageText', {
    get() {
      return discoverMessageElement(input).textContent;
    },

    set(value) {
      discoverMessageElement(input).textContent = value;
    }

  });
}

function defineMessageHtmlAccessors(input) {
  Object.defineProperty(input, 'messageHtml', {
    get() {
      return discoverMessageElement(input).innerHTML;
    },

    set(value) {
      discoverMessageElement(input).innerHTML = value;
    }

  });
}

function defineContainerElementGetter(input) {
  Object.defineProperty(input, 'container', {
    get() {
      return discoverContainerElement(input);
    }

  });
}

class Form {
  constructor(formFields) {
    this.formFields = formFields;
  }
  /**
   * @return {void}
   */


  bind() {
    for (const key in this.formFields) {
      const field = this.formFields[key];
      field.element = document.querySelector(field.inputSelector);
      defineTextInputAccessors(field);
      defineMessageTextAccessors(field);
      defineMessageHtmlAccessors(field);
      defineContainerElementGetter(field);
    }
  }
  /**
   * @return {void}
   */


  bindValidation() {
    for (const key in this.formFields) {
      const field = this.formFields[key];
      field.element.oninput = Object(_own__WEBPACK_IMPORTED_MODULE_0__["debounce"])(() => {
        const errorMessages = this.validateField(field);
        this.updateFieldError(field, errorMessages);
      }, 150);
    }
  }
  /**
   * @param {Object} field
   * @param {String} errorMessage
   * @return {void}
   */


  setError(field, errorMessage) {
    field.messageText = errorMessage;
    field.container.classList.add(IS_ERROR_CLASS);
  }
  /**
   * @param {Object} field
   * @return {void}
   */


  clearError(field) {
    field.container.classList.remove(IS_ERROR_CLASS);
    field.messageText = '';
  }
  /**
   * @param {Object} field
   *
   * @return {Array}
   */


  validateField(field) {
    const errorMessages = validate_js__WEBPACK_IMPORTED_MODULE_1___default.a.single(field.value, field.constraints);
    return errorMessages || [];
  }
  /**
   * @return {Object}
   */


  validateAllFields() {
    const result = {
      isValid: true,
      messages: []
    };

    for (const key in this.formFields) {
      const field = this.formFields[key];
      const errorMessages = this.validateField(field);

      if (errorMessages.length) {
        result.isValid = false;
        result.messages[key] = errorMessages;
      }
    }

    return result;
  }
  /**
   * @return {ValidationResult}
   */


  validateForm() {
    const result = new _Result_ValidationResult__WEBPACK_IMPORTED_MODULE_4__["ValidationResult"]();

    for (const key in this.formFields) {
      const field = this.formFields[key];
      const errorMessages = this.validateField(field);

      if (errorMessages.length) {
        result.setIsSuccess(false);
        result.addMessageCollection(new _Result_MessageCollection__WEBPACK_IMPORTED_MODULE_3__["default"](key, errorMessages));
      }

      this.updateFieldError(field, errorMessages);
    }

    return result;
  }
  /**
   * @param {Object} field
   * @param {Array} errorMessages
   * @return {void}
   */


  updateFieldError(field, errorMessages) {
    if (errorMessages && errorMessages.length > 0) {
      this.setError(field, errorMessages[0]);
    } else {
      this.clearError(field);
    }
  }
  /**
   * @return {FieldsResult}
   */


  findEmptyFields() {
    const emptyFieldsResult = new _FieldsResult__WEBPACK_IMPORTED_MODULE_2__["FieldsResult"]();

    for (const key in this.formFields) {
      const field = this.formFields[key];

      if (field.value !== '') {
        emptyFieldsResult.addField(key, field);
      }
    }

    return emptyFieldsResult;
  }
  /**
   * @return {Element}
   */


  findFormElement() {
    // noinspection LoopStatementThatDoesntLoopJS
    for (const key in this.formFields) {
      const field = this.formFields[key];
      return field.element.closest('form');
    }

    throw new Error('Form element not found');
  }

}
const defaultOptions = {
  containerSelector: '.js-form-field',
  messageElementSelector: '.js-input-message'
};

/***/ }),

/***/ "./src-presentation/js/component/Modal/EventKey.js":
/*!*********************************************************!*\
  !*** ./src-presentation/js/component/Modal/EventKey.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  OPEN_LOADING_SCREEN: 'modal-open_loading_screen',
  CLOSE_LOADING_SCREEN: 'modal-close_loading_screen',
  OPEN_ERROR_NOTIFICATION_MODAL: 'modal-open_error_notification'
});

/***/ }),

/***/ "./src-presentation/js/component/Modal/Facade.js":
/*!*******************************************************!*\
  !*** ./src-presentation/js/component/Modal/Facade.js ***!
  \*******************************************************/
/*! exports provided: initModal, openLoadingScreen, closeLoadingScreen, openErrorNotificationModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initModal", function() { return initModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "openLoadingScreen", function() { return openLoadingScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "closeLoadingScreen", function() { return closeLoadingScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "openErrorNotificationModal", function() { return openErrorNotificationModal; });
/* harmony import */ var _Starter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Starter */ "./src-presentation/js/component/Modal/Starter.js");
/* harmony import */ var _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../kernel/Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/component/Modal/EventKey.js");



function initModal() {
  Object(_Starter__WEBPACK_IMPORTED_MODULE_0__["startModal"])();
}
function openLoadingScreen() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_2__["default"].OPEN_LOADING_SCREEN);
}
function closeLoadingScreen() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_2__["default"].CLOSE_LOADING_SCREEN);
}
function openErrorNotificationModal(message) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_2__["default"].OPEN_ERROR_NOTIFICATION_MODAL, {
    message
  });
}

/***/ }),

/***/ "./src-presentation/js/component/Modal/NotificationModal.js":
/*!******************************************************************!*\
  !*** ./src-presentation/js/component/Modal/NotificationModal.js ***!
  \******************************************************************/
/*! exports provided: bindEvents, showNotificationModal, hideNotificationModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bindEvents", function() { return bindEvents; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showNotificationModal", function() { return showNotificationModal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hideNotificationModal", function() { return hideNotificationModal; });
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../own */ "./src-presentation/js/component/own.js");
/* harmony import */ var _Screen_Overlay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Screen/Overlay */ "./src-presentation/js/component/Modal/Screen/Overlay.js");



function buildHtml(headerHtml, contentHtml) {
  return `
  <div class="c-own-modal js-own-modal">
    <div class="content-frame">
      <button class="btn-close js-own-modal-close"></button>
      <div class="content-wrapper">
        <div class="c-own-modal-header">
          ${headerHtml}
        </div>
        <div class="c-own-modal-content">
          ${contentHtml}
        </div>
        <footer class="c-own-modal-footer">
          <button class="button is-small js-own-modal-close">Close</button>
        </footer>
      </div>
    </div>
  </div>
</div>
    `;
}
/**
 * @return {void}
 */


function bindEvents() {
  Object(_own__WEBPACK_IMPORTED_MODULE_0__["onLiveClick"])('.js-own-modal-close', function () {
    hideNotificationModal();
    Object(_Screen_Overlay__WEBPACK_IMPORTED_MODULE_1__["hideOverlay"])();
  });
}
/**
 * @param {string} headerHtml
 * @param {string} contentHtml
 *
 * @return {void}
 */

function showNotificationModal(headerHtml, contentHtml) {
  const element = document.createElement('div');
  element.innerHTML = buildHtml(headerHtml, contentHtml);
  const modalElement = element.firstElementChild;
  Object(_Screen_Overlay__WEBPACK_IMPORTED_MODULE_1__["showOverlay"])();
  Object(_Screen_Overlay__WEBPACK_IMPORTED_MODULE_1__["replaceOverlayContentElement"])(modalElement);
  modalElement.classList.add('c-own-modal__is-visible');
}
/**
 * @return {void}
 */

function hideNotificationModal() {
  const modalElement = findModalElement();

  if (modalElement) {
    modalElement.classList.remove('c-own-modal__is-visible');
  }
}
/**
 * @return {Element}
 */

function findModalElement() {
  return document.querySelector('.js-own-modal');
}

/***/ }),

/***/ "./src-presentation/js/component/Modal/Screen/Loading.js":
/*!***************************************************************!*\
  !*** ./src-presentation/js/component/Modal/Screen/Loading.js ***!
  \***************************************************************/
/*! exports provided: showLoadingScreen, hideLoadingScreen */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showLoadingScreen", function() { return showLoadingScreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hideLoadingScreen", function() { return hideLoadingScreen; });
/* harmony import */ var _Overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Overlay */ "./src-presentation/js/component/Modal/Screen/Overlay.js");

const LOADING_IS_HIDDEN = 'hidden';
const LOADING_IS_VISIBLE = 'visible';
const LOADING_NOT_FOUND = 'not_found';
/**
 * @return {string}
 */

function buildHtml() {
  return `
<div class="c-own-loading js-own-loading">
    <span class="c-own-loading--icon qa-own-loading-icon"></span>
</div>`;
}
/**
 * @return {void}
 */


function showLoadingScreen() {
  removeHiddenLoading();
  const element = document.createElement('div');
  element.innerHTML = buildHtml();
  const loadingElement = element.firstElementChild;
  Object(_Overlay__WEBPACK_IMPORTED_MODULE_0__["showOverlay"])();
  Object(_Overlay__WEBPACK_IMPORTED_MODULE_0__["replaceOverlayContentElement"])(loadingElement);
  loadingElement.classList.add('c-own-loading--icon__is-visible');
}
/**
 * @return {void}
 */

function hideLoadingScreen() {
  hideLoading();
  Object(_Overlay__WEBPACK_IMPORTED_MODULE_0__["hideOverlay"])();
}
/**
 * @return {void}
 */

function removeHiddenLoading() {
  const loadingElement = findHiddenLoading();

  if (loadingElement) {
    loadingElement.remove();
  }
}
/**
 * @return {void}
 */


function hideLoading() {
  const loadingElement = findLoadingElement();

  if (loadingElement) {
    loadingElement.classList.remove('c-own-loading--icon__is-visible');
  }
}
/**
 * @param {Element|null} loadingElement
 *
 * @return {boolean}
 */


function isLoadingHidden(loadingElement) {
  return discoverLoadingVisibility(loadingElement) === LOADING_IS_HIDDEN;
}
/**
 * @param {Element|null} loadingElement
 *
 * @return {string}
 */


function discoverLoadingVisibility(loadingElement) {
  if (!loadingElement) {
    return LOADING_NOT_FOUND;
  }

  if (loadingElement.classList.contains('c-own-loading--icon__is-visible')) {
    return LOADING_IS_VISIBLE;
  }

  return LOADING_IS_HIDDEN;
}
/**
 * @return {Element|null}
 */


function findHiddenLoading() {
  const loadingElement = findLoadingElement();

  if (isLoadingHidden(loadingElement)) {
    return loadingElement;
  }

  return null;
}
/**
 * @return {Element}
 */


function findLoadingElement() {
  return document.querySelector('.js-own-loading');
}

/***/ }),

/***/ "./src-presentation/js/component/Modal/Screen/Overlay.js":
/*!***************************************************************!*\
  !*** ./src-presentation/js/component/Modal/Screen/Overlay.js ***!
  \***************************************************************/
/*! exports provided: showOverlay, replaceOverlayContentElement, hideOverlay */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showOverlay", function() { return showOverlay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "replaceOverlayContentElement", function() { return replaceOverlayContentElement; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hideOverlay", function() { return hideOverlay; });
const OVERLAY_IS_HIDDEN = 'hidden';
const OVERLAY_IS_VISIBLE = 'visible';
const OVERLAY_NOT_FOUND = 'not_found';
/**
 * @return {Element}
 */

function showOverlay() {
  const overlayElement = findVisibleOverlay();

  if (overlayElement) {
    return overlayElement;
  }

  return createAndShowOverlay();
}
/**
 * @param {Element} contentElement
 */

function replaceOverlayContentElement(contentElement) {
  const overlayElement = findOverlayElement();
  overlayElement.innerHTML = '';
  overlayElement.insertAdjacentElement('afterbegin', contentElement);
}
/**
 * @return {void}
 */

function hideOverlay() {
  const overlayElement = findOverlayElement();

  if (overlayElement) {
    overlayElement.classList.remove('c-own-overlay__is-visible');
  }
}
/**
 * @return {Element}
 */

function createAndShowOverlay() {
  removeHiddenOverlay();
  const element = document.createElement('div');
  element.innerHTML = '<div class="c-own-overlay js-own-overlay"></div>';
  const overlayElement = element.firstElementChild;
  document.body.insertAdjacentElement('afterbegin', overlayElement);
  overlayElement.classList.add('c-own-overlay__is-visible');
  return overlayElement;
}
/**
 * @return {void}
 */


function removeHiddenOverlay() {
  const overlayElement = findHiddenOverlay();

  if (overlayElement) {
    overlayElement.remove();
  }
}
/**
 * @param {Element|null} overlayElement
 *
 * @return {boolean}
 */


function isOverlayHidden(overlayElement) {
  return discoverOverlayVisibility(overlayElement) === OVERLAY_IS_HIDDEN;
}
/**
 * @param {Element|null} overlayElement
 *
 * @return {boolean}
 */


function isOverlayVisible(overlayElement) {
  return discoverOverlayVisibility(overlayElement) === OVERLAY_IS_VISIBLE;
}
/**
 * @param {Element|null} overlayElement
 *
 * @return {string}
 */


function discoverOverlayVisibility(overlayElement) {
  if (!overlayElement) {
    return OVERLAY_NOT_FOUND;
  }

  if (overlayElement.classList.contains('c-own-overlay__is-visible')) {
    return OVERLAY_IS_VISIBLE;
  }

  return OVERLAY_IS_HIDDEN;
}
/**
 * @return {Element|null}
 */


function findHiddenOverlay() {
  const overlayElement = findOverlayElement();

  if (isOverlayHidden(overlayElement)) {
    return overlayElement;
  }

  return null;
}
/**
 * @return {Element|null}
 */


function findVisibleOverlay() {
  const overlayElement = findOverlayElement();

  if (isOverlayVisible(overlayElement)) {
    return overlayElement;
  }

  return null;
}
/**
 * @return {Element}
 */


function findOverlayElement() {
  return document.querySelector('.js-own-overlay');
}

/***/ }),

/***/ "./src-presentation/js/component/Modal/Starter.js":
/*!********************************************************!*\
  !*** ./src-presentation/js/component/Modal/Starter.js ***!
  \********************************************************/
/*! exports provided: startModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startModal", function() { return startModal; });
/* harmony import */ var _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../kernel/Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/component/Modal/EventKey.js");
/* harmony import */ var _Screen_Loading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Screen/Loading */ "./src-presentation/js/component/Modal/Screen/Loading.js");
/* harmony import */ var _NotificationModal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./NotificationModal */ "./src-presentation/js/component/Modal/NotificationModal.js");




function startModal() {
  Object(_NotificationModal__WEBPACK_IMPORTED_MODULE_3__["bindEvents"])();
  registerModalListeners();
}

function registerModalListeners() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].OPEN_LOADING_SCREEN, function () {
    Object(_Screen_Loading__WEBPACK_IMPORTED_MODULE_2__["showLoadingScreen"])();
  });
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].CLOSE_LOADING_SCREEN, function () {
    Object(_Screen_Loading__WEBPACK_IMPORTED_MODULE_2__["hideLoadingScreen"])();
  });
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].OPEN_ERROR_NOTIFICATION_MODAL, function (key, data) {
    Object(_NotificationModal__WEBPACK_IMPORTED_MODULE_3__["showNotificationModal"])('Error Message', data.message);
  });
}

/***/ }),

/***/ "./src-presentation/js/component/Result/MessageCollection.js":
/*!*******************************************************************!*\
  !*** ./src-presentation/js/component/Result/MessageCollection.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MessageCollection; });
class MessageCollection {
  /**
   * @param {string} code
   * @param {Array} messages
   *
   * @return {void}
   */
  constructor(code = '', messages = []) {
    this.code = code;
    this.messages = messages;
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Result/MessageSet.js":
/*!************************************************************!*\
  !*** ./src-presentation/js/component/Result/MessageSet.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MessageSet; });
/* harmony import */ var _MessageCollection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MessageCollection */ "./src-presentation/js/component/Result/MessageCollection.js");

class MessageSet {
  constructor() {
    this.isEmpty = true;
    this.messages = {};
  }
  /**
   * @return {boolean}
   */


  hasMessages() {
    return !this.isEmpty;
  }
  /**
   * @param {MessageCollection} messageCollection
   *
   * @return {void}
   */


  addMessageCollection(messageCollection) {
    this.isEmpty = false;
    this.messages[messageCollection.code] = messageCollection;
  }
  /**
   * @return {MessageCollection[]}
   */


  getAll() {
    return this.messages;
  }
  /**
   * @return {MessageCollection}
   */


  getOnlyChild() {
    if (this.hasMessages()) {
      // noinspection LoopStatementThatDoesntLoopJS
      for (const code in this.messages) {
        return this.messages[code];
      }
    }

    return new _MessageCollection__WEBPACK_IMPORTED_MODULE_0__["default"]();
  }
  /**
   * @param {string} code
   *
   * @return {MessageCollection}
   */


  getByCode(code) {
    return this.messages[code];
  }
  /**
   * @param {MessageSet} messageSet
   *
   * @return {this}
   */


  merge(messageSet) {
    this.messages = this.messages.concat(messageSet.getAll());
    return this;
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Result/Result.js":
/*!********************************************************!*\
  !*** ./src-presentation/js/component/Result/Result.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Result; });
/* harmony import */ var _MessageSet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MessageSet */ "./src-presentation/js/component/Result/MessageSet.js");

class Result {
  constructor(isSuccess = false) {
    this.isSuccess = isSuccess;
    this.messageSet = new _MessageSet__WEBPACK_IMPORTED_MODULE_0__["default"]();
  }
  /**
   * @return {boolean}
   */


  getIsSuccess() {
    return this.isSuccess;
  }
  /**
   * @param {boolean} flag
   *
   * @return {void}
   */


  setIsSuccess(flag) {
    this.isSuccess = flag;
  }
  /**
   * @param {MessageCollection} messageCollection
   *
   * @return {void}
   */


  addMessageCollection(messageCollection) {
    this.messageSet.addMessageCollection(messageCollection);
  }
  /**
   * @return {MessageSet}
   */


  getMessageSet() {
    return this.messageSet;
  }

}

/***/ }),

/***/ "./src-presentation/js/component/Result/ValidationResult.js":
/*!******************************************************************!*\
  !*** ./src-presentation/js/component/Result/ValidationResult.js ***!
  \******************************************************************/
/*! exports provided: ValidationResult */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationResult", function() { return ValidationResult; });
/* harmony import */ var _Result__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Result */ "./src-presentation/js/component/Result/Result.js");

class ValidationResult extends _Result__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor() {
    super(true);
  }
  /**
   * @return {boolean}
   */


  get isValid() {
    return this.isSuccess;
  }

}

/***/ }),

/***/ "./src-presentation/js/component/StepEngine/EventKey.js":
/*!**************************************************************!*\
  !*** ./src-presentation/js/component/StepEngine/EventKey.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  STEP_DISPLAYED: 'step_engine-step_displayed'
});

/***/ }),

/***/ "./src-presentation/js/component/StepEngine/Facade.js":
/*!************************************************************!*\
  !*** ./src-presentation/js/component/StepEngine/Facade.js ***!
  \************************************************************/
/*! exports provided: sayStepDisplayed, onStepDisplayed */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sayStepDisplayed", function() { return sayStepDisplayed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onStepDisplayed", function() { return onStepDisplayed; });
/* harmony import */ var _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../kernel/Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/component/StepEngine/EventKey.js");
/* harmony import */ var _Modal_Facade__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Modal/Facade */ "./src-presentation/js/component/Modal/Facade.js");



/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */

/**
 * @return {void}
 */

function sayStepDisplayed() {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].STEP_DISPLAYED);
}
/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */

function onStepDisplayed(callback) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].STEP_DISPLAYED, callback);
}

/***/ }),

/***/ "./src-presentation/js/component/StepEngine/Step.js":
/*!**********************************************************!*\
  !*** ./src-presentation/js/component/StepEngine/Step.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class Step {
  /**
   * @param {StepEngine} stepEngine
   */
  constructor(stepEngine) {
    this.stepEngine = stepEngine;
  }
  /**
   * @param {Element} stepContainerElement
   *
   * @return {Promise}
   */


  async init(stepContainerElement) {
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  /**
   * @param {Element} stepContainerElement
   *
   *  @return {void}
   */


  async mount(stepContainerElement) {}
  /**
   * @param {Element} stepContainerElement
   *
   * @return {void}
   */


  async show(stepContainerElement) {
    stepContainerElement.classList.remove('is-hidden');
  }
  /**
   * @param {Element} stepContainerElement
   *
   * @return {void}
   */


  async hide(stepContainerElement) {
    stepContainerElement.classList.add('is-hidden');
  }
  /**
   * @return {boolean}
   */


  async isFinalized() {
    return false;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Step);

/***/ }),

/***/ "./src-presentation/js/component/StepEngine/StepEngine.js":
/*!****************************************************************!*\
  !*** ./src-presentation/js/component/StepEngine/StepEngine.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _kernel_Facade__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../kernel/Facade */ "./src-presentation/js/kernel/Facade.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Facade */ "./src-presentation/js/component/StepEngine/Facade.js");



class StepEngine {
  /**
   * @param {Element} container
   */
  constructor(container) {
    this.container = container;
    this.currentStepIndex = 0;
    this.steps = [];
  }
  /**
   * @param {Step[]} steps
   *
   * @return {void}
   */


  run(steps) {
    Promise.all(this.initSteps(steps)).then(() => {
      this.renderSteps();
    });
  }
  /**
   * @param {Step[]} steps
   *
   * @return {Promise[]}
   */


  initSteps(steps) {
    const stepPromises = [];
    const stepsCount = steps.length;

    for (let stepIndex = 0; stepIndex < stepsCount; stepIndex++) {
      const step = steps[stepIndex];
      const stepContainerElement = document.createElement('div');
      stepPromises.push(step.init(stepContainerElement));
      this.steps[stepIndex] = {
        'step': step,
        'containerElement': stepContainerElement
      };
    }

    return stepPromises;
  }
  /**
   * @return {void}
   */


  async renderSteps() {
    const stepsCount = this.steps.length;

    for (let stepIndex = 0; stepIndex < stepsCount; stepIndex++) {
      const stepItem = this.steps[stepIndex];
      const step = stepItem.step;
      this.container.appendChild(stepItem.containerElement);

      try {
        await step.mount(stepItem.containerElement);
      } catch (error) {
        Object(_kernel_Facade__WEBPACK_IMPORTED_MODULE_0__["sayApplicationError"])('Something went wrong. We are working on fixing the issue. Please try again later');
        console.log('Could not mount step: ', stepItem, error);
        return;
      }

      const isFinalized = await step.isFinalized();

      if (!isFinalized) {
        this.currentStepIndex = stepIndex;
        await this.displayStep(stepIndex);
        break;
      }
    }
  }
  /**
   * @param {int} index
   * @param {int} hideIndex
   *
   * @return {void}
   */


  displayStep(index, hideIndex = -1) {
    if (this.steps[hideIndex]) {
      const stepItemToHide = this.steps[hideIndex];
      stepItemToHide.step.hide(stepItemToHide.containerElement);
    }

    const stepItemToShow = this.steps[index];
    stepItemToShow.step.show(stepItemToShow.containerElement);
    Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["sayStepDisplayed"])();
  }
  /**
   * @return {void}
   */


  async next() {
    if (this.currentStepIndex < this.steps.length - 1) {
      const stepItem = this.steps[this.currentStepIndex + 1];

      if (stepItem.containerElement.innerHTML === '') {
        this.container.appendChild(stepItem.containerElement);

        try {
          await stepItem.step.mount(stepItem.containerElement);
        } catch (error) {
          Object(_kernel_Facade__WEBPACK_IMPORTED_MODULE_0__["sayApplicationError"])('Something went wrong. We are working on fixing the issue. Please try again later');
          console.log('Could not mount step: ', stepItem, error);
          return;
        }
      }

      this.currentStepIndex++;
      this.displayStep(this.currentStepIndex, this.currentStepIndex - 1);
    }
  }
  /**
   * @return {void}
   */


  async prev() {
    if (this.currentStepIndex > 0) {
      this.currentStepIndex--;
      this.displayStep(this.currentStepIndex, this.currentStepIndex + 1);
    }
  }

}

/* harmony default export */ __webpack_exports__["default"] = (StepEngine);

/***/ }),

/***/ "./src-presentation/js/component/TabbedAccordion/EventKey.js":
/*!*******************************************************************!*\
  !*** ./src-presentation/js/component/TabbedAccordion/EventKey.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  TABBED_ACCORDION_SELECTION: 'tabbed-accordion_selection'
});

/***/ }),

/***/ "./src-presentation/js/component/TabbedAccordion/Facade.js":
/*!*****************************************************************!*\
  !*** ./src-presentation/js/component/TabbedAccordion/Facade.js ***!
  \*****************************************************************/
/*! exports provided: initTabbedAccordion, onSelect */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initTabbedAccordion", function() { return initTabbedAccordion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onSelect", function() { return onSelect; });
/* harmony import */ var _TabbedAccordion__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TabbedAccordion */ "./src-presentation/js/component/TabbedAccordion/TabbedAccordion.js");
/* harmony import */ var _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../kernel/Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/component/TabbedAccordion/EventKey.js");



function initTabbedAccordion() {
  Object(_TabbedAccordion__WEBPACK_IMPORTED_MODULE_0__["bindEvents"])();
}
/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */

/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */

function onSelect(callback) {
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_1__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_2__["default"].TABBED_ACCORDION_SELECTION, callback);
}

/***/ }),

/***/ "./src-presentation/js/component/TabbedAccordion/TabbedAccordion.js":
/*!**************************************************************************!*\
  !*** ./src-presentation/js/component/TabbedAccordion/TabbedAccordion.js ***!
  \**************************************************************************/
/*! exports provided: bindEvents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bindEvents", function() { return bindEvents; });
/* harmony import */ var _own__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../own */ "./src-presentation/js/component/own.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/component/TabbedAccordion/EventKey.js");
/* harmony import */ var _kernel_Universe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../kernel/Universe */ "./src-presentation/js/kernel/Universe.js");



/**
 * @return {void}
 */

function bindEvents() {
  Object(_own__WEBPACK_IMPORTED_MODULE_0__["onLiveClick"])('.js-tabbed-accordion-acc-header', function (event) {
    const itemEl = event.target.closest('.js-tabbed-accordion-acc-item');
    const contentContainerEl = event.target.closest('.js-tabbed-accordion-content-container');
    handleAccHeaderElements(itemEl, contentContainerEl);
  });
  Object(_own__WEBPACK_IMPORTED_MODULE_0__["onLiveClick"])('.js-tabbed-accordion-nav-item', function (event) {
    const itemEl = event.target.closest('.js-tabbed-accordion-nav-item');
    handleTabNavElements(itemEl);
    handleTabContentElements(itemEl);
  });
}
/**
 * @param {Element} itemEl
 * @param {Element} containerEl
 */


function handleAccHeaderElements(itemEl, containerEl) {
  const activeEl = containerEl.querySelector('.tacc-is-active');
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_2__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].TABBED_ACCORDION_SELECTION, {
    'active': activeEl,
    'selected': itemEl
  });

  if (activeEl) {
    activeEl.classList.remove('tacc-is-active');
  }

  if (activeEl !== itemEl) {
    itemEl.classList.add('tacc-is-active');
  }
}
/**
 * @param {Element} itemEl
 *
 * @return {void}
 */


function handleTabNavElements(itemEl) {
  const parentNavEl = itemEl.closest('.js-tabbed-accordion-tab-nav');
  const activeNavEl = parentNavEl.querySelector('.tacc-is-active');
  _kernel_Universe__WEBPACK_IMPORTED_MODULE_2__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].TABBED_ACCORDION_SELECTION, {
    'active': activeNavEl,
    'selected': itemEl
  });

  if (activeNavEl) {
    activeNavEl.classList.remove('tacc-is-active');
  }

  if (activeNavEl !== itemEl) {
    itemEl.classList.add('tacc-is-active');
  }
}
/**
 * @param {Element} itemEl
 *
 * @return {void}
 */


function handleTabContentElements(itemEl) {
  const contentSelector = '.' + itemEl.getAttribute('data-tabs-target');
  const contentEl = document.querySelector(contentSelector);
  const contentContainerEl = contentEl.closest('.js-tabbed-accordion-content-container');
  const activeContentEl = contentContainerEl.querySelector('.tacc-is-active');

  if (activeContentEl) {
    activeContentEl.classList.remove('tacc-is-active');
  }

  if (activeContentEl !== contentEl) {
    contentEl.classList.add('tacc-is-active');
  }
}



/***/ }),

/***/ "./src-presentation/js/component/own.js":
/*!**********************************************!*\
  !*** ./src-presentation/js/component/own.js ***!
  \**********************************************/
/*! exports provided: onLiveClick, debounce, deferrCall, formData2Object */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onLiveClick", function() { return onLiveClick; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "debounce", function() { return debounce; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deferrCall", function() { return deferrCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formData2Object", function() { return formData2Object; });
/**
 * @callback liveClickCallback
 * @param {Event} event
 *
 * @return {void}
 */

/**
 * @param {string} selector
 * @param {liveClickCallback} callback
 * @param {Element} [elementScope=document]
 *
 * @return {void}
 */
function onLiveClick(selector, callback, elementScope = document) {
  elementScope.addEventListener('click', function (event) {
    const element = event.target.closest(selector);

    if (element) {
      callback(event);
    }
  });
}
/**
 * @param {Function} func
 * @param {int} wait
 *
 * @return {function(...[*]=)}
 */

function debounce(func, wait) {
  let timeout;
  return function () {
    function laterCall() {
      timeout = null;
      func();
    }

    clearTimeout(timeout);
    timeout = setTimeout(laterCall, wait);
  };
}
/**
 * @param {Function} func
 * @param {int} [waitMs=100]
 * @param {int} [limit=10]
 *
 * @return {void}
 */

function deferrCall(func, waitMs = 100, limit = 10) {
  const interval = window.setInterval(() => {
    const isDone = func();

    if (limit-- < 1 || isDone) {
      window.clearInterval(interval);
    }
  }, waitMs);
}
/**
 *
 * @param {FormData} formData
 *
 * @return {Object}
 */

function formData2Object(formData) {
  const obj = {};

  for (const key of formData.keys()) {
    obj[key] = formData.get(key);
  }

  return obj;
}

/***/ }),

/***/ "./src-presentation/js/kernel/BackendClient.js":
/*!*****************************************************!*\
  !*** ./src-presentation/js/kernel/BackendClient.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var superagent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! superagent */ "./node_modules/superagent/lib/client.js");
/* harmony import */ var superagent__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(superagent__WEBPACK_IMPORTED_MODULE_0__);

const host = 'http://customer-webportal-dev.de.local';
/* harmony default export */ __webpack_exports__["default"] = ({
  /**
   * @param {string} uri
   *
   * @return {Promise}
   */
  get(uri) {
    return new Promise(function (resolve, reject) {
      superagent__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${host}${uri}`).then(function (res) {
        resolve(res);
      }).catch(error => {
        reject(error);
      });
    });
  },

  /**
   * @param {string} uri
   * @param {Object} data
   *
   * @return {Promise}
   */
  post(uri, data) {
    return new Promise(function (resolve, reject) {
      superagent__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${host}${uri}`).send(data).then(function (res) {
        resolve(res);
      }).catch(function (error) {
        reject(error);
      });
    });
  }

});

/***/ }),

/***/ "./src-presentation/js/kernel/Bootstrap.js":
/*!*************************************************!*\
  !*** ./src-presentation/js/kernel/Bootstrap.js ***!
  \*************************************************/
/*! exports provided: bootstrap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bootstrap", function() { return bootstrap; });
/* harmony import */ var _component_Modal_Facade__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../component/Modal/Facade */ "./src-presentation/js/component/Modal/Facade.js");
/* harmony import */ var _Facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Facade */ "./src-presentation/js/kernel/Facade.js");


function bootstrap() {
  Object(_Facade__WEBPACK_IMPORTED_MODULE_1__["onApplicationError"])(function (key, data) {
    console.log('Runtime error: ' + data.message);
    Object(_component_Modal_Facade__WEBPACK_IMPORTED_MODULE_0__["openErrorNotificationModal"])(data.message);
  });
}

/***/ }),

/***/ "./src-presentation/js/kernel/EventKey.js":
/*!************************************************!*\
  !*** ./src-presentation/js/kernel/EventKey.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  RUNTIME_APPLICATION_ERROR: 'runtime-application-error'
});

/***/ }),

/***/ "./src-presentation/js/kernel/Facade.js":
/*!**********************************************!*\
  !*** ./src-presentation/js/kernel/Facade.js ***!
  \**********************************************/
/*! exports provided: sayApplicationError, onApplicationError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sayApplicationError", function() { return sayApplicationError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onApplicationError", function() { return onApplicationError; });
/* harmony import */ var _Universe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Universe */ "./src-presentation/js/kernel/Universe.js");
/* harmony import */ var _EventKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EventKey */ "./src-presentation/js/kernel/EventKey.js");


/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */

/**
 * @param {String} message
 */

function sayApplicationError(message) {
  _Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.notify(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].RUNTIME_APPLICATION_ERROR, {
    'message': message
  });
}
/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */

function onApplicationError(callback) {
  _Universe__WEBPACK_IMPORTED_MODULE_0__["default"].Event.subscribe(_EventKey__WEBPACK_IMPORTED_MODULE_1__["default"].RUNTIME_APPLICATION_ERROR, callback);
}

/***/ }),

/***/ "./src-presentation/js/kernel/Universe.js":
/*!************************************************!*\
  !*** ./src-presentation/js/kernel/Universe.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const Universe = {};
/**
 * @param {Object[]} target
 * @param {Object} properties
 * @param {string} properties.key
 * @param {int} properties.priority
 * @param {Function} properties.callback
 */

function subscribeTo(target, properties) {
  const {
    key,
    priority,
    callback
  } = properties;

  if (!target[key]) {
    target[key] = [];
  }

  if (!target[key][priority]) {
    target[key][priority] = [];
  }

  target[key][priority].push(callback);
}

const state = {};
const stateListeners = [];
Universe.State = {
  /**
   * @param {string} key
   * @param {*} value
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */
  set(key, value, notificationData = {}) {
    state[key] = value;
    this.notify(key, value, notificationData);
  },

  /**
   * @param {string} key
   * @param {*} fallback
   *
   * @return {*}
   */
  get(key, fallback = undefined) {
    if (!state.hasOwnProperty(key)) {
      return fallback;
    }

    return state[key];
  },

  /**
   * @callback stateSubscribeCallback
   * @param {string} key
   * @param {*} value
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */

  /**
   * @param {string} key
   * @param {stateSubscribeCallback} callback
   * @param {int} [priority=500]
   *
   * @return {void}
   */
  subscribe(key, callback, priority = 500) {
    subscribeTo(stateListeners, {
      key,
      priority,
      callback
    });
  },

  /**
   * @param {string} key
   * @param {*} value
   * @param {*} notificationData
   *
   * @return {void}
   */
  notify(key, value, notificationData = {}) {
    if (stateListeners[key]) {
      const priorities = Object.keys(stateListeners[key]);

      for (const priorityKey in priorities) {
        const priority = priorities[priorityKey];

        for (const index in stateListeners[key][priority]) {
          const callback = stateListeners[key][priority][index];
          callback(key, value, notificationData);
        }
      }
    }
  }

};
const eventListeners = [];
Universe.Event = {
  /**
   * @callback eventSubscribeCallback
   * @param {string} key
   * @param {*} [notificationData={}]
   *
   * @return {void}
   */

  /**
   * @param {string} key
   * @param {eventSubscribeCallback} callback
   * @param {int} [priority=500]
   *
   * @return {void}
   */
  subscribe(key, callback, priority = 500) {
    subscribeTo(eventListeners, {
      key,
      priority,
      callback
    });
  },

  /**
   * @param {string} key
   * @param {*} [notificationData={}]
   *
   * @return {Array}
   */
  notify(key, notificationData = {}) {
    let responses = [];

    if (eventListeners[key]) {
      const priorities = Object.keys(eventListeners[key]);

      for (const priorityKey in priorities) {
        const priority = priorities[priorityKey];

        for (const index in eventListeners[key][priority]) {
          const callback = eventListeners[key][priority][index];
          responses.push(callback(key, notificationData));
        }
      }
    }

    return responses;
  }

};
/* harmony default export */ __webpack_exports__["default"] = (Universe);

/***/ }),

/***/ "./src-presentation/js/page/checkout.js":
/*!**********************************************!*\
  !*** ./src-presentation/js/page/checkout.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var element_closest_polyfill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! element-closest-polyfill */ "./node_modules/element-closest-polyfill/index.js");
/* harmony import */ var element_closest_polyfill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(element_closest_polyfill__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _kernel_Bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../kernel/Bootstrap */ "./src-presentation/js/kernel/Bootstrap.js");
/* harmony import */ var _component_Checkout_Facade__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../component/Checkout/Facade */ "./src-presentation/js/component/Checkout/Facade.js");
/* harmony import */ var _component_Modal_Facade__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../component/Modal/Facade */ "./src-presentation/js/component/Modal/Facade.js");
/* harmony import */ var _component_TabbedAccordion_Facade__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../component/TabbedAccordion/Facade */ "./src-presentation/js/component/TabbedAccordion/Facade.js");





Object(_kernel_Bootstrap__WEBPACK_IMPORTED_MODULE_1__["bootstrap"])();
Object(_component_Modal_Facade__WEBPACK_IMPORTED_MODULE_3__["initModal"])();
Object(_component_Checkout_Facade__WEBPACK_IMPORTED_MODULE_2__["initCheckout"])();
Object(_component_TabbedAccordion_Facade__WEBPACK_IMPORTED_MODULE_4__["initTabbedAccordion"])();

/***/ })

/******/ });
//# sourceMappingURL=checkout_script.js.map