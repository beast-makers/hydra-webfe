import {onLiveClick} from '../../own';
import {openLoadingScreen} from '../../Modal/Facade';
import {sayCartAccepted} from '../Facade';
import {sayApplicationError} from '../../../kernel/Facade';

export default class CartUi {
  /**
   * @param {Cart} cart
   */
  constructor (cart) {
    this.cart = cart;
  }

  /**
   * @return {void}
   */
  init () {
    this.bindEvents();
  }

  /**
   * @return {void}
   */
  bindEvents () {
    onLiveClick('.js-accept-cart', this.onAcceptCart.bind(this));
  }

  /**
   * @param {Element} containerElement
   *
   * @return {void}
   */
  async mount (containerElement) {
    if (document.querySelector('.js-accept-cart')) {
      return;
    }

    await this.cart.fetchCartPageData()
      .then((cartPage) => {
        const tempContainer = document.createElement('div');
        tempContainer.innerHTML = cartPage.cart_page_html;
        tempContainer.querySelector('.js-cart-details').innerHTML = cartPage.cart_details_html;

        containerElement.classList.add('is-hidden');
        containerElement.innerHTML = tempContainer.innerHTML;
      })
      .catch(() => {
        throw new Error('Could not mount CartUi');
      });
  }

  /**
   * @return {void}
   */
  onAcceptCart () {
    openLoadingScreen();

    const requestData = {
      products: {
        'id_1234': {}
      },
      voucher_code: 'babayaga'
    };

    this.cart.storeCartData(requestData)
      .then(() => {
        sayCartAccepted();
      })
      .catch(() => {
        sayApplicationError('Something went wrong. We are working on fixing the issue. Please try again later');
      });
  }
}
