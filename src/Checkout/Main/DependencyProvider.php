<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Main;

use BeastMakers\Shared\DependencyLoader;
use BeastMakers\Shared\Kernel\BaseDependencyProvider;
use BeastMakers\Shared\SessionStorage\SessionManager;
use Smarty;

class DependencyProvider extends BaseDependencyProvider
{
  use DependencyLoader\TemplateEngine;
  use DependencyLoader\PHPSessionManagerLoader;

  /**
   * @return Smarty
   */
  public function loadTemplateEngine(): Smarty
  {
    return $this->shareTemplateEngine();
  }

  /**
   * @return SessionManager
   */
  public function loadSessionManager(): SessionManager
  {
    return $this->sharePHPSessionManager();
  }
}
