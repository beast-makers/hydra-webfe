<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Shared\Result;

use BeastMakers\Shared\Result\Result;

class ValidateDeliveryDataResult extends Result
{
}
