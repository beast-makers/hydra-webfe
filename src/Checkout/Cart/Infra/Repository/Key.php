<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Infra\Repository;

interface Key
{
  public const CHECKOUT_PRODUCTS_PREFIX = 'checkout_products_';
  public const CHECKOUT_VOUCHER_PREFIX = 'checkout_voucher_';
}
