import Step from '../../StepEngine/Step';
import {onDeliveryAccepted} from '../Facade';

class DeliveryStep extends Step {
  /**
   * @param {StepEngine} stepEngine
   * @param {DeliveryForm} deliveryForm
   * @param {DeliveryUi} deliveryUi
   */
  constructor (stepEngine, deliveryForm, deliveryUi) {
    super(stepEngine);

    this.deliveryForm = deliveryForm;
    this.deliveryUi = deliveryUi;
  }
  /**
   * @inheritDoc
   */
  async init (stepContainerElement) {
    this.deliveryUi.init();

    onDeliveryAccepted(this.onAcceptDelivery.bind(this));

    return super.init();
  }

  /**
   * @inheritDoc
   */
  async mount (stepContainerElement) {
    await this.deliveryUi.mount(stepContainerElement);
    this.deliveryForm.mount();

    if (!this.deliveryForm.areAllFieldsEmpty()) {
      this.deliveryForm.refreshForm();
    }
  }

  /**
   * @inheritDoc
   */
  async isFinalized () {
    return this.deliveryForm.isFormValid();
  }

  /**
   * @return {void}
   */
  onAcceptDelivery () {
    this.stepEngine.next();
  }
}

export default DeliveryStep;

