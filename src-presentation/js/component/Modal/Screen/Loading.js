import {hideOverlay, replaceOverlayContentElement, showOverlay} from './Overlay';

const LOADING_IS_HIDDEN = 'hidden';
const LOADING_IS_VISIBLE = 'visible';
const LOADING_NOT_FOUND = 'not_found';

/**
 * @return {string}
 */
function buildHtml () {
  return `
<div class="c-own-loading js-own-loading">
    <span class="c-own-loading--icon qa-own-loading-icon"></span>
</div>`;
}

/**
 * @return {void}
 */
export function showLoadingScreen () {
  removeHiddenLoading();

  const element = document.createElement('div');
  element.innerHTML = buildHtml();
  const loadingElement = element.firstElementChild;

  showOverlay();
  replaceOverlayContentElement(loadingElement);
  loadingElement.classList.add('c-own-loading--icon__is-visible');
}

/**
 * @return {void}
 */
export function hideLoadingScreen () {
  hideLoading();
  hideOverlay();
}

/**
 * @return {void}
 */
function removeHiddenLoading () {
  const loadingElement = findHiddenLoading();

  if (loadingElement) {
    loadingElement.remove();
  }
}

/**
 * @return {void}
 */
function hideLoading () {
  const loadingElement = findLoadingElement();

  if (loadingElement) {
    loadingElement.classList.remove('c-own-loading--icon__is-visible');
  }
}

/**
 * @param {Element|null} loadingElement
 *
 * @return {boolean}
 */
function isLoadingHidden (loadingElement) {
  return discoverLoadingVisibility(loadingElement) === LOADING_IS_HIDDEN;
}

/**
 * @param {Element|null} loadingElement
 *
 * @return {string}
 */
function discoverLoadingVisibility (loadingElement) {
  if (!loadingElement) {
    return LOADING_NOT_FOUND;
  }

  if (loadingElement.classList.contains('c-own-loading--icon__is-visible')) {
    return LOADING_IS_VISIBLE;
  }

  return LOADING_IS_HIDDEN;
}

/**
 * @return {Element|null}
 */
function findHiddenLoading () {
  const loadingElement = findLoadingElement();

  if (isLoadingHidden(loadingElement)) {
    return loadingElement;
  }

  return null;
}

/**
 * @return {Element}
 */
function findLoadingElement () {
  return document.querySelector('.js-own-loading');
}
