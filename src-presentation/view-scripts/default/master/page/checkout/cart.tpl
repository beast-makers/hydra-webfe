<div class="checkout-cart-page">
    <div class="page-title">{$page_title}</div>
    <div class="main-columns row justify-content-center">
        <div class="col-md-12 col-lg-9 col-xl-7">
            <div class="cart-details box js-cart-details"></div>
        </div>
    </div>
</div>
