<?php
declare(strict_types=1);

namespace BeastMakers\Application\Bootstrap;

use BeastMakers\Application\SmartyDefault;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use Smarty;

class TemplateEngine
{
  private ?SmartyDefault $smartyDefault = null;

  /**
   * @return void
   */
  public function run(): void
  {
    $diContainer = DependencyContainer::getInstance();

    $diContainer->set(DependencyContainerItem::TEMPLATE_ENGINE, function () {
      return $this->shareTemplateEngine();
    });
  }

  /**
   * @return Smarty
   * @noinspection PhpDocMissingThrowsInspection
   */
  private function shareTemplateEngine(): Smarty
  {
    if (!$this->smartyDefault) {
      $diContainer = DependencyContainer::getInstance();
      $config = $diContainer->get(DependencyContainerItem::APPLICATION_CONFIG);
      /** @noinspection PhpUnhandledExceptionInspection */
      $this->smartyDefault = new SmartyDefault($config);
    }

    return $this->smartyDefault;
  }
}
