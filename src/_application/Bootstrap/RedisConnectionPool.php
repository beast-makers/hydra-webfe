<?php
declare(strict_types=1);

namespace BeastMakers\Application\Bootstrap;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use BeastMakers\Shared\RedisConnectionCatalog;
use BeastMakers\Shared\RedisConnector\ClientPool;
use BeastMakers\Shared\RedisConnector\Connection;
use BeastMakers\Shared\RedisConnector\ConnectionPair;
use BeastMakers\Shared\RedisConnector\RedisClient;

class RedisConnectionPool
{
  private ?ClientPool $clientPool = null;

  /**
   * @return void
   */
  public function run(): void
  {
    $diContainer = DependencyContainer::getInstance();

    $diContainer->set(DependencyContainerItem::REDIS_CONNECTION_POOL, function () use ($diContainer) {
      if ($this->clientPool === null) {
        $this->createPool($diContainer->get(DependencyContainerItem::APPLICATION_CONFIG));
      }

      return $this->clientPool;
    });
  }

  /**
   * @param Config $config
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  private function createPool(Config $config): void
  {
    $this->clientPool = new ClientPool();

    $this->registerWebfeMaster($config->getConfig('redis.webfe_master'));
  }

  /**
   * @param Config $clientConfig
   * @throws MissingConfigOptionException
   */
  private function registerWebfeMaster(Config $clientConfig): void
  {
    $redisConnection = new Connection(
      $clientConfig->getString('host'),
      $clientConfig->getInt('port'),
      $clientConfig->getInt('index_db'),
    );

    $connectionPair = new ConnectionPair($redisConnection, $redisConnection);
    /** @psalm-suppress PossiblyNullReference */
    $this->clientPool->register(new RedisClient($connectionPair), RedisConnectionCatalog::WEBFE);
  }
}
