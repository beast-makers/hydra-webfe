<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

class DependencyContainer
{
  /**
   * @var null|DependencyContainer
   */
  private static $instance = null;

  /**
   * @var callable[]
   */
  private $dependencies = [];

  private function __construct()
  {
  }

  /**
   * @psalm-suppress UnsafeInstantiation
   * @return DependencyContainer
   */
  public static function getInstance(): self
  {
    if (static::$instance === null) {
      static::$instance = new static();
    }

    return static::$instance;
  }

  /**
   * @param string $key
   * @param callable $dependency
   */
  public function set(string $key, callable $dependency): void
  {
    $this->dependencies[$key] = $dependency;
  }

  /**
   * @param string $key
   *
   * @return mixed
   */
  public function get(string $key)
  {
    return $this->dependencies[$key]();
  }

  /**
   * @param string $key
   *
   * @return bool
   */
  public function contains(string $key): bool
  {
    return array_key_exists($key, $this->dependencies);
  }
}
