import {startModal} from './Starter';
import Universe from '../../kernel/Universe';
import EventKey from './EventKey';

export function initModal() {
  startModal();
}

export function openLoadingScreen () {
  Universe.Event.notify(EventKey.OPEN_LOADING_SCREEN);
}

export function closeLoadingScreen () {
  Universe.Event.notify(EventKey.CLOSE_LOADING_SCREEN);
}

export function openErrorNotificationModal (message) {
  Universe.Event.notify(EventKey.OPEN_ERROR_NOTIFICATION_MODAL, {
    message
  });
}
