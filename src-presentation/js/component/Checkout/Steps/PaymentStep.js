import Step from '../../StepEngine/Step';
import {onPaymentAccepted} from '../Facade';

class PaymentStep extends Step {
  /**
   * @param {StepEngine} stepEngine
   * @param {PaymentUi} paymentUi
   */
  constructor (stepEngine, paymentUi) {
    super(stepEngine);

    this.paymentUi = paymentUi;
  }

  /**
   * @inheritDoc
   */
  async init (stepContainerElement) {
    this.paymentUi.init();

    onPaymentAccepted(this.onAcceptPayment.bind(this));

    return super.init();
  }

  /**
   * @inheritDoc
   */
  async mount (stepContainerElement) {
    await this.paymentUi.mount(stepContainerElement);
  }

  /**
   * @inheritDoc
   */
  async isFinalized () {
    return false;
  }

  /**
   * @return {void}
   */
  onAcceptPayment () {
    console.log('payment accepted');
    // this.stepEngine.next();
  }
}

export default PaymentStep;
