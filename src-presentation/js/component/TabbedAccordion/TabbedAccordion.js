import {onLiveClick} from '../own';
import EventKey from './EventKey';
import Universe from "../../kernel/Universe";

/**
 * @return {void}
 */
function bindEvents () {
  onLiveClick('.js-tabbed-accordion-acc-header', function (event) {
    const itemEl = event.target.closest('.js-tabbed-accordion-acc-item');
    const contentContainerEl = event.target.closest('.js-tabbed-accordion-content-container');

    handleAccHeaderElements(itemEl, contentContainerEl);
  });

  onLiveClick('.js-tabbed-accordion-nav-item', function (event) {
    const itemEl = event.target.closest('.js-tabbed-accordion-nav-item');
    handleTabNavElements(itemEl);
    handleTabContentElements(itemEl);
  });
}

/**
 * @param {Element} itemEl
 * @param {Element} containerEl
 */
function handleAccHeaderElements (itemEl, containerEl) {
  const activeEl = containerEl.querySelector('.tacc-is-active');

  Universe.Event.notify(EventKey.TABBED_ACCORDION_SELECTION, {'active': activeEl, 'selected': itemEl});

  if (activeEl) {
    activeEl.classList.remove('tacc-is-active');
  }

  if (activeEl !== itemEl) {
    itemEl.classList.add('tacc-is-active');
  }

}

/**
 * @param {Element} itemEl
 *
 * @return {void}
 */
function handleTabNavElements (itemEl) {
  const parentNavEl = itemEl.closest('.js-tabbed-accordion-tab-nav');
  const activeNavEl = parentNavEl.querySelector('.tacc-is-active');

  Universe.Event.notify(EventKey.TABBED_ACCORDION_SELECTION, {'active': activeNavEl, 'selected': itemEl});

  if (activeNavEl) {
    activeNavEl.classList.remove('tacc-is-active');
  }

  if (activeNavEl !== itemEl) {
    itemEl.classList.add('tacc-is-active');
  }
}

/**
 * @param {Element} itemEl
 *
 * @return {void}
 */
function handleTabContentElements (itemEl) {
  const contentSelector = '.' + itemEl.getAttribute('data-tabs-target');
  const contentEl = document.querySelector(contentSelector);
  const contentContainerEl = contentEl.closest('.js-tabbed-accordion-content-container');
  const activeContentEl = contentContainerEl.querySelector('.tacc-is-active');

  if (activeContentEl) {
    activeContentEl.classList.remove('tacc-is-active');
  }

  if (activeContentEl !== contentEl) {
    contentEl.classList.add('tacc-is-active');
  }
}

export {bindEvents};


