<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart;

use BeastMakers\Checkout\Cart\Action\CartPageCommand;
use BeastMakers\Checkout\Cart\Action\CartPageConfigCommand;
use BeastMakers\Checkout\Cart\Action\StoreCartDataCommand;
use BeastMakers\Checkout\Cart\Infra\CartData;
use BeastMakers\Checkout\Cart\Infra\Repository\CartReadRepository;
use BeastMakers\Checkout\Cart\Infra\Repository\CartWriteRepository;
use BeastMakers\Checkout\Cart\Infra\Repository\ReadRepository;
use BeastMakers\Checkout\Cart\Infra\Repository\WriteRepository;
use BeastMakers\Checkout\Cart\Infra\Request\StoreCartDataRequest;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\SessionStorage\SessionManager;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ServerRequestInterface;
use Smarty;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @return Smarty
   */
  public function shareTemplateEngine(): Smarty
  {
    return $this->dependencyProvider->loadTemplateEngine();
  }

  /**
   * @return CartPageCommand
   */
  public function newCartPageCommand(): CartPageCommand
  {
    return new CartPageCommand();
  }

  /**
   * @return CartPageConfigCommand
   */
  public function newCartPageConfigCommand(): CartPageConfigCommand
  {
    return new CartPageConfigCommand($this->shareCartReadRepository(), $this->shareSessionManager());
  }

  /**
   * @return StoreCartDataCommand
   */
  public function newStoreCartDataCommand(): StoreCartDataCommand
  {
    return new StoreCartDataCommand($this->shareCartWriteRepository(), $this->shareSessionManager());
  }

  /**
   * @param ServerRequestInterface $request
   *
   * @return CartData
   * @throws ArrayValidationException
   */
  public function createCartDataFromStoreCartDataRequest(ServerRequestInterface $request): CartData
  {
    $storeCartDataRequest = new StoreCartDataRequest($this->dependencyProvider->shareArrayValidator());

    return $storeCartDataRequest->createCartDataFromRequest($request);
  }

  /**
   * @return SessionManager
   */
  public function shareSessionManager(): SessionManager
  {
    return $this->dependencyProvider->loadSessionManager();
  }

  /**
   * @return ReadRepository
   */
  public function shareCartReadRepository(): ReadRepository
  {
    /** @var ReadRepository $instance */
    $instance = $this->shareInstance(
      CartReadRepository::class,
      static function (Factory $f): ReadRepository {
        return $f->newCartReadRepository();
      }
    );

    return $instance;
  }

  /**
   * @return ReadRepository
   */
  private function newCartReadRepository(): ReadRepository
  {
    return new CartReadRepository($this->dependencyProvider->loadRedisWebfeClient());
  }

  /**
   * @return WriteRepository
   */
  protected function shareCartWriteRepository(): WriteRepository
  {
    /** @var WriteRepository $instance */
    $instance = $this->shareInstance(
      CartWriteRepository::class,
      static function (Factory $f): WriteRepository {
        return $f->newCartWriteRepository();
      }
    );

    return $instance;
  }

  /**
   * @return WriteRepository
   */
  private function newCartWriteRepository(): WriteRepository
  {
    return new CartWriteRepository($this->dependencyProvider->loadRedisWebfeClient());
  }
}
