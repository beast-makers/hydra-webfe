<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Action;

use BeastMakers\Checkout\Cart\Infra\Repository\ReadRepository;
use BeastMakers\Checkout\Shared\SessionKey;
use BeastMakers\Shared\SessionStorage\SessionManager;

class CartPageConfigCommand
{
  private ReadRepository $readRepository;

  private SessionManager $sessionManager;

  /**
   * @param ReadRepository $readRepository
   * @param SessionManager $sessionManager
   */
  public function __construct(
    ReadRepository $readRepository,
    SessionManager $sessionManager
  ) {
    $this->readRepository = $readRepository;
    $this->sessionManager = $sessionManager;
  }

  /**
   * @return array
   */
  public function buildConfig(): array
  {
    $checkoutId = $this->sessionManager->read(SessionKey::CHECKOUT_ID, '');

    if (!$checkoutId) {
      return [];
    }

    $cartData = $this->readRepository->fetchCartData($checkoutId);

    return [
      'products' => $cartData->getProducts(),
      'voucher' => $cartData->getVoucher(),
    ];
  }
}
