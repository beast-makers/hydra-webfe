<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\Checkout\Cart;

use BeastMakers\Checkout\Cart\Config;
use BeastMakers\Checkout\Cart\DependencyProvider;
use BeastMakers\Checkout\Cart\Factory;
use BeastMakers\Checkout\Cart\Infra\Repository\WriteRepository;

class FactoryPublicDouble extends Factory
{
  public function __construct()
  {
    parent::__construct(new DependencyProvider(), new Config());
  }

  /**
   * @return WriteRepository
   */
  public function shareCartWriteRepository(): WriteRepository
  {
    return parent::shareCartWriteRepository();
  }
}
