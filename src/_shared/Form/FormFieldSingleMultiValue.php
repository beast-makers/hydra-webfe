<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Form;

class FormFieldSingleMultiValue extends FormFieldSingleValue
{
  public const ATTR_ALL_VALUES = 'all_values';

  /**
   * @return array
   */
  public function getAllValues(): array
  {
    return $this->attributes[static::ATTR_ALL_VALUES];
  }

  /**
   * @return array
   */
  protected function attributeList(): array
  {
    return array_merge(parent::attributeList(), [
      static::ATTR_ALL_VALUES,
    ]);
  }
}
