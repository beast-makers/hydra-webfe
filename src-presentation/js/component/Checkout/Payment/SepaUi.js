import {onLiveClick} from '../../own';

export default class SepaUi {

  /**
   * @param {SepaForm} sepaForm
   */
  constructor (sepaForm) {
    this.sepaForm = sepaForm;
  }
  /**
   * @return {void}
   */
  init () {
    this.bindEvents();
  }

  mount () {
    const sepaPanel = document.querySelector('.js-sepa-panel');

    if (!sepaPanel.getAttribute('loaded-marker')) {
      sepaPanel.setAttribute('loaded-marker', true);
      this.sepaForm.mount();
      if (!this.sepaForm.areAllFieldsEmpty()) {
        this.sepaForm.refreshForm();
      }
    }
  }
  /**
   * @return {void}
   */
  bindEvents () {
    onLiveClick('.js-accept-sepa', this.onAcceptPayment.bind(this));
  }

  /**
   * @return {void}
   */
  onAcceptPayment () {
    const validationResult = this.sepaForm.validateForm();

    if (validationResult.isValid) {
      console.log('payment is valid');
    }
  }
}
