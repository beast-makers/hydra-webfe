<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\BackendClient\BackendClient;
use BeastMakers\Shared\BackendClient\Config;
use BeastMakers\Shared\BackendClient\DependencyProvider;
use BeastMakers\Shared\BackendClient\Factory;

trait BackendClientLoader
{
  private ?Factory $backendClientFactory = null;

  /**
   * @return BackendClient
   */
  public function shareBackendClient(): BackendClient
  {
    if (!$this->backendClientFactory) {
      $this->backendClientFactory = new Factory(new DependencyProvider(), new Config());
    }

    return $this->backendClientFactory->shareBackendClient();
  }
}
