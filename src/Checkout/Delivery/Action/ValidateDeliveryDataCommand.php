<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Action;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Checkout\Delivery\Infra\StoreDeliveryDataValidator;
use BeastMakers\Checkout\Shared\ErrorCode;
use BeastMakers\Checkout\Shared\Result\ValidateDeliveryDataResult;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\Result\Error;
use GuzzleHttp\Exception\GuzzleException;

class ValidateDeliveryDataCommand
{
  private StoreDeliveryDataValidator $validator;

  /**
   * @param StoreDeliveryDataValidator $validator
   */
  public function __construct(StoreDeliveryDataValidator $validator)
  {
    $this->validator = $validator;
  }

  /**
   * @param DeliveryData $deliveryData
   *
   * @return ValidateDeliveryDataResult
   * @throws GuzzleException
   * @throws MissingConfigOptionException
   */
  public function validateDeliveryData(DeliveryData $deliveryData): ValidateDeliveryDataResult
  {
    $modelResult = new ValidateDeliveryDataResult();

    $validationMessages = $this->validator->validate($deliveryData);
    if ($validationMessages) {
      $modelResult->addError(new Error(ErrorCode::DELIVERY_DATA_VALIDATION, $validationMessages));

      return $modelResult;
    }

    $modelResult->setIsSuccess(true);

    return $modelResult;
  }
}
