<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Payment;

use BeastMakers\Checkout\Payment\Action\PaymentPageCommand;
use BeastMakers\Checkout\Payment\Infra\SepaForm;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\SessionStorage\SessionManager;
use Smarty;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @return Smarty
   */
  public function shareTemplateEngine(): Smarty
  {
    return $this->dependencyProvider->loadTemplateEngine();
  }

  /**
   * @return SessionManager
   */
  public function shareSessionManager(): SessionManager
  {
    return $this->dependencyProvider->loadSessionManager();
  }

  /**
   * @return PaymentPageCommand
   */
  public function newPaymentPageCommand(): PaymentPageCommand
  {
    return new PaymentPageCommand(
      new SepaForm()
    );
  }
}
