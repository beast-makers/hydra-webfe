import {defaultOptions} from '../../Form/Form';

const internationalNameRegExp = new RegExp(/[^0-9_!¡÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]*/);
const accountNumberRegExp = new RegExp(/[^_!¡÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]*/);

export default {
  account_owner: {
    inputSelector: '#account_owner.input',
    constraints: {
      presence: {allowEmpty: false},
      format: {
        pattern: internationalNameRegExp
      }
    },
    ...defaultOptions
  },

  account_number: {
    inputSelector: '#account_number.input',
    constraints: {
      presence: {allowEmpty: false},
      format: {
        pattern: accountNumberRegExp
      }
    },
    ...defaultOptions
  },
};
