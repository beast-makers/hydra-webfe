<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\DependencyContainerItem;

class BaseConfig
{
  /**
   * @var Config
   */
  private $appConfig;

  public function __construct()
  {
    $this->appConfig = DependencyContainer::getInstance()->get(DependencyContainerItem::APPLICATION_CONFIG);
  }

  /**
   * @param string $configPath
   *
   * @return Config
   * @throws MissingConfigOptionException
   */
  public function getConfig(string $configPath): Config
  {
    return $this->appConfig->getConfig($configPath);
  }

  /**
   * @param string $configPath
   *
   * @return string
   * @throws MissingConfigOptionException
   */
  public function getValue(string $configPath): string
  {
    return $this->appConfig->getString($configPath);
  }
}
