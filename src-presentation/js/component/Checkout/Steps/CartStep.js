import Step from '../../StepEngine/Step';
import {getCartItems, onCartAccepted} from '../Facade';

class CartStep extends Step {
  /**
   * @param {StepEngine} stepEngine
   * @param {Cart} cart
   * @param {CartUi} cartUi
   */
  constructor (stepEngine, cart, cartUi) {
    super(stepEngine);

    this.cart = cart;
    this.cartUi = cartUi;
  }
  /**
   * @inheritDoc
   */
  async init (stepContainerElement) {
    this.cartUi.init();

    onCartAccepted(this.onAcceptCart.bind(this));

    return super.init();
  }

  /**
   * @inheritDoc
   */
  async mount (stepContainerElement) {
    await this.cartUi.mount(stepContainerElement);
  }

  /**
   * @inheritDoc
   */
  async isFinalized () {
    const cartItems = getCartItems();

    return cartItems.length > 0;
  }

  /**
   * @return {void}
   */
  onAcceptCart () {
    this.stepEngine.next();
  }
}

export default CartStep;

