<div class="checkout-delivery-page">
    <div class="page-title">Address</div>
    <form method="get" action="" class="qa-checkout-delivery-form">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-8 col-lg-7">
                <div class="box">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group-title">Personal details</div>
                            {include file="component/form/input-text.tpl"
                                id=$form.first_name.id
                                label=$form.first_name.label
                                placeholder=$form.first_name.placeholder
                                value=$form.first_name.value
                            }
                            {include file="component/form/input-text.tpl"
                                id=$form.last_name.id
                                label=$form.last_name.label
                                placeholder=$form.last_name.placeholder
                                value=$form.last_name.value
                            }
                            {include file="component/form/input-text.tpl"
                                id=$form.email.id
                                label=$form.email.label
                                placeholder=$form.email.placeholder
                                value=$form.email.value
                            }
                            {include file="component/form/input-text.tpl"
                                id=$form.birth_date.id
                                label=$form.birth_date.label
                                placeholder=$form.birth_date.placeholder
                                value=$form.birth_date.value
                            }
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group-title">Address details</div>
                            {include file="component/form/input-select.tpl"
                                id=$form.country_code.id
                                label=$form.country_code.label
                                placeholder=$form.country_code.placeholder
                                all_values=$form.country_code.all_values
                                selected_value=$form.country_code.value
                            }
                            {include file="component/form/input-text.tpl"
                                id=$form.street.id
                                label=$form.street.label
                                placeholder=$form.street.placeholder
                                value=$form.street.value
                            }
                            {include file="component/form/input-text.tpl"
                                id=$form.address_extension.id
                                label=$form.address_extension.label
                                placeholder=$form.address_extension.placeholder
                                value=$form.address_extension.value
                            }
                            {include file="component/form/input-text.tpl"
                                id=$form.city.id
                                label=$form.city.label
                                placeholder=$form.city.placeholder
                                value=$form.city.value
                            }
                            {include file="component/form/input-text.tpl"
                                id=$form.zip.id
                                label=$form.zip.label
                                placeholder=$form.zip.placeholder
                                value=$form.zip.value
                            }
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="button" class="button js-accept-delivery qa-accept-delivery">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
