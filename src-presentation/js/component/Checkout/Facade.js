import {startCheckout} from './Starter';
import Universe from '../../kernel/Universe';
import StateKey from './StateKey';
import EventKey from './EventKey';

/**
 * @return {void}
 */
export function initCheckout () {
  startCheckout();
}

/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */

/**
 * @param {Object} config
 *
 * @return {void}
 */
export function setCheckoutConfig (config) {
  Universe.State.set(StateKey.CHECKOUT_CONFIG, config);
}

/**
 * @return {*}
 */
export function getCheckoutConfig () {
  return Universe.State.get(StateKey.CHECKOUT_CONFIG);
}

/**
 * @param {Object} config
 *
 * @return {void}
 */
export function setCartPageConfig (config) {
  Universe.State.set(StateKey.CART_PAGE_CONFIG, config);
}

/**
 * @return {*}
 */
export function getCartPageConfig () {
  return Universe.State.get(StateKey.CART_PAGE_CONFIG);
}

/**
 * @param {Object} config
 *
 * @return {void}
 */
export function setDeliveryPageConfig (config) {
  Universe.State.set(StateKey.DELIVERY_PAGE_CONFIG, config);
}

/**
 * @return {*}
 */
export function getDeliveryPageConfig () {
  return Universe.State.get(StateKey.DELIVERY_PAGE_CONFIG);
}

/**
 * @param {Object} config
 *
 * @return {void}
 */
export function setPaymentPageConfig (config) {
  Universe.State.set(StateKey.PAYMENT_PAGE_CONFIG, config);
}

/**
 * @return {void}
 */
export function sayCartAccepted() {
  Universe.Event.notify(EventKey.CART_ACCEPTED);
}

/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */
export function onCartAccepted(callback) {
  Universe.Event.subscribe(EventKey.CART_ACCEPTED, callback);
}

/**
 * @return {void}
 */
export function sayDeliveryAccepted() {
  Universe.Event.notify(EventKey.DELIVERY_ACCEPTED);
}

/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */
export function onDeliveryAccepted(callback) {
  Universe.Event.subscribe(EventKey.DELIVERY_ACCEPTED, callback);
}

/**
 * @return {void}
 */
export function sayPaymentAccepted() {
  Universe.Event.notify(EventKey.PAYMENT_ACCEPTED);
}

/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */
export function onPaymentAccepted(callback) {
  Universe.Event.subscribe(EventKey.PAYMENT_ACCEPTED, callback);
}

/**
 * @param {Object} cartItems
 *
 * @return {void}
 */
export function setCartItems(cartItems) {
  Universe.State.set(StateKey.CART_ITEMS, cartItems);
}

/**
 * @return {*}
 */
export function getCartItems () {
  return Universe.State.get(StateKey.CART_ITEMS);
}

/**
 * @param {String} voucher
 *
 * @return {void}
 */
export function setVoucher(voucher) {
  Universe.State.set(StateKey.CART_VOUCHER, voucher);
}
