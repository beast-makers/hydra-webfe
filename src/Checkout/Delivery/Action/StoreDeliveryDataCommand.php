<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Action;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Checkout\Delivery\Infra\Repository\WriteRepository;
use BeastMakers\Checkout\Shared\ErrorCode;
use BeastMakers\Checkout\Shared\Result\StoreDeliveryDataResult;
use BeastMakers\Checkout\Shared\SessionKey;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\SessionStorage\SessionManager;

class StoreDeliveryDataCommand
{
  private WriteRepository $writeRepository;

  private SessionManager $sessionManager;

  /**
   * @param WriteRepository $writeRepository
   * @param SessionManager $sessionManager
   */
  public function __construct(
    WriteRepository $writeRepository,
    SessionManager $sessionManager
  ) {
    $this->writeRepository = $writeRepository;
    $this->sessionManager = $sessionManager;
  }

  /**
   * @param DeliveryData $deliveryData
   *
   * @return StoreDeliveryDataResult
   */
  public function storeDeliveryData(DeliveryData $deliveryData): StoreDeliveryDataResult
  {
    $modelResult = new StoreDeliveryDataResult();

    $checkoutId = $this->sessionManager->read(SessionKey::CHECKOUT_ID, '');
    if (!$checkoutId) {
      $modelResult->addError(new Error(ErrorCode::MISSING_CHECKOUT_ID, []));

      return $modelResult;
    }

    $this->writeRepository->pushDeliveryData($deliveryData, $checkoutId);

    $modelResult->setIsSuccess(true);

    return $modelResult;
  }
}
