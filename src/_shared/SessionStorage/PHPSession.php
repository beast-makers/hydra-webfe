<?php
declare(strict_types=1);

namespace BeastMakers\Shared\SessionStorage;

class PHPSession implements SessionManager
{
  /**
   * @inheritDoc
   */
  public function write(string $key, $value): void
  {
    $_SESSION[$key] = $value;
  }

  /**
   * @inheritDoc
   */
  public function read(string $key, $default = null)
  {
    return $_SESSION[$key] ?? $default;
  }

  /**
   * @inheritDoc
   */
  public function clear(string $key): void
  {
    unset($_SESSION[$key]);
  }
}
