<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Form;

class Form
{
  /** @var FormField[] */
  private array $fields = [];

  /**
   * @param string $identity
   * @param FormField $formField
   *
   * @return void
   */
  public function addField(string $identity, FormField $formField): void
  {
    $this->fields[$identity] = $formField;
  }

  /**
   * @param string $identity
   *
   * @return FormField
   */
  public function getField(string $identity): FormField
  {
    return $this->fields[$identity];
  }

  /**
   * @return array
   */
  public function toArray(): array
  {
    $result = [];

    foreach ($this->fields as $fieldIdentity => $field) {
      $result[$fieldIdentity] = $field->asArray();
    }

    return $result;
  }
}
