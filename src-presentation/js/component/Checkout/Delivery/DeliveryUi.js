import {onLiveClick} from '../../own';
import {openLoadingScreen} from '../../Modal/Facade';
import {sayDeliveryAccepted} from '../Facade';
import {sayApplicationError} from '../../../kernel/Facade';

export default class DeliveryUi {
  /**
   * @param {Delivery} delivery
   * @param {DeliveryForm} deliveryForm
   */
  constructor (delivery, deliveryForm) {
    this.delivery = delivery;
    this.deliveryForm = deliveryForm;
  }

  /**
   * @return {void}
   */
  async init () {
    this.bindEvents();
  }

  /**
   * @return {void}
   */
  bindEvents () {
    onLiveClick('.js-accept-delivery', this.onAcceptDelivery.bind(this));
  }

  /**
   * @param {Element} containerElement
   *
   * @return {void}
   */
  async mount (containerElement) {
    if (document.querySelector('.js-accept-delivery')) {
      return;
    }

    await this.delivery.fetchDeliveryPageData()
      .then((deliveryPage) => {
        const tempContainer = document.createElement('div');
        tempContainer.innerHTML = deliveryPage.delivery_page_html;

        containerElement.classList.add('is-hidden');
        containerElement.innerHTML = tempContainer.innerHTML;
      })
      .catch(() => {
        throw new Error('Could not mount DeliveryUi');
      });
  }

  /**
   * @return {void}
   */
  onAcceptDelivery () {
    const validationResult = this.deliveryForm.validateForm();
    const formData = new FormData(this.deliveryForm.findFormElement());

    if (validationResult.isValid) {
      openLoadingScreen();
    }

    this.delivery.storeDeliveryData(formData)
      .then(() => {})
      .catch(() => {
        sayApplicationError('Something went wrong. We are working on fixing the issue. Please try again later');
      });

    if (validationResult.isValid) {
      sayDeliveryAccepted();
    }
  }
}
