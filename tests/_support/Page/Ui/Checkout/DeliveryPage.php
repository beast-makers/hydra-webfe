<?php
declare(strict_types=1);

namespace BeastMakersTest\Page\Ui\Checkout;

use BeastMakersTest\UiTester;

class DeliveryPage
{
  public const FORM_CLASS_NAME = 'qa-checkout-delivery-form';
  public const FORM_SELECTOR = '.' . self::FORM_CLASS_NAME;

  public const CART_ITEM_LINE_SELECTOR = '.qa-cart-item-line';
  public const PAYMENT_SELECTION_SELECTOR = '.qa-checkout-payment-selection';
  public const INPUT_FIELD_MESSAGE_SELECTOR = '.qa-input-message';

  public const ACCEPT_CART_BUTTON = '.qa-accept-cart';
  public const ACCEPT_DELIVERY_BUTTON = '.qa-accept-delivery';
  /**
   * @var UiTester;
   */
  protected $uiTester;

  public function __construct(UiTester $i)
  {
    $this->uiTester = $i;
  }

  /**
   * @param string $fieldName
   * @param string $value
   *
   * @return void
   */
  public function fillField(string $fieldName, string $value): void
  {
    $formClassName = self::FORM_CLASS_NAME;

    $selector = "//form[contains(@class,'{$formClassName}')]//input[@name='{$fieldName}']";
    $this->uiTester->fillField($selector, $value);
  }

  /**
   * @param string $fieldName
   * @param string $value
   *
   * @return void
   */
  public function seeInField(string $fieldName, string $value): void
  {
    $formClassName = self::FORM_CLASS_NAME;

    $selector = "//form[contains(@class,'{$formClassName}')]//input[@name='{$fieldName}']";
    $this->uiTester->seeInField($selector, $value);
  }

  /**
   * @param string $fieldName
   * @param string $value
   *
   * @return void
   */
  public function seeInSelect(string $fieldName, string $value): void
  {
    $formClassName = self::FORM_CLASS_NAME;

    $selector = "//form[contains(@class,'{$formClassName}')]//select[@name='{$fieldName}']";
    $this->uiTester->seeInField($selector, $value);
  }

  /**
   * @param string $fieldName
   *
   * @return string
   */
  public function selectFirstOption(string $fieldName): string
  {
    $formClassName = self::FORM_CLASS_NAME;

    $selector = "//form[contains(@class,'{$formClassName}')]//select[@name='{$fieldName}']";

    return $this->uiTester->selectFirstOption($selector);
  }
}
