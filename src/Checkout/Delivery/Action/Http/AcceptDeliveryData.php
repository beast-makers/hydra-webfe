<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Action\Http;

use BeastMakers\Checkout\Delivery\Factory;
use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Checkout\Delivery\Infra\Request\DeliveryDataRequest;
use BeastMakers\Checkout\Shared\ErrorCode;
use BeastMakers\Checkout\Shared\Result\AcceptDeliveryDataResult;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\Result\Error;
use BeastMakers\Shared\Result\Result;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class AcceptDeliveryData extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws MissingConfigOptionException
   * @throws GuzzleException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $ddRequest = $this->factory->newDeliveryDataRequest()->readRequest($request);
    $deliveryData = (new DeliveryData())
      ->setAddress($ddRequest->createAddress())
      ->setCustomer($ddRequest->createCustomer())
    ;

    $acceptDeliveryDataModel = $this->factory->newAcceptDeliveryDataCommand();
    $acceptDeliveryDataResult = $acceptDeliveryDataModel->acceptDeliveryData($deliveryData);
    $actionResult = $this->mapDeliveryDataValidationResult2RequestParams($acceptDeliveryDataResult, $ddRequest);

    if ($actionResult->getErrorCollection()->hasErrors()) {
      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($actionResult, $this->getHttpCodeErrorMap());
    }

    return $this->actionResponseBuilder->createJsonHttpResponse(204, []);
  }

  /**
   * @param AcceptDeliveryDataResult $acceptDeliveryDataResult
   * @param DeliveryDataRequest $ddRequest
   *
   * @return Result
   */
  private function mapDeliveryDataValidationResult2RequestParams(
    AcceptDeliveryDataResult $acceptDeliveryDataResult,
    DeliveryDataRequest $ddRequest
  ): Result {
    $result = new Result();
    $errorCollection = $acceptDeliveryDataResult->getErrorCollection();

    if ($errorCollection->hasErrors()) {
      $error = $errorCollection->getFirstError();

      $addressParamsMap = array_flip($ddRequest::defineRequestParamsToAddressMap());
      $customerParamsMap = array_flip($ddRequest::defineRequestParamsToCustomerMap());
      $paramsMap = array_merge($addressParamsMap, $customerParamsMap);

      $remappedMessages = [];
      foreach ($error->message as $deliveryAttribute => $errorMessages) {
        $remappedMessages[$paramsMap[$deliveryAttribute]] = $errorMessages;
      }

      $result->addError(new Error($error->errorCode, $remappedMessages));
    }

    return $result;
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::MISSING_CHECKOUT_ID => 500,
      ErrorCode::DELIVERY_DATA_VALIDATION => 400,
    ];
  }
}
