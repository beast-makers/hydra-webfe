<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Infra\Repository;

use BeastMakers\Checkout\Cart\Infra\CartData;

interface WriteRepository
{
  /**
   * @param CartData $cartData
   * @param string $checkoutId
   *
   * @return void
   */
  public function pushCartData(CartData $cartData, string $checkoutId): void;
}
