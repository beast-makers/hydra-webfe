<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;
use Smarty;

trait TemplateEngine
{
  /**
   * @return Smarty
   */
  protected function shareTemplateEngine(): Smarty
  {
    return DependencyContainer::getInstance()->get(DependencyContainerItem::TEMPLATE_ENGINE);
  }
}
