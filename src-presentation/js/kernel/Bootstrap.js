import {openErrorNotificationModal} from '../component/Modal/Facade';
import {onApplicationError} from './Facade';

export function bootstrap () {
  onApplicationError(function (key, data ) {
    console.log('Runtime error: ' + data.message);
    openErrorNotificationModal(data.message);
  });
}
