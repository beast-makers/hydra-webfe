import {onLiveClick} from '../own';
import {hideOverlay, replaceOverlayContentElement, showOverlay} from './Screen/Overlay';

function buildHtml (headerHtml, contentHtml) {
  return `
  <div class="c-own-modal js-own-modal">
    <div class="content-frame">
      <button class="btn-close js-own-modal-close"></button>
      <div class="content-wrapper">
        <div class="c-own-modal-header">
          ${headerHtml}
        </div>
        <div class="c-own-modal-content">
          ${contentHtml}
        </div>
        <footer class="c-own-modal-footer">
          <button class="button is-small js-own-modal-close">Close</button>
        </footer>
      </div>
    </div>
  </div>
</div>
    `;
}

/**
 * @return {void}
 */
export function bindEvents () {
  onLiveClick('.js-own-modal-close', function () {
    hideNotificationModal();
    hideOverlay();
  });
}

/**
 * @param {string} headerHtml
 * @param {string} contentHtml
 *
 * @return {void}
 */
export function showNotificationModal (headerHtml, contentHtml) {
  const element = document.createElement('div');
  element.innerHTML = buildHtml(headerHtml, contentHtml);
  const modalElement = element.firstElementChild;

  showOverlay();
  replaceOverlayContentElement(modalElement);
  modalElement.classList.add('c-own-modal__is-visible');
}

/**
 * @return {void}
 */
export function hideNotificationModal () {
  const modalElement = findModalElement();

  if (modalElement) {
    modalElement.classList.remove('c-own-modal__is-visible');
  }
}

/**
 * @return {Element}
 */
function findModalElement () {
  return document.querySelector('.js-own-modal');
}
