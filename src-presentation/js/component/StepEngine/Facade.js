import Universe from '../../kernel/Universe';
import EventKey from './EventKey';
import {closeLoadingScreen} from '../Modal/Facade';

/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */

/**
 * @return {void}
 */
export function sayStepDisplayed () {
  Universe.Event.notify(EventKey.STEP_DISPLAYED);
}

/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */
export function onStepDisplayed(callback) {
  Universe.Event.subscribe(EventKey.STEP_DISPLAYED, callback);
}



