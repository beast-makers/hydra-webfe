<?php
declare(strict_types=1);

namespace BeastMakers\Application\Bootstrap;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\DependencyContainer;

class ApplicationConfig
{
  /** @psalm-suppress UndefinedConstant */
  private const CONFIG_DIR = PROJECT_DIR . 'config/';

  /**
   * @return void
   */
  public function run(): void
  {
    /** @psalm-suppress UnresolvableInclude */
    $configuration = include(static::CONFIG_DIR . '_generated/config.php');
    $this->applyCurrentStore($configuration);
    $config = new Config($configuration);

    $diContainer = DependencyContainer::getInstance();
    $diContainer->set(DependencyContainerItem::APPLICATION_CONFIG, function () use ($config) {
      return $config;
    });
  }

  /**
   * @param array $configuration
   *
   * @return void
   * @noinspection PhpDocMissingThrowsInspection
   */
  private function applyCurrentStore(array &$configuration): void
  {
    /** @noinspection PhpUnhandledExceptionInspection */
    $configuration['current_store'] = self::readCurrentStore();
  }

  /**
   * @return string
   * @throws \Exception
   */
  public static function readCurrentStore(): string
  {
    if ($_SERVER['APP_STORE']) {
      return $_SERVER['APP_STORE'];
    }

    if (is_string(getenv('APP_STORE'))) {
      /** @psalm-suppress FalsableReturnStatement */
      return getenv('APP_STORE');
    }

    throw new \Exception('APP_STORE has to be defined');
  }
}
