import request from 'superagent';

const host = 'http://customer-webportal-dev.de.local';

export default {
  /**
   * @param {string} uri
   *
   * @return {Promise}
   */
  get (uri) {
    return new Promise(function (resolve, reject) {
      request
        .get(`${host}${uri}`)
        .then( function (res) {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  /**
   * @param {string} uri
   * @param {Object} data
   *
   * @return {Promise}
   */
  post (uri, data) {
    return new Promise(function (resolve, reject) {
      request
        .post(`${host}${uri}`)
        .send(data)
        .then( function (res) {
          resolve(res);
        })
        .catch( function (error) {
          reject(error);
        });
    });
  }
};
