<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Kernel;

use Slim\App;

interface RoutesInterface
{
  /**
   * @param App $app
   *
   * @return void
   */
  public function defineRoutes(App $app): void;
}
