import {deferrCall} from '../../own';

export default class PayPalUi {
  /**
   * @return {void}
   */
  init () {}

  mount () {
    const paypalPanel = document.querySelector('.js-paypal-panel');

    if (!paypalPanel.getAttribute('loaded-marker')) {
      paypalPanel.setAttribute('loaded-marker', true);
      this.loadPayPalScript();
      deferrCall(this.deferrPayPalButtonRender.bind(this));
    }
  }

  /**
   * @return {void}
   */
  loadPayPalScript () {
    const tag = document.createElement('script');
    tag.src = 'https://www.paypalobjects.com/api/checkout.js';
    document.querySelector('head').append(tag);
  }

  /**
   * @return {boolean}
   */
  deferrPayPalButtonRender () {
    if (window.paypal) {
      this.renderPayPalButton();

      return true;
    }

    return false;
  }

  /**
   * @return {void}
   */
  renderPayPalButton () {
    paypal.Button.render({
      env: 'sandbox',
      payment: function (data, actions) {
        console.log('payment');
      },
      onAuthorize: function (data, actions) {
        console.log('authorize');
      },
      style: {
        size: 'responsive'
      }
    }, '#paypal-button');
  }
}
