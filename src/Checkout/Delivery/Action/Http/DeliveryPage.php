<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Action\Http;

use BeastMakers\Checkout\Delivery\Factory;
use BeastMakers\Checkout\Routes;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class DeliveryPage extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws \SmartyException
   * @throws \Exception
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $deliveryPageModel = $this->factory->newDeliveryPageCommand();

    $templateEngine = $this->factory->shareTemplateEngine();
    $templateEngine->assign('form', $deliveryPageModel->buildDeliveryForm()->toArray());

    return $this->actionResponseBuilder->createJsonHttpResponse(200, [
      'delivery_page_html' => $templateEngine->fetch('page/checkout/delivery.tpl'),
      'delivery_page_config' => $this->getDeliveryPageConfig(),
    ]);
  }

  /**
   * @return array
   */
  private function getDeliveryPageConfig(): array
  {
    $model = $this->factory->newDeliveryPageConfigCommmand();

    $config = $model->buildConfig();
    $config += [
      'endpoints' => [
        'store_delivery_data' => $this->routeParser->urlFor(Routes::ROUTE_CHECKOUT_STORE_DELIVERY_DATA),
        'accept_delivery_data' => $this->routeParser->urlFor(Routes::ROUTE_CHECKOUT_ACCEPT_DELIVERY_DATA),
      ],
    ];

    return $config;
  }
}
