<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Shared\Domain;

use BeastMakers\Shared\TransferObject\ArrayDto;

class CartItemLine extends ArrayDto
{
  public const ATTR_HEADLINE = 'headline';
  public const ATTR_SUBHEADLINE = 'subheadline';
  public const ATTR_DESCRIPTION = 'description';
  public const ATTR_ITEM_IMAGE_URL = 'item_image_url';
  public const ATTR_PRICE = 'price';
  public const ATTR_CURRENCY_SYMBOL = 'currency_symbol';

  /**
   * @return string
   */
  public function getHeadline(): string
  {
    return $this->get(self::ATTR_HEADLINE, '');
  }

  /**
   * @return string
   */
  public function getSubheadline(): string
  {
    return $this->get(self::ATTR_SUBHEADLINE, '');
  }

  /**
   * @return string
   */
  public function getDescription(): string
  {
    return $this->get(self::ATTR_DESCRIPTION, '');
  }

  /**
   * @return string
   */
  public function getItemImageUrl(): string
  {
    return $this->get(self::ATTR_ITEM_IMAGE_URL, '');
  }

  /**
   * @return int
   */
  public function getPrice(): int
  {
    return $this->get(self::ATTR_PRICE, -1);
  }

  /**
   * @return string
   */
  public function getCurrencySymbol(): string
  {
    return $this->get(self::ATTR_CURRENCY_SYMBOL, '');
  }

  /**
   * @inheritDoc
   */
  public function definition(): array
  {
    return [
      self::ATTR_HEADLINE => self::DEF_SCALAR,
      self::ATTR_SUBHEADLINE => self::DEF_SCALAR,
      self::ATTR_DESCRIPTION => self::DEF_SCALAR,
      self::ATTR_ITEM_IMAGE_URL => self::DEF_SCALAR,
      self::ATTR_PRICE => self::DEF_SCALAR,
      self::ATTR_CURRENCY_SYMBOL => self::DEF_SCALAR,
    ];
  }
}
