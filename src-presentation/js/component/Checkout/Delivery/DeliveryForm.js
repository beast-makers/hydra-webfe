import {Form} from '../../Form/Form';

export default class DeliveryForm {
  /**
   * @param {Object} formFields
   */
  constructor (formFields) {
    this.formFields = formFields;
    this.formBinder = new Form(this.formFields);
  }

  /**
   * @return {void}
   */
  mount () {
    this.formBinder.bind();
    this.formBinder.bindValidation();
  }

  /**
   * @return {boolean}
   */
  areAllFieldsEmpty () {
    const result = this.formBinder.findEmptyFields();

    return result.isEmpty;
  }

  /**
   * @return {void}
   */
  refreshForm () {
    this.formBinder.validateForm();
  }

  /**
   * @return {ValidationResult}
   */
  validateForm () {
    return this.formBinder.validateForm();
  }

  /**
   * @return {boolean}
   */
  isFormValid () {
    const result = this.formBinder.validateAllFields();

    return result.isValid;
  }

  /**
   * @return {Element}
   */
  findFormElement () {
    return this.formBinder.findFormElement();
  }
}
