import 'element-closest-polyfill';
import {bootstrap} from '../kernel/Bootstrap';
import {initModal} from '../component/Modal/Facade';

bootstrap();
initModal();
