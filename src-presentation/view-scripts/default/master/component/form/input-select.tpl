<div class="form-field js-form-field">
    {if !empty($label)}
        <label class="label" for="{$id}">{$label}</label>
    {/if}
    <div class="form-field-control form-select-field">
        <select class="select" id="{$id}" name="{$id}">
            {foreach $all_values as $value=>$label}
                {$selected=''}
                {if strtolower($value) === strtolower($selected_value)}{$selected='selected'}{/if}
                <option value="{$value}" {$selected}>{$label}</option>
            {/foreach}
            {if !empty($placeholder)}
                {$selected=''}
                {if empty($selected_value)}{$selected='selected'}{/if}
                <option value="" disabled hidden {$selected}>{$placeholder}</option>
            {/if}
        </select>
    </div>
    <div class="input-message js-input-message qa-input-message"></div>
</div>
