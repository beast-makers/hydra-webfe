<?php
declare(strict_types=1);

namespace BeastMakers\Shared\BackendClient;

use BeastMakers\Shared\Kernel\BaseDependencyProvider;

class DependencyProvider extends BaseDependencyProvider
{
}
