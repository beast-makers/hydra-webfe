<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Action;

use BeastMakers\Checkout\Delivery\Infra\DeliveryForm;
use BeastMakers\Checkout\Delivery\Infra\Repository\ReadRepository;
use BeastMakers\Checkout\Shared\SessionKey;
use BeastMakers\Shared\SessionStorage\SessionManager;

class DeliveryPageCommand
{
  private DeliveryForm $deliveryForm;

  private SessionManager $sessionManager;

  private ReadRepository $readRepository;

  /**
   * @param DeliveryForm $deliveryForm
   * @param SessionManager $sessionManager
   * @param ReadRepository $readRepository
   */
  public function __construct(
    DeliveryForm $deliveryForm,
    SessionManager $sessionManager,
    ReadRepository $readRepository
  ) {
    $this->deliveryForm = $deliveryForm;
    $this->sessionManager = $sessionManager;
    $this->readRepository = $readRepository;
  }

  /**
   * @return DeliveryForm
   */
  public function buildDeliveryForm(): DeliveryForm
  {
    $checkoutId = $this->sessionManager->read(SessionKey::CHECKOUT_ID, '');
    $deliveryData = $this->readRepository->fetchDeliveryData($checkoutId);
    $this->deliveryForm->initialize($deliveryData);

    return $this->deliveryForm;
  }
}
