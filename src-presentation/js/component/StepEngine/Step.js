class Step {
  /**
   * @param {StepEngine} stepEngine
   */
  constructor (stepEngine) {
    this.stepEngine = stepEngine;
  }

  /**
   * @param {Element} stepContainerElement
   *
   * @return {Promise}
   */
  async init (stepContainerElement) {
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }

  /**
   * @param {Element} stepContainerElement
   *
   *  @return {void}
   */
  async mount (stepContainerElement) {}

  /**
   * @param {Element} stepContainerElement
   *
   * @return {void}
   */
  async show (stepContainerElement) {
    stepContainerElement.classList.remove('is-hidden');
  }

  /**
   * @param {Element} stepContainerElement
   *
   * @return {void}
   */
  async hide (stepContainerElement) {
    stepContainerElement.classList.add('is-hidden');
  }

  /**
   * @return {boolean}
   */
  async isFinalized () {
    return false;
  }
}

export default Step;
