<?php
declare(strict_types=1);

namespace BeastMakers\Shared\SessionStorage;

interface SessionManager
{
  /**
   * @param string $key
   * @param mixed $value
   *
   * @return void
   */
  public function write(string $key, $value): void;

  /**
   * @param string $key
   * @param $default
   *
   * @return mixed
   */
  public function read(string $key, $default = null);

  /**
   * @param string $key
   *
   * @return void
   */
  public function clear(string $key): void;
}
