<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Config;

class MissingConfigOptionException extends \Exception
{
  public function __construct(string $optionPath)
  {
    parent::__construct("Configuration option not found [{$optionPath}]");
  }
}
