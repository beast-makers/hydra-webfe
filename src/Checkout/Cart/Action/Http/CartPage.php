<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Action\Http;

use BeastMakers\Checkout\Cart\Factory;
use BeastMakers\Checkout\Cart\Infra\CartData;
use BeastMakers\Checkout\Routes;
use BeastMakers\Checkout\Shared\SessionKey;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class CartPage extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws \SmartyException
   * @throws \Exception
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $templateEngine = $this->factory->shareTemplateEngine();
    $templateEngine->assign([
      'page_title' => 'Starten Sie Ihre Behandlung mit dem Abdruckset',
      'cart_details' => $this->factory->newCartPageCommand()->fetchCartDetails('fake cart id'),
    ]);

    $cartData = $this->readCartData();

    return $this->actionResponseBuilder->createJsonHttpResponse(200, [
      'cart_page_html' => $templateEngine->fetch('page/checkout/cart.tpl'),
      'cart_details_html' => $templateEngine->fetch('page/checkout/cart/cart-details.tpl'),
      'cart_page_config' => $this->getCartPageConfig(),
      'cart_items' => $cartData->getProducts(),
      'voucher' => $cartData->getVoucher(),
    ]);
  }

  /**
   * @return array
   */
  private function getCartPageConfig(): array
  {
    $model = $this->factory->newCartPageConfigCommand();

    $config = $model->buildConfig();
    $config += [
      'endpoints' => [
        'store_cart_data' => $this->routeParser->urlFor(Routes::ROUTE_CHECKOUT_STORE_CART_DATA),
      ],
    ];

    return $config;
  }

  /**
   * @return CartData
   */
  public function readCartData(): CartData
  {
    $checkoutId = $this->factory->shareSessionManager()->read(SessionKey::CHECKOUT_ID);

    if (!$checkoutId) {
      return new CartData();
    }

    return $this->factory->shareCartReadRepository()->fetchCartData($checkoutId);
  }
}
