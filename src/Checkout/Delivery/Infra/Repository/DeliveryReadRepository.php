<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra\Repository;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Checkout\Shared\Domain\Address;
use BeastMakers\Checkout\Shared\Domain\Customer;
use BeastMakers\Shared\RedisConnector\RedisClient;
use BeastMakers\Shared\RedisConnector\RedisException;

class DeliveryReadRepository implements ReadRepository
{
  private RedisClient $redisClient;

  public function __construct(RedisClient $redisClient)
  {
    $this->redisClient = $redisClient;
  }

  /**
   * @inheritDoc
   * @throws RedisException
   */
  public function fetchDeliveryData(string $checkoutId): DeliveryData
  {
    $data = $this->redisClient->redis()->mget([
      Key::CHECKOUT_DELIVERY_ADDRESS_PREFIX . $checkoutId,
      Key::CHECKOUT_DELIVERY_CUSTOMER_PREFIX . $checkoutId,
    ]);

    $addressAsJson = $data[0] ?: '{}';
    $customerAsJson = $data[1] ?: '{}';
    $addressDto = Address::createFromJson($addressAsJson);
    $customerDto = Customer::createFromJson($customerAsJson);

    $deliveryData = new DeliveryData();
    $deliveryData->setAddress($addressDto);
    $deliveryData->setCustomer($customerDto);

    return $deliveryData;
  }
}
