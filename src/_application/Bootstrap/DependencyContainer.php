<?php
declare(strict_types=1);

namespace BeastMakers\Application\Bootstrap;

use BeastMakers\Shared\DependencyContainerItem;
use BeastMakers\Shared\Kernel\Action\ActionResponseBuilder;
use BeastMakers\Shared\Kernel\DependencyContainer as Di;
use Slim\App;
use Slim\Interfaces\RouteParserInterface;

class DependencyContainer
{
  private ?ActionResponseBuilder $actionResponseBuilder = null;

  private ?RouteParserInterface $actionRouteParser = null;

  /**
   * @return void
   */
  public function run(App $app): void
  {
    $diContainer = Di::getInstance();
    $this->shareActionResponseBuilder($diContainer);
    $this->shareActionRouteParser($diContainer, $app);
  }

  /**
   * @param Di $diContainer
   *
   * @return void
   */
  private function shareActionResponseBuilder(Di $diContainer): void
  {
    $diContainer->set(DependencyContainerItem::ACTION_RESPONSE_BUILDER, function () {
      if ($this->actionResponseBuilder === null) {
        $this->actionResponseBuilder = new ActionResponseBuilder();
      }

      return $this->actionResponseBuilder;
    });
  }

  /**
   * @param Di $diContainer
   * @param App $app
   *
   * @return void
   */
  private function shareActionRouteParser(Di $diContainer, App $app): void
  {
    $diContainer->set(DependencyContainerItem::ACTION_ROUTE_PARSER, function () use ($app) {
      if ($this->actionRouteParser === null) {
        $this->actionRouteParser = $app->getRouteCollector()->getRouteParser();
      }

      return $this->actionRouteParser;
    });
  }
}
