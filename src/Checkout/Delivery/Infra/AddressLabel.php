<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra;

interface AddressLabel
{
  public const COUNTRY = 'Country';
  public const STREET = 'Street';
  public const ADDRESS_EXTENSION = 'Address extension';
  public const CITY = 'City';
  public const ZIP = 'Postcode';
}
