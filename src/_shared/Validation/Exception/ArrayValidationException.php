<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Validation\Exception;

use Throwable;

class ArrayValidationException extends \Exception
{
  private array $errors = [];

  /**
   * @param array $errors
   * @param Throwable|null $previous
   */
  public function __construct(array $errors, ?Throwable $previous = null)
  {
    $this->errors = $errors;
    parent::__construct('Validation exception', 0, $previous);
  }

  /**
   * @return array
   */
  public function getErrors(): array
  {
    return $this->errors;
  }
}
