export default {
  OPEN_LOADING_SCREEN: 'modal-open_loading_screen',
  CLOSE_LOADING_SCREEN: 'modal-close_loading_screen',

  OPEN_ERROR_NOTIFICATION_MODAL: 'modal-open_error_notification'
};

