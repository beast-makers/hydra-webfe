<?php
declare(strict_types=1);

namespace BeastMakers\Application;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\Kernel\RoutesInterface;
use Slim\App;

class RoutesLoader
{
  private const COMMON_ROUTE_CLASSNAME_PATTERN = 'BeastMakers\%s\Routes';
  private const STORE_SPECIFIC_ROUTE_CLASSNAME_PATTERN = 'BeastMakers\%s\Routes%s';

  /** @psalm-suppress UndefinedConstant */
  private const PROJECT_DIR = PROJECT_DIR;

  /** @var Config */
  private $appConfig;

  public function __construct(Config $appConfig)
  {
    $this->appConfig = $appConfig;
  }

  /**
   * @param App $app
   * @param string $store
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  public function loadRoutes(App $app, string $store): void
  {
    $this->initRouteCaching($app, $store);

    $classNames = $this->readCachedRoutesClassNames($store);
    $this->loadApplicationRoutes($app, $classNames);
  }

  /**
   * @param App $app
   * @param string $store
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  public function buildRoutesCache(App $app, string $store): void
  {
    $globRoutesPattern = self::PROJECT_DIR . 'src/*/Routes.php';
    $routeClassFilePaths = glob($globRoutesPattern);

    $classNames = $this->discoverRoutesClassNames($store, $routeClassFilePaths);
    $this->loadApplicationRoutes($app, $classNames);
    $this->buildRoutesClassNamesCache($classNames, $store);
  }

  /**
   * @param App $app
   * @param string $store
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  private function initRouteCaching(App $app, string $store): void
  {
    $fastRouteCachePath = $this->getFastRouteCachePath($store);

    $routeCollector = $app->getRouteCollector();
    $routeCollector->setCacheFile($fastRouteCachePath);
  }

  /**
   * @param string $store
   * @param array $routeClassFilePaths
   *
   * @return array
   */
  private function discoverRoutesClassNames(string $store, array $routeClassFilePaths): array
  {
    $classNames = [];

    foreach ($routeClassFilePaths as $classFilePath) {
      $moduleName = $this->extractModuleName($classFilePath);

      $className = sprintf(self::STORE_SPECIFIC_ROUTE_CLASSNAME_PATTERN, $moduleName, $store);
      if (!class_exists($className)) {
        $className = sprintf(self::COMMON_ROUTE_CLASSNAME_PATTERN, $moduleName);
      }
      $classNames[] = $className;
    }

    return $classNames;
  }

  /**
   * @param string $classFilePath
   *
   * @return string
   */
  private function extractModuleName(string $classFilePath): string
  {
    $tokens = explode('/', $classFilePath);
    end($tokens);

    return prev($tokens);
  }

  /**
   * @param App $app
   * @param array $classNames
   *
   * @return void
   */
  private function loadApplicationRoutes(App $app, array $classNames): void
  {
    foreach ($classNames as $className) {
      /** @var RoutesInterface $instance */
      $instance = new $className();
      $instance->defineRoutes($app);
    }
  }

  /**
   * @param array $classNames
   * @param string $store
   *
   * @return void
   * @throws MissingConfigOptionException
   */
  private function buildRoutesClassNamesCache(array $classNames, string $store): void
  {
    $filePath = $this->getRoutesClassFilesCachePath($store);

    $cacheContent = var_export($classNames, true);
    $finalContent = <<<CONFIG
<?php
return {$cacheContent};
CONFIG;
    file_put_contents($filePath, $finalContent);
  }

  /**
   * @return array
   * @throws MissingConfigOptionException
   */
  private function readCachedRoutesClassNames(string $store): array
  {
    $filePath = $this->getRoutesClassFilesCachePath($store);
    return require_once $filePath;
  }

  /**
   * @param string $store
   *
   * @return string
   * @throws MissingConfigOptionException
   */
  private function getRoutesClassFilesCachePath(string $store): string
  {
    $fileName = "route-class-cache{$store}.php";
    return self::PROJECT_DIR . $this->appConfig->getString('route_cache_dir') . $fileName;
  }

  /**
   * @param string $store
   *
   * @return string
   * @throws MissingConfigOptionException
   */
  private function getFastRouteCachePath(string $store): string
  {
    $fileName = "fast-route-cache{$store}.php";
    return self::PROJECT_DIR . $this->appConfig->getString('route_cache_dir') . $fileName;
  }
}
