<?php
declare(strict_types=1);

namespace BeastMakers\Checkout;

use BeastMakers\Checkout\Cart\Action\Http\CartPage;
use BeastMakers\Checkout\Cart\Action\Http\StoreCartData;
use BeastMakers\Checkout\Delivery\Action\Http\AcceptDeliveryData;
use BeastMakers\Checkout\Delivery\Action\Http\DeliveryPage;
use BeastMakers\Checkout\Delivery\Action\Http\StoreDeliveryData;
use BeastMakers\Checkout\Main\Action\Http\MainPage;
use BeastMakers\Checkout\Payment\Action\Http\AcceptSepaPayment;
use BeastMakers\Checkout\Payment\Action\Http\PaymentPage;
use BeastMakers\Checkout\Payment\Action\Http\StorePaymentData;
use BeastMakers\Shared\Kernel\RoutesInterface;
use Slim\App;

class Routes implements RoutesInterface
{
  public const ROUTE_CHECKOUT_MAIN_PAGE = 'checkout.main-page';
  public const ROUTE_CHECKOUT_CART_PAGE = 'checkout.cart-page';
  public const ROUTE_CHECKOUT_DELIVERY_PAGE = 'checkout.delivery-page';
  public const ROUTE_CHECKOUT_PAYMENT_PAGE = 'checkout.payment-page';

  public const ROUTE_CHECKOUT_STORE_CART_DATA = 'checkout.store-cart-data';
  public const ROUTE_CHECKOUT_STORE_DELIVERY_DATA = 'checkout.store-delivery-data';
  public const ROUTE_CHECKOUT_ACCEPT_DELIVERY_DATA = 'checkout.accept-delivery-data';
  public const ROUTE_CHECKOUT_STORE_PAYMENT_DATA = 'checkout.store-payment-data';

  public const ROUTE_CHECKOUT_ACCEPT_SEPA_PAYMENT = 'checkout.accept-sepa-payment';

  /**
   * @param App $app
   *
   * @return void
   */
  public function defineRoutes(App $app): void
  {
    $app->get('/checkout', MainPage::class)
      ->setName(self::ROUTE_CHECKOUT_MAIN_PAGE);

    $app->get('/checkout/cart', CartPage::class)
      ->setName(self::ROUTE_CHECKOUT_CART_PAGE);

    $app->get('/checkout/delivery', DeliveryPage::class)
      ->setName(self::ROUTE_CHECKOUT_DELIVERY_PAGE);

    $app->get('/checkout/payment', PaymentPage::class)
      ->setName(self::ROUTE_CHECKOUT_PAYMENT_PAGE);

    $app->post('/checkout/store-cart-data', StoreCartData::class)
      ->setName(self::ROUTE_CHECKOUT_STORE_CART_DATA);

    $app->post('/checkout/accept-delivery-data', AcceptDeliveryData::class)
      ->setName(self::ROUTE_CHECKOUT_ACCEPT_DELIVERY_DATA);

    $app->post('/checkout/store-delivery-data', StoreDeliveryData::class)
      ->setName(self::ROUTE_CHECKOUT_STORE_DELIVERY_DATA);

    $app->post('/checkout/store-payment-data', StorePaymentData::class)
      ->setName(self::ROUTE_CHECKOUT_STORE_PAYMENT_DATA);

    $app->post('/checkout/accept-sepa-payment', AcceptSepaPayment::class)
      ->setName(self::ROUTE_CHECKOUT_ACCEPT_SEPA_PAYMENT);
  }
}
