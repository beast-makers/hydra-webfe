<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Payment\Infra;

interface SepaLabel
{
  public const ACCOUNT_OWNER = 'Account owner';
  public const ACCOUNT_NUMBER = 'Account number';
}
