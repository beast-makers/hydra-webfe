export default {
  CART_ACCEPTED: 'checkout-cart_accepted',
  PAYMENT_ACCEPTED: 'checkout-payment_accepted',
  DELIVERY_ACCEPTED: 'checkout-delivery_accepted'
};

