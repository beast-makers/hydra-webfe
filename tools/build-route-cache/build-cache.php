#!/usr/bin/php
<?php
declare(strict_types=1);

use BeastMakers\Application\RoutesLoader;
use BeastMakers\Shared\Config\Config;
use Slim\Factory\AppFactory;

define('PROJECT_DIR', __DIR__ . '/../../');
define('CONFIG_DIR', PROJECT_DIR . 'config/');
define('TOOLS_DIR', PROJECT_DIR . 'tools/');

require PROJECT_DIR . 'vendor/autoload.php';

$configuration = require_once CONFIG_DIR . '_generated/config.php';
$appConfig = new Config($configuration);
$routeCacheDir = PROJECT_DIR . $appConfig->getString('route_cache_dir');
$allStores = $appConfig->getArray('available_stores');

//remove all cache files
array_map('unlink', glob($routeCacheDir . '*.php'));

$routes = new RoutesLoader($appConfig);
foreach ($allStores as $store) {
  $app = AppFactory::create();
  $routes->buildRoutesCache($app, $store);
}
