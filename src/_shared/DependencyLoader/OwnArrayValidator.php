<?php
declare(strict_types=1);

namespace BeastMakers\Shared\DependencyLoader;

use BeastMakers\Shared\Validation\ArrayValidator;

trait OwnArrayValidator
{
  protected ?ArrayValidator $ownValidator = null;

  /**
   * @return ArrayValidator
   */
  public function shareArrayValidator(): ArrayValidator
  {
    if ($this->ownValidator === null) {
      $this->ownValidator = new ArrayValidator();
    }

    return $this->ownValidator;
  }
}
