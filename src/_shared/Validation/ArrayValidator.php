<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Validation;

use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Rakit\Validation\Validation;

class ArrayValidator
{
  private RakitValidator $validator;

  public function __construct()
  {
    $this->validator = new RakitValidator();
  }

  /**
   * @param array $inputs
   * @param array $rules
   * @param array $messages
   *
   * @return void
   * @throws ArrayValidationException
   */
  public function validate(array $inputs, array $rules, array $messages = []): void
  {
    $validationResult = $this->validator->validate(
      $inputs,
      $rules,
      $messages
    );

    if ($validationResult->fails()) {
      $errors = $this->structureErrors($validationResult);
      throw new ArrayValidationException($errors);
    }
  }

  /**
   * @param Validation $validationResult
   *
   * @return array
   */
  private function structureErrors(Validation $validationResult): array
  {
    /** @var array $invalidData */
    $invalidData = $validationResult->getInvalidData();
    $errorsAsArray = $validationResult->errors()->toArray();

    $result = [];
    foreach ($errorsAsArray as $fieldName => $errors) {
      $result[$fieldName] = $this->brokenRules($fieldName, $errors, $invalidData[$fieldName]);
    }

    return $result;
  }

  /**
   * @param string $fieldName
   * @param array $errors
   * @param $invalidValue
   *
   * @return array
   */
  private function brokenRules(string $fieldName, array $errors, $invalidValue): array
  {
    $brokenRules = [];
    foreach ($errors as $ruleName => $errorMessage) {
      $brokenRules[] = [
        'field' => $fieldName,
        'rule' => $ruleName,
        'messageTemplate' => $errorMessage,
        'invalidValue' => $invalidValue,
      ];
    }

    return $brokenRules;
  }
  /**
   * @return RakitValidator
   */
  public function rakit(): RakitValidator
  {
    return $this->validator;
  }
}
