{assign form value=$data.form}

<div class="js-sepa-panel">
    <div class="sepa-form">
        <form method="post" action="{$form.actionUrl}">
            {include file="component/form/input-text.tpl"
                id=$form.account_owner.id
                label=$form.account_owner.label
                placeholder=$form.account_owner.placeholder
            }
            {include file="component/form/input-text.tpl"
                id=$form.account_number.id
                label=$form.account_number.label
                placeholder=$form.account_number.placeholder
            }
            <div class="text-center">
                <button type="button" class="button js-accept-sepa">Continue</button>
            </div>
        </form>
    </div>
</div>
