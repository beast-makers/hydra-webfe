<?php
declare(strict_types=1);

namespace BeastMakers\Shared\BackendClient;

use BeastMakers\Shared\Config\MissingConfigOptionException;
use BeastMakers\Shared\Kernel\BaseFactory;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

/**
 * @property Config $config
 */
class Factory extends BaseFactory
{
  private const CONNECTION_TIMEOUT_SECONDS = 1;

  /**
   * @return BackendClient
   * @noinspection PhpDocMissingThrowsInspection
   */
  public function shareBackendClient(): BackendClient
  {
    /** @var BackendClient|null $instance */
    $instance = $this->getCachedInstance('BackendClient');
    if ($instance === null) {
      /** @noinspection PhpUnhandledExceptionInspection */
      $instance = new BackendClient($this->shareHttpClient(), $this->config->getBackendClientAuthConfig());
      $this->setCachedInstance('BackendClient', $instance);
    }

    return $instance;
  }

  /**
   * @return ClientInterface
   * @throws MissingConfigOptionException
   */
  private function shareHttpClient(): ClientInterface
  {
    /** @var ClientInterface|null $instance */
    $instance = $this->getCachedInstance('BackendHttpClient');
    if ($instance === null) {
      $instance = new Client([
        'base_uri' => $this->config->getBackendClientBaseUri(),
        'connect_timeout' => self::CONNECTION_TIMEOUT_SECONDS,
      ]);
      $this->setCachedInstance('BackendHttpClient', $instance);
    }

    return $instance;
  }
}
