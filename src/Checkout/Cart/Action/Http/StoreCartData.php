<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Action\Http;

use BeastMakers\Checkout\Cart\Factory;
use BeastMakers\Checkout\Shared\ErrorCode;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use BeastMakers\Shared\Validation\Exception\ArrayValidationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class StoreCartData extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws ArrayValidationException
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $cartData = $this->factory->createCartDataFromStoreCartDataRequest($request);

    $model = $this->factory->newStoreCartDataCommand();
    $storeCartDataResult = $model->storeCartData($cartData);

    if ($storeCartDataResult->getErrorCollection()->hasErrors()) {
      return $this->actionResponseBuilder->buildJsonHttpErrorResponse($storeCartDataResult, $this->getHttpCodeErrorMap());
    }

    return $this->actionResponseBuilder->createJsonHttpResponse(204, []);
  }

  /**
   * @return array
   */
  private function getHttpCodeErrorMap(): array
  {
    return [
      ErrorCode::MISSING_CHECKOUT_ID => 500,
    ];
  }
}
