<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra\Repository;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Shared\RedisConnector\RedisClient;
use BeastMakers\Shared\RedisConnector\RedisException;

class DeliveryWriteRepository implements WriteRepository
{
  private const EXPIRY_3_MONTHS = 3600 * 24 * 30 * 3;

  private RedisClient $redisClient;

  public function __construct(RedisClient $redisClient)
  {
    $this->redisClient = $redisClient;
  }

  /**
   * @inheritDoc
   * @throws RedisException
   */
  public function pushDeliveryData(DeliveryData $deliveryData, string $checkoutId): void
  {
    $address = $deliveryData->getAddress();
    $this->redisClient->redis()->setex(
      Key::CHECKOUT_DELIVERY_ADDRESS_PREFIX . $checkoutId,
      self::EXPIRY_3_MONTHS,
      $address->toJson()
    );

    $customer = $deliveryData->getCustomer();
    $this->redisClient->redis()->setex(
      Key::CHECKOUT_DELIVERY_CUSTOMER_PREFIX . $checkoutId,
      self::EXPIRY_3_MONTHS,
      $customer->toJson()
    );
  }
}
