<div class="cart-item-line qa-cart-item-line">
    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <img class="image" src="{$product_image}" />
            <div class="price-block is-image-relative d-block d-sm-none">
                <div class="ribbon ribbon-shadow"></div>
                <div class="ribbon ribbon-back">{$price}</div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9">
            {if !empty($headline)}
                <div class="headline">{$headline}</div>
            {/if}
            {if !empty($subheadline)}
                <div class="subheadline">{$subheadline}</div>
            {/if}
            {if !empty($description)}
                <div class="description">{$description}</div>
            {/if}
        </div>
    </div>
    <div class="price-block is-line-relative d-none d-sm-block">
        <div class="ribbon ribbon-shadow"></div>
        <div class="ribbon ribbon-back">{$price}</div>
    </div>
</div>
