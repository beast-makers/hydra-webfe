<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery;

use BeastMakers\Checkout\Delivery\Action\AcceptDeliveryDataCommand;
use BeastMakers\Checkout\Delivery\Action\DeliveryPageCommand;
use BeastMakers\Checkout\Delivery\Action\DeliveryPageConfigCommand;
use BeastMakers\Checkout\Delivery\Action\StoreDeliveryDataCommand;
use BeastMakers\Checkout\Delivery\Action\ValidateDeliveryDataCommand;
use BeastMakers\Checkout\Delivery\Infra\DeliveryForm;
use BeastMakers\Checkout\Delivery\Infra\Repository\DeliveryReadRepository;
use BeastMakers\Checkout\Delivery\Infra\Repository\DeliveryWriteRepository;
use BeastMakers\Checkout\Delivery\Infra\Repository\ReadRepository;
use BeastMakers\Checkout\Delivery\Infra\Repository\WriteRepository;
use BeastMakers\Checkout\Delivery\Infra\Request\DeliveryDataRequest;
use BeastMakers\Checkout\Delivery\Infra\StoreDeliveryDataValidator;
use BeastMakers\Shared\Kernel\BaseFactory;
use BeastMakers\Shared\SessionStorage\SessionManager;
use Smarty;

/**
 * @property DependencyProvider $dependencyProvider
 */
class Factory extends BaseFactory
{
  /**
   * @return Smarty
   */
  public function shareTemplateEngine(): Smarty
  {
    return $this->dependencyProvider->loadTemplateEngine();
  }

  /**
   * @return DeliveryPageCommand
   */
  public function newDeliveryPageCommand(): DeliveryPageCommand
  {
    return new DeliveryPageCommand(
      new DeliveryForm(),
      $this->shareSessionManager(),
      $this->shareDeliveryReadRepository()
    );
  }

  /**
   * @return DeliveryPageConfigCommand
   */
  public function newDeliveryPageConfigCommmand(): DeliveryPageConfigCommand
  {
    return new DeliveryPageConfigCommand();
  }

  /**
   * @return StoreDeliveryDataCommand
   */
  public function newStoreDeliveryDataCommand(): StoreDeliveryDataCommand
  {
    return new StoreDeliveryDataCommand(
      $this->shareDeliveryWriteRepository(),
      $this->shareSessionManager()
    );
  }

  /**
   * @return AcceptDeliveryDataCommand
   */
  public function newAcceptDeliveryDataCommand(): AcceptDeliveryDataCommand
  {
    return new AcceptDeliveryDataCommand(
      $this->newValidateDeliveryDataCommand(),
      $this->newStoreDeliveryDataCommand()
    );
  }

  /**
   * @return DeliveryDataRequest
   */
  public function newDeliveryDataRequest(): DeliveryDataRequest
  {
    return new DeliveryDataRequest($this->dependencyProvider->shareArrayValidator());
  }

  /**
   * @return SessionManager
   */
  public function shareSessionManager(): SessionManager
  {
    return $this->dependencyProvider->loadSessionManager();
  }

  /**
   * @return ReadRepository
   */
  protected function shareDeliveryReadRepository(): ReadRepository
  {
    /** @var ReadRepository $instance */
    $instance = $this->shareInstance(
      DeliveryReadRepository::class,
      static function (Factory $f): ReadRepository {
        return $f->newDeliveryReadRepository();
      }
    );

    return $instance;
  }

  /**
   * @return ReadRepository
   */
  private function newDeliveryReadRepository(): ReadRepository
  {
    return new DeliveryReadRepository(
      $this->dependencyProvider->loadRedisWebfeClient()
    );
  }

  /**
   * @return WriteRepository
   */
  protected function shareDeliveryWriteRepository(): WriteRepository
  {
    /** @var WriteRepository $instance */
    $instance = $this->shareInstance(
      DeliveryWriteRepository::class,
      static function (Factory $f): WriteRepository {
        return $f->newDeliveryWriteRepository();
      }
    );

    return $instance;
  }

  /**
   * @return WriteRepository
   */
  private function newDeliveryWriteRepository(): WriteRepository
  {
    return new DeliveryWriteRepository(
      $this->dependencyProvider->loadRedisWebfeClient()
    );
  }

  /**
   * @return ValidateDeliveryDataCommand
   */
  private function newValidateDeliveryDataCommand(): ValidateDeliveryDataCommand
  {
    return new ValidateDeliveryDataCommand(
      $this->newStoreDeliveryDataValidator()
    );
  }

  /**
   * @return StoreDeliveryDataValidator
   */
  private function newStoreDeliveryDataValidator(): StoreDeliveryDataValidator
  {
    return new StoreDeliveryDataValidator($this->dependencyProvider->shareBackendClient());
  }
}
