import CheckoutClient from '../CheckoutClient';

export default class Payment {
  /**
   * @return {Promise<Object>}
   */
  fetchPaymentPageData () {
    return new Promise((resolve, reject) => {
      CheckoutClient.fetchPaymentPageData()
        .then((paymentPage) => {
          resolve(paymentPage);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
