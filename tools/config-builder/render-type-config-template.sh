#!/usr/bin/env bash
set -euo pipefail

source includes/require-app-type.sh

printf "Building [${APP_TYPE}] config\n"

INPUT_TEMPLATE_FILE="/var/webfe/config/env/${APP_TYPE}/config.tmpl.json"
OUTPUT_CONFIG_FILE="/var/webfe/config/_generated/config-type.json"

envsubst < ${INPUT_TEMPLATE_FILE} > ${OUTPUT_CONFIG_FILE}
