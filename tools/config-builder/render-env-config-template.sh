#!/usr/bin/env bash
set -euo pipefail

source includes/require-app-type.sh
source includes/require-app-env.sh

printf "Building [${APP_ENV}] config\n"

INPUT_TEMPLATE_FILE="/var/webfe/config/env/${APP_TYPE}/${APP_ENV}/config.tmpl.json"
OUTPUT_CONFIG_FILE="/var/webfe/config/_generated/config-env.json"

envsubst < ${INPUT_TEMPLATE_FILE} > ${OUTPUT_CONFIG_FILE}
