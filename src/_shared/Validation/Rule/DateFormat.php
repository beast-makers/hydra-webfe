<?php
declare(strict_types=1);

namespace BeastMakers\Shared\Validation\Rule;

use Rakit\Validation\MissingRequiredParameterException;
use Rakit\Validation\Rule;

class DateFormat extends Rule
{
  /** @var string */
  protected $message = 'The :attribute is not a valid date format';

  /** @var array */
  protected $fillableParams = ['format'];

  /** @var array */
  protected $params = [
    'format' => 'Y-m-d',
  ];

  /**
   * Check the $value is valid
   *
   * @param mixed $value
   * @return bool
   * @throws MissingRequiredParameterException
   */
  public function check($value): bool
  {
    $this->requireParameters($this->fillableParams);
    $dateFormat = $this->parameter('format');
    $date = date_create_from_format($dateFormat, $value);

    return $date && $date->format($dateFormat) === $value;
  }
}
