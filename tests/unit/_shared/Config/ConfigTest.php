<?php
declare(strict_types=1);

namespace BeastMakersTest\Unit\Shared\Config;

use BeastMakers\Shared\Config\Config;
use BeastMakers\Shared\Config\MissingConfigOptionException;

class ConfigTest extends \Codeception\Test\Unit
{
  /**
   * @return void
   * @throws MissingConfigOptionException
   */
  public function testGetFirstLevelValue(): void
  {
    $config = new Config($this->sampleConfig());
    $this->assertTrue($config->getBool('is_enabled'));
  }

  /**
   * @return void
   * @throws MissingConfigOptionException
   */
  public function testGetSecondLevelValue(): void
  {
    $config = new Config($this->sampleConfig());
    $this->assertInstanceOf(Config::class, $config->getConfig('database.webapi'));
  }

  /**
   * @return void
   * @throws MissingConfigOptionException
   */
  public function testGetThirdLevelValue(): void
  {
    $config = new Config($this->sampleConfig());
    $this->assertSame('123', $config->getString('database.webapi.port'));
  }

  /**
   * @return array
   */
  private function sampleConfig(): array
  {
    return [
      'database' => [
        'webapi' => [
          'name' => 'some_name',
          'port' => '123',
        ],
      ],
      'is_enabled' => true,
    ];
  }
}
