<?php
declare(strict_types=1);

namespace BeastMakers\Shared\TransferObject\Strategy;

use BeastMakers\Shared\TransferObject\ArrayDto;

class ToArrayStrategy
{
  public static function scalar($value)
  {
    return $value;
  }

  /**
   * @param array $array
   *
   * @return array
   */
  public static function assocArrayWithDto(array $array): array
  {
    $result = [];
    foreach ($array as $_key => $dto) {
      $result[$_key] = $dto->toArray();
    }

    return $result;
  }

  /**
   * @param ArrayDto $dto
   *
   * @return array
   */
  public static function dtoToArray(ArrayDto $dto): array
  {
    return $dto->toArray();
  }
}
