<?php
declare(strict_types=1);

namespace BeastMakersTest\Integration\Checkout\Delivery\Infra\Repository;

use BeastMakers\Checkout\Delivery\Infra\DeliveryData;
use BeastMakers\Checkout\Delivery\Infra\Repository\ReadRepository;
use BeastMakers\Checkout\Delivery\Infra\Repository\WriteRepository;
use BeastMakers\Checkout\Shared\Domain\Address;
use BeastMakers\Checkout\Shared\Domain\Customer;
use BeastMakersTest\Integration\Checkout\Delivery\FactoryPublicDouble;
use BeastMakersTest\IntegrationTester;
use Codeception\Test\Unit;

class RedisRepositoryTest extends Unit
{
  protected IntegrationTester $tester;

  private static ReadRepository $readRepository;

  private static WriteRepository $writeRepository;

  /**
   * @return void
   */
  public static function setUpBeforeClass(): void
  {
    $moduleFactory = new FactoryPublicDouble();
    self::$readRepository = $moduleFactory->shareDeliveryReadRepository();
    self::$writeRepository = $moduleFactory->shareDeliveryWriteRepository();
  }

  /**
   * @return void
   */
  public function testWriteReadDeliveryData(): void
  {
    $expectedDeliveryData = $this->buildDeliveryDataSample();
    $checkoutId = $this->tester->faker()->uuid;

    self::$writeRepository->pushDeliveryData($expectedDeliveryData, $checkoutId);
    $actualDeliveryData = self::$readRepository->fetchDeliveryData($checkoutId);

    $this->assertSame(
      $expectedDeliveryData->getAddress()->toArray(),
      $actualDeliveryData->getAddress()->toArray()
    );
    $this->assertSame(
      $expectedDeliveryData->getCustomer()->toArray(),
      $actualDeliveryData->getCustomer()->toArray()
    );
  }

  /**
   * @return DeliveryData
   */
  private function buildDeliveryDataSample(): DeliveryData
  {
    $faker = $this->tester->faker();
    $firstName = $faker->firstName;
    $lastName = $faker->lastName;

    $addressDto = Address::createFromArray([
      Address::ATTR_FIRST_NAME => $firstName,
      Address::ATTR_LAST_NAME => $lastName,
      Address::ATTR_COUNTRY_CODE => $faker->countryCode,
      Address::ATTR_STREET => $faker->streetAddress,
      Address::ATTR_CITY => $faker->city,
      Address::ATTR_ZIP => $faker->postcode,
    ]);

    $customerDto = Customer::createFromArray([
      Customer::ATTR_FIRST_NAME => $firstName,
      Customer::ATTR_LAST_NAME => $lastName,
      Customer::ATTR_EMAIL => $faker->email,
      Customer::ATTR_BIRTH_DATE => $faker->date('d-m-Y'),
    ]);

    $deliveryData = new DeliveryData();
    $deliveryData->setAddress($addressDto);
    $deliveryData->setCustomer($customerDto);

    return $deliveryData;
  }
}
