<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra\Repository;

interface Key
{
  public const CHECKOUT_DELIVERY_ADDRESS_PREFIX = 'checkout_delivery_address_';
  public const CHECKOUT_DELIVERY_CUSTOMER_PREFIX = 'checkout_delivery_customer_';
}
