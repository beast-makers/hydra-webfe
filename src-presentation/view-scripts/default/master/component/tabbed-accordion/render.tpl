{capture tabbedAccordionNavigation assign=tabbedAccordionNavigation}
    <div class="js-tabbed-accordion-tab-nav">
    {foreach $paymentMethods as $props}
        <div class="c-tabbed-accordion-tab-nav-item js-tabbed-accordion-nav-item d-none d-md-block" data-tabs-target="{$props.targetClass}">
            <span class="c-tabbed-accordion-state-icon font-icon icofont-rounded-right"></span>
            <div>{$props.title}</div>
        </div>
    {/foreach}
    </div>
{/capture}

{capture tabbedAccordionContent assign=tabbedAccordionContent}
    <div class="js-tabbed-accordion-content-container">
        {foreach $paymentMethods as $props}
            <div class="c-tabbed-accordion-acc-item js-tabbed-accordion-acc-item {$props.targetClass}">
                <div class="c-tabbed-accordion-acc-header js-tabbed-accordion-acc-header d-md-none">
                    <span class="c-tabbed-accordion-state-icon font-icon icofont-rounded-down"></span>
                    <div>{$props.title}</div>
                </div>
                <div class="c-tabbed-accordion-content-frame">
                    <div class="c-tabbed-accordion-content">
                        {include file=$props.tpl data=$props.data}
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
{/capture}
