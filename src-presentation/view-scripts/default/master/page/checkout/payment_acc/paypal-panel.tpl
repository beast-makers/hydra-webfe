<div class="js-paypal-panel">
    <div class="paypal-form">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-6 col-md-10">
                <div id="paypal-button"></div>
            </div>
        </div>
    </div>
</div>
