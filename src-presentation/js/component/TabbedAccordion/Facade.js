import {bindEvents} from './TabbedAccordion';
import Universe from '../../kernel/Universe';
import TabbedAccordionEventKey from './EventKey';

export function initTabbedAccordion () {
  bindEvents();
}
/**
 * @callback eventCallback
 * @param {string} key
 * @param {*} [notificationData={}]
 *
 * @return {void}
 */
/**
 * @param {eventCallback} callback
 *
 * @return {void}
 */
export function onSelect(callback) {
  Universe.Event.subscribe(TabbedAccordionEventKey.TABBED_ACCORDION_SELECTION, callback);
}


