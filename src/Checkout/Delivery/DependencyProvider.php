<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery;

use BeastMakers\Shared\DependencyLoader;
use BeastMakers\Shared\Kernel\BaseDependencyProvider;
use BeastMakers\Shared\RedisConnector\RedisClient;
use BeastMakers\Shared\SessionStorage\SessionManager;
use Smarty;

class DependencyProvider extends BaseDependencyProvider
{
  use DependencyLoader\TemplateEngine;
  use DependencyLoader\PHPSessionManagerLoader;
  use DependencyLoader\RedisWebfeClient;
  use DependencyLoader\OwnArrayValidator;
  use DependencyLoader\BackendClientLoader;

  /**
   * @return Smarty
   */
  public function loadTemplateEngine(): Smarty
  {
    return $this->shareTemplateEngine();
  }

  /**
   * @return SessionManager
   */
  public function loadSessionManager(): SessionManager
  {
    return $this->sharePHPSessionManager();
  }

  /**
   * @return RedisClient
   */
  public function loadRedisWebfeClient(): RedisClient
  {
    return $this->shareRedisWebfeClient();
  }
}
