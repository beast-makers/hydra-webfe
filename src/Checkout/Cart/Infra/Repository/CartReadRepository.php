<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Cart\Infra\Repository;

use BeastMakers\Checkout\Cart\Infra\CartData;
use BeastMakers\Shared\RedisConnector\RedisClient;
use BeastMakers\Shared\RedisConnector\RedisException;

class CartReadRepository implements ReadRepository
{
  private RedisClient $redisClient;

  public function __construct(RedisClient $redisClient)
  {
    $this->redisClient = $redisClient;
  }

  /**
   * @inheritDoc
   * @throws RedisException
   */
  public function fetchCartData(string $checkoutId): CartData
  {
    $data = $this->redisClient->redis()->mget([
      Key::CHECKOUT_PRODUCTS_PREFIX . $checkoutId,
      Key::CHECKOUT_VOUCHER_PREFIX . $checkoutId,
    ]);

    $productsAsJson = $data[0] ?: '{}';
    $voucherAsJson = $data[1] ?: '{}';

    $products = json_decode($productsAsJson, true);
    $voucher = json_decode($voucherAsJson, true);

    $cartData = new CartData();
    $cartData->setProducts($products);
    $cartData->setVoucher($voucher);

    return $cartData;
  }
}
