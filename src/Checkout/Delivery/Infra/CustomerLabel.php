<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Infra;

interface CustomerLabel
{
  public const FIRST_NAME = 'First name';
  public const LAST_NAME = 'Last name';
  public const EMAIL = 'Email';
  public const BIRTH_DATE = 'Birth date';
}
