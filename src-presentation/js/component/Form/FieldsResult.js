import Result from '../Result/Result';

export class FieldsResult extends Result {
  constructor () {
    super();

    this.isEmpty = true;
    this.fields = {};
  }

  /**
   * @param {string} key
   * @param {Object} field
   *
   * @return {void}
   */
  addField (key, field) {
    this.isEmpty = false;
    this.fields[key] = field;
  }
}
