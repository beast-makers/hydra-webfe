<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Payment\Action\Http;

use BeastMakers\Checkout\Payment\Action\PaymentPageCommand;
use BeastMakers\Checkout\Payment\Factory;
use BeastMakers\Checkout\Routes;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Smarty;

/**
 * @property Factory $factory
 */
class PaymentPage extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws \SmartyException
   * @throws \Exception
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $paymentPageModel = $this->factory->newPaymentPageCommand();
    $templateEngine = $this->factory->shareTemplateEngine();
    $this->assignPaymentFormsToViewTemplate($templateEngine, $paymentPageModel);

    return $this->actionResponseBuilder->createJsonHttpResponse(200, [
      'payment_page_html' => $templateEngine->fetch('page/checkout/payment.tpl'),
      'payment_page_config' => $this->getPaymentPageConfig(),
    ]);
  }

  /**
   * @param Smarty $templateEngine
   * @param PaymentPageCommand $paymentPageModel
   *
   * @return void
   */
  private function assignPaymentFormsToViewTemplate(
    Smarty $templateEngine,
    PaymentPageCommand $paymentPageModel
  ): void {
    foreach ($this->buildPaymentForms($paymentPageModel) as $formKey => $formProperties) {
      $templateEngine->assign($formKey, $formProperties);
    }
  }

  /**
   * @param PaymentPageCommand $paymentPageModel
   *
   * @return array
   */
  private function buildPaymentForms(PaymentPageCommand $paymentPageModel): array
  {
    return [
      'sepaForm' =>
        $paymentPageModel->buildSepaForm()->toArray()
        + [
          'actionUrl' => $this->routeParser->urlFor(Routes::ROUTE_CHECKOUT_ACCEPT_SEPA_PAYMENT),
        ],
    ];
  }

  /**
   * @return array
   */
  private function getPaymentPageConfig(): array
  {
    return [
      'endpoints' => [
        'store_payment_data' => $this->routeParser->urlFor(Routes::ROUTE_CHECKOUT_STORE_PAYMENT_DATA),
      ],
    ];
  }
}
