<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Payment\Action\Http;

use BeastMakers\Checkout\Payment\Factory;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 */
class AcceptSepaPayment extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    return $this->actionResponseBuilder->createJsonHttpResponse(200, []);
  }
}
