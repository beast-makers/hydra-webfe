<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Delivery\Action;

class DeliveryPageConfigCommand
{
  /**
   * @return array
   */
  public function buildConfig(): array
  {
    return [];
  }
}
