<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Main\Action\Http;

use BeastMakers\Checkout\Main\Config;
use BeastMakers\Checkout\Main\Factory;
use BeastMakers\Checkout\Routes;
use BeastMakers\Checkout\Shared\SessionKey;
use BeastMakers\Shared\Kernel\Action\BaseHttpAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property Factory $factory
 * @property Config $config
 */
class MainPage extends BaseHttpAction
{
  /**
   * @param ServerRequestInterface $request
   *
   * @return ResponseInterface
   * @throws \SmartyException
   * @throws \Exception
   */
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $checkoutId = $this->provideCheckoutId();

    $templateEngine = $this->factory->shareTemplateEngine();
    $templateEngine->assign('title', 'Kit Checkout');
    $templateEngine->assign('checkout_config', $this->getCheckoutConfig($checkoutId));

    $content = $templateEngine->fetch('page/checkout/_main.tpl');

    return $this->actionResponseBuilder->buildHtmlResponse($content);
  }

  /**
   * @return string
   */
  private function provideCheckoutId(): string
  {
    $sessionManager = $this->factory->shareSessionManager();

    $checkoutId = $sessionManager->read(
      SessionKey::CHECKOUT_ID,
      $this->config->generateCheckoutId()
    );
    $sessionManager->write(SessionKey::CHECKOUT_ID, $checkoutId);

    return $checkoutId;
  }

  /**
   * @param string $checkoutId
   *
   * @return array
   */
  private function getCheckoutConfig(string $checkoutId): array
  {
    return [
      'endpoints' => [
        'cart_page' => $this->routeParser->urlFor(Routes::ROUTE_CHECKOUT_CART_PAGE),
        'delivery_page' => $this->routeParser->urlFor(Routes::ROUTE_CHECKOUT_DELIVERY_PAGE) . '?theme=xmas',
        'payment_page' => $this->routeParser->urlFor(Routes::ROUTE_CHECKOUT_PAYMENT_PAGE),
      ],
      'checkout_id' => $checkoutId,
      'checkout_variant' => 'kit',
    ];
  }
}
