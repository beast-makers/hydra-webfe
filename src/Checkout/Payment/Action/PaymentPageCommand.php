<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Payment\Action;

use BeastMakers\Checkout\Payment\Infra\SepaForm;

class PaymentPageCommand
{
  private SepaForm $sepaForm;

  /**
   * @param SepaForm $sepaForm
   */
  public function __construct(SepaForm $sepaForm)
  {
    $this->sepaForm = $sepaForm;
  }

  /**
   * @return SepaForm
   */
  public function buildSepaForm(): SepaForm
  {
    $this->sepaForm->initialize();

    return $this->sepaForm;
  }
}
