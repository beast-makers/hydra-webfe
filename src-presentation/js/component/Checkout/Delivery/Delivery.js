import CheckoutClient from '../CheckoutClient';

export default class Delivery {
  /**
   * @return {Promise<Object>}
   */
  fetchDeliveryPageData () {
    return new Promise((resolve, reject) => {
      CheckoutClient.fetchDeliveryPageData()
        .then((deliveryPage) => {
          resolve(deliveryPage);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * @param {FormData} formData
   *
   * @return {Promise}
   */
  storeDeliveryData (formData) {
    return new Promise((resolve) => {
      CheckoutClient.storeDeliveryData(formData).then(() => {
        resolve(true);
      });
    });
  };
}
