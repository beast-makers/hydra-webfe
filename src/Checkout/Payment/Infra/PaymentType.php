<?php
declare(strict_types=1);

namespace BeastMakers\Checkout\Payment\Infra;

interface PaymentType
{
  public const SEPA = 'sepa';
}
